if(1==2){
agent <- "Government"
for(parameter in parameter_values){
for(aggr in available_aggregation_levels){
load(file=paste(data_directory,"data_",aggr,"_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
#View(object)
object$eco_policy_budget[which(is.na(object$eco_policy_budget))]<-0
object$monthly_eco_tax_revenue[which(is.na(object$monthly_eco_tax_revenue))]<-0
object$monthly_eco_subsidy_payment[which(is.na(object$monthly_eco_subsidy_payment))]<-0
save(object, file=paste(data_directory,paste("data_",aggr,"_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
#View(object)
}
}
}

if(1==2){
  agent<-"Eurostat"
  for(parameter in parameter_values){
    for(aggr in available_aggregation_levels){
      load(file=paste(data_directory,"data_",aggr,"_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
      #View(object)
      select<-c("avg_productivity_gap", "firm_average_productivity_conv", "firm_average_productivity_eco")
      sel_ind<-match(select, colnames(object))
      if(is.element(NA, sel_ind)){
        print("Stop: There must be a typo or so... ")
        stop()
      }
      vars<-object[,sel_ind]
      eco<-object$firm_average_productivity_eco
      conv<-object$firm_average_productivity_conv
      replace<-2*(conv-eco)/(conv+eco)
      new<-conv/eco
      
      object$avg_productivity_gap<-replace
      object<-cbind(object, avg_productivity_ratio=new)
      
      
      save(object, file=paste(data_directory,paste("data_",aggr,"_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
      #View(object)
    }
  }
  
}