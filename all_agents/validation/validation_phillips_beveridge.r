index_rm_outliers_and_na <- function(x,lower, na.rm = TRUE, ...) {
  qnt <- quantile(x, probs=c(.25, .75), na.rm = na.rm, ...)
  H <- 0.125*IQR(x, na.rm = na.rm)
  y <- x
  y[x < (qnt[1] - lower*0.5*H)] <- NA
  y[x > (qnt[2] + H)] <- NA
  inds<-which(!is.na(y))
}

# Validation: Philipps and beveridge curve
# 
plot_dir_temp<-paste(plot_dir_validation,"/phillips_beveridge_plots/", sep="")
if(dir.exists(plot_dir_temp)!=TRUE){
  dir.create(plot_dir_temp)
}
# # Use bandpass filtered data, i.e. trend_full
load(paste(data_directory,"series_bk_var_8_32.RData", sep=""))
load(paste(data_directory,"cycle_bk_var_8_32.RData", sep=""))
load(paste(data_directory,"trend_bk_var_8_32.RData", sep=""))

data_temp<-series_full
trend_temp<-trend_full
vars_temp<-c("no_vacancies", "unemployment_rate", "monthly_inflation_rate")
cols_temp<-match(c("Run", "Type", "Periods", vars_temp), colnames(data_temp))
if(is.element(NA, cols_temp)){
  print(paste("One of your desired variables is not available: ", c("Run", "Type", "Periods", vars_temp)[is.na(cols_temp)]))
  print("Get data first. Not Phillips curve and Beveridge curve generated.")
}else{
data_temp<-data_temp[cols_temp]
trend_temp<-trend_temp[cols_temp]

# normalize: 
#c_temp<-match(vars_temp, data_temp)
#for(c in c_temp){
#  data_temp[,c]<-data_temp[,c]/trend_temp[,c]
#}
runs_temp<-unique(data_temp$Run)
for(i_temp in c("all_runs", "random_run")){
# Consider only 10 year snapshot in time at beginning and end of 
# simulation
p<-1
for(p in 1:2){
  
  if(i_temp == "random_run"){
    # Select random run: 
    
    r <- sample(runs_temp, 1)
    inds_temp<-which(data_temp$Run == r)
    data_temp_1<-data_temp[inds_temp,]
    trend_temp<-trend_temp[inds_temp,]
    unemp_threshold_temp<-19
    vac_threshold_temp<-0
    size_temp<-1
    lower_temp<-1
    if(p == 1){
      periods_temp<-seq(20, 15000, 20)
      per_temp<-paste("all")
    }else if(p == 2){
      periods_temp<-seq(10220, 15000, 20)
      per_temp<-paste("second_half")
    }
  }else if(i_temp == "all_runs"){
    data_temp_1<-data_temp
    trend_temp<-trend_temp
    unemp_threshold_temp<-15
    vac_threshold_temp<-2
    size_temp<-0.025
    lower_temp<-0.125
    if(p == 1){
      periods_temp<-seq(3020, 5400, 20)
      per_temp<-paste("first_half")
    }else if(p == 2){
      periods_temp<-seq(12620, 15000, 20)
      per_temp<-paste("second_half")
    }
  }
  inds_temp<-which(as.numeric(as.character(data_temp_1$Periods)) %in% periods_temp)
  data_temp_1<-data_temp_1[inds_temp,]
  # Remove outliers in data: 
  
  # Here rule of thumb, i.e. unempl. > 15 pct
  #remove<-which(data_temp_1$unemployment_rate > unemp_threshold_temp)
  #remove<-which(data_temp_1$no_vacancies < vac_threshold_temp)
  inds_in<-index_rm_outliers_and_na(data_temp_1[,match("unemployment_rate", colnames(data_temp_1))], lower_temp)
  data_temp_1<-data_temp_1[inds_in,]
  #View(data_temp_1[remove,])
  #data_temp_1<-data_temp_1[-remove,]
  i<-2
  for(i in 1:2){
  
    x<-data_temp_1$unemployment_rate/100
    xlab<-"Unemployment rate"

    if(i == 1){
      vars_temp_plot<-c("no_vacancies", "unemployment_rate")
      y<-data_temp_1$no_vacancies/1600
      ylab<-"Vacancy rate"
      name_temp<-paste("Beveridge",i_temp, per_temp, sep="_")
    }else if(i == 2){
      vars_temp_plot<-c("unemployment_rate", "monthly_inflation_rate")
      y<-data_temp_1$monthly_inflation_rate
      ylab<-"Inflation"
      name_temp<-paste("Phillips",i_temp,per_temp, sep="_")
    }
    pdf(paste(plot_dir_temp,name_temp,".pdf",sep=""))
    
    plot(x,y, xlab = xlab, ylab=ylab, cex = size_temp, pch=20)
    #abline(lsfit(x,y), col="red")
    #abline(line(x,y), col="blue")
    lines(lowess(x,y), col="red")
    dev.off()
  } # i in random run or all runs


} # p in initial or end period

} # Random or all runs... i_temp
} # Run routine only if data available! 

