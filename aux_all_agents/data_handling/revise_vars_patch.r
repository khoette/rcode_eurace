## Revise variables
fix_vars_temp<-c("Run","Periods","Type","Firm_Type","Adopter_Type","Parameter","Learn_Type","Ease","Spill","id","active", "Number_Runs")
for(d in which_data_types){
  
  for(parameter in parameters_tmp){
    for(aggr in aggregation_levels){
      print(aggr)
      for(agent in list_of_agents){
        data_file_temp<-paste(experiment_directory,"/rdata/",kind_of_data,"/",data_type[d],"/data_",aggr,"_",experiment_name,"_",parameter,"_",agent,".RData",sep="")
        if(file.exists(data_file_temp)){
          load(data_file_temp)
          variables_temp<-setdiff(colnames(object),fix_vars_temp)
          cols<-match(variables_temp,colnames(object))
          for(v in cols){
            temp<-object[,v]
            print(colnames(object)[v])
            if(class(temp)[length(class(temp))] != "numeric"){
              #stop("why?")
              object[,v]<-as.numeric(as.character(temp))
            }
            if(colnames(object)[v] == "temp2[, match(col, colnames(temp2))]"){
              colnames(object)[v]<-"variance_share"
            }
            
            
          }# v in cols
          if(aggr == "batch"){
            if(is.element("Type",colnames(object))){
              object<-object[which(!grepl("Type",colnames(object)))]
              #stop("TEST")
            }
            if(is.element("using_cost_difference_eco_conv_capital",colnames(object))){
              
              if(length(which(!grepl("using_cost_difference_eco_conv_capital",colnames(object))))>1){
                cols_temp<-c(which(!grepl("using_cost_difference_eco_conv_capital",colnames(object))),
                                   match("using_cost_difference_eco_conv_capital",colnames(object)))
                object<-object[cols_temp]
              }
            }
            
          }
          save(object,file=data_file_temp)
        }else{
          print(paste("data does not exist: ",data_file_temp))
        }
        
      }# end agent loop  
    }# end aggr loop
  }# end p in paramters_tmp
}# end d in which data_types
