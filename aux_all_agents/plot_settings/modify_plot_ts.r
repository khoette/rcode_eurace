## Modify plot
## 
## 
#print("mod_plot")
if(draw_line != 0){
  plot<-plot + geom_hline(yintercept = draw_line, linetype = "dashed", color = "gray30")
}

if(exists("legend_pos_temp") && LEGEND){
  plot <- plot+ theme(legend.position=legend_pos_temp)
}else if(LEGEND){
  plot <- plot+ theme(legend.position=c(0.1, 0.9))
}else{
  plot <- plot+ theme(legend.position="none")
}

if((experiment_name == "learning_spillover_strength" || experiment_name == "min_learning_coefficient_const")&& aggr == "single_large"){
  #'eco.0.0' = "chartreuse1", 'eco.0.5' = "chartreuse3", 'eco.2.0' = "chartreuse4", 
  #                'conv.0.0' = "tomato1", 'conv.0.5' = "tomato3", 'conv.2.0' = "tomato4", 
  #                'switch.0.0' = "skyblue1", 'switch.0.5' = "skyblue3", 'switch.2.0' = "skyblue4")
  if(parameter == "0.0"){
    eco<-"chartreuse1"; conv<-"tomato1"
  }else if(parameter == "0.5"){
    eco<-"chartreuse3"; conv<-"tomato3"
  }else{
    eco<-"chartreuse4"; conv<-"tomato4"
  }
}


if(BLACK_WHITE || DIFFERENT_LINE_SHAPES){
  #lty_values<-c('Experiment'=lty0, 'Baseline'=lty1, 'Both' = lty2, '0' = lty1, '1' = lty0, '2' = lty2, '3' = lty3, '4' = lty4, 
  #              'None'=lty1, 'Skills'=lty3, 'Productivity'=lty2, 
  #              'Consumption'=lty0, 'Investment'=lty3, 
  #              'None'=lty0, 'Skills'=lty3, 'Productivity'=lty2              ) 
  if(aggr == "eco_conv_batch"){
    lty_temp<-c(lty0, lty1)
  }else if(aggr == "eco_conv_switch"){
    lty_temp<-c(lty0, lty1, lty2)
  }else{
    lty_temp<-c(1,1,1,1,1,1,1,1,1,1,1,1)
  }
  if(aggr == "batch"){
    length_temp<-1
  }else if(is.element(aggr, c("single_large", "eco_conv_batch","Spill", "Ease"))){
    length_temp<-2
  }else if(is.element(aggr, c("single_switch", "eco_conv_switch"))){
    length_temp <- 3
  }else{
    length_temp <- 4
  }
  if(is.element(aggr, c("single_large", "single_switch")) == FALSE){
    if(is.element("Type", colnames(temp))){
      plot <- plot + geom_point(aes(shape = Type), size = 3)
      if(!is.element(aggr, c("batch_Spill", "batch_Ease", "batch_Learn_Type"))){
        plot<-plot+scale_shape_manual(name='', values=c('eco' = 0, 'conv'=8, 'switch'=10), guide=LEGEND)
      }else{
        plot<-plot+scale_shape_manual(name='', values =
            c('high'=15, 'low'=1,'high-high'=15, 'high-low'=16, 'low-low'= 1, 'low-high'=0), guide = LEGEND)
      }
    }
  }else if(1==2){ # else aggr is single run data
    # Here I do not plot different point types for different scenario types. 
    # Looks too confounding...
    if(is.element("Type", colnames(temp)) && agent!= "Firm"){
      plot <- plot + geom_point(aes(shape = Type, group=interaction(Type,Run)))
      plot<-plot+scale_shape_manual(name='', values=c('eco' = 0, 'conv'=8, 'switch'=10), guide=LEGEND)
    }else if(is.element("Spill", colnames(temp))){
      plot <- plot + geom_point(aes(shape = Spill, group=interaction(Spill,Run)))
      plot<-plot+scale_shape_manual(name='', values=c('high'=15, 'low'=1), guide=LEGEND)
      
      #'eco.low'=0,'eco.high'=15, 'conv.low'=1, 'conv.high'=16,'switch.low'=2, 'switch.high'=17))
      
    }else if(is.element("Ease", colnames(temp))){
      plot <- plot + geom_point(aes(shape = Ease, group=interaction(Ease,Run)))
      plot<-plot+scale_shape_manual(name='', values=c('high'=15, 'low'=1), guide = LEGEND)
    }else if(is.element("Learn_Type", colnames(temp))){
      plot <- plot + geom_point(aes(shape = Learn_Type, group=interaction(Learn_Type,Run)))
      plot<-plot+scale_shape_manual(name='', values=c('high-high'=15, 'high-low'=16, 'low-low'=1, 'low-high'=0)
                                    , guide = LEGEND)
    }
  }
  
  if(BLACK_WHITE){
    plot<-plot + scale_colour_manual(name='', values=c(1,1,1,1,1,1,1,1,1,1,1,1,1),guide = FALSE) 
  }
  #plot <- plot + guides(shape = guide_legend(override.aes = list(linetype = rep(1, length_temp), color=rep(1, length_temp))), guide=LEGEND)
}

plot <- plot + scale_linetype_manual(name='', values=lty_temp, guide = FALSE)

if(BLACK_WHITE == 0){
  #plot <- plot + scale_linetype_manual(name='', values=c(1,1,1,1,1,1,1,1,1,1,1,1,1), guide = FALSE)
  plot<-plot + scale_colour_manual(name='', values=c('eco'=eco, 'conv'=conv, 'switch'=switch,
                                            'high-high'=highhigh, 'high-low'=highlow, 'low-low'=lowlow, 'low-high'=lowhigh, 
                                            'high'=high, 'low'=low, 
                                            'eco.low'=ecol,'eco.high'=ecoh, 'conv.low'=convl, 'conv.high'=convh,'switch.low'=switchl, 'switch.high'=switchh), 
                                   guide = LEGEND) 

  
}
#plot<- plot+ theme(legend.position="none")
#plot<-plot+ scale_linetype_manual(name='', name='Regime', values=c('eco'='dashed', 'conv'='dotted', 'switch'='lines'))


#if(exists("legend_pos_temp")){
#  plot <- plot+ theme(legend.position=legend_pos_temp)
#}else{
#  plot <- plot+ theme(legend.position=c(0.1, 0.9))
#}
if(LEGEND == FALSE){
  plot <- plot+ theme(legend.position="none")
}
xmin_temp<-"120"
xmax_temp<-"15000"
plot <- plot + xlab("Periods") + aes(ymin=min, ymax=max, xmin=xmin_temp, xmax=xmax_temp)
#plot <- plot + scale_x_discrete(breaks=factor(c("3000","6000","9000","12000")))
plot<-plot + scale_x_discrete(breaks=factor(c("3000","6000","9000","12000"))) #+ scale_y_continuous(limits=c(min,max))

if(exists("ylabel")){
  plot<-plot + ylab(paste(ylabel)) + scale_y_continuous(limits=c(min,max)) 
}else{
  plot<-plot + ylab(paste(value)) + scale_y_continuous(limits=c(min,max)) 
}

if(WHITE_BACKGROUND){
  plot<- plot + theme_bw() + theme(panel.border = element_rect(color = "black"), 
                                   #axis.line.y.left = element_line(), 
                                   #axis.line.x.top = element_line(), 
                                   #axis.line.y.right = element_line(), 
                                   #axis.line.x.bottom = element_line(), 
                                   axis.ticks.x = element_line())
  
  #plot<- plot + theme_bw() + theme(panel.border = element_rect(), panel.grid.major = element_blank(),panel.grid.minor = element_blank())
  #, axis.line = element_line(colour = "black"))
}

plot<-plot + theme(axis.text.x = element_text(size=16),
                   axis.text.y = element_text(size=16),
                   axis.title.x = element_text(size=18),
                   axis.title.y = element_text(size=18))

