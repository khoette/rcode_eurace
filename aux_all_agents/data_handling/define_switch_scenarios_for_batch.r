
# Get data in data frame for ggplot
print("Define switch scenarios as differentiated batch")
if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/", data_type[d] ,"/",paste("data_eco_conv_switch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))!=TRUE){
  
load(file=paste(data_directory,"data_single_switch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
temp_mat<-object
if(agent == "Firm"){
  temp_mat<-temp_mat[which(temp_mat$active ==1),]
}
eval(call("<-",(paste("variables",sep="")),as.name(paste("variables_available_in_panel_",agent,sep=""))))

variables<-variables[which(variables != "min_learning_coefficient_var")]
variables<-variables[which(variables != "learning_spillover_strength_var")]

cols<-match(variables, colnames(temp_mat))
cols<-cols[which(is.na(cols) == FALSE)]
start<-cols[1]
periods_temp<-length(unique(temp_mat$Periods))
steps<-(number_xml/periods_temp)*20
it_vector<-seq(steps,steps*periods_temp,steps)
    
for(type in c(1:3)){
  if(type == 1){
    temp<-temp_mat[which(as.character(temp_mat$Type) == "eco"),]
    print("batch_type: eco")
  }else if(type == 2){
    temp<-temp_mat[which(as.character(temp_mat$Type) == "conv"),]
    print("batch_type: conv")
  }else if(type == 3){
    temp<-temp_mat[which(as.character(temp_mat$Type) == "switch"),]
    print("batch_type: switch")
  }
  for(i in c(1:length(it_vector))){
    temp3<-temp[which(temp$Periods == it_vector[i] ),]
    #for(i in 1:10){
    it<-i + (type-1)*periods_temp
  
    for(v in cols){
      if(it==1 && type == 1 && v==start){
        temp2<-data.frame(matrix(0, 3*number_xml,length(colnames(temp_mat))))
        colnames(temp2) <- colnames(temp_mat)
      }
      if(v==start && i == 1){
          temp2$Type[(1+(type-1)*number_xml):(type*number_xml)]<-as.character(temp$Type[i])
          temp2$Periods[(1+(type-1)*number_xml):(type*number_xml)]<-it_vector
          temp2$Run[(1+(type-1)*number_xml):(type*number_xml)]<-length(unique(temp$Run))
          if(it==1){
            mean1<-mean(as.numeric(temp3$min_learning_coefficient_var), na.rm=TRUE)
            mean2<-mean(as.numeric(temp3$learning_spillover_strength_var), na.rm=TRUE)
          }
          temp2$min_learning_coefficient_var[(1+(type-1)*number_xml):(type*number_xml)]<-mean1
          temp2$learning_spillover_strength_var[(1+(type-1)*number_xml):(type*number_xml)]<-mean2
        }
      temp2[it,v]<-mean(temp3[,v], na.rm=TRUE)
    } # v in variables
    #temp<-temp[which(temp$Periods != it_vector[i] ),]
  }# i in it vector
} # type in types
temp_mat <- temp2
rm(temp, temp2, i, type, it_vector, start, temp3)


eval(call("<-",(paste("data_eco_conv_switch_",experiment_name,"_",parameter,"_",agent,sep="")),temp_mat))
object<-eval(as.name(paste("data_eco_conv_switch_",experiment_name,"_",parameter,"_",agent,sep="")))
save(object, file=paste(data_directory,"/",paste("data_eco_conv_switch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
rm(temp_mat, object)
}else{
  print("Data already exists")
}

