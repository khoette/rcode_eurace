## Check mixture model
## 
pdf(paste(plot_dir_temp_aic,"/histogram_",targ,"_multimode.pdf", sep=""))

truehist(target,nbins=50,col="grey",xlab=as.character(targ),prob=T,
         ylab="Frequency")#, ylim=c(0,max(targ)))
lines(histSmo(target), lty=1, lwd=2)
lines(histSmoC(target, df=7), lty=2, lwd=2)
legend("top",legend=c("local ML","fixed df"),lty=1:2, cex=1)

dev.off()

## Make decision on control variables: 
contr_all_temp<-colnames(data_temp)[which(is.na(match(colnames(data_temp), c("Periods", "Type","Run", target_vars_temp))))]
covariates_temp<-c()
if(with_rand_learn){
  learn_temp<-c("min_learning_coefficient_var","learning_spillover_strength_var")
  covariates_temp<-c(covariates_temp, learn_temp)
}
if(with_rand_barr){
  if(agent =="Eurostat"){
    barr_temp<-c("specific_skill_gap","frontier_gap" )
  }else if(agent=="Firm"){
    barr_temp<-c("specific_skill_gap","frontier_gap" )
    stop("CHECK THIS")
  }
  covariates_temp<-c(covariates_temp, barr_temp)
}
if(with_rand_policy){
  pol_temp<-c("eco_tax_rate","eco_consumption_subsidy","eco_investment_subsidy" )
  covariates_temp<-c(covariates_temp, pol_temp)
}
inter_vars<-c(learn_temp,barr_temp,pol_temp)
interaction_terms<-c()
for(i in 1:length(covariates_temp)){
  c<-covariates_temp[i]
  for(i1 in i:length(covariates_temp)){
    c1<-covariates_temp[i1]
    if(c == c1){
      interaction_terms<-c(interaction_terms, paste("I(",c,"*",c1,")", sep=""))
    }else if(c != c1 && is.element(c,inter_vars)){
      interaction_terms<-c(interaction_terms, paste("I(",c,":",c1,")", sep=""))
    }
  }
}
controls<-contr_all_temp[which(is.na(match(contr_all_temp,covariates_temp)))]

regressors_reg<-unique(c(covariates_temp,interaction_terms,contr_all_temp))
regressors_reg<-unique(c(covariates_temp,contr_all_temp))

data_temp_mix<-cbind(target, data_temp[,match(regressors_reg, colnames(data_temp))])
attach(data_temp_mix)
