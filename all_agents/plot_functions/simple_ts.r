
value<-as.name(colnames(data)[var])
temp<-data[, c(fix_vars, var)]

temp$Periods<-as.vector(as.numeric(as.character(temp$Periods)))

if(BLACK_WHITE || DIFFERENT_LINE_SHAPES){
  source(paste(script_dir,"/aux_all_agents/plot_settings/plot_subset_of_data.r", sep=""))
}
index_var<-length(fix_vars)+1
if(exists(paste("cut_transition_phase_",agent, sep="")) && is.element(var, cut_cols)){
  temp[which(as.vector(as.numeric(as.character(temp$Periods)))<(day_market_entry+20)),index_var]<-NA
  #if(is.na(abs(temp[which(as.vector(as.character(temp$Periods))==day_market_entry),index_var]-temp[which(as.vector(as.character(temp$Periods))==(day_market_entry+120)),index_var]))){
  #  temp[which(as.vector(as.character(temp$Periods))<(day_market_entry+120)),index_var]<-NA
  #}
  #start<-day_market_entry
  min1<-min(as.numeric(temp[,index_var]), na.rm=TRUE)
  max1<-max(as.numeric(temp[,index_var]), na.rm=TRUE)
  median<-mean(as.numeric(temp[,index_var]), na.rm=TRUE)
  start<-0
}else{ 
  start<-0
  min1<-min(as.numeric(temp[,index_var]), na.rm=TRUE)
  max1<-max(as.numeric(temp[,index_var]), na.rm=TRUE)
  median<-median(as.numeric(temp[,index_var]), na.rm=TRUE)
} 
if(exists("min_vec") && aggr == "batch"&&is.element(var, cut_cols)==FALSE && is.element("single_large", aggregation_levels)){
  min<-min_vec[index]
  max<-max_vec[index]
}else{
  min<-min1
  max<-max1
}
index<-index+1
source(paste(script_dir,"/aux_all_agents/plot_settings/settings_axis_labels.r", sep=""))
if(is.na(median) || is.element(NaN, c(min, max, median)) || (max > median*1e30 || abs(min) > abs(median)*1e30 )  && abs(median) > 1e-3 ){
  paste("Presumably, there is an error in the data: Un realistically high value for ", value)
}else{
  if(is.na(min) == FALSE && is.na(max) == FALSE && abs(min) != Inf && abs(max) != Inf  ){
    if(aggr == "single_large" || aggr == "single_switch" ){
      pdf(paste(plot_dir_temp,"/",pdf_name_temp, sep=""))
      p1<-ggplot(temp, aes(x=reorder(Periods, as.numeric(as.character(Periods))), y=as.numeric(temp[,index_var])))
      if(agent == "Firm"){
        plot<-p1 + geom_path(aes(color=Type, group=interaction(Type,Run,id)),alpha=alpha, lwd=lwd) 
#        plot<-p1 + geom_path(aes(color=Type, lty=Type, group=interaction(Type,Run,id)),alpha=alpha, lwd=lwd) 
      }else{
        plot<-p1 + geom_path(aes(color=Type, group=interaction(Type,Run)),alpha=alpha, lwd=lwd)  
#        plot<-p1 + geom_path(aes(color=Type, lty=Type, group=interaction(Type,Run)),alpha=alpha, lwd=lwd)  
      }
      #plot<- plot+ theme(legend.position="none")
      if(exists("ylabel")){
        plot<-plot  + ylab(paste(ylabel)) + scale_y_continuous(limits=c(min,max)) 
      }else{
        plot<-plot + ylab(paste(value)) + scale_y_continuous(limits=c(min,max)) 
      }
      source(paste(script_dir,"/aux_all_agents/plot_settings/modify_plot_ts.r", sep=""))
      print(plot)
      dev.off()
      
      if(is.element("Learn_Type", colnames(temp)) && exists("with_rand_learn") && with_rand_learn == 1){
        pdf(paste(plot_dir_temp,"/",pdf_name_temp,"_Learn_type_",parameter, sep=""))
        #pdf(paste(plot_dir_temp,"/",agent,"_Learn_type_",parameter,"_",value,"_",aggr,".pdf",sep=""))
        p1<-ggplot(temp, aes(x=reorder(Periods, as.numeric(Periods)), y=as.numeric(temp[,index_var])))
        if(agent == "Firm"){
          #plot<-p1 + geom_path(aes(color=Spill, linetype=Ease, group=interaction(Spill, Ease,Run,id)),alpha=alpha, lwd=lwd)  
          plot<-p1 + geom_path(aes(color=Learn_Type,  group=interaction(Learn_Type,Run,id)),alpha=alpha, lwd=lwd)  
          
        }else{
          plot<-p1 + geom_path(aes(color=Learn_Type, group=interaction(Learn_Type,Run)),alpha=alpha, lwd=lwd)  
          #plot<-p1 + geom_path(aes(color=Spill, linetype=Ease, group=interaction(Spill, Ease,Run)),alpha=alpha, lwd=lwd)  
        }
        plot<-plot+theme(legend.direction = "vertical", legend.text = element_text(size=7), legend.title = element_text(size=8), legend.box.spacing = unit(1, "lines"), legend.spacing.y = unit(0.75,"lines"), legend.key.size = unit(2,"lines"), legend.key=element_rect(size=2))
        print(plot)
        dev.off()
      }
      
      if(is.element("Ease", colnames(temp)) && exists("with_rand_learn") && with_rand_learn == 1){
        pdf(paste(plot_dir_temp,"/",pdf_name_temp,"_Spill_",parameter, sep=""))
        #pdf(paste(plot_dir_temp,"/",agent,"_Spill_",parameter,"_",value,"_",aggr,".pdf",sep=""))
        p1<-ggplot(temp, aes(x=reorder(Periods, as.numeric(Periods)), y=as.numeric(temp[,index_var])))
        if(agent == "Firm"){
          plot<-p1 + geom_path(aes(color=Spill, group=interaction(Ease,Run,id)),alpha=alpha, lwd=lwd)  
        }else{
          plot<-p1 + geom_path(aes(color=Spill, group=interaction(Ease,Run)),alpha=alpha, lwd=lwd)  
        }
        source(paste(script_dir,"/aux_all_agents/plot_settings/modify_plot_ts.r", sep=""))
        print(plot)
        dev.off()
        
        pdf(paste(plot_dir_temp,"/",agent,"_Spill_by_type_",parameter,"_",value,"_",aggr,".pdf",sep=""))
        p1<-ggplot(temp, aes(x=reorder(Periods, as.numeric(Periods)), y=as.numeric(temp[,index_var])))
        if(agent == "Firm"){
          plot<-p1 + geom_path(aes(color=interaction(Type, Spill), group=interaction(Spill, Type,Run,id)),alpha=alpha, lwd=lwd)  
          #plot<-p1 + geom_path(aes(color=Spill, linetype=Type, group=interaction(Spill, Type,Run,id)),alpha=alpha, lwd=lwd)  
        }else{
          #plot<-p1 + geom_path(aes(color=Spill, linetype=Type, group=interaction(Spill, Type,Run)),alpha=alpha, lwd=lwd)  
          plot<-p1 + geom_path(aes(color=interaction(Type, Spill), group=interaction(Spill, Type,Run)),alpha=alpha, lwd=lwd)  
        }
        source(paste(script_dir,"/aux_all_agents/plot_settings/modify_plot_ts.r", sep=""))
        
        print(plot)
        dev.off()
      } # end if "Spill" is element of colnames(temp)
      
      
      if(is.element("Ease", colnames(temp)) && exists("with_rand_learn") && with_rand_learn == 1){
        pdf(paste(plot_dir_temp,"/",pdf_name_temp,"_Ease_",parameter, sep=""))
        #pdf(paste(plot_dir_temp,"/",agent,"_Ease_",parameter,"_",value,"_",aggr,".pdf",sep=""))
        p1<-ggplot(temp, aes(x=reorder(Periods, as.numeric(Periods)), y=as.numeric(temp[,index_var])))
        if(agent == "Firm"){
          plot<-p1 + geom_path(aes(color=Ease, group=interaction(Ease,Run,id)),alpha=alpha, lwd=lwd)  
        }else{
          plot<-p1 + geom_path(aes(color=Ease, group=interaction(Ease,Run)),alpha=alpha, lwd=lwd)  
        }
        
        source(paste(script_dir,"/aux_all_agents/plot_settings/modify_plot_ts.r", sep=""))
        print(plot)
        dev.off()
        
        pdf(paste(plot_dir_temp,"/",pdf_name_temp,"_Ease_by_type_",parameter, sep=""))
        #pdf(paste(plot_dir_temp,"/",agent,"_Ease_by_type_",parameter,"_",value,"_",aggr,".pdf",sep=""))
        p1<-ggplot(temp, aes(x=reorder(Periods, as.numeric(Periods)), y=as.numeric(temp[,index_var])))
        if(agent == "Firm"){
          #plot<-p1 + geom_path(aes(color=Ease, linetype=Type, group=interaction(Ease, Type,Run,id)),alpha=alpha, lwd=lwd)  
          plot<-p1 + geom_path(aes(color=interaction(Type, Ease),  group=interaction(Ease, Type,Run,id)),alpha=alpha, lwd=lwd)  
        }else{
          #plot<-p1 + geom_path(aes(color=Ease, linetype=Type, group=interaction(Ease, Type,Run)),alpha=alpha, lwd=lwd)  
          plot<-p1 + geom_path(aes(color=interaction(Type, Ease),  group=interaction(Ease, Type,Run)),alpha=alpha, lwd=lwd)  
        }
        
        source(paste(script_dir,"/aux_all_agents/plot_settings/modify_plot_ts.r", sep=""))
        print(plot)
        dev.off()
      } # end if "Ease" is element of colnames(temp)
      
    }else if(aggr == "batch"){
      pdf(paste(plot_dir_temp,"/",pdf_name_temp, sep=""))
      
      #pdf(paste(plot_dir_temp,"/",agent,"_",parameter,"_",value,"_batch_runs.pdf",sep=""))
      p1<-ggplot(temp, aes(x=reorder(Periods, as.numeric(Periods)), y=as.numeric(temp[,index_var]), group=1))
      plot<-p1 + geom_path()  
      
      source(paste(script_dir,"/aux_all_agents/plot_settings/modify_plot_ts.r", sep=""))
      print(plot)
      dev.off()
      #plot<-plot(data[,1], data[,var1], ylim=c(min,max),type = "l",xlab = "Periods", ylab = (paste(variables[var]) )) 
    }else if(is.element(aggr, c("eco_conv_batch", "eco_conv_switch"))){
      pdf(paste(plot_dir_temp,"/",pdf_name_temp, sep=""))
      #pdf(paste(plot_dir_temp,"/",agent,"_",parameter,"_",value,"_",aggr,".pdf",sep=""))
      p1<-ggplot(temp, aes(x=reorder(Periods, as.factor(as.character(Periods))), y=as.numeric(temp[,index_var]), group=Type, color=Type))
      plot<-p1 + geom_path() 
      
      source(paste(script_dir,"/aux_all_agents/plot_settings/modify_plot_ts.r", sep=""))
      print(plot)
      dev.off()
    }else if(is.element(aggr, c("batch_Spill")) && exists("with_rand_learn") && with_rand_learn == 1){
      pdf(paste(plot_dir_temp,"/",pdf_name_temp, sep=""))
      #pdf(paste(plot_dir_temp,"/",agent,"_",parameter,"_",value,"_",aggr,".pdf",sep=""))
      p1<-ggplot(temp, aes(x=reorder(Periods, as.numeric(Periods)), y=as.numeric(temp[,index_var]), group=Type, color=Type, lty=Type))
      
      plot<-p1 + geom_path() 
      
      source(paste(script_dir,"/aux_all_agents/plot_settings/modify_plot_ts.r", sep=""))
      dev.off()
    }else if(is.element(aggr, c("batch_Ease")) && exists("with_rand_learn") && with_rand_learn == 1){
      pdf(paste(plot_dir_temp,"/",pdf_name_temp, sep=""))
      
      #pdf(paste(plot_dir_temp,"/",agent,"_",parameter,"_",value,"_",aggr,".pdf",sep=""))
      p1<-ggplot(temp, aes(x=reorder(Periods, as.numeric(Periods)), y=as.numeric(temp[,index_var]), group=Type, color=Type, lty=Type))
      plot<-p1 + geom_path()  
      source(paste(script_dir,"/aux_all_agents/plot_settings/modify_plot_ts.r", sep=""))
      print(plot)
      
      dev.off()
    }else if(is.element(aggr, c("batch_Learn_Type")) && exists("with_rand_learn") && with_rand_learn == 1){
      pdf(paste(plot_dir_temp,"/",pdf_name_temp, sep=""))
      
      #pdf(paste(plot_dir_temp,"/",agent,"_",parameter,"_",value,"_",aggr,".pdf",sep=""))
      p1<-ggplot(temp, aes(x=reorder(Periods, as.numeric(Periods)), y=as.numeric(temp[,index_var]), group=Type, color=Type, lty=Type))
      plot<-p1 + geom_path()  
      source(paste(script_dir,"/aux_all_agents/plot_settings/modify_plot_ts.r", sep=""))
      print(plot)
      dev.off()
    }
  }
} # Chekc for unrealistically high values in data. 
rm(ylabel)
