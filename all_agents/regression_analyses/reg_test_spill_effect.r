print("Note that it takes some time until this script is run, especially when using 
      firm level data.. Data preprocesing takes some time. ")

source(paste(script_dir,"/all_agents/regression_analyses/aux/reg_settings.r", sep=""))



if(exists("run_regression")!=T){run_regression <-T}

if(is.element(agent, c("Firm", "Eurostat"))){
  
  source(paste(script_dir,"/all_agents/regression_analyses/aux/functions_reg.r",sep=""))
  
  if(agent=="Eurostat"){
    no_agents<-1
  }else if(agent == "Firm"){
    no_agents<-120
  }else{
    stop(paste("No regression for ", agent, "wanted. Better if this sript not called."))
  }
  reg_level<-agent
  

  source(paste(script_dir,"/aux_all_agents/create_directories_for_plotting.r",sep=""))
  plot_dir<-paste(plot_dir_selection,"/Regression_on_spills", sep="")
  if(exists("plot_dir") == FALSE && dir.exists(plot_dir) == FALSE){
    dir.create(plot_dir, recursive = T)
  }
  
  #if(exists("data_from")!=T){
  aggr<-"single_large"
  p0<-parameter
  data_directory<-paste(experiment_directory,"/rdata/",kind_of_data,"/",data_type[d],"/",sep="")
  data_from<-paste(data_directory,"data_", aggr, "_",experiment_name,"_",p0,"_",sep="")
  #}

  if(file.exists(paste(data_from,"data_",reg_level,"_",parameter,".RData",sep=""))!=T){
    source(paste(script_dir,"/all_agents/regression_analyses/aux/get_data_spill_test.r", sep=""))
    #output: data_euro, data_firm
  }else{
    load(paste(data_from,"data_",reg_level,"_",parameter,".RData",sep=""))
  }
  
  agent<-reg_level

  #
  #
  # optionally: Add additional target variables to data_euro resp. data_firm
  #  agent<-reg_level
  if(exists("reg_spill_threshold") && reg_spill_threshold){
    # Within this routines, new target variables are derived, i.e. critical levels of 
    # relative knowledge stocks, Period, share_conv_cap at the point in time when the 
    # last sign flip in the diffusion curve occurred. 
    if(file.exists(paste(experiment_directory,"/",agent,"data_on_last_sign_flip_in_diffusion_",parameter,".RData", sep="")) != T){
      print(paste("Get threshold data:",parameter))
      if(agent=="Firm"){
        print("Be careful. This routine takes a lot of time.")
      }
      source(paste(script_dir, "/all_agents/regression_analyses/aux/transform_data_for_regr_to_predict_thresholds.r", sep=""))
    }else{
      load(paste(experiment_directory,"/",agent,"data_on_last_sign_flip_in_diffusion_",parameter,".RData", sep=""))
    }
    
    # Merge data to get larger set of target variables: 
    source(paste(script_dir,"/all_agents/regression_analyses/aux/merge_data.r", sep=""))
  }
  
  if(use_plot_threshold || run_regression){
    if((exists("use_type_adjusted_crit_levels") && use_type_adjusted_crit_levels) 
       || (exists("use_variance_as_target") && use_variance_as_target)){
      
      agent<-reg_level
      if(agent=="Eurostat"){temp<-data_euro}else if(agent=="Firm"){temp<-data_firm}
      
      # Transform knowledge stock ratio variables such that "absolute" value is taken, i.e. 
      # hihger value in favor of dominant technology, i.e. dep on final regime type.
      if(exists("use_type_adjusted_crit_levels") && use_type_adjusted_crit_levels){
        if(length(grep("abs_crit", colnames(temp)))==0){
          flag_new<-1
          eco_inds<-which(temp$Type == "eco")
          cols<-grep("crit", colnames(temp))
          temp_abs<-temp[,cols]
          for(c1 in 1:length(cols)){
            c<-cols[c1]
            if(colnames(temp)[c] == "crit_share"){
              temp_abs[,c1]<-(temp[,c]/(1-temp[,c])) # transform into ratio: K^c/K^g
              colnames(temp_abs)[c1] <- "crit_cap_ratio"
            }
            temp_abs[eco_inds,c1]<-(temp[eco_inds,c])**{-1} # Take "absolute" values, i.e. tranform ratio 
            # of conv/green to ratio dominant/non-dominant technology. 
            # cosmetics: 
            temp[which(is.infinite(temp[,c])),c]<-NA
          }
          colnames(temp_abs)<-c(paste(paste("abs_",colnames(temp_abs),sep="")))
          temp<-cbind(temp, temp_abs)
          if(agent=="Eurostat"){data_euro<-temp}else if(agent=="Firm"){data_firm<-temp}
          rm(temp_abs)
        }# verify that not yet existing
      } # type_adjusted levels
      
      var_ind<-c()
      if(exists("use_variance_as_target") && use_variance_as_target){
        if(length(grep("variance_share_full", colnames(temp)))==0){
          flag_new<-1
          temp<-cbind(temp, variance_share_full=rep(NA, length(temp$Periods)))
          # Separate all data where no replacement tbd from data where variance share will be added
          temp0<-temp[which(as.numeric(as.character(temp$Period)) != max(as.numeric(as.character(temp$Period)), na.rm=T)),]
          temp<-temp[which(as.numeric(as.character(temp$Period)) == max(as.numeric(as.character(temp$Period)), na.rm=T)),]
          # Get original data to make activity check and run id match
          load(paste(data_directory,"data_single_large_",experiment_name,"_",p0,"_",agent,".RData",sep=""))
          if(agent=="Firm"){
            its<-round(as.numeric(as.character(object$id))) + 0.001*round(as.numeric(as.character(object$Run)))
            object$id<-its
            object<-object[order(its),]
            its<-object$id
            #its<-round(as.numeric(as.character(object$id))) + 0.001*round(as.numeric(as.character(object$Run)))
            
            ids<-(as.numeric(temp$id) + 0.001*as.numeric(temp$Run))
            
            temp$id<-(as.numeric(temp$id) + 0.001*as.numeric(temp$Run))
            temp<-temp[order(ids),]
            ids<-temp$id
            #stop("remove_incomplete_firms")
            # Remove incomplete firms (i.e. those that do not survive until end of time)
            if(exists("remove_incomplete_firms") && remove_incomplete_firms){
              inds_incomplete<-which(as.numeric(object$active)==0)
              ids_incomplete<-unique(its[inds_incomplete])
              #stop(print(length(ids_incomplete)))
              inds_incomplete<-which(ids %in% ids_incomplete)
              temp_test<-temp[-inds_incomplete,]
              temp<-temp_test
              ids<-temp$id
              
              
              inds_incomplete<-which(its %in% ids_incomplete)
              object_test<-object[-inds_incomplete,]
              if(is.element(0, object_test$active)){stop("Removal of inactive firms did not work!")}
              object<-object_test
              object<-object[order(object$id),]
              temp<-temp[order(temp$id),]
              rm(object_test, inds_incomplete, ids_incomplete, temp_test)
              its<-object$id
            }
            inds_select<-which(its %in% ids)
            object<-object[inds_select,]
            rm(inds_select)
            its<-object[,match("id", colnames(object))]
            iterate<-unique(its)
            if(length(temp$id)!=length(iterate)){stop("HILFE")}
            share<-object[,match("share_conventional_vintages_used",colnames(object))]
            if(length(iterate)<1000){stop("Firm id implementation check")}
            rm(object)
            
            # stop("CONTROL WHETHER LENGTH TEMP == LENGTH ITERATE, i.e. possibly I removed inactive firms from temp")
          }else if(agent=="Eurostat"){
            iterate<-unique(object$Run)
            its<-object$Run
            share<-object$share_conv_capital_used
            rm(object)
          }
          empty<-rep(NA, length(iterate))
          if(agent == "Firm" && length(iterate) != length(temp$id)){stop("Check whether you really have the intersection!!")}
          for(i in 1:length(iterate)){
            if(agent == "Firm" && i%%100 == 0){print(paste("compute variance", i, length(iterate)))}
            id<-iterate[i]
            idx<-which(its == id)
            empty[i]<-var(share[idx], na.rm=T)
          }
          empty[which(empty<1e-7)]<-NA
          if(agent=="Firm"){
            test<-round(as.numeric(as.character(iterate)), digit=4)-(as.numeric(as.character(temp$id)))
            if(abs(sum(test))>1e-10 || is.element(NA, test)){stop("verify this")}
          }
          temp$variance_share_full<-empty
          temp_full<-rbind(temp0, temp)
          if(agent=="Eurostat"){temp_full<-temp_full[order(temp_full$Run, temp_full$Periods),]
          }else if(agent=="Firm"){temp_full<-temp_full[order(temp_full$Run, temp_full$id, temp_full$Periods),]}
          temp<-temp_full
          rm(iterate, share, i, id, its, empty, temp0, temp_full)
          flag_new<-1
        } # data not yet avail
      } # if use variance
      #stop("Test")
      if(exists("flag_new") && flag_new == 1){
        if(is.element("abs_crit_cap_ratio", colnames(temp))){
          temp$abs_crit_cap_ratio[which(is.infinite(temp$abs_crit_cap_ratio))]<-NA
        }
        if(agent=="Eurostat"){
          data_euro<-temp
          save(data_euro, file = paste(data_from,"data_",agent,"_",parameter,".RData",sep=""))
        }else{
          save(data_firm, file = paste(data_from,"data_",agent,"_backup_",parameter,".RData",sep=""))
          data_firm<-temp
          if(exists("remove_incomplete_firms") && remove_incomplete_firms){
            save(data_firm, file = paste(data_from,"data_",agent,"_",parameter,".RData",sep=""))
          }else{
            save(data_firm, file = paste(data_from,"data_",agent,"_",parameter,"_incl_incomplete_firms.RData",sep=""))
          }
        }
        rm(flag_new, temp)
      }# if no new data created
      rm(temp, var_ind)
    } # use variance or abs_crit as targets
    
    
    
    if(reg_level == "Firm"){
      
      # Add macro control variables and initial conditions to firm data
      
      if(exists("data_euro") == FALSE){ # Macro-econ. controls for firm level 
        # analysis existing? If not, get it. 
        reg_level<-"Eurostat"
        if(file.exists(paste(data_from,"data_",reg_level,"_",parameter,".RData",sep=""))!=T){
          source(paste(script_dir,"/all_agents/regression_analyses/aux/get_data_spill_test.r", sep=""))
          #output: data_euro, data_firm
        }else{load(paste(data_from,"data_",reg_level,"_",parameter,".RData",sep=""))}
        reg_level<-"Firm"
      }
      #
      data_euro<-data_euro[,c(1:match("crit_share", colnames(data_euro)))]
      
      
      macro_vars<-c("techn_frontier_conv", "no_active_firms", "eco_price_wage_ratio", 
                    "frontier_gap", "specific_skill_gap", 
                    "min_learning_coefficient_var","learning_spillover_strength_var")
      if(with_policy && with_rand_policy){
        macro_vars<-c(macro_vars, "eco_tax_rate", "eco_consumption_subsidy", "eco_investment_subsidy")
        macro_vars<-macro_vars[which(macro_vars != "eco_price_wage_ratio")]
      }
      source(paste(script_dir,"/all_agents/regression_analyses/aux/get_macro_data_for_firm.r", sep=""))
      
    }
  }
  
  
  # Make decision boundary plot (using k-means clustering)
  if(exists("use_plot_threshold") && use_plot_threshold){
    print(paste("Plot decision boundaries for ", agent))
    # Data to be plotted: 
    # data_euro resp. data_firm -> Initial techn. conditions and crit levels. 
    source(paste(script_dir,"/all_agents/regression_analyses/aux/plots_threshold.r", sep=""))
  }else{print(paste("No decision boundary plot for ", agent, parameter))}
  
  if(run_regression){
    print(paste("Start regression analysis for ", agent))
    
    if(sum(with_rand_barr, with_rand_policy, with_rand_learn) > 1){
      if(exists("use_tax_spill_skill") && use_tax_spill_skill){
        use_tax_spill_skill<-T
      }
    }else{
      use_tax_spill_skill<-F
    }
    #
 
    #
    if(with_rand_barr || with_rand_learn || with_rand_policy){
      plot_dir_temp<-paste(plot_dir,"/",reg_level,"_level_of_analysis/",sep="")
      if(dir.exists(plot_dir_temp)!=T){
        dir.create(plot_dir_temp, recursive =T)
      }
      plot_dir_temp_aic<-paste(plot_dir_temp,"/stepAIC_results/",sep="")
      if(dir.exists(plot_dir_temp_aic)!=T){
        dir.create(plot_dir_temp_aic, recursive =T)
      }
      name_extension<-c()
      if(exists("use_2sls_manually") && use_2sls_manually){name_extension<-c(name_extension, "manual_2sls")}
      if(exists("use_rounded_type_hat") && use_rounded_type_hat){name_extension<-c(name_extension, "rounded_type")}
      if(exists("macro_control_regression") && macro_control_regression){name_extension<-c(name_extension,"macro_control")}
      if(exists("use_mixture_model") && use_mixture_model){name_extension<-c(name_extension,"mixture")}

      if(reg_level == "Eurostat"){
        source(paste(script_dir,"/all_agents/regression_analyses/macro_level_analysis.r", sep=""))
      }else if(reg_level == "Firm"){
        source(paste(script_dir,"/all_agents/regression_analyses/firm_level_analysis.r", sep=""))
      }
      
      rm(use_truncated_data)
      }else{print("Regression analyses are not run because no random parameter setting.")}  
  }
}else{print("Regression not run because run_regression == 0 or gov. agent")}

if(exists("reg_level")){
  agent<-reg_level
  rm(reg_level)
}
rm(list=(ls(pattern="macro_")));rm(list= ls(pattern="micro_")); rm(list=ls(pattern="ids_")); rm(list=ls(pattern="inds_"))

rm(use_probit,use_logit,use_gamlss, k_neighbors, use_plot_threshold)
on.exit(if(is.null(dev.list()) == F){ dev.off()}, add=T)  

