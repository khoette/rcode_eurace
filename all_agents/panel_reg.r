# To dos: For regression: I should include "scenario fixed effects", 
# i.e. identifier for final technological regime. But this only makes sense 
# for the regressions where I want to explain something other than the share 
# of conv capital used.... 
# Then, I can study properties also differentiated for regime type
# 
make_plots<-1

parameter<-parameters_tmp[1]
agent<-"Firm"
agent<-"Eurostat"

for(p1 in parameters_tmp){
  
  parameter<-p1
  for(agent in c("Eurostat", "Firm")){
    #  for(agent in c("Eurostat","Firm")){
    
    #for(agent in c("Firm")){
    if(agent=="Eurostat"){
      make_plots <- 1
      no_agents<-1
      effect_type<-0
      eff_types<-c(0,1,2,3)
      group<-0
      groups<-c(0,1) # group 0: Type, 1: Run, 3: id, FURTHER GROUPS POSSIBLE
    }else if(agent == "Firm"){
      make_plots <- 1
      no_agents<-120
      effect_type<-1
      eff_types<-c(1)
      group<-0
      groups<-c(0,1)
    }
    #parameter<-""
    
    #parameter<-p1
    
    data_types_available<-c("levels", "ann_growth_rates", "normalized_by_init_avg", "normalized_by_per-run_avg", "Time_series", "Trend", "Cycle")
    available_aggregation_levels<-c("single_large", "batch", "eco_conv_batch", "single_switch", "eco_conv_switch", "batch_Learn_Type", "batch_Ease", "batch_Spill")
    data_type<-data_types_available[1]
    #available_aggregation_levels<-c("single_large", "batch", "eco_conv_batch", "single_switch", "eco_conv_switch")
    
    kind_of_data<-"logs"
    d<-1
    
    agent0<-agent 
    agent<-"Panel_regression_init_cond_final_state_learning"
    source(paste(script_dir,"/aux_all_agents/create_directories_for_plotting.r",sep=""))
    if(file.exists(plot_dir_selection) == FALSE){
      dir.create(plot_dir_selection)
    }
    agent<-agent0
    rm(agent0)
    
    if(agent == "Eurostat"){
      agent_temp<-"Eurostat"
      variables_init_cond_temp<-c("specific_skill_gap", "frontier_gap", "average_s_skill_conv", 
                                  "techn_frontier_conv",  "monthly_output", 
                                  "no_active_firms","eco_price_wage_ratio", "share_conv_capital_used", 
                                  "price_per_prod_unit_ratio_frontier", 
                                  "real_wage", "min_learning_coefficient_var", "learning_spillover_strength_var")
      target_var_temp_const<-c("share_conv_capital_used", "no_active_firms", "monthly_output")
      #var_of_interest<-c("variance_share", "variance_no_firms")
      var_of_interest<-c("share_conv_capital_used", "no_active_firms", "monthly_output")
      ind_var_of_interest<-match(var_of_interest, target_var_temp_const)
      number_models<-c(1:8)
    }else if(agent == "Firm"){
      agent_temp<-"Firm"
      variables_init_cond_temp<-c( "technology_conv" , "mean_spec_skills_conv", "mean_wage",
                                   "no_employees", "output", "age", "price", "unit_costs",
                                   "share_conventional_vintages_used", "technology_gap", 
                                   "mean_specific_skill_gap", "min_learning_coefficient_var", 
                                   "learning_spillover_strength_var")
      target_var_temp_const<-c("share_conventional_vintages_used", "output", "no_employees", "unit_costs")
      var_of_interest<-target_var_temp_const
      ind_var_of_interest<-match(var_of_interest, target_var_temp_const)
      number_models<-c(1:4)
    }
    
    ind_var_of_interest<-ind_var_of_interest[which(is.na(ind_var_of_interest)==FALSE)]
    
    
    kind_of_data<-"logs"
    data_directory<-paste(experiment_directory,"/rdata/",kind_of_data,"/",data_type[d],"/",sep="")
    
    aggr_temp<-1
    if(aggr_temp == 1){
      load(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent_temp,".RData",sep=""))
    }else if(aggr_temp == 2){
      load(file=paste(data_directory,"data_single_switch_",experiment_name,"_",parameter,"_",agent_temp,".RData",sep=""))
    }
    ind0<-match(c("id", "Periods", "Type", "Firm_Type", "Adopter_Type", "Run","active", target_var_temp_const, variables_init_cond_temp), colnames(object))
    ind0<-unique(ind0[which(is.na(ind0)==FALSE)])
    data0<-object[ind0]
    rm(object)
    if(agent=="Firm"){
      data0<-data0[which(data0$active==1),]
    }
    
    ind0<-match(c(target_var_temp_const, variables_init_cond_temp), colnames(data0))
    ind0<-unique(ind0[which(is.na(ind0)==FALSE)])
    
    fix_vars_temp<-match(c("Parameter","id", "Periods", "Type", "Firm_Type", "Run","active"), colnames(data0))
    fix_vars_temp<-unique(fix_vars_temp[which(is.na(fix_vars_temp)== FALSE) ])
    
    if(agent=="Firm"){ # If I want to reduce the size of the data set, 
      # for firms aggr. to e.g. yearly avg
    time_temp<-seq(600,(20*number_xml), 600)
    
    if(agent=="Firm"){
      len<-as.integer(length(data0[,1])*0.2)
    }else{
      len<-length(time_temp)*runs*length(parameters_tmp)*no_agents    
    }
    data_temp<-as.data.frame(array(NA, dim=c(len, length(data0[1,]))))
    rm(len)
    colnames(data_temp)<-colnames(data0)
    vars<-c(1:length(data0))
    vars<-vars[which(is.element(vars,fix_vars_temp)==FALSE)]
    
    b<-c(1:(runs*no_agents))
    c<-c(1:length(time_temp))
    if(is.element("Parameter", colnames(data0))){
      data0$Parameter<-as.vector(as.character(data0$Parameter))
    }
    data0$Run<-floor(as.numeric(as.vector(as.character(data0$Run))))
    data0$Periods<-as.vector(as.character(data0$Periods))
    data0$Type<-(as.character(data0$Type))
    
    if(is.element("Firm_type", colnames(data0))){
      data0$Firm_Type<-floor(as.numeric(as.vector(as.character(data0$Firm_Type))))
    }
    if(is.element("id", colnames(data0))){
      data0$id<-(as.character(data0$id))
    }
    for(i in (length(fix_vars_temp)+1):length(colnames(data0))){
      data0[,i]<-as.numeric(data0[,i])
    }
    i1<-1
    for(p in parameters_tmp){
      if(is.element("Parameter", colnames(data0))){
        data02<-data0[which(data0$Parameter == p),]
      }else{
        data02<-data0
        rm(data0)
      }
      for(t in c){
        time_temp2<-seq(time_temp[t]-100, (time_temp[t]), 20)
        temp2<-data02[which(as.vector(as.character(data02$Periods)) %in% time_temp2),]
        rm(time_temp2)
        temp2<-temp2[order(as.numeric(as.character(temp2$Run))),]
        if(agent=="Firm"){
          temp2<-temp2[order(as.numeric(as.character(temp2$id))),]
          temp2$id<-as.numeric(as.character(temp2$id))
        }
        
        index<-1
        if(no_agents>1){
          print(paste("Time ", time_temp[t]))
          for(ag in unique(temp2$id)){
            temp4<-temp2[which(temp2$id == ag),]
            temp3<-as.matrix(temp4[,vars])
            a<-apply(temp3,FUN=mean, MARGIN=2, na.rm=TRUE)
            data_temp[i1,vars]<-a
            data_temp[i1,fix_vars_temp]<-temp4[6,fix_vars_temp]
            i1<-i1+1
          }
        }else{
          for(i in b){
            temp3<-as.matrix(temp2[c(index:(index+5)),vars])
            a<-apply(temp3,FUN=mean, MARGIN=2, na.rm=TRUE)
            data_temp[i1,vars]<-a
            data_temp[i1,fix_vars_temp]<-temp2[index,fix_vars_temp]
            index<-index+6
            i1<-i1+1
          }
        }
      }
    }
    data0<-data02
    
    rm(a, b, c, index, i1, i, temp3, temp2, data02, vars)
    if(no_agents>1){
      rm(ag)
    }

    }
    

    
    for(group in groups){ # group 0: Type, 1: Run, 3: id, FURTHER GROUPS POSSIBLE
    
      if(group == 0){
        if(agent == "Eurostat"){
          index<-c("Run", "Periods", "Type")
        }else if(agent == "Firm"){
          index<-c("id", "Periods", "Type")
        }
      }else if(group == 1){
        if(agent == "Eurostat"){
          index<-c("Run", "Periods")
        }else if(agent == "Firm"){
          index=c("id", "Periods", "Run")
        }
      }else if(group == 2){
        index=c("id", "Periods")
      }
      data0<-pdata.frame(data0, index=index, drop.index=TRUE, row.names = TRUE)
      if(length(index)==2){
        eff_types<-c(0,1,2)
      }else if(length(index == 3)){
        eff_types<-c(3)
      }
    
      #if(is.pbalanced(data0) != TRUE){
      #  make.pbalanced(data0)
      #}
      #if(agent=="Firm"){
      #  if(group == 0){
      #    data0<-pdata.frame(data0, index=c("id", "Periods", "Type"), drop.index=TRUE, row.names = TRUE)
      #  }else if(group == 1){
      #    data0<-pdata.frame(data0, index=c("id", "Periods", "Run"), drop.index=TRUE, row.names = TRUE)
      #  }
      #}else{
      #  if(group == 1){
      #    data0<-pdata.frame(data0, index=c("Run", "Periods", "Type"), drop.index=TRUE, row.names = TRUE)
      #  }else if(group == 0){
      #    data0<-pdata.frame(data0, index=c("Type", "Periods", "Run"), drop.index=TRUE, row.names = TRUE)
      # }
      #data0<-pdata.frame(data0, index=c("Run", "Periods"), drop.index=TRUE, row.names = TRUE)
      #}
    

  
    ind0<-match(c("id", "Periods", "Type", "Firm_Type", "Run", target_var_temp_const, variables_init_cond_temp), colnames(data0))
    ind0<-unique(ind0[which(is.na(ind0)==FALSE)])

    ind_controls<-match(variables_init_cond_temp, colnames(data0))
    ind_controls<-ind_controls[which(is.na(ind_controls)==FALSE)]
    ind_targets<-match(target_var_temp_const, colnames(data0))
    ind_targets<-ind_targets[which(is.na(ind_targets)==FALSE)]
    type<-match("Type", colnames(data0))
    share<-match(c("share_conv_capital_used", "share_conventional_vintages_used"), colnames(data0))
    spill<-match("learning_spillover_strength_var", colnames(data0))
    ease<-match("min_learning_coefficient_var", colnames(data0))
    sk_gap<-match(c("specific_skill_gap", "mean_specific_skill_gap"), colnames(data0))
    fr_gap<-match(c("frontier_gap","technology_gap"), colnames(data0))
    conv_tech<-match(c("techn_frontier_conv", "technology_conv" ), colnames(data0))
    conv_skill<-match(c("mean_spec_skills_conv","average_s_skill_conv"), colnames(data0))
    output<-match(c("monthly_output","output"), colnames(data0))
    pr_eco<-match("eco_price_wage_ratio", colnames(data0))
    pr_prod_unit<-match("price_per_prod_unit_ratio_frontier", colnames((data0)))
    wage<-match(c("mean_wage", "real_wage"), colnames(data0))
    no_empl<-match("no_employees", colnames(data0))
    age<-match("age", colnames(data0))
    price<-match("price", colnames(data0))
    unit_costs<-match("unit_costs", colnames(data0))
    mark_up<-match("actual_mark_up", colnames(data0))
    no_active_firm<-match("no_active_firms", colnames(data0))
    if(agent == "Eurostat" && is.na(no_active_firm==FALSE)){
      data0$no_active_firms<-0.01*data0$no_active_firms
    }
   
    share<-share[which(is.na(share) != TRUE)]
    spill<-spill[which(is.na(spill) != TRUE)]
    ease<-ease[which(is.na(ease) != TRUE)]
    if(var(as.numeric(data0[,spill]), na.rm=TRUE) < 0.00001){
      spill<-c()
      no_spill<-1
    }
    if(var(as.numeric(data0[,ease]), na.rm=TRUE)<0.00001){
      ease<-c()
      no_ease<-1
    }
    sk_gap<-sk_gap[which(is.na(sk_gap) != TRUE)]
    fr_gap<-fr_gap[which(is.na(fr_gap) != TRUE)]
    conv_tech<-conv_tech[which(is.na(conv_tech) != TRUE)]
    conv_skill<-conv_skill[which(is.na(conv_skill) != TRUE)]
    output<-output[which(is.na(output) != TRUE)]
    pr_eco<-pr_eco[which(is.na(pr_eco) != TRUE)]
    pr_prod_unit<-pr_prod_unit[which(is.na(pr_prod_unit) != TRUE)]
    wage<-wage[which(is.na(wage) != TRUE)]
    no_empl<-no_empl[which(is.na(no_empl) != TRUE)]
    price<-price[which(is.na(price) != TRUE)]
    unit_costs<-unit_costs[which(is.na(unit_costs) != TRUE)]
    mark_up<-mark_up[which(is.na(mark_up) != TRUE)]
    no_active_firm<-no_active_firm[which(is.na(no_active_firm) != TRUE)]
    
    dir_temp0<-paste(plot_dir_selection,"/Regression_results/",sep="")
    if(dir.exists(dir_temp0)==FALSE){
      dir.create(dir_temp0)
    }
    
     if(group == 0){
        dir_temp<-paste(dir_temp0,"/Group_by_indiv_effect/",sep="")
        if(dir.exists(dir_temp)==FALSE){
          dir.create(dir_temp)
        }
      }else if(group == 1) {
        dir_temp<-paste(dir_temp0,"/Group_by_run_effect/",sep="")
        if(dir.exists(dir_temp)==FALSE){
          dir.create(dir_temp)
        }
      }else if(group == 2) {
        dir_temp<-paste(dir_temp0,"/Group_by_type_effect/",sep="")
        if(dir.exists(dir_temp)==FALSE){
          dir.create(dir_temp)
        }
      }

    for(target in ind_targets){
    for(m in number_models){
      interact<-c()
      if(agent == "Eurostat"){
        if(m == 1){
          if(colnames(data0)[target]!="monthly_output"){
            model = data0[,target] ~ lag(data0[,output], 12) + lag(data0[,sk_gap], 12) + lag(data0[,fr_gap],12) + lag(data0[,conv_skill],12)
            indices<-c(target, output, sk_gap, fr_gap, conv_skill)
          }else{
            model = data0[,target] ~ lag(data0[,share], 12) + lag(data0[,sk_gap], 12) + lag(data0[,fr_gap],12) + lag(data0[,conv_skill],12)
            indices<-c(target, share, sk_gap, fr_gap, conv_skill)
          }
        }else if(m == 2){
          model = data0[,target] ~ lag(data0[,share], 240) 
          indices<-c(target, target)
        }else if(m == 3){
          if(exists("no_spill") && exists("no_ease") && no_spill && no_ease){
            model = data0[,target] ~ lag(data0[,no_active_firm],12)+ data0[,output] + lag(data0[,sk_gap], 12) + lag(data0[,fr_gap], 12)  + lag(data0[,conv_skill],12) + lag(data0[,conv_tech],12)+ lag(data0[,pr_eco],12)+ lag(data0[,pr_prod_unit],12)+ lag(data0[,wage],12)
            indices<-c(target, no_active_firm, output, sk_gap, fr_gap, conv_skill, conv_tech, pr_eco)
          }else{
            model = data0[,target] ~ lag(data0[,no_active_firm],12)+ data0[,output] + lag(data0[,sk_gap], 12) + lag(data0[,fr_gap], 12)  + lag(data0[,conv_skill],12) + lag(data0[,conv_tech],12)+ lag(data0[,pr_eco],12)+ lag(data0[,pr_prod_unit],12)+ lag(data0[,wage],12)+  data0[,ease]+ data0[,spill]
            indices<-c(target, no_active_firm, output, sk_gap, fr_gap, conv_skill, conv_tech, pr_eco,  ease, spill)
          }
        }else if(m == 4){
          if(exists("no_spill") && exists("no_ease") && no_spill && no_ease){
            model = data0[,target] ~ lag(data0[,no_active_firm],12)+ data0[,output]  + lag(data0[,conv_skill],12) + lag(data0[,conv_tech],12)+ lag(data0[,pr_eco],12)+ lag(data0[,pr_prod_unit],12)+ lag(data0[,wage],12)
            indices<-c(target, no_active_firm, output, conv_skill, conv_tech, pr_eco, pr_prod_unit, wage)
          }else{
            model = data0[,target] ~ lag(data0[,no_active_firm],12)+ data0[,output]  + lag(data0[,conv_skill],12) + lag(data0[,conv_tech],12)+ lag(data0[,pr_eco],12)+ lag(data0[,pr_prod_unit],12)+ lag(data0[,wage],12)+ data0[,ease]+ data0[,spill]
            indices<-c(target, no_active_firm, output, conv_skill, conv_tech, pr_eco, pr_prod_unit, wage, ease, spill)
          }
        }else if(m == 5){
          if(exists("no_spill")!=TRUE && exists("no_ease")!=TRUE){
            model = data0[,target] ~ data0[,spill]*lag(data0[,target],480)  + data0[,ease]*lag(data0[,target],480)
            indices<-c(target, target)
            interact<-c(spill, target, ease, target)
          }else{
            #model = data0[,target] ~ lag(data0[,target],480)*lag(data0[,target],480)  + lag(data0[,target],960)
            #indices<-c(target)
            
          }
        }else if(m == 6){
          model = data0[,target] ~ data0[,conv_tech]
          indices<-c(target,conv_tech)
        }else if(m == 7){
          model = data0[,sk_gap] ~ data0[,fr_gap]
          indices<-c(sk_gap,fr_gap)
        }else if(m == 8){
          if(exists("no_spill") && exists("no_ease") && no_spill && no_ease){
            model = data0[,target] ~ data0[,type]*lag(data0[,no_active_firm],12)+ data0[,type]*data0[,output]  + data0[,type]*lag(data0[,conv_skill],12) + data0[,type]*lag(data0[,conv_tech],12)+ data0[,type]*lag(data0[,pr_eco],12)+ data0[,type]*lag(data0[,pr_prod_unit],12)+ data0[,type]*lag(data0[,wage],12)
            indices<-c(target, type, no_active_firm, output, conv_skill, conv_tech, pr_eco, pr_prod_unit, wage)
          }else{
            model = data0[,target] ~ data0[,type]*lag(data0[,no_active_firm],12)+ data0[,type]*data0[,output]  + data0[,type]*lag(data0[,conv_skill],12) + data0[,type]*lag(data0[,conv_tech],12)+ data0[,type]*lag(data0[,pr_eco],12)+ data0[,type]*lag(data0[,pr_prod_unit],12)+ data0[,type]*lag(data0[,wage],12)+ data0[,type]*data0[,ease]+ data0[,type]*data0[,spill]
            indices<-c(target,type, no_active_firm, output, conv_skill, conv_tech, pr_eco, pr_prod_unit, wage, ease, spill)
          }
        }
      }else if(agent=="Firm"){
        if(m==1){
          if(exists("no_spill") && exists("no_ease") && no_spill && no_ease){
            model = data0[,target] ~ lag(data0[,sk_gap], 240) + lag(data0[,fr_gap], 240) + lag(data0[,sk_gap], 240) + lag(data0[,fr_gap], 240)
            indices<-c(target,sk_gap, fr_gap)
          }else{
            model = data0[,target] ~ data0[,spill]*lag(data0[,sk_gap], 240) + data0[,spill]*lag(data0[,fr_gap], 240) + data0[,ease]*lag(data0[,sk_gap], 240) + data0[,ease]*lag(data0[,fr_gap], 240)
            indices<-c(target, spill, ease, sk_gap, fr_gap)
            interact<-c(spill, ease, sk_gap, fr_gap)
          }
        }else if(m==2){
          if(exists("no_spill") && exists("no_ease") && no_spill && no_ease){
            model = data0[,target] ~ lag(data0[,conv_tech], 240) + lag(data0[,conv_skill], 240) + lag(data0[,conv_tech], 240) + lag(data0[,conv_skill], 240)
            indices<-c(target, sk_gap, fr_gap)
          }else{
            model = data0[,target] ~ data0[,spill]*lag(data0[,conv_tech], 240) + data0[,spill]*lag(data0[,conv_skill], 240) + data0[,ease]*lag(data0[,conv_tech], 240) + data0[,ease]*lag(data0[,conv_skill], 240)
            indices<-c(target, spill, ease, sk_gap, fr_gap)
            interact<-c(spill, ease, sk_gap, fr_gap)
          }
        }else if(m==3){
          if(exists("no_spill") && exists("no_ease") && no_spill && no_ease){
            model = data0[,target] ~ data0[,output] + data0[,no_empl] + data0[,wage] + data0[,unit_costs] + data0[,price] + data0[,sk_gap] + data0[,sk_gap]
            indices<-c(target, output, no_empl, wage, unit_costs, price, sk_gap, sk_gap)
          }else{
            model = data0[,target] ~ data0[,output] + data0[,no_empl] + data0[,wage] + data0[,unit_costs] + data0[,price] + data0[,ease]*data0[,sk_gap] + data0[,spill]*data0[,sk_gap]
            indices<-c(target, output, no_empl, wage, unit_costs, price, ease, sk_gap, spill, sk_gap)
            interact<-c(spill, ease, sk_gap)
          }
        }else if(m==4){
          if(exists("no_spill") && exists("no_ease") && no_spill && no_ease){
            model = data0[,target] ~ data0[,type]*data0[,output] + data0[,type]*data0[,no_empl] + data0[,type]*data0[,wage] + data0[,type]*data0[,unit_costs] + data0[,type]*data0[,price] + data0[,type]*data0[,sk_gap] + data0[,type]*data0[,sk_gap]
            indices<-c(target, type, output, no_empl, wage, unit_costs, price, sk_gap, sk_gap)
          }else{
            model = data0[,target] ~ data0[,type]*data0[,output] + data0[,type]*data0[,no_empl] + data0[,type]*data0[,wage] + data0[,type]*data0[,unit_costs] + data0[,type]*data0[,price] + data0[,type]*data0[,ease]*data0[,sk_gap] + data0[,type]*data0[,spill]*data0[,sk_gap]
            indices<-c(target, type, output, no_empl, wage, unit_costs, price, ease, sk_gap, spill, sk_gap)
            interact<-c(spill, ease, sk_gap, type)
          }
        }
      }
      if(exists("indices")){
        descr<-paste("Explain ", colnames(data0)[target], "by", paste(colnames(data0)[indices[2:length(indices)]], collapse = " + "), " and possibly interaction between:", paste(colnames(data0)[interact], collapse = "_"), "\n Method: Effect type: ", effect_type, "(0: indiv (for eurostat: run, for firm: id), 1: group (eurostat: Type, firm: Run OR Type), 2: time)")
        if(length(interact) != 0){
          if(length(indices) < 7){
            filename<-paste(dir_temp,"/",agent,"_",colnames(data0)[target], "_on_", paste(colnames(data0)[indices[2:min(6:length(indices))]], collapse="_"), "_interaction", sep="")
          }else{
            filename<-paste(dir_temp,"/",agent,"_",colnames(data0)[target], "_on_", paste(colnames(data0)[indices[2:3]], collapse="_"), "and_more_",m,"_and_interaction", sep="")
          }
        }else{
          if(length(indices) < 7){
            filename<-paste(dir_temp,"/",agent,"_",colnames(data0)[target], "_on_", paste(colnames(data0)[indices[2:min(6:length(indices))]], collapse="_"), sep="")
          }else{
            filename<-paste(dir_temp,"/",agent,"_",colnames(data0)[target], "_on_", paste(colnames(data0)[indices[2:min(6:length(indices))]],"_and_more_",m, collapse="_"), sep="")
          }
        }
        
      for(effect_type in eff_types){
        if(effect_type == 1){
          effect <- "individual"
        }else if(effect_type == 2){
          effect <- "time"
        }else if(effect_type == 3){
          effect <- "twoways"
        }else if(effect_type == 4){
          effect <- "nested"
        }
        
        if(effect_type == eff_types[1]){
          capture.output(paste(descr), file=filename, append=FALSE)
        }else{
          capture.output(paste(descr), file=filename, append=TRUE)
        }
        capture.output(paste(model), file=filename, append=TRUE)
        fe <- plm(model ,data=data0, model = "within", effect = effect)
        
        capture.output(summary(fe, digits=4), file=filename, append=TRUE)
        capture.output(pbgtest(fe), file=filename, append=TRUE)
        if(is.element(effect, c("individual", "time"))){
          pvcmfe <- pvcm(model, data=data0, model = "within", effect = effect)
          capture.output(print("Is model better than a pooling model? (i.e. same coefficients applied to all indiv.?)"))
          capture.output(pooltest(fe, pvcmfe))
        }
        if(is.element(effect, c("individual", "nested"))){
          fe0<-fe
        }
        
        rm(fe)
      } # end effect in eff_types

        
        if((m!=3 &&m!= 4 && m!=5) && agent=="Eurostat"){
          re <- plm(model, data=data0, model = "random", effect = effect)
          
          capture.output(summary(re, digits=4), file=filename, append=TRUE)
          capture.output(pbgtest(re), file=filename, append=TRUE)
          if(is.element(effect, c("individual", "time"))){
            pvcmre <- pvcm(model, data=data0, model = "random", effect = effect)
            
            capture.output(print("Is model better than a pooling model? (i.e. same coefficients applied to all indiv.?)"))
            capture.output(pooltest(re, pvcmre))
            rm(pvcmre)
          }
          rm(re)
        }
        
      } # effect type in eff types
        
        fd <- plm(model, data=data0, model = "fd")
        capture.output(summary(fd, digits=4), file=filename, append=TRUE)
        capture.output(pbgtest(fd), file=filename, append=TRUE)
        rm(fd)
        
        pl <- plm(model, data=data0, model = "pooling")
        capture.output(summary(pl, digits=4), file=filename, append=TRUE)
        capture.output(pbgtest(pl), file=filename, append=TRUE)
        rm(pl)
        
        bw <- plm(model, data=data0, model = "between")
        capture.output(summary(bw, digits=4), file=filename, append=TRUE)
        #capture.output(pbgtest(bw), file=filename, append=TRUE)
        rm(bw)
        
        if(length(indices)==2 && indices[1]!=indices[2] ){
          capture.output(pgrangertest(data0[,1] ~ data0[,indices[2]], data=data0), file=filename, append = TRUE)
        }
        
        if(agent!="Firm" && exists("fe0")){
          capture.output(print("Fixed effects from FE model: (1) level, (2) dmean"))
          capture.output(fixef(fe0, type="level"), file=filename, append=TRUE)
          capture.output(fixef(fe0, type="dmean"), file=filename, append=TRUE)
          #rm(fe0)
        }
        
        capture.output(stat.desc(data0), file=filename , append = TRUE)
        
        rm(indices, interact)
        
      }# end if exists indices
    
} # model in 1: number of models
    }#target in ind_targets
    }# effect type in eff_types (whether group or indiv. effects)
    rm(effect_type, eff_types)
    if(exists("no_spill")){
      rm(no_spill, no_ease)
    }
    if(length(groups)>1){
      data0<-as.data.frame(data0)
    }
    } # group by run in groups (refers only to firm: 
    #   If group == 1, group index is run, if == 0, group index is type)

    rm(data0, group, groups)
} # agent in list of agents
  
} # p1 in parameters_tmp
rm(list=ls(patter="temp"))

