library("RSQLite")
library("mFilter")
library("spam")
library("abind")
library("MASS")
library("xtable")
library("reshape2")
library("ggplot2")
library("AER") 
library("ivprobit")
#library("coin")
#library("RSEIS")
#library("mgcv")
library("gamlss")
library("gamlss.mx")
#library("forecast")
library("plm")
library("tibble")
#library("moments")
#library("tseries")
#library("urca")
#library("doParallel")
#library("mlogit")
library("pastecs")
library("nnet")
#library("rms")
library("BaylorEdPsych")
library(caret)
library(RColorBrewer)
library(ggthemes)
library(ivtools)



# Get data in data frame for ggplot
# 
list_of_agents<-c("Eurostat", "Government", "Firm")
for(agent in list_of_agents){
  
  if(agent == "Firm"){
    no_agents<-Firm
  }else{
    no_agents<-1
  }
  
  #Find out which data is available. 
  if(exists("USE_SUBSET_OF_DATA") && USE_SUBSET_OF_DATA == 1 ){
    variable_table<-read.delim(paste(experiment_directory,"variables_reduced.txt",sep=""), header=FALSE, sep="\t", skip=1)
  } else{
    variable_table<-read.delim(paste(experiment_directory,"variables.txt",sep=""), header=FALSE, sep="\t", skip=1)
  }
  attach(variable_table)
  variable_table_agent<-variable_table[which(variable_table[,3] == agent),]
  detach(variable_table)
  list_of_var_complete<-as.vector(variable_table_agent[,1])
  # Ensure that "id" and "active" are at beginning of variable vector (for firm agent!)
  list_of_var_complete<-list_of_var_complete[which(list_of_var_complete != "id")]
  list_of_var_complete<-list_of_var_complete[which(list_of_var_complete != "active")]
  if(no_agents > 1 && is.na(match("id",list_of_var_complete))!=FALSE){
    list_of_var_complete<-append("id",list_of_var_complete, after = length("id"))
  }
  if(agent == "Firm" && is.na(match("active",list_of_var_complete))!=FALSE){
    list_of_var_complete<-append("active",list_of_var_complete, after = length("active"))
  }
  list_of_var<-list_of_var_complete
  # Ensure that "id" is collected for more-than-one-individual agents: 
  eval(call("<-",(paste("list_of_var_",agent,sep="")),list_of_var))
}
eval(call("<-",(paste("list_of_var",sep="")),as.name(paste("list_of_var_",agent,sep=""))))

