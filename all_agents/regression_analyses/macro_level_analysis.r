# Data given: data_euro
source(paste(script_dir,"/aux_all_agents/plot_settings/settings_axis_labels.r", sep=""))
data_work<-data_euro
vars_temp<-colnames(data_work)
periods_temp<-unique(data_work$Periods)
periods_temp_target<-periods_temp[order(periods_temp, decreasing = T)][1] # take only end of time
periods_temp_controls<-periods_temp[order(periods_temp)][1] # take only first obs.

if(exists("macro_control_regression") && macro_control_regression){
  periods_temp_target<-periods_temp[order(periods_temp,decreasing = T)]
}

if(exists("use_type_adjusted_crit_levels") && use_type_adjusted_crit_levels){
  target_vars_temp<-c("share" = "share_conv_capital_used", 
                      "crit_time" = "Time", 
                      "abs_crit_frontier_ratio" = "abs_crit_frontier_ratio",
                      "abs_crit_skill_ratio" = "abs_crit_skill_ratio",
                      "abs_crit_cap_ratio" = "abs_crit_cap_ratio", 
                      "variance_share_full" = "variance_share_full")
}else{
  target_vars_temp<-c("share" = "share_conv_capital_used", 
                      "crit_time" = "Time", 
                      "crit_frontier_ratio" = "crit_frontier_ratio",
                      "crit_skill_ratio" = "crit_skill_ratio",
                      "crit_share" = "crit_share", 
                      "variance_share_full" = "variance_share_full")
}

if(exists("macro_control_regression") && macro_control_regression){
  target_vars_temp<-c("monthly_output", "unemployment_rate", "no_active_firms")
  target_vars_temp<-unique(target_vars_temp[which(!is.na(match(target_vars_temp, colnames(data_work))))])
}
target_vars_temp<-target_vars_temp[which(!is.na(match(target_vars_temp, colnames(data_work))))]

control_vars_temp<-c("techn_frontier_conv", "average_s_skill_conv", 
                     "monthly_output", "no_active_firms", "eco_price_wage_ratio")

if(with_policy && with_rand_policy){control_vars_temp<-control_vars_temp[which(control_vars_temp != "eco_price_wage_ratio")]}
barrier_vars_temp<-c("frontier_gap", "specific_skill_gap")

policy_vars_temp<-c("eco_tax_rate", "eco_consumption_subsidy", "eco_investment_subsidy")


targ<-target_vars_temp[1]
p<-periods_temp_target[1]
if(exists("exp_temp")!=T){exp_temp<-""}
if(exists("name_extension") != T){name_extension<-""}
if(exists("macro_control_regression")&&macro_control_regression){name_extension<-c(name_extension,"macro")}

  file_temp<-paste(plot_dir_temp_aic, exp_temp,reg_level,"_testAIC_",paste(name_extension, collapse="_"),".RData",sep="")
  file_name_aic<-paste(plot_dir_temp_aic, "StepAIC_ordered_list_",paste(name_extension,collapse="_"),sep="")


if(exists("use_2sls_manually") && use_2sls_manually){
  write(paste("\n \n ### USE 2SLS manually for TYPE \n"), file=file_name_aic, append=F)
  write(paste("Results of stepAIC tests"), file=file_name_aic, append=T)
}else{
  use_2sls_manually<-0
  write(paste("Results of stepAIC tests"), file=file_name_aic, append=F)
  write(paste("\n \n use_2sls_manually == FALSE"), file=file_name_aic, append=T)
}


write(paste("Results of stepAIC tests"), file=file_name_aic, append=F)
table_for_stepaic<-matrix(NA,nrow=0,ncol=0)
if(use_truncated_data && is.element("Time", colnames(data_work))){
  # Remove data with almost 100% of only one technology type 
  # This concerns critical-levels: If last sign conversion in first or last period, conv. techn. 
  # use is almost 100 pct. 
  pers_temp<-unique(data_work$Periods)
  times_temp<-unique(data_work$Time)
  if(is.element(min(pers_temp, na.rm=T), times_temp) ||is.element(max(pers_temp, na.rm=T), times_temp)){
    inds_temp<-which(data_work$Time %in% c(min(pers_temp, na.rm=T), (max(pers_temp, na.rm=T))))
  #if(is.element(max(pers_temp, na.rm=T), times_temp)){
    #inds_temp<-which(data_work$Time %in% c((max(pers_temp, na.rm=T))))
    if(exists("use_type_adjusted_crit_levels") && use_type_adjusted_crit_levels){
      c<-match(c("Time", "abs_crit_frontier_ratio", "abs_crit_skill_ratio", "abs_crit_cap_ratio", "variance_share_full"), colnames(data_work))
    }else{
      c<-match(c("Time", "crit_frontier_ratio", "crit_skill_ratio", "crit_share"), colnames(data_work))
    }
    if(is.element(NA,c)){
      print(paste("Warning: Are you using the desired data?", paste(colnames(data_work), collapse=" ")))
      c<-c[which(!is.na(c))]
    }
    data_work[inds_temp,c]<-NA
  }
  rm(pers_temp, times_temp)
}
for(targ in target_vars_temp){

  inds_target_temp<-match(targ, colnames(data_work))
  for(p in periods_temp_target){
    inds_t_temp<-which(data_work$Periods == p)
    type<-data_work[inds_t_temp,match("Type", colnames(data_work))]
    type<-as.character(type)
    type_named<-type
    type[which(as.character(type)=="conv")]<-0
    type[which(as.character(type)=="eco")]<-1
    type<-as.numeric(type)

    target <- data_work[inds_t_temp, inds_target_temp]
    if(targ == "variance_share_full" && abs(max(target, na.rm=T)) < 0.5){
      target<-100*target
    }
    min_targ<-min(target, na.rm=T)
    max_targ<-max(target, na.rm=T)
    descr_temp<-c(paste("Target variable ",targ, "measured in ",p))
    p2<-periods_temp_controls[1]
    
    for(p2 in periods_temp_controls){
      file_name<-paste(plot_dir_temp, targ,"_in_",p,"_contr_in_",p2,sep="")
      write(paste("\n \n \n #### REGRESSION ON TARGET VAR: ", targ, "IN", p), file=file_name_aic, append = T)
      write(paste("\n \n \n #### REGRESSION ON TARGET VAR: ", targ, "IN", p), file=file_name, append = F)
      file_name_tex<-paste(plot_dir_temp, targ,"_in_",p,"_contr_in_",p2, "_table_notes.txt",sep="")
      write(paste("\n \n \n #### REGRESSION ON TARGET VAR: ", targ, "IN", p), file=file_name_tex, append = F)
      
      data_temp<-data_work[which(data_work$Periods == p2),]
      macro_<-as.matrix(data_temp[,match(control_vars_temp, colnames(data_temp))])
      b0<-cbind(skill=data_temp$specific_skill_gap, frontier=data_temp$frontier_gap)
      if(with_policy && with_rand_policy){
        p0<-as.matrix(data_temp[,match(policy_vars_temp, colnames(data_temp))])
        colnames(p0)<-c("tax", "cons", "inv")
        pbc_inter<-1
      }else{
        p0<-c()
      }
      c0 <- cbind(data_temp$min_learning_coefficient_var, data_temp$learning_spillover_strength_var)
      colnames(c0)<-c("int", "spill")
      if((reg_level == "Eurostat" && targ!= "share_conv_capital_used") || 
         (reg_level == "Firm" && targ != "share_conventional_vintages_used")){
        add_type_dummy<-1
        d_<-type
      }else{
        d_<-rep(0, length(target))
      }
      if(use_ols_procedure){
        p_<-p0
        b_<-b0
        fitdist_temp<-fitDist(target)
        write(paste("\n \n Results of fitDist(target) \n \n "),file=file_name,append=T)
        capture.output(fitdist_temp, file=file_name, append=T)
        families_temp<-c(fitdist_temp$family[1])
        if(min_targ>=0 && max_targ<=1 && targ != "variance_share_full"){
          fitdist_temp_bin<-fitDist(round(target))
          write(paste("\n \n Results of fitDist(round(target)) for binary \n \n "),file=file_name,append=T)
          capture.output(fitdist_temp_bin, file=file_name, append=T)
          families_temp<-c(families_temp, fitdist_temp_bin$family[1])
        }
        # Inputs for run_model.r script: target, c_ as matrix of controls
        c_<-c0
        p_reg_simple<-1
        source(paste(script_dir,"/all_agents/regression_analyses/aux/run_model.r", sep=""))
        p_reg_simple<-F
        interact_int_spill<-data_temp$min_learning_coefficient_var*data_temp$learning_spillover_strength_var
        c_ <- cbind(c0, interact_int_spill)
        interact_skill_front<-data_temp$specific_skill_gap*data_temp$frontier_gap        
        b_ <- cbind(b0, interact_skill_front)
        source(paste(script_dir,"/all_agents/regression_analyses/aux/run_model.r", sep=""))
        int_sq<-data_temp$min_learning_coefficient_var*data_temp$min_learning_coefficient_var
        spill_sq<-data_temp$learning_spillover_strength_var*data_temp$learning_spillover_strength_var
        chi_sq<-cbind(int_sq, spill_sq)
        c_ <- cbind(c0, chi_sq)
        skill_sq<-data_temp$specific_skill_gap*data_temp$specific_skill_gap
        front_sq<-data_temp$frontier_gap*data_temp$frontier_gap
        barr_sq<-cbind(skill_sq, front_sq)
        rm(skill_sq, front_sq)
        b_ <- cbind(b0, barr_sq)
        source(paste(script_dir,"/all_agents/regression_analyses/aux/run_model.r", sep=""))
        c_ <- cbind(c_, interact_int_spill)
        b_ <- cbind(b_, interact_skill_front)
        rm(list = ls(pattern="interact_"))
        
        #if(experiment_directory == "/home/kerstin/Schreibtisch/learn_spill/rand_exper/rand_learn_rand_pol_fix_barr3/"){stop("MAKE spline tests")}
        
        source(paste(script_dir,"/all_agents/regression_analyses/aux/run_model.r", sep=""))
  
      } # if (use_ols_procedure)

      if((exists("use_stepAIC") && use_stepAIC) || (exists("use_mixture_model") && use_mixture_model)){
        print("Make stepAIC tests")
        source(paste(script_dir,"/all_agents/regression_analyses/aux/stepAIC_test.r", sep=""))
      }
      if(exists("table_for_tex")){
        save(table_for_tex, file=paste(plot_dir_temp, targ,"_in_",p,"_contr_in_",p2,"_table_for_tex_data.RData",sep=""))
        rm(table_for_tex)
      }
    } # p2 in periods control
  }# end p in periods_temp_target
}# end targ in target_vars_temp
save(table_for_stepaic, file=file_temp)
rm(table_for_stepaic, file_temp, exp_temp, file_name_tex)
rm(p, p2, targ, b_, b0, barr_sq, c_, c0, chi_sq, macro_)
rm(list = ls(pattern="_sq"))

