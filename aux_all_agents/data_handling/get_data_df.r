# Get data in data frame for ggplot
print("Get data for ggplot")
if(exists("day_market_entry")==FALSE){
  day_market_entry<-600
}
if(exists("ts_data")== FALSE || ts_data == 0){
  name<-"levels"
}else{
  name<-ts_names[ts]
}


add_cols<-3

if(is.element(1, cases_temp)){
#if(exists(paste("list_of_var_", agent,sep="")) == FALSE){
  #Find out which data is available. 
  eval(call("<-",(paste("list_of_var",sep="")),as.name(paste("list_of_var_",agent,sep=""))))
  if(exists("variables_selection") == FALSE ||is.element(NA, match(variables_selection, list_of_var)) ){
    source(paste(script_dir,"/aux_all_agents/variables_settings_for_agents.r",sep=""))
  }
#}
}
  parameter<-parameters_tmp[p]
  
  # Is script called immediately after bandpass? 
if(exists("ts_data")==TRUE && ts_data==1){
  variables_non_ts<-variables
  if(exists("variables_selection") == TRUE && is.element(NA, match(variables_selection, list_of_var))==FALSE){
    variables<-variables_selection
  }else{
    eval(call("<-",(paste("variables",sep="")),as.name(paste("list_of_var_",agent,sep=""))))
    #variables<-variables_for_bandpass
      }
  if(agent == "Firm" && is.element(NA, match(c("id", "active"), variables)) || 
     (agent!="Firm" && is.element(NA, match(c("id", "active"), variables))==FALSE) ){
    print(paste("ERROR: Agent is firm but id and active had not been added to list of var! 
                (in get data for ggplot). This is crucial here because this info is added to data.frame 
                or agent != Firm but id and active in list of var"))
    break
}
 v<-match(variables, list_of_var)
  v<-v[which(is.na(v)==FALSE)]
  data<-data_mFilter_ts[ts,,,,drop=FALSE]
}else if(is.element(1, cases_temp) == TRUE){
  
 print(paste("Get data ggplot for ", paste(variables_selection, collapse=" ")))
  
  load(file=paste(experiment_directory,"/rdata/raw_data/eco_id_matrix_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  data_eco_id<-object
  rm(object)
  
  
  if(agent == "Firm" && is.element(NA, match(c("id", "active"), variables))){
    print(paste("ERROR: Agent is firm but id and active had not been added to list of var! (in get data for ggplot)"))
    break
  }
  load(file=paste(experiment_directory,"/rdata/raw_data/data_all_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  data<-object
  rm(object)
  v<-match(variables, list_of_var)
  if(agent=="Firm" && is.element(NA,(match(c("active", "id"),variables)))){
    variables<-c("active","id", variables)
  }
  
  load(file=paste(experiment_directory,"/rdata/raw_data/eco_id_matrix_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  data_eco_id<-object
  #  data_eco_id<-eval(as.symbol(paste("eco_id_matrix_",experiment_name,"_",parameter,"_",agent, sep="")))
  rm(object)

  data<-data[,,,v, drop=FALSE]
  
  # Take logs but check that logs not yet taken... 

  if(agent == "Eurostat"){
    temp_test<-"monthly_output"
  }else if(agent=="Government"){
    temp_test<-"monthly_income"
    if(is.na(match(temp_test, list_of_var)) == TRUE){
      temp_test<-var_log[length(var_log)]
    }
  }else if(agent=="Firm"){
    temp_test<-"output"
  }else{
    take_logs<-0
  }
  if(exists("variables_selection")){
    if(is.element(NA, match(var_log, variables)) == TRUE){
      var_log<-var_log[is.na(match(var_log, variables)) == FALSE]
    }
  }

  if(no_agents==1){
    ag<-1
  }else{
    ag<-as.integer(no_agents/2)
  }
  if(take_logs == 1 && max(data[ag,,,match(temp_test, list_of_var)], na.rm = TRUE) > 1000/no_agents && exists("logs_taken")!=TRUE) {
    indices<-match(var_log, list_of_var)
    indices<-indices[which(is.na(indices) == FALSE)]
    data_temp<-data
    for(i in 1:no_agents){
      #foreach(r = 1:runs) %dopar% {
      for(r in 1:runs){
        for(t in 1:number_xml){
          for(v_temp in indices){
            if(is.infinite(data[i,t,r,v_temp])){
              data[i,t,r,v_temp] <- NA
            }
            if(is.na(data[i,t,r,v_temp])==FALSE){
              if(data[i,t,r,v_temp]>0){
                data_temp[i,t,r,v_temp]<-log(data[i,t,r,v_temp])
              }else{
                data_temp[i,t,r,v_temp]<-(-1)*log(abs(data[i,t,r,v_temp]))
              }
            }
          }
        }
      }
    }
    #data_temp[1:no_agents,,,indices]<-log(data[1:no_agents,,,indices])
    #data_temp[is.nan(data_temp)]<-(-1)*log(abs(data[1:no_agents, ,,indices]))
    data<-data_temp
    logs_taken<-1
    rm(data_temp)
  }
}else if(is.element(1, cases_temp) == FALSE){ # end NOT ts_data
## ggplot data per single run already exists 
    #(is.element(1,cases_temp) == FALSE)
    load(paste(experiment_directory,"/rdata/",kind_of_data,"/",name,"/",paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
    variables<-colnames(object)[which(is.element(colnames(object), c("Run","Periods", "Type", "Adopter_Type", "Firm_Type"))==FALSE)]
    temp_mat_single<-object
    rm(object)
} # end else if is.element(1, cases_temp == FALSE)

for(c in cases_temp){

  if(length(variables) != length(unique(variables))){
    paste("Error: list of variables became too long, double entries. How does this happen?? ")
    variables<-unique(variables)
  } 
  print(paste("Iteration c: ", c,sep=""))
  if(c==1){
#if(exists("parameter_script")!= TRUE || parameter_script==0){
#foreach(var = 1:length(v)){
    par_temp<-0.01*as.numeric(parameters_tmp[p])
    if(is.na(par_temp)){
      par_temp<-0
    }
for(var in 1:length(v)){
  if(agent == "Firm"){
    print(paste(var, list_of_var[var]))
  }
  data_single<-array(data[c(1:no_agents),1:number_xml, 1:runs,var], dim=c(no_agents, number_xml, runs, length(var)))

  rs<-runs
  length<-number_xml*no_agents
  each<-no_agents
  
  # data_mean(,,1) is number_xml times runs matrix of output
  if(var==1){
    temp_mat<-data.frame(matrix(0, rs*length,(add_cols+length(v))), stringsAsFactors = FALSE)
  }
  for(r in 1:rs){
    index<-(r-1)*length +1      
    temp_mat[index:(index+length-1),var+add_cols]<-(c(data_single[,,r,]))
    temp_mat[index:(index+length-1),3]<-r + par_temp

    if(var==1){
      temp_mat[index:(index+length-1),2]<-rep(seq(20,number_xml*20,20), each=each)
      eco<-data_eco_id[number_xml,r]
      if(eco < threshold_eco_scen){
        temp_mat[index:(index+length-1),1]<-"eco"
      }else{
        temp_mat[index:(index+length-1),1]<-"conv"
      }
    }
  }
  rm(data_single)
}
rm(data, data_eco_id)
colnames(temp_mat) <- c("Type", "Periods", "Run", list_of_var)

  if(exists("variables_orig") == FALSE){
    variables_orig<-variables
    variables<-variables_selection
    if(agent=="Firm" && is.element(NA, match(c("active", "id"), variables))){
      print("Check get_data: id and active not element of firm variables")
    }
    #if(agent=="Firm" && is.na(match("id",variables))){
    #  variables<-append("id",variables, after = length("id"))
    #}
    #if(agent=="Firm" && is.na(match("active",variables))){
    #  variables<-append("active",variables, after = length("active"))
    #}
  }
variables_temp<-variables

if(agent=="Eurostat" && is.na(match("price_per_productivity_unit_conv", colnames(temp_mat))) == TRUE && (exists("ts_data") == FALSE || ts_data==0)){
# Add modified variables: Price per productivity unit
temp<-cbind(temp_mat, (temp_mat$price_highest_vintage_conv/temp_mat$techn_frontier_conv))
variables<-append(variables_temp,"price_per_productivity_unit_conv", after = length(variables_temp))
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

if(length(variables) != length(unique(variables))){
    stop("Error: list of variables became too long, double entries. How does this happen?? ABORT")
} 

temp<-cbind(temp_mat, (temp_mat$price_highest_vintage_eco/temp_mat$techn_frontier_eco))
variables<-append(variables,"price_per_productivity_unit_eco")
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

if(is.element("total_capital_stock_units", variables) ==FALSE && is.element("total_capital_stock_units", list_of_var)){
  temp<-cbind(temp_mat, (temp_mat$total_capital_stock_units_eco+temp_mat$total_capital_stock_units_conv))
  variables<-append(variables,"total_capital_stock_units")
  colnames(temp)[length(temp)]<-variables[length(variables)]
  temp_mat<-temp
}

temp<-cbind(temp_mat, pmax(temp_mat$firm_average_productivity_conv,temp_mat$firm_average_productivity_eco))
variables<-append(variables,"firm_average_productivity")
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

temp<-cbind(temp_mat, (temp_mat$price_highest_vintage_eco-temp_mat$price_highest_vintage_conv))
variables<-append(variables,"frontier_price_difference")
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp


temp<-cbind(temp_mat, (temp_mat$price_per_productivity_unit_eco - temp_mat$price_per_productivity_unit_conv))
variables<-append(variables,"price_per_productivity_unit_difference")
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

ind_temp_wage<-match("firm_average_wage", colnames(temp_mat))
if(is.na(ind_temp_wage)){
  ind_temp_wage<-match("average_wage", colnames(temp_mat))
}

temp<-cbind(temp_mat, temp_mat$price_per_productivity_unit_difference/temp_mat[,ind_temp_wage])
variables<-append(variables,"price_per_productivity_unit_difference_wage_ratio")
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

temp<-cbind(temp_mat, temp_mat$price_per_productivity_unit_conv/temp_mat[,ind_temp_wage])
variables<-append(variables,"price_per_productivity_unit_conv_wage_ratio")
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

temp<-cbind(temp_mat, temp_mat$price_per_productivity_unit_eco/temp_mat[,ind_temp_wage])
variables<-append(variables,"price_per_productivity_unit_eco_wage_ratio")
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

temp<-cbind(temp_mat, temp_mat$price_eco/temp_mat[,ind_temp_wage])
variables<-append(variables,"eco_price_wage_ratio")
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

firm_average_productivity_progress<-temp_mat$firm_average_productivity
temp<-(firm_average_productivity_progress[2:length(firm_average_productivity_progress)] 
       - firm_average_productivity_progress[1:(length(firm_average_productivity_progress)-1)])
firm_average_productivity_progress[2:length(firm_average_productivity_progress)]<-temp/firm_average_productivity_progress[2:length(firm_average_productivity_progress)]
temp<-seq(1, length(firm_average_productivity_progress),number_xml)
firm_average_productivity_progress[temp]<-NA
temp<-cbind(temp_mat, firm_average_productivity_progress)
variables<-append(variables,"firm_average_productivity_progress")
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

temp<-((temp_mat$average_s_skill_conv)/(temp_mat$average_s_skill_eco))
temp<-cbind(temp_mat, temp)
variables<-append(variables,"s_skill_ratio", after = length(variables))
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

temp<-((temp_mat$techn_frontier_conv)/(temp_mat$techn_frontier_eco))
temp<-cbind(temp_mat, temp)
variables<-append(variables,"frontier_productivity_ratio", after = length(variables))
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

temp<-((temp_mat$techn_frontier_conv/temp_mat$average_s_skill_conv)/(temp_mat$techn_frontier_eco/temp_mat$average_s_skill_eco))
temp<-cbind(temp_mat, temp)
variables<-append(variables,"degree_of_novelty_ratio", after = length(variables))
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

temp<-((temp_mat$price_highest_vintage_conv/(temp_mat$price_highest_vintage_eco)))
temp<-cbind(temp_mat, temp)
variables<-append(variables,"price_ratio_frontier", after = length(variables))
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

temp<-((temp_mat$price_highest_vintage_conv/temp_mat$techn_frontier_conv)/(temp_mat$price_highest_vintage_eco/temp_mat$techn_frontier_eco))
temp<-cbind(temp_mat, temp)
variables<-append(variables,"price_per_prod_unit_ratio_frontier", after = length(variables))
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

temp<-((temp_mat[,ind_temp_wage]/temp_mat$price_index))
temp<-cbind(temp_mat, temp)
variables<-append(variables,"real_wage", after = length(variables))
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp

temp<-((temp_mat$price_eco/temp_mat$real_wage))
temp<-cbind(temp_mat, temp)
variables<-append(variables,"eco_price_wage_ratio", after = length(variables))
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp


temp<-((temp_mat$price_highest_vintage_conv/temp_mat$techn_frontier_conv) - (temp_mat$price_highest_vintage_eco/temp_mat$techn_frontier_eco))
if(is.element("price_diff_highest_vintage", colnames(temp_mat))){
  temp_mat$price_diff_highest_vintage <- temp
}else{
  temp<-cbind(temp_mat, temp)
  variables<-append(variables,"price_diff_highest_vintage", after = length(variables))
  colnames(temp)[length(temp)]<-variables[length(variables)]
  temp_mat<-temp
}

temp<-((temp_mat$price_lowest_vintage_conv/temp_mat$lowest_productivity_conv) - (temp_mat$price_lowest_vintage_eco/temp_mat$lowest_productivity_eco))
if(is.element("price_diff_lowest_vintage", colnames(temp_mat))){
  temp_mat$price_diff_highest_vintage <- temp
}else{
  temp<-cbind(temp_mat, temp)
  variables<-append(variables,"price_diff_lowest_vintage", after = length(variables))
  colnames(temp)[length(temp)]<-variables[length(variables)]
  temp_mat<-temp
}

temp<-temp_mat$price_diff_highest_vintage/temp_mat[,ind_temp_wage]
if(is.element("price_diff_highest_vintage_wage_ratio", colnames(temp_mat))){
  temp_mat$price_diff_highest_vintage_wage_ratio <- temp
}else{
  temp<-cbind(temp_mat, temp)
  variables<-append(variables,"price_diff_highest_vintage_wage_ratio", after = length(variables))
  colnames(temp)[length(temp)]<-variables[length(variables)]
  temp_mat<-temp
}

temp<-temp_mat$price_diff_lowest_vintage/temp_mat[,ind_temp_wage]
if(is.element("price_diff_lowest_vintage_wage_ratio", colnames(temp_mat))){
  temp_mat$price_diff_lowest_vintage_wage_ratio <- temp
}else{
  temp<-cbind(temp_mat, temp)
  variables<-append(variables,"price_diff_lowest_vintage_wage_ratio", after = length(variables))
  colnames(temp)[length(temp)]<-variables[length(variables)]
  temp_mat<-temp
}




if(is.element("eco_tax_rate", colnames(temp_mat))){
  temp_mat$proxy_using_costs_conv_techn<-(temp_mat[,ind_temp_wage]+temp_mat$price_eco*(1+temp_mat$eco_tax_rate))/min(temp_mat$average_s_skill_conv, temp_mat$techn_frontier_conv)
}else{
  temp_mat$proxy_using_costs_conv_techn<-(temp_mat[,ind_temp_wage]+temp_mat$price_eco)/min(temp_mat$average_s_skill_conv, temp_mat$techn_frontier_conv)
}
temp_mat$proxy_using_costs_eco_techn<-(temp_mat[,ind_temp_wage])/min(temp_mat$average_s_skill_eco, temp_mat$techn_frontier_eco)
temp_mat$using_cost_difference_eco_conv_capital<-(temp_mat$proxy_using_costs_conv_techn - temp_mat$proxy_using_costs_eco_techn)/temp_mat$proxy_using_costs_conv_techn

temp<-((temp_mat$proxy_using_costs_conv_techn/temp_mat$proxy_using_costs_eco_techn))
temp<-cbind(temp_mat, temp)
variables<-append(variables,"proxy_using_cost_ratio", after = length(variables))
colnames(temp)[length(temp)]<-variables[length(variables)]
temp_mat<-temp




}
else if(agent=="Firm"  && (exists("ts_data") == FALSE || ts_data==0)){
  
  temp_mat[which(is.na(temp_mat$active)),match("active", colnames(temp_mat))]<-0
  
  if(is.element("mean_spec_skills_conv", colnames(temp_mat))){
  # Add modified variables: Price per productivity unit
  temp<-cbind(temp_mat, (2*(temp_mat$mean_spec_skills_conv-temp_mat$mean_spec_skills_eco)/(temp_mat$mean_spec_skills_conv+temp_mat$mean_spec_skills_eco)))
  variables<-append(variables_temp,"mean_specific_skill_gap", after = length(variables_temp))
  colnames(temp)[length(temp)]<-variables[length(variables)]
  temp_mat<-temp
  }
  if(is.element("technology_conv", colnames(temp_mat))){
  temp<-cbind(temp_mat, (2*(temp_mat$technology_conv-temp_mat$technology_eco)/(temp_mat$technology_conv+temp_mat$technology_eco)))
  variables<-append(variables,"technology_gap", after = length(variables))
  colnames(temp)[length(temp)]<-variables[length(variables)]
  temp_mat<-temp
  }
  if(is.element("Firm_Type", colnames(temp_mat))!=TRUE){
    print(paste("Add Firm_Type"))
    temp2<-as.data.frame(cbind(share_conventional_vintages_used=temp_mat$share_conventional_vintages_used, id=temp_mat$id, Run=temp_mat$Run, Periods=temp_mat$Periods, active=temp_mat$active))
    temp2<-temp2[order(temp2$id,temp2$Run),]
    temp3<-temp2$share_conventional_vintages_used
    firm_temp<-c(1:number_xml)
    idx_temp<-firm_temp
    its_temp<-no_agents*runs
    half_temp<-as.integer(number_xml/2)
  for(i in 1:its_temp){
    if(is.na(temp2$share_conventional_vintages_used[idx_temp[number_xml]]) == FALSE &&is.na(temp2$share_conventional_vintages_used[idx_temp[half_temp]]) == FALSE){
    if(temp2$share_conventional_vintages_used[idx_temp[number_xml]] > 0.5 ){
      if(temp2$share_conventional_vintages_used[idx_temp[half_temp]] > 0.5 ){
        temp3[idx_temp]<-"conv"
      }else{
        temp3[idx_temp]<-"switch-e2c"
      }
    }else{
      if(temp2$share_conventional_vintages_used[idx_temp[half_temp]] < 0.5 ){
        temp3[idx_temp]<-"eco"
      }else{
        temp3[idx_temp]<-"switch-c2e"
      }
    }
    }else if(is.na(temp2$share_conventional_vintages_used[idx_temp[half_temp]]) == FALSE){
      if(temp2$share_conventional_vintages_used[idx_temp[half_temp]] > 0.5 ){
        temp3[idx_temp]<-"conv"
      }else{
        temp3[idx_temp]<-"eco"
      }
    }else{
      temp3[idx_temp]<-NA
    }
    if(length(unique(temp2$id[idx_temp]))!= 1){
      print(paste("WARNING: ERROR in iteration", i, idx_temp[1], idx_temp[half_temp], idx_temp[number_xml]))
    }
    idx_temp<-idx_temp+number_xml
  }
  

  temp2$share_conventional_vintages_used<-temp3
  rm(temp3)
  temp<-temp2[order(temp2$Run, temp2$Periods, temp2$active),]
  rm(temp2)
  temp<-cbind(temp$share_conventional_vintages_used, temp_mat)
  variables<-append("Firm_Type", variables)
  colnames(temp)[1]<-variables[1]
  temp_mat<-temp
  rm(temp)
  
  }
  if(is.element("Adopter_Type", colnames(temp_mat))!=TRUE){
    
  print(paste("Add Adopter Type"))
  temp2<-as.data.frame(cbind(share_conventional_vintages_used=temp_mat$share_conventional_vintages_used, id=temp_mat$id, Run=temp_mat$Run, Periods=temp_mat$Periods, active=temp_mat$active))
  temp2<-temp2[order(temp2$id,temp2$Run),]
  temp3<-temp2$share_conventional_vintages_used
  firm_temp<-c(1:number_xml)
  idx_temp<-firm_temp
  its_temp<-no_agents*runs
  early_temp<-1800

  for(i in 1:its_temp){
    if(is.na(temp2$share_conventional_vintages_used[idx_temp[number_xml]]) == FALSE && is.na(temp2$share_conventional_vintages_used[idx_temp[early_temp/20]]) == FALSE){
      if(temp2$share_conventional_vintages_used[idx_temp[early_temp/20]] < 0.5 ){
        temp3[idx_temp]<-"early_adopter"
      }else{
        temp3[idx_temp]<-"laggard"
      }
    }else{
      temp3[idx_temp]<-NA
    }
    if(length(unique(temp2$id[idx_temp]))!= 1){
      print(paste("WARNING: ERROR in iteration", i, idx_temp[1], idx_temp[early_temp/20], idx_temp[number_xml]))
    }
    idx_temp<-idx_temp+number_xml
  }
 
  temp2$share_conventional_vintages_used<-temp3
  rm(temp3)
  temp<-temp2[order(temp2$Run, temp2$Periods, temp2$active),]
  rm(temp2)
  temp<-cbind(temp$share_conventional_vintages_used, temp_mat)
  variables<-append("Adopter_Type", variables)
  colnames(temp)[1]<-variables[1]
  temp_mat<-temp
  
  rm(temp, early_temp, idx_temp, firm_temp)
  }
}else if(agent=="Government" && is.element("monthly_eco_subsidy_payment", list_of_var)&&  is.na(match("monthly_eco_subsidy_payment", colnames(temp_mat))) == TRUE && (exists("ts_data") == FALSE || ts_data==0)){
    # Add modified variables: Price per productivity unit
    temp<-cbind(temp_mat, ((temp_mat$monthly_eco_tax_revenue - temp_mat$monthly_eco_subsidy_payment)/(temp_mat$monthly_eco_tax_revenue + temp_mat$monthly_eco_subsidy_payment)))
    variables<-append(variables_temp,"monthly_eco_budget_balance_pct", after = length(variables_temp))
    colnames(temp)[length(temp)]<-variables[length(variables)]
    temp_mat<-temp
    
    temp<-cbind(temp_mat, ((temp_mat$monthly_eco_tax_revenue - temp_mat$monthly_eco_subsidy_payment)))
    variables<-append(variables_temp,"monthly_eco_budget_balance", after = length(variables_temp))
    colnames(temp)[length(temp)]<-variables[length(variables)]
    temp_mat<-temp
}# end add agent wise special variables
 if(length(variables) != length(unique(variables))){
   paste("Error: list of variables became too long, double entries. How does this happen?? ")
   variables<-unique(variables)
 } 

temp_mat_single_large <- temp_mat
rm(temp_mat)


if(length(variables) != length(unique(variables))){
  paste("Error: list of variables became too long, double entries. How does this happen?? ")
  variables<-unique(variables)
}
# Convert temp_mat_single into panel data. 
# 1: Unique identifier: id-run
if(agent == "Firm"){
  id_run<-as.numeric(temp_mat_single_large$id)+0.001*as.numeric(temp_mat_single_large$Run)
  #temp_mat_single_large<-add_column(temp_mat_single_large, id_run, .before = "id")
  temp_mat_single_large$id<-id_run
  data_single_large<-pdata.frame(temp_mat_single_large, index=c("id", "Periods", "Run"), drop.index = FALSE, row.names = TRUE)
}else{
  data_single_large<-pdata.frame(temp_mat_single_large, index=c("Run", "Periods"), drop.index = FALSE, row.names = TRUE)
}

eval(call("<-",(paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep="")),data_single_large))
rm(data_single_large)


if(dir.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/",sep=""))!=1){
  dir.create(paste(experiment_directory,"/rdata/",kind_of_data,"/",sep=""))
}
if(dir.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/",name,"/",sep=""))!=1){
  dir.create(paste(experiment_directory,"/rdata/",kind_of_data,"/",name,"/",sep=""))
}

object<-eval(as.name(paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep="")))
rm(list=ls(pattern="data_single_large_"))
save(object, file=paste(experiment_directory,"/rdata/",kind_of_data,"/",name,"/",paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
rm(object, temp)

if(agent == "Firm"){
  temp_mat_single <- temp_mat_single_large[which(temp_mat_single_large$active == 1),]
}else{
  temp_mat_single<-temp_mat_single_large
}
rm(temp_mat_single_large)

}else if(c==3){ # c is case with batch for eco and conv scen. 
  if(length(variables) != length(unique(variables))){
    paste("Error: list of variables became too long, double entries. How does this happen?? ")
    variables<-unique(variables)
  } 
  
  
  types_temp<-c("eco", "conv")
  variables_temp<-variables[which(is.element(variables, c("id", "active")) == FALSE)]
  type<-types_temp[1]
  var<-variables_temp[1]
  all_periods_temp<-temp_mat_single$Periods
  all_runs_temp<-temp_mat_single$Run
  its_temp<-unique(all_periods_temp)
  
  for(type in types_temp){
    t_temp<-match(type, types_temp)
    inds_type_temp<-which(temp_mat_single$Type == type)
    periods_temp<-all_periods_temp[inds_type_temp]
    runs_temp<-all_runs_temp[inds_type_temp]
    temp<-temp_mat_single[inds_type_temp,]
    i<-1
    for(i in 1:length(its_temp)){
      it<-i + (t_temp-1)*number_xml
      inds_temp<-which(periods_temp == its_temp[i])
      #foreach(v = start:length(temp_mat)){
      for(var in variables_temp){
        if(i==1 && t_temp == 1 && var==variables_temp[1]){
          temp2<-data.frame(matrix(0, 2*number_xml,(3+length(variables_temp))))
          colnames(temp2) <- c("Type", "Periods", "Number_Runs",  variables_temp)
        }
        data_temp<-as.numeric(temp[inds_temp,match(var, colnames(temp))])
        data_temp<-data_temp[which(!is.na(data_temp))]
        data_temp<-data_temp[which(!is.infinite(data_temp))]
        
       
        if(length(data_temp)>0){
          if(is.element(NaN,data_temp)){stop("NaN check")}
          v1<-match(var, colnames(temp2))
          if(var==variables_temp[1] && i==1){
            temp2$Type[(1+(t_temp-1)*number_xml):(t_temp*number_xml)]<-type
            temp2$Periods[(1+(t_temp-1)*number_xml):(t_temp*number_xml)]<-seq(20,20*number_xml, 20)
            temp2$Number_Runs[(1+(t_temp-1)*number_xml):(t_temp*number_xml)]<-length(unique(runs_temp))
          }
          m<-mean(data_temp,na.rm=TRUE)
          if(is.nan(m)){stop("Why NaN??")}
        }else{m<-NA} # Only NA may occur in first periods if eco-techn not yet available. 
        temp2[it,v1]<-m
      } # var in variables_temp

      } # i in its_temp
    } # type in types_temp

  temp_mat <- temp2
  rm(temp, temp2, i, type, it_vector, start,m)
  
  temp_mat_eco_conv_batch <- data.frame(temp_mat)
  rm(temp_mat)
  
  # Convert temp_mat_single into panel data. 
  data_eco_conv_batch<-pdata.frame(temp_mat_eco_conv_batch, index=c("Type","Periods"), drop.index = FALSE, row.names = TRUE)
  
  eval(call("<-",(paste("data_eco_conv_batch_",experiment_name,"_",parameter,"_",agent,sep="")),data_eco_conv_batch))
  
  object<-eval(as.name(paste("data_eco_conv_batch_",experiment_name,"_",parameter,"_",agent,sep="")))
  save(object, file=paste(experiment_directory,"/rdata/",kind_of_data,"/",name,"/",paste("data_eco_conv_batch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
  
  rm(list=ls(pattern="data_eco_conv_"))
  rm(object)
} # if c== 3
  
  
  if(length(variables) != length(unique(variables))){
    paste("Error: list of variables became too long, double entries. How does this happen?? ")
    variables<-unique(variables)
  } 

if(c==2){
  
  if(agent=="Firm"){
    variables<-variables[which(variables != "id")]
    variables<-variables[which(variables != "active")]
  }
  if(length(variables) != length(unique(variables))){
    paste("Error: list of variables became too long, double entries. How does this happen?? ")
    variables<-unique(variables)
    break
  } 
    temp_mat<-array(NA,dim=c(number_xml, add_cols+length(variables)))
    v<-match(variables, colnames(temp_mat_single))
    # after having removed inactive firms.
    its<-seq(20,20*number_xml,20) 
    for(it in 1:number_xml){
      subset<-temp_mat_single[which(temp_mat_single$Periods == its[it]),]
      subset2<-subset[,v]
      #foreach(v = c((add_cols+1):(length(variables)+add_cols))){
      for(var in 1:length(v)){
        temp_mat[it,(add_cols+var)]<-mean(subset2[,var], na.rm=TRUE)
      }
    }
  temp_mat_batch<-data.frame(temp_mat)
  rm(temp_mat)
  
  colnames(temp_mat_batch)<-c("Type", "Periods", "Run", variables)
  temp_mat_batch$Periods<-seq(20,20*number_xml,20)
  temp_mat_batch$Run<-NA
  colnames(temp_mat_batch)<-c("Type", "Periods", "Run", variables)
  
  rm(its, it,v, subset, subset2)
  
  # Convert temp_mat_single into panel data. 
  # 1: Unique identifier: id-run
  if(agent == "Firm"){
    temp_mat_batch$id<-1
    data_batch<-pdata.frame(temp_mat_batch, index=c("id","Periods"), drop.index = FALSE, row.names = TRUE)
  }else{
    temp_mat_batch<-cbind(1,temp_mat_batch)
    colnames(temp_mat_batch)[1]<-"Run"
    data_batch<-pdata.frame(temp_mat_batch, index=c("Run","Periods"), drop.index = FALSE, row.names = TRUE)
    colnames(data_batch)[1]<-"Run"
  }

  remove<-match(c("Run", "Type"), colnames(temp_mat_batch))
  data_batch<-data_batch[,c(-remove)]
  data_batch<-data_batch[,which(colnames(data_batch)!="active")]
  data_batch<-data_batch[,which(colnames(data_batch)!="Run")]
  data_batch<-data_batch[,which(colnames(data_batch)!="Type")]
  rm(temp_mat_batch)
  
  eval(call("<-",(paste("data_batch_",experiment_name,"_",parameter,"_",agent,sep="")),data_batch))

  object<-eval(as.name(paste("data_batch_",experiment_name,"_",parameter,"_",agent,sep="")))
  save(object, file=paste(experiment_directory,"/rdata/",kind_of_data,"/",name,"/",paste("data_batch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
  
  rm(object, data_batch)
  rm(list=ls(pattern="data_batch_"))
  

}

} # c in cases_temp (single, batch, eco_conv_batch)

  if(agent=="Firm"){
    variables<-variables[which(variables != "id")]
    variables<-variables[which(variables != "active")]
  }


#rm(list=ls(pattern="temp"))
rm(add_cols, c, eco_share, eco, indices, it, index, length)

# Now, plm methods can be used. Data entries have id,period,run identifiers. 

