# To dos: 
# 
# Load data (single large)
# 
if(agent == "Firm" || agent == "Eurostat"){
  # add variables if needed. 
  # 
  data_directory<-paste(experiment_directory,"/rdata/",kind_of_data,"/levels/",sep="")
  if(file.exists(paste(data_directory,"data_single_switch_",experiment_name,"_",parameters_tmp[1],"_",agent,".RData",sep=""))){
    
    for(pr in parameters_tmp){
      if(file.exists(paste(data_directory,"data_single_large_",experiment_name,"_",pr,"_",agent,".RData",sep=""))){
        load(paste(data_directory,"data_large_",experiment_name,"_",pr,"_",agent,".RData",sep=""))
        cols<-match(c("Run","Periods","id","Type","variance_share","share_conventional_vintages_used","share_conv_capital_used"),colnames(object))
        cols<-cols[which(!is.na(cols))]
        x<-cbind(Parameter = rep(pr,nrow(object)),object[cols])
        if(pr==parameters_tmp[1]){
          data_temp<-x
        }else{data_temp<-rbind(data_temp,x)}
        rm(cols, x, object)
      }else{# if file.exists
        print("Data of interest does not exists")
      }
    }# end parameter loop
    rm(pr)
# 
# Compute aggr. variance at end of simulation 
# (for firms per run-id, for Eurostat per run)
# for firms: Think about solution for start-ups (i.e. "recycled ids")
# 
    s<-match(c("share_conv_capital_used","share_conventional_vintages_used"),colnames(data_temp))
    s<-s[which(!is.na(s))]

    for(pr in parameters_tmp){
      inds_par<-which(data_temp$Parameter == pr)
      temp<-data_temp[inds_par,]
      if(agent=="Firm"){its<-round(as.numeric(temp$Run)) + 0.001*round(as.numeric(temp$id))
      }else{its<-round(as.numeric(temp$Run))}
      its_unique<-unique(its)
      share<-100*temp[,s]
      empty<-rep(NA,length(its_unique))
      for(i in 1:length(its_unique)){
        if(i %% 1000 == 0){print(i)}
        it<-its_unique[i]
        inds_it<-which(its == it)
        share_it<-share[inds_it]
        empty[i]<-var(share_it,na.rm=T) # Here, I do not care about firms that are temporarily not 
        # active, resp. "recycled" ids after bankrupcy. 
      }
      cols<-match(c("Parameter","Run","Type","id"),colnames(temp))
      cols<-cols[which(!is.na(cols))]
      temp1<-cbind(temp[which(temp$Periods == temp$Periods[nrow(temp)]),cols],variance_share_full=empty)
      if(pr == parameters_tmp[1]){temp2<-temp1
      }else{temp2<-rbind(temp2,temp1)}
      rm(temp1, empty, cols)
    }# end pr in parameters_tmp
# 
# Analysis: 
# 
# - Descriptive comparison across different types fix parameter and across parameters for batch and each scenario type
# - In table: Mean, Std. across runs, wilcox. 
# -- For firms: 
# ----avg_var_share = mean(mean_i(variance_share_ir,na.rm=T)) 
#     -> Compute first mean within each run, then mean across runs to control for different number of active firms
# -----Standard deviation: sd_var_share = sd(mean_i(variance_share_ir,na.rm=T)) 
#       AND sd_var_share_across_firms = mean(sd_i(variance_share_i,r,na.rm=T))
#     -> Compute deviation across runs and avg. deviation within runs
    for(pr in parameters_tmp){
      inds_p<-which(temp2$Parameter == pr)
      temp1<-temp2[inds_p,]
      s<-match(c("variance_share_full"),colnames(temp1))
      s<-s[which(!is.na(s))]
      types_temp<-c("aggr",unique(temp1$Type))
      for(t in types_temp){
        if(t == aggr){
          
        }
      }
    }
# - Wilcoxon test parameter pairs using variance across whole time horizon    

# 
  }else{print("data does not exist")} # if data exists
}# agent == Eurostat || Firm