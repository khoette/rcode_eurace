# Data given: temp_mat_batch, temp_mat_single, temp_mat_eco_conv_scen -> all saved as 
# pdata.frames in data folder. 
#
eval(call("<-",(paste("list_of_var",sep="")),as.name(paste("list_of_var_",agent,sep=""))))
#
eval(call("<-",(paste("variables_available_in_panel",sep="")),as.name(paste("variables_available_in_panel_",agent,sep=""))))

#
if(is.element(NA, match(variables_selection, list_of_var)) == FALSE || exists("variables_selection") == FALSE){
  if(is.element(NA, match(available_variables_in_panel, list_of_var)) == FALSE){
    variables<-available_variables_in_panel
  }else{
    variables<-list_of_var
  }
}
if(is.element(TRUE,(is.element(c("id", "active"), variables)))){
  print(paste("Warning: id and/or active not removed from variables array in 
              get_data_normalized. They are removed here because they do 
              not make sense in the normalization", agent))
  variables<-variables[is.na(match(variables, c("id", "active")))]
}

if(exists("take_logs") == TRUE && take_logs==1){
  kind_of_data<-"logs"
}else{
  kind_of_data<-"levels"
}
# Load panel data frames in their "raw" version (i.e. not yet normalized or filtered, only logs evenually applied)
#for(p in 1:length(parameters_tmp)){
  #parameter<-parameters_tmp[p]
  load(file=paste(experiment_directory,"/rdata/",kind_of_data,"/levels/",paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
  data_single_large<-object
  load(file=paste(experiment_directory,"/rdata/",kind_of_data,"/levels/",paste("data_batch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
  data_batch<-object
  load(file=paste(experiment_directory,"/rdata/",kind_of_data,"/levels/",paste("data_eco_conv_batch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
  data_eco_conv_batch<-object
  rm(object)
  
  #if(agent == "Eurostat"){ # I do not use it at the moment: Growth in no of firms could be interesting. 
   # variables_to_exclude_from_normalization<-c("no_conv_firms", "no_eco_firms", "price_per_productivity_unit_conv",  "price_per_productivity_unit_eco")
  #}

if(agent!="Firm"){
# 2: Normalize by initial average agent
# 
if(file.exists(file=paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_init_avg/data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep="")) == FALSE){ 
  print(paste("Normalize by initial average agent"))
  data<-data_single_large
  data_temp<-data
  runs<-max(floor(as.numeric(data$Run)))
  cols<-match(variables, colnames(data))
  for(r in 1:runs){
    print(paste("Run: ", r))
    #per<-its[it]
    subset<-data[which(floor(as.numeric(data$Run)) == r),]
    subset0<-subset[which(subset$Periods == 20),,drop=FALSE]
    for(v in cols){
      if(is.na(subset0[v])){
        temp<-subset[which(is.na(subset[,v])!=TRUE),v]
        if(length(temp)>0){
          subset0[v]<-temp[1]
        }else{
          subset0[v]<-0
        }
      }
      if(subset0[v] == 0){
        temp<-subset[which(subset[,v]!=0),v]
      if(length(temp)>0){
        subset0[v]<-temp[1]
      }else{
        subset0[v]<-NA
      }
    }
    }
    data<-data[which(floor(as.numeric(data$Run)) != r),]
    
  #indx <- which(d$q!=0)

  for(v in cols){
  # Divisor (average agent in t0)
  if(no_agents>1){
    div<-mean(subset0[,v], na.rm=TRUE)
  }else{
    div<-subset0[,v]
  }
    if(is.na(div) == FALSE){
      if(div != 0){
        subset[,v]<-subset[,v]/div
      }else{
        subset[,v]<-NA
        print(paste("Error: Mean agent has value zero???? get_data_firm_normalize.",div, colnames(subset)[v], sep=" "))
      }
      }else{
        print(paste("Error: div is NA??? get_data_firm_normalize.",div,colnames(subset)[v], sep=" "))
      }
  }
  data_temp[which(floor(as.numeric(data_temp$Run)) == r),]<-subset
  }
  
  data_single_normalized_by_init_avg<-data_temp
  if(dir.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_init_avg/",sep=""))!=1){
    dir.create(paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_init_avg/",sep=""))
  }
  eval(call("<-",(paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep="")),data_temp))
  object<-eval(as.name(paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep="")))
  save(object, file=paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_init_avg/",paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))

  cols<-match(variables,colnames(data_batch))
  data_batch_normalized_by_init_avg<-data_batch
  div<-matrix(as.numeric(data_batch[1,cols]), number_xml, length(cols), byrow=TRUE)
  data_batch_normalized_by_init_avg[cols]<-data_batch[cols]/div
  eval(call("<-",(paste("data_batch_",experiment_name,"_",parameter,"_",agent,sep="")),data_batch_normalized_by_init_avg))
  object<-eval(as.name(paste("data_batch_",experiment_name,"_",parameter,"_",agent,sep="")))
  save(object, file=paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_init_avg/",paste("data_batch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
  
  cols<-match(variables,colnames(data_eco_conv_batch))
  data_eco_conv_batch_normalized_by_init_avg<-data_eco_conv_batch
  div<-(matrix(as.numeric(data_eco_conv_batch[1,cols]), number_xml, length(cols), byrow=TRUE))
  data_eco_conv_batch_normalized_by_init_avg[1:number_xml,cols]<-data_eco_conv_batch[1:number_xml,cols]*(1/div)
  div<-matrix(as.numeric(data_eco_conv_batch[1,cols]), number_xml, length(cols), byrow=TRUE)
  data_eco_conv_batch_normalized_by_init_avg[(number_xml+1):(2*number_xml),cols]<-data_eco_conv_batch[1:number_xml,cols]/div

  eval(call("<-",(paste("data_eco_conv_batch_",experiment_name,"_",parameter,"_",agent,sep="")),data_eco_conv_batch_normalized_by_init_avg))
  object<-eval(as.name(paste("data_eco_conv_batch_",experiment_name,"_",parameter,"_",agent,sep="")))
  save(object, file=paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_init_avg/",paste("data_eco_conv_batch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
  
  rm(data, subset, subset0, object)
  rm(div)
}else{ # if data file.exists
  print(paste("Normalized_by_init_avg data already existing for ",agent, parameter))
  print(paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_init_avg/data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
}
}
# 3: Compute growth rates (annual)
# Use panel data version. 
if(file.exists(file=paste(experiment_directory,"/rdata/",kind_of_data,"/ann_growth_rates/data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep="")) == FALSE){ 
  for(c in 1:3){
    print(paste("Compute growth rates (annual)", c))
  
    if(c==1){
      data<-data_single_large
    }else if(c==2){
      data<-data_batch
    }else if(c==3){
      data<-data_eco_conv_batch
    }
  cols<-match(variables, colnames(data))

  for(col in cols){
    dif<-data[,col]
    dif<-diff(dif,12)
    data[,col]<-dif/data[,col]
  }
  if(dir.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/ann_growth_rates/",sep=""))!=1){
    dir.create(paste(experiment_directory,"/rdata/",kind_of_data,"/ann_growth_rates/",sep=""))
  }

  if(c==1){
    #data_single_large_ann_growth_rates<-data
    eval(call("<-",(paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep="")),data))
    object<-eval(as.name(paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep="")))
    save(object, file=paste(experiment_directory,"/rdata/",kind_of_data,"/ann_growth_rates/",paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
  }else if(c==2){
    #data_batch_ann_growth_rates<-data
    eval(call("<-",(paste("data_batch_",experiment_name,"_",parameter,"_",agent,sep="")),data))
    object<-eval(as.name(paste("data_batch_",experiment_name,"_",parameter,"_",agent,sep="")))
    save(object, file=paste(experiment_directory,"/rdata/",kind_of_data,"/ann_growth_rates/",paste("data_batch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
  }else if(c==3){
    #data_eco_conv_batch_ann_growth_rates<-data
    eval(call("<-",(paste("data_eco_conv_batch_",experiment_name,"_",parameter,"_",agent,sep="")),data))
    object<-eval(as.name(paste("data_eco_conv_batch_",experiment_name,"_",parameter,"_",agent,sep="")))
    save(object, file=paste(experiment_directory,"/rdata/",kind_of_data,"/ann_growth_rates/",paste("data_eco_conv_batch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
  }
}
rm(col, dif, data, object, cols)
}else{ # if data file.exists
  print(paste("Ann_growth_rates data already existing for ",agent, parameter))
  print(paste(experiment_directory,"/rdata/",kind_of_data,"/ann_growth_rates/data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
}

if(1==2){  
# This normalization only makes sense if more than 1 agent. 
  if(no_agents>1){
    if(exists("normalize_avg_agent_all_periods") && normalize_avg_agent_all_periods ==1){  
    
      print(paste("Normalize by average agent in t for all periods"))
      flag_temp <- 1
        
      if(file.exists(file=paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_per-run_avg/data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep="")) == FALSE){ 

      # 1: Normalize by average agent in t.  This normalization by period run average only makes sense 
      # if more than one agent! 
      data<-data_single_large#[which(temp_mat_single_large$active == 1),]
      runs<-max(as.numeric(data$Run))
      data_temp<-data

      cols<-match(variables, colnames(data))
      
      its<-seq(20, 20*number_xml,20)
      for(it in 1:number_xml){
        if(it %% 10){
          print(paste("Period: ", it, length(data[,1])))
        }
        per<-its[it]
        subset0<-data[which(data$Periods == per),]
        subset0_temp<-subset0
        for(r in 1:runs){
          if(r %% 10 == 0){
            print(paste("Run: ",r, length(subset0_temp[,1])))
          }
          subset<-subset0_temp[which(floor(as.numeric(subset0_temp$Run)) == r),] # take floor because run accounts for 
          # parameter identity in decimal place
          subset0_temp<-subset0_temp[which(floor(as.numeric(subset0_temp$Run)) != r),]
          for(v in cols)
          {
            # Divisor (mean agent or initial value) -> better: period-run average agent! 
            div<-mean(subset[,v], na.rm=TRUE)
            if(div==0){
              print(paste("Error: Mean agent has value zero???? get_data_firm_normalize", v, colnames(subset)[v], r))
            }
            if(div!=0){
              subset[,v]<-subset[,v]/div
            }else{
              subset[,v]<-NA
            }
          }
          subset0[which(floor(as.numeric(subset0$Run)) == r),]<-subset
        }
        data_temp[which(data_temp$Period == per),]<-subset0
        data<-data[which(data$Periods != per),]
        
      }
      rm(subset, subset0)
      
      data_single_normalized<-data_temp
      if(dir.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_per-run_avg/",sep=""))!=1){
        dir.create(paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_per-run_avg/",sep=""))
      }
      eval(call("<-",(paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep="")),data_temp))
      object<-eval(as.name(paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep="")))
      save(object, file=paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_per-run_avg/",paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
    }else{
      print(paste("Normalized by per-run avg data already existing for ",agent, parameter))
      print(paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_per-run_avg/data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
    } # else data file.exists
    }else{ # normalize only for selected periods
      source(paste(script_dir,"/aux_all_agents/normalize_by_per-run_avg_selected_snapshots.r",sep=""))
    }# else of normalize for all periods
  }# if no_agents > 1
    
} 
  
rm(list=ls(pattern="temp"))


