if(agent == "Firm" || agent == "Eurostat"){
# add variables if needed. 
# stop()
  revise_some_variables<-T
  add_sd_share_only<-F
  for(parameter in parameters_tmp){
    data_directory<-paste(experiment_directory,"/rdata/",kind_of_data,"/levels/",sep="")
    if(file.exists(paste(data_directory,"data_single_switch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))){
      load(paste(data_directory,"data_batch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
      if(!is.element("variance_share", colnames(object)) || !is.element("sd_share",colnames(object))){
        add_var<-1
      }else{
        add_var<-0
      }
      rm(object)
    }
    if(exists("revise_some_variables")&& revise_some_variables){
      add_var<-1
    }
    #stop()
    
    if(add_var){
  
      vars_added<-c()
  
      if(file.exists(paste(data_directory,"data_single_switch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))){
        data_types_temp<-c("large", "switch")
      }else{
        data_types_temp<-"large"
      }
  
    load(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
    if(is.element("variance_share", colnames(object))){
      if(!is.element("sd_share",colnames(object)) && add_sd_share_only){
        variance_share<-object$variance_share
        sd_share<-sqrt(variance_share)
        object<-cbind(object, sd_share = sd_share)
        vars_added<-c("variance_share","sd_share")
      }else{
        add_sd_share_only<-F
        cols<-c()
        for(c1 in colnames(object)){
          if(!grepl("variance_share",c1)){cols<-c(cols,c1)}
        }
        object<-object[match(cols, colnames(object))]
      }
    }else{
      add_sd_share_only<-F
    }
    temp<-object
    rm(object)
    
    if(add_sd_share_only == F){
      vars_added<-c()
      if(add_var){
        if(agent=="Firm"){
          temp<-temp[order(temp$Run, temp$id, temp$Periods),]
          its<-round(as.numeric(as.character(temp$id))) + 0.001*round(as.numeric(as.character(temp$Run)))
          share<-temp$share_conventional_vintages_used
        }else if(agent == "Eurostat"){
          temp<-temp[order(temp$Run, temp$Periods),]
          its<-round(as.numeric(as.character(temp$Run)))
          share<-temp$share_conv_capital_used
        }
      
        share<-as.numeric(share)*100
        months_lag<-30 # corresponds to 1:30 periods, i.e. 2.5 years
        periods<-length(unique(as.numeric(as.character(temp$Periods))))
        its_unique<-unique(its)
        active<-as.numeric(temp$active)
        variance_share<-rep(NA, length(share))
        for(t in c(1:length(its_unique))){
          it<-its_unique[t]
          if(agent == "Firm" && t%%100 == 0){ print(paste("compute variance share: ",t))}
          inds_its<-which(its == it)
          share[inds_its[c(1:(months_lag-1))]]<-NA
          for(i in inds_its[c((months_lag):periods)]){
            if(agent == "Eurostat" || (agent == "Firm" && sum(active[c((i-(months_lag-1)):i)],na.rm=T)> 12)){
              variance_share[i]<-var(share[c((i-(months_lag-1)):i)],na.rm=T)
            }
          }
        } # for t in its_unique
  
        temp0<-cbind(temp, variance_share)
        rm(its, its_unique, it, share, variance_share, active, t, months_lag)
        temp<-temp0
        rm(temp0)
        vars_added<-c(vars_added, "variance_share")
        if(length(vars_added)!=length(unique(vars_added))){stop(paste("some doubling occurred",paste(vars_added,collapse=" ")))}
      }
      # Add standard deviation
      if(!is.element("sd_share",colnames(temp))){
        sd_share<-sqrt(temp$variance_share)
        temp<-cbind(temp, sd_share=sd_share)
        vars_added<-c(vars_added, "sd_share")
      }
    }# if add sd share only == F
    
    temp_mat<-temp
    if(!is.element("sd_share",colnames(temp))||!is.element("variance_share",colnames(temp))){stop("You wanted to add variance and sd share....")}
    rm(temp)
    if(agent=="Eurostat"){

      temp<-((temp_mat$price_highest_vintage_conv/temp_mat$techn_frontier_conv) - (temp_mat$price_highest_vintage_eco/temp_mat$techn_frontier_eco))
      if(is.element("price_diff_highest_vintage", colnames(temp_mat))){
        temp_mat$price_diff_highest_vintage <- temp
      }else{temp_mat<-cbind(temp_mat, price_diff_highest_vintage=temp)}
      
      temp<-((temp_mat$price_lowest_vintage_conv/temp_mat$lowest_productivity_conv) - (temp_mat$price_lowest_vintage_eco/temp_mat$lowest_productivity_eco))
      if(is.element("price_diff_lowest_vintage", colnames(temp_mat))){temp_mat$price_diff_lowest_vintage <- temp
      }else{temp_mat<-cbind(temp_mat, price_diff_lowest_vintage=temp)}
      
      temp<-temp_mat$price_diff_highest_vintage/temp_mat$average_wage
      if(is.element("price_diff_highest_vintage_wage_ratio", colnames(temp_mat))){temp_mat$price_diff_highest_vintage_wage_ratio <- temp
      }else{temp_mat<-cbind(temp_mat, price_diff_highest_vintage_wage_ratio=temp)}
    
      temp<-temp_mat$price_diff_lowest_vintage/temp_mat$average_wage
      if(is.element("price_diff_lowest_vintage_wage_ratio", colnames(temp_mat))){temp_mat$price_diff_lowest_vintage_wage_ratio <- temp
      }else{temp<-cbind(temp_mat, price_diff_lowest_vintage_wage_ratio=temp)}
    
      if(is.element("eco_tax_rate", colnames(temp_mat))){temp_mat$proxy_using_costs_conv_techn<-(temp_mat$average_wage+temp_mat$price_eco*(1+temp_mat$eco_tax_rate))/min(temp_mat$average_s_skill_conv, temp_mat$techn_frontier_conv)
      }else{temp_mat$proxy_using_costs_conv_techn<-(temp_mat$average_wage+temp_mat$price_eco)/min(temp_mat$average_s_skill_conv, temp_mat$techn_frontier_conv)}
      temp_mat$proxy_using_costs_eco_techn<-(temp_mat$average_wage)/min(temp_mat$average_s_skill_eco, temp_mat$techn_frontier_eco)
      temp_mat$using_cost_difference_eco_conv_capital<-(temp_mat$proxy_using_costs_conv_techn - temp_mat$proxy_using_costs_eco_techn)/temp_mat$proxy_using_costs_conv_techn
      
      vars_added<-c(vars_added,"price_diff_highest_vintage","price_diff_lowest_vintage",
                    "price_diff_highest_vintage_wage_ratio", "price_diff_lowest_vintage_wage_ratio",
                    "proxy_using_costs_eco_techn", "proxy_using_costs_conv_techn", 
                    "using_cost_difference_eco_conv_capital")
      if(length(vars_added) != length(unique(vars_added))){stop(paste("Some doubling occurred",paste(vars_added,sep=" ")))}
    
    } # end check agent -> and end of generating variables
  
    object<-temp_mat
    #if(length(which(grepl(".1",colnames(object))))>0){
    #  warning(paste("Most likely, there is a double entry. Do you want to remove it?",colnames(object)[which(grepl(".1",colnames(object)))]))
    #  
    #  if(length(which(grepl(".1",colnames(object))))){object<-object[-which(grepl(".1",colnames(object)))]}
    #}
    
    
    save(object, file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
    rm(object)
    
   # For new variables, batch, eco conv batch etc. have to be computed. 
    # 
  
    for(d_temp in data_types_temp){
      vars_added<-unique(c(vars_added, "sd_share"))
      
      print(d_temp)
      if(d_temp == "large"){
        cases_temp<-c("eco_conv_batch", "batch") # "eco_conv_switch"
        load(paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
        if(agent == "Firm"){object<-object[order(object$Run, object$id, object$Periods),]
        }else if(agent == "Eurostat"){object<-object[order(object$Run, object$Periods),]}
        if(!is.element("variance_share",colnames(object))){temp_mat<-cbind(object, variance_share = variance_share)
        }else{temp_mat<-object}
        if(!is.element("sd_share",colnames(object))){temp_mat<-cbind(object, sd_share = sd_share)
        }else{temp_mat<-object}
        variance_share<-temp_mat$variance_share
        sd_share<-temp_mat$sd_share
        
      }else{
        load(paste(data_directory,"data_single_",d_temp,"_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
        if(agent == "Firm"){object<-object[order(object$Run, object$id, object$Periods),]
        }else if(agent == "Eurostat"){object<-object[order(object$Run, object$Periods),]}
        if(!is.element("variance_share",colnames(object))){object<-cbind(object, variance_share = variance_share)}
        if(!is.element("sd_share",colnames(object))){object<-cbind(object, sd_share = sd_share)}
        cases_temp<-c("eco_conv_switch")
        temp_mat<-object
        vars_added<-unique(c(vars_added,"sd_share","variance_share"))
        rm(object)
      }
      
      cols<-match(c("Periods", "Run", "Type",vars_added),colnames(temp_mat))
      if(is.element(NA, cols)){
        print("Something wrong in adding columns! Ignore NA, but check this!!")
        cols<-cols[which(is.na(cols) != TRUE)]
      }
      
      
      #if(agent == "Firm"){
      #  temp_mat<-temp_mat[which(as.numeric(as.character(temp_mat$active))==1),]
      #}
      
      temp_mat<-as.data.frame(temp_mat[,cols])
      its_temp<-unique(temp_mat$Periods)
      
      v_cols<-match(vars_added, colnames(temp_mat))
      
      for(c in cases_temp){
        
        if(c=="eco_conv_switch" || c == "eco_conv_batch"){
          types_temp <- unique(temp_mat$Type)
        }else if(c == "batch"){
          types_temp <- 1
          
        }
  
        temp2<-data.frame(matrix(NA, length(types_temp)*number_xml, (2+length(vars_added))))
        colnames(temp2) <- c("Periods","Type",vars_added)
        
        for(typ in 1:length(types_temp)){
          type <- types_temp[typ]
          print(type)
          if(length(types_temp)>1){
            inds_type<-which(temp_mat$Type == type)
          }else{ inds_type<-c(1:nrow(temp_mat))}
          
          temp1 <-temp_mat[inds_type,]
          periods<-temp1$Periods
          periods_unique<-unique(periods[which(!is.na(periods))])
          
          for(p11 in 1:length(periods_unique)){
            if(p11 %% 250 == 0){print(p11)}
            p1<-periods_unique[p11]
            inds_p<-which(periods == p1)
            temp11<-temp1[inds_p, ] 
            temp22<-c(as.character(p1),as.character(type))
            for(v in 1:length(v_cols)){
              m<-mean(temp11[,v_cols[v]], na.rm=TRUE)
              if(is.nan(m)){m<-NA}
              temp22<-c(temp22,m)
            }
            p12<-(typ-1) * length(periods_unique) + p11
            temp2[p12,]<-temp22
          } # p1 in periods_unique
          rm(p1,p12,p11,temp22,temp11)
          
        } # type in types_temp
        rm(type,typ,types_temp)
        day_of_market_entry<-600
        load(file=paste(data_directory,"data_", c, "_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
        if(c != "batch"){object<-object[order(object$Type,as.numeric(as.character(object$Periods))),]
        temp2<-temp2[order(temp2$Type,as.numeric(as.character(temp2$Periods))),]
        if((sum((as.numeric(as.factor(temp2$Type))-as.numeric(as.factor(object$Type)))) != 0)
           ||(sum((as.numeric(as.factor(temp2$Periods))-as.numeric(as.factor(object$Periods)))) != 0)){
          stop("Some error in the order")}
        cols_to_add<-colnames(temp2)[which(is.na(match(colnames(temp2),c("Periods","Type"))))]
        
        }else{
          object<-object[order(as.numeric(as.character(object$Periods))),]
          temp2<-temp2[order(as.numeric(as.character(temp2$Periods))),]
          if(sum((as.numeric(as.factor(temp2$Periods))-as.numeric(as.factor(object$Periods)))) != 0){
            stop("Some error in the order")}
          cols_to_add<-colnames(temp2)[which(is.na(match(colnames(temp2),c("Periods"))))]
          if(c == "batch" && is.element("Type",cols_to_add)){
            cols_to_add<-cols_to_add[which(!grepl("Type",cols_to_add))]
          }
            
          }
        
        for(col in cols_to_add){
          if(is.element(col, colnames(object))){
            object[,match(col, colnames(object))]<-as.numeric(as.character(temp2[,match(col, colnames(temp2))]))
          }else{
            object<-cbind(object, as.numeric(as.character(temp2[,match(col, colnames(temp2))])))
            colnames(object)[ncol(object)]<-col
          }
          print(col)
          if(is.element(col, c("price_diff_highest_vintage","price_diff_lowest_vintage",
                               "price_diff_highest_vintage_wage_ratio", "price_diff_lowest_vintage_wage_ratio",
                               "proxy_using_costs_eco_techn", "proxy_using_costs_conv_techn", 
                               "using_cost_difference_eco_conv_capital"))){
            object[which(as.numeric(as.character(object$Periods)) < day_of_market_entry),match(col,colnames(object))]<-NA
          }
        }
        if(!is.element("sd_share",colnames(object)) || !is.element("variance_share",colnames(object))){
          stop("Inclusion of sd share and/ or var share did not work. Y?")
        }
        save(object, file=paste(experiment_directory,"/rdata/",kind_of_data,"/levels/",paste("data_",c,"_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
        rm(temp2)
  } # end c in cases
  rm(object)

} # d in data_type_temp (single large and single switch) 

  rm(day_of_market_entry)
  
  
} # end parameter loop  
  
  rm(temp_mat)
  
} # end if add_var
  
  
  
  
} # end if agent == Firm || Eurostat  
  
add_var<-0
