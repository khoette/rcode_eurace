### Settings for regression analysis ### 
###
reg_spill_threshold<-1
use_stepAIC<-1
use_2sls_manually<-0 # Use manually computed 2sls for "type" dummy (endog. dep. on e.g. level of barriers)
use_only_type_endog<-1
use_rounded_type_hat<-0
make_ivglm_tests<-1
use_mixture_model<-0
run_regression<-T
use_ols_procedure<-T
use_normal_reg<-F


OTHER_NAMES<-1

macro_control_regression<-F

use_probit<-T
use_logit<-F
use_gamlss<-T
use_plot_threshold<-F
plot_only_selection<-F
use_tax_spill_skill<-F
if(agent == "Firm"){plot_only_selection<-F}


# Specifications for regression data pre-processing
use_truncated_data<-1
use_type_adjusted_crit_levels<-T
#if(use_mixture_model<-1){use_type_adjusted_crit_levels<-F}
use_variance_as_target<-T
remove_incomplete_firms<-T


if(agent == "Firm"){
  k_neighbors<-3
}else{
  k_neighbors<-75
}


use_sq<-1
use_sq_in_type<-F
frontier_as_instrument<-F

use_gamlss_also_for_ols<-0


scale_data<-1 # scale explanatory vars: Zero mean and div. by std., see info scale()
method_temp<-c(100,5) # Method to fit gamlss obj: mixed(100 times RS, 5 times CG)
