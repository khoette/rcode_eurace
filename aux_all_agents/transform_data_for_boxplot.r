if(agent == "Firm"){
  print("This routine will take a while. Make sure that you really want it!")
}
if(exists("variables_temp")==FALSE){
  variables_temp<-variables
}
load(file=paste(data_directory,"data_",aggr,"_",experiment_name,"_",p1,"_",agent,".RData",sep=""))

vars<-match(c("Parameter","id", "Periods", "Type", "Firm_Type", "Adopter_Type", "Run", variables_temp), colnames(object))
vars<-unique(vars[is.na(vars)==FALSE])
temp_data<-object[,vars]
rm(object)

temp<-max(as.numeric(as.character(temp_data$Periods)))

selected_its_temp<-array(NA, dim=(12*temp/1200))
len<-length(selected_its_temp)/12

index<-c(1:12)
start<-380
end<-600
for(i in 1:len){
  selected_its_temp[index]<-seq(start, end, by=20)
  index<-index+12
  start<-start+1200
  end<-end+1200
}

selected_its_temp<-selected_its_temp[which(is.na(selected_its_temp)==FALSE)]
time_temp<-seq(600, max(selected_its_temp), by=1200)

rm(temp, end, start)

temp_data<-temp_data[which(as.vector(as.character(temp_data$Periods)) %in% selected_its_temp),]

fix_vars_temp<-match(c("Parameter","id", "Periods", "Type", "Firm_Type", "Adopter_Type", "Run"), colnames(temp_data))
fix_vars_temp<-unique(fix_vars_temp[which(is.na(fix_vars_temp)== FALSE) ])

data_temp<-as.data.frame(array(NA, dim=c((length(time_temp)*runs*no_agents), length(temp_data[1,]))))
colnames(data_temp)<-colnames(temp_data)
vars<-c(1:length(temp_data))

vars<-vars[which(is.element(vars,fix_vars_temp)==FALSE)]

b<-c(1:(runs*no_agents))
c<-c(1:(length(selected_its_temp)/12))
if(is.element("Parameter", colnames(temp_data))){
  temp_data$Parameter<-as.vector(as.character(temp_data$Parameter))
}
temp_data$Run<-floor(as.numeric(as.vector(as.character(temp_data$Run))))

temp_data$Periods<-as.vector(as.character(temp_data$Periods))
temp_data$Type<-(as.character(temp_data$Type))


if(is.element("Firm_Type", colnames(temp_data))){
  temp_data$Firm_Type<-as.character(temp_data$Firm_Type)
}
if(is.element("Adopter_Type", colnames(temp_data))){
  temp_data$Adopter_Type<-as.character(temp_data$Adopter_Type)
}
if(is.element("id", colnames(temp_data))){
  temp_data$id<-(as.character(temp_data$id))
}
for(i in (length(fix_vars_temp)+1):length(colnames(temp_data))){
  temp_data[,i]<-as.numeric(temp_data[,i])
}
i1<-1
#for(p0 in parameters_tmp){
  if(is.element("Parameter", colnames(temp_data))){
    temp_data2<-temp_data[which(temp_data$Parameter == p0),]
  }else{
    temp_data2<-temp_data
  }
  for(t in c){
    time_temp2<-seq((time_temp[t]-220), (time_temp[t]), 20)
    temp2<-temp_data2[which(as.vector(as.character(temp_data2$Periods)) %in% time_temp2),]
    rm(time_temp2)
    temp2<-temp2[order(as.numeric(as.character(temp2$Run))),]
    if(no_agents>1){
      temp2<-temp2[order(as.numeric(as.character(temp2$id))),]
    }
    
    index<-1
    if(no_agents>1){
      print(paste("Time ", time_temp[t]))
      for(ag in unique(temp2$id)){
        temp4<-temp2[which(temp2$id == ag),]
        temp3<-as.matrix(temp4[,vars])
        a<-apply(temp3,FUN=mean, MARGIN=2, na.rm=TRUE)
        data_temp[i1,vars]<-a
        data_temp[i1,fix_vars_temp]<-temp4[1,fix_vars_temp]
        i1<-i1+1
      }
    }else{
      for(i in b){
        temp3<-as.matrix(temp2[c(index:(index+11)),vars])
        a<-apply(temp3,FUN=mean, MARGIN=2, na.rm=TRUE)
        data_temp[i1,vars]<-a
        data_temp[i1,fix_vars_temp]<-temp2[index,fix_vars_temp]
        index<-index+12
        i1<-i1+1
      }
    }
  }
#} # end p0 in parameter loop
#reg_data_temp<-reg_data_temp[which(is.na(reg_data_temp$Run)==FALSE),]

rm(a, b, c, index, i1, i, temp3, temp2, temp_data2, vars)
if(no_agents>1){
  rm(ag, temp4)
}
data_temp$Type<-as.factor(data_temp$Type)

