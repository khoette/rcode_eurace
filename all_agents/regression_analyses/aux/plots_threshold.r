#stop()
#if(agent=="Firm"){stop("before threshold plot")}
if(agent == "Eurostat"){data_temp0<-data_euro}else if(agent == "Firm"){
  if(exists("data_work")){    data_temp0<-data_work
  }else{data_temp0<-data_firm}
}
# Select only relevant period (Only critical level data): 
data_temp0<-data_temp0[which(as.character(data_temp0$Periods) == "15000"),]

if(agent == "Eurostat"){repeat_it<-c(1,2)}else{repeat_it<-1}
for(r in repeat_it){
  if(agent == "Firm"){select_temp<-c("crit_skill_ratio_crit_prod_ratio")
  }else if(agent == "Eurostat" && r == 1){select_temp<-"crit_skill_ratio_crit_frontier_ratio"
  }else if(agent == "Eurostat" && r == 2){select_temp<-"diff_dist"}
  
  # Diff types of plots on critical levels of relative knowledge stocks
  data_temp<-data_temp0
  if(agent=="Eurostat"){
    if(r == 1){
      init_temp<-c("specific_skill_gap", "frontier_gap")
      vars_to_plot_temp<-c("s_skill_ratio","frontier_productivity_ratio", 
                           "crit_skill_ratio", "crit_frontier_ratio")
      names_temp<-c("Relative skill level", "Relative techn. frontier", 
                    "Critical skill ratio", "Critical frontier ratio")
      color_vars_temp<-c("Type", "Time", "variance_share_full", 
                         "min_learning_coefficient_var","learning_spillover_strength_var")
    }else if(r == 2){
      init_temp<-c("min_learning_coefficient_var", "learning_spillover_strength_var")
      vars_to_plot_temp<-c("min_learning_coefficient_var","learning_spillover_strength_var")
      names_temp<-c("Techn. difficulty", "Techn. distance")
      color_vars_temp<-c("Type", "Time", "variance_share_full")
    }
    point_size<-0.5
    
  }else if(agent== "Firm"){
    init_temp<-c("mean_specific_skill_gap", "technology_gap")
    vars_to_plot_temp<-c("skill_ratio", "technology_ratio", 
                         "crit_skill_ratio","crit_prod_ratio", "crit_techn_ratio")
    names_temp<-c("Initial skill ratio", "Initial technology ratio", 
                  "Critical skill ratio", "Critical productivity ratio", "Critical technology ratio")
    point_size<-0.05
    color_vars_temp<-c("Type", "Time", "variance_share_full", 
                       "min_learning_coefficient_var","learning_spillover_strength_var")
    
  }
  if(plot_only_selection && r == 1){
    if(agent == "Firm"){
      vars_to_plot_temp<-c("crit_skill_ratio","crit_techn_ratio")
      names_temp<-c("Critical skill ratio", "Critical technology ratio")
    }else if(agent == "Eurostat"){
      vars_to_plot_temp<-c("crit_skill_ratio","crit_frontier_ratio")
      names_temp<-c("Critical skill ratio", "Critical frontier ratio")
    }
  }

  color_vars_temp<-color_vars_temp[which(!is.na(match(color_vars_temp,colnames(data_temp))))]
  cols_temp<-match(c("id", "Periods", "Run", color_vars_temp, vars_to_plot_temp), colnames(data_temp))
  data_temp<-data_temp[,cols_temp[which(!is.na(cols_temp))]]

  if(agent == "Eurostat"){
    # Get "true" initial conditions (in data_temp only 1 year average available)
    load(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
    temp_init<-object[which(as.character(object$Periods) == "600" ), match(c("Run", "Periods", init_temp), colnames(object))]
    rm(object)
    cols_temp<-match(init_temp, colnames(temp_init))
    if(r == 1){
      temp<-1/(1-temp_init[,cols_temp])
    }else if(r == 2){
      temp<-temp_init[,cols_temp]
    }
    rm(temp_init, cols_temp)
    inds_init<-which(data_temp$Periods == unique(data_temp$Periods)[1])
    na_cols<-as.data.frame(matrix(NA, ncol = ncol(temp), nrow = nrow(data_temp)))
    na_cols[inds_init,]<-temp
    if(r == 1){
      colnames(na_cols)<-c("s_skill_ratio", "frontier_productivity_ratio") # colnames(temp)
    }
    full_temp<-cbind(data_temp, na_cols)
    data_temp<-full_temp
    rm(full_temp)
  }else if(agent == "Firm"){
    # Reduce data to relevant subset (if one-year average used)
    temp<-unique(data_firm$Periods)[1]#[length(unique(data_temp$Periods))]
    # For firm agent, get also init conds: 
    data_temp_init<-data_firm[which(as.character(data_firm$Periods) == temp),match(c("Run", "id", init_temp), colnames(data_firm))]
    # Create unique run-id-indices: 
    crit<-as.numeric(as.character(data_temp$Run)) + 0.001*as.numeric(as.character(data_temp$id))
    init<-as.numeric(as.character(data_temp_init$Run)) + 0.001*as.numeric(as.character(data_temp_init$id))
    both<-intersect(crit, init)
    inds_crit<-match(both, crit)
    inds_init<-match(both, init)
    data_temp_init<-data_temp_init[inds_init,]
    data_temp_crit<-data_temp[inds_crit,]
    test<-as.numeric(as.character(data_temp_init$Run))-as.numeric(as.character(data_temp_crit$Run))
    if(sum(test) != 0){stop("There must be a mistake in run matching")}
    test<-as.numeric(as.character(data_temp_init$id))-as.numeric(as.character(data_temp_crit$id))
    if(sum(test) != 0){stop("There must be a mistake in id matching")}
    rm(test, both, inds_crit, inds_init)
    # Convert pct barrier into ratios: 
    ratios_temp<-(100)/(100-data_temp_init[,match(init_temp, colnames(data_temp_init))])
    colnames(ratios_temp)<-c("skill_ratio", "technology_ratio")
     # And for data merging there need to be taken accound of run-id-consistency 
    data_temp<-cbind(data_temp_crit, ratios_temp)
    rm(data_temp_crit, data_temp_init, ratios_temp)
  }
  vars_to_plot_temp<-vars_to_plot_temp[which(!is.na(match(vars_to_plot_temp,colnames(data_temp))))]
  source(paste(script_dir,"/aux_all_agents/plot_settings/plot_settings.r",sep=""))
  source(paste(script_dir,"/aux_all_agents/plot_settings/settings_axis_labels.r", sep=""))

  value<-"Type"

  # Remove NAs in data: 
  cols<-match(vars_to_plot_temp, colnames(data_temp))
  inds_na<-c()
  for(c in cols){
    inds_na<-c(inds_na, which(is.na(data_temp[,c])))
  }
  inds_na<-unique(inds_na)
  if(length(inds_na) > 0){data_temp<-data_temp[-inds_na,]}
  # Remove data with almost 100% of only one technology type 
  # This concerns critical-levels: If last sign conversion in first or last period, conv. techn. 
  # use is almost 100 pct. 
  pers_temp<-unique(data_temp$Time)
  if(is.element((number_xml*20), pers_temp) ||is.element(20, pers_temp)){
    inds_temp<-which(data_temp$Time %in% c(20, (20*number_xml)))
    data_temp<-data_temp[-inds_temp,]
  }
  

  iterate<-length(vars_to_plot_temp)
  
  if(exists("plot_dir_selection")!= T){source(paste(script_dir,"/aux_all_agents/create_directories_for_plotting.r", sep=""))}
  dir_temp<-paste(plot_dir_selection,"/Decision_boundaries/",sep="")
  if(dir.exists(dir_temp)!=T){dir.create(dir_temp, recursive=T)}
  text_temp<-c("These plots are scatter plots and a decision boundary derived from a k-means 
               clustering algorithm separating subsets of data by regime type. \n 
               The data used is pre-processed data on last sign flips. I.e. there is only one observation 
               per run-agent collected at the point in time when the last sign-flip in the diffusion 
               curve (measured as share of conventional capital used) occurred (data is 
               one-year-average smoothed data). See r-scripts for further info.")
  write(text_temp, file=paste(dir_temp,"/Some_info_",agent,".txt", sep=""))


  i<-1
  j<-2
  for(i in 1:(iterate-1)){
    var1_temp<-vars_to_plot_temp[i]
    for(j in (i+1):iterate){
      var2_temp<-vars_to_plot_temp[j]
      
      vars_temp<-c(var1_temp,var2_temp)
      value<-"Type"
      data_tmp<-data_temp[,match(c(vars_temp,value),colnames(data_temp))]
      pair_title <- paste(vars_temp, collapse="_")
  
      min1<-min(data_tmp[,1], na.rm=TRUE)
      min2<-min(data_tmp[,2], na.rm=TRUE)
      max1<-max(data_tmp[,1], na.rm=TRUE)
      max2<-max(data_tmp[,2], na.rm=TRUE)
  
      dir_temp_here<-paste(dir_temp,"/Scatterplots/",sep="")
      if(dir.exists(dir_temp_here) !=T){dir.create(dir_temp_here, recursive=TRUE)}
      pdf(paste(dir_temp_here,exp_temp, "_",agent,"_",parameter,"_",pair_title,"_scat_ratios_Type",".pdf",sep=""))

      value<-"Type"
      xlabel<-names_temp[i]
      ylabel<-names_temp[j]
      #xlabel<-colnames(data_tmp)[1]
      #ylabel<-colnames(data_tmp)[2]
  
      plot<-ggplot(data_tmp, aes(x=as.numeric(data_tmp[,1]), y=as.numeric(data_tmp[,2])))
      #source(paste(script_dir, "/aux_all_agents/plot_settings/modify_plot_scatter.r", sep=""))
      plot<-plot+geom_point(size=point_size, aes(color=Type))+ scale_colour_manual(values=c('eco'=eco, 'conv'=conv))
      plot<- plot + theme(legend.justification=c(0.95,0.95), legend.position="none", legend.title = element_blank()) 
      plot<-plot +scale_x_continuous(limits=c(min1, max1)) + scale_y_continuous(limits=c(min2,max2)) + xlab(xlabel)+ylab(ylabel)
      print(plot)
      dev.off()
  
      
      if(with_rand_barr == 0  && length(intersect(c(var1_temp,var2_temp), c("s_skill_ratio","frontier_productivity_ratio"))) >=1){
        break
      }
      # Little check on decision boundary plot derived by K-means
      ## Loading required package: lattice
      ## Loading required package: ggplot2
      # k_neighbors is the number of neigbors to be used in knn3
      if(exists("k_neighbors")!=T){k_neighbors<-5}
      # only crit techn. levels used for prediction
      model_temp <- knn3(Type ~ ., data=data_tmp, k = k_neighbors)
      # all vars used for prediction
      #model_temp_full <- knn3(Type ~ ., data=data_temp, k = k_neighbors)
    
      if(length(intersect(c(var1_temp, var2_temp), c("s_skill_ratio", "frontier_productivity_ratio"))) == 0 ){
        range_temp<-matrix(c(0.75,1.75,0.75,1.75), ncol=2, nrow=2)
        colnames(range_temp)<-colnames(data_tmp)[1:2]
      }else if(length(intersect(c(var1_temp,var2_temp),c("min_learning_coefficient_var", 
                                                "learning_spillover_strength_var"))) == 2){
      range_temp<-NULL
      #colnames(range_temp)<-colnames(data_tmp)[1:2]
      }else if(with_rand_barr){
        range_temp<-matrix(c(0,1.112,0,1.112), ncol=2, nrow=2)
        colnames(range_temp)<-colnames(data_tmp)[1:2]
      }else if(agent == "Firm"){
        range_temp<-matrix(c(0.5,2,0.5,2), ncol=2, nrow=2)
      }else{range_temp <- NULL}
      clr<-color_vars_temp[1]

      for(clr in color_vars_temp){
        if(clr == "Type"){
          file_name<-paste(dir_temp,exp_temp,"_",agent,"_",parameter,"_",pair_title,"_decision_Type_",k_neighbors,".pdf", sep="")
          #file_name_full<-paste(dir_temp,exp_temp,"_",agent,"_",parameter,"_",pair_title,"_decision_Type_",k_neighbors,"_full.pdf", sep="")
          color_temp = NULL
          data_dec_temp<-data_tmp
        }else{
          file_name<-paste(dir_temp,exp_temp,"_",agent,"_",parameter,"_",pair_title,"_decision_",clr,"_",k_neighbors,".pdf", sep="")
          #file_name_full<-paste(dir_temp,exp_temp,"_",agent,"_",parameter,"_",pair_title,"_decision_",clr,"_",k_neighbors,"_full.pdf", sep="")
          color_temp<-clr
          data_dec_temp<-cbind(data_tmp, data_temp[,match(clr, colnames(data_temp))])
          colnames(data_dec_temp)[length(colnames(data_dec_temp))]<-clr
        }
        if(length(grep(select_temp, pair_title))){paste(dir_temp,exp_temp,"_",parameter,"_skill_prod_",clr,"_",k_neighbors,".pdf", sep="")}
      
        title<-"Decision boundary (kNN)"
        if(agent == "Eurostat"){range_temp <- NULL}
        decisionplot(model_temp, data_dec_temp, file_name, class = "Type", labels_temp = c(xlabel, ylabel),
                 main = title, col = c(eco, conv), range_temp = range_temp, 
                 color_temp = color_temp)
      
      #decisionplot(model_temp_full, data_dec_tmp_full, file_name_full, class = "Type", labels_temp = c(xlabel, ylabel),
      #             main = title, col = c(eco, conv), range_temp = range_temp, 
      #             color_temp = color_temp)
    }# # clr in color_vars_temp
    
    rm(min1, max1, min2, max2, xlabel, ylabel, value)
    rm(data_tmp)
  } # j in (i+1):iterate
} # i in 1:(iterate-1)

} # end r in repeat_it
rm(list=ls(pattern="temp"))
rm(title, i, j, clr)
