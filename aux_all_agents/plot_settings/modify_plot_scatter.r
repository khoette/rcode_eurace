## Modify plot of scatter plot
## 
## 
if(WHITE_BACKGROUND){
  #plot<- plot + theme_bw() + theme(panel.border = element_blank(), panel.grid.major = element_blank(),
  #                                       panel.grid.minor = element_blank(), axis.line = element_line(colour = "black"))
  plot<- plot + theme_bw() + theme(panel.border = element_rect(color = "black")) 
                                   #axis.line.y.left =  element_line(colour = "black"), axis.line.x.bottom =  element_line(colour = "black"), 
                                   #axis.line.y.right = element_line(colour = "black"), axis.line.x.top = element_line(colour = "black")
                                   #)
  # plot <- theme(plotpanel.border = element_rect(colour = "black", fill=NA, size=1))
}

#if(BLACK_WHITE == 0 || DIFFERENT_LINE_SHAPES){
  #point_size <- 4
  #print(paste("point size", point_size))
  if(target_var_temp0[j1] != "Type"){
    plot<-plot+geom_point(size=point_size, aes(color=value), alpha=alpha)
    plot<-plot+scale_colour_continuous(low=(eco),  high=(conv))
  }else if(target_var_temp0[j1] == "Type"){
    plot<-plot+geom_point(size=point_size, aes(color=value, shape=value))
    plot<-plot+ scale_colour_manual(values=c('eco'=eco, 'conv'=conv))
    plot<-plot+scale_shape_manual(name='', values=c('eco' = 0, 'conv'=8, 'switch'=13))
    
    #plot<-plot + scale_colour_manual(name='', values=c('eco'=eco, 'conv'=conv, 'switch'=switch))
  }else{
    plot<-plot+geom_point(size=point_size, aes(color=Type, shape = Type))+scale_colour_discrete("")
    plot<-plot+scale_shape_manual(name='', values=c('eco' = 0, 'conv'=8, 'switch'=13))
    
  }

#}else{
#  if(target_var_temp0[j1] != "Type"){
#    plot<-plot+geom_point(size=point_size, aes(color=value), alpha=alpha)+scale_colour_continuous(low="gray10",  high="black")
#  }else if(target_var_temp0[j1] == "Type"){
#    plot<-plot+geom_point(size=point_size, aes(shape=value))+scale_colour_discrete("") #+ scale_colour_manual(values=c('eco'=eco, 'conv'=conv))
#    #plot<-plot + scale_colour_manual(name='', values=c('eco'=eco, 'conv'=conv, 'switch'=switch))
#    #plot<-plot+scale_shape_discrete(name='', values=c('eco' = 1, 'conv'=3, 'switch'=13))
#    plot<-plot+scale_shape_manual(name='', values=c('eco' = 0, 'conv'=8, 'switch'=13))
#    
#  }else{
#    plot<-plot+geom_point(size=point_size, aes(shape=Type))+scale_colour_discrete("")
#    #plot<-plot+scale_shape_discrete(name='', values=c('eco' = 1, 'conv'=3, 'switch'=13))
#    plot<-plot+scale_shape_manual(name='', values=c('eco' = 0, 'conv'=8, 'switch'=13))
    
#  }
#}


plot<-plot + theme(axis.text.x = element_text(size=16),
                   axis.text.y = element_text(size=16),
                   axis.title.x = element_text(size=18),
                   axis.title.y = element_text(size=18))



#plot<-plot+ scale_linetype_manual(name='Regime', values=c('eco'='dashed', 'conv'='dotted', 'switch'='lines'))
