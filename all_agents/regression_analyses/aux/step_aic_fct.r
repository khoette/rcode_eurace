## stepAIC reg routine
capture_results <- function(reg_equ, object_raw, file_name_aic, table_for_stepaic, colname_temp){
  write(paste("\n \n ### \n ", paste(targ, paste(reg_equ)[c(1,3)], collapse=" "), "\n"), file=file_name_aic, append=T)
  capture.output(object_raw, file=file_name_aic, append=T)
  capture.output(summary(object_raw, diagnostics=T), file=file_name_aic, append=T)
  table_for_stepaic<-write_results_to_table(object_raw, table_for_stepaic, file_name_aic)
  return(table_for_stepaic)
}


use_fitDist_models<-0
add_type_manually<-1

## 
if(iv_reg_for_type){ 
  write(paste("\n \n ### \n First stage regression: Type on exp. vars \n"), file=file_name_aic, append=T)
  if(max(type_orig) > 1){stop("Verify range of type!")}
  #type_tmp<-as.numeric(as.factor(type_orig))-1
  type_tmp<-type_orig
  write(paste("\n",type_orig[1], "converted to ", type_tmp[1]," \n"), file=file_name_aic, append=T)
  model_type<-eval(as.formula(paste("type_tmp ~",paste(regressors_type, collapse = "+"))))
  for(j in 1:2){
    if(j == 1){
      type_reg<-gamlss(model_type, family=BI(mu.link="probit"))
      if(type_reg$converged != T){type_reg<-gamlss(model_type, family=BI(mu.link="probit"), method=mixed(5,200))}
    }else if(j == 2){
      type_reg<-gamlss(model_type, family=NO())
    }
    stepgaic_type<-stepGAIC(type_reg, k=log(n))
    write(paste("\n \n ### \n ",  paste("type", paste(model_type)[c(1,3)], collapse=" "), "\n"), file=file_name_aic, append=T)
    #capture.output(stepgaic_type, file=file_name_aic, append=T)
    capture.output(summary(stepgaic_type, diagnostics=T), file=file_name_aic, append=T)
    object_raw<-stepgaic_type
    #type_reg_eq<-stepgaic_type$call[2]
    type_reg_eq<-stepgaic_type$mu.formula
    # Get the instruments which should be used in first stage reg:
    temp<-as.character(type_reg_eq)[3]
    type_instruments<-c()
    for(i in c(regressors_type)){if(length(grep(i, temp))>0){type_instruments <- c(type_instruments, i)}} # for i in var
    #table_for_stepaic<-write_results_to_table(object_raw, table_for_stepaic, file_name_aic)
    if(j == 2){type_instruments_ols<-type_instruments}else{type_instruments_probit<-type_instruments}
  }  # for j in 1:2 (ols and probit type fitting)
  rm(i, j, type_instruments, temp)
  type_hat_not_rounded<-fitted.values(object_raw)
  type_hat_rounded<-round(fitted.values(object_raw))
  
  if(exists("use_rounded_type_hat") && use_rounded_type_hat){type_hat<-type_hat_rounded
  }else{type_hat<-type_hat_not_rounded}
  
  iterate_temp<-2
  write(paste("\n \n ### RUN REGRESSION TWICE: Once ignoring endogeneity of type, once using fitted types"), file=file_name_aic, append=T)
  
}else{iterate_temp<-1} # if iv_reg=T


reg_equation<-eval(as.formula(paste("target ~",paste(regressors_reg, collapse = "+"))))

for(it in 1:iterate_temp){
  if(it == 1){
    write(paste("\n \n ### \n REPLACE TYPE BY TYPE_orig and ignore possible endogeneity of type"), file=file_name_aic, append=T)
    type<-type_orig
    write(paste(unique(type)), file=file_name_aic, append=T)
    
    
  }else if(it == 2){
    if(iv_reg_for_type != T){stop("This should not occur... if iv_reg_for_type, iterate_temp == 1")}
    write(paste("\n \n ### \n REPLACE TYPE BY TYPE_HAT and make manual 2sls calculation. 
                \n This is also used to find the best model for 2sls model specification"), file=file_name_aic, append=T)
    type<-type_hat
    write(paste(unique(round(type))), file=file_name_aic, append=T)
    
    if(exists("use_2sls_manually") && use_2sls_manually){use_only_type_endog<-0}
  }
### 1. OLS model
# Find "best" model specification (BIC evaluated) for 2nd stage model: 
  if(exists("use_gamlss") && use_gamlss && use_gamlss_also_for_ols){
  reg_obj<-gamlss(reg_equation, family=NO())
  reg_obj_temp1<-reg_obj
  if(reg_obj$converged != T){reg_obj<-gamlss(reg_equation, family=NO(), method=mixed(method_temp[1],method_temp[2]))}
  object_raw<-stepGAIC(reg_obj, k=log(n))
}else{
  reg_obj<-lm(reg_equation, na.action=na.exclude)
  reg_obj_temp1<-reg_obj
  object_raw<-stepAIC(reg_obj, k=log(n))
}
  #object_raw<-stepAIC(reg_obj)
  reg_equ<-reg_equation
  table_for_stepaic<-capture_results(reg_equ, object_raw, file_name_aic, table_for_stepaic)
  reg_call_target_ols<-(as.character(object_raw$call)[2])
  vars_reg_temp_target_ols<-as.character(as.formula(reg_call_target_ols))[3]

  ### 2. Possibly other distributions in main regr. 
  if(exists("families_temp") && use_fitDist_models && families_temp[1]!="NET"){
  reg_obj<-try(gamlss(reg_equation, family = families_temp[1], method=mixed(method_temp[1],method_temp[2])))#, method=mixed(method_temp[1],method_temp[2]))
  if(class(reg_obj)=="try-error"){
    write(paste("gamlss with fitDist family ",families_temp[1], "failes with method: ", method_temp, "for ", targ, 
                paste(paste(reg_equation)[c(1,3)], collapse=" "),". Alternative: RS n.cyc=300"), file=file_name_aic, append=T)
    reg_obj<-try(gamlss(reg_equation, family = families_temp[1], n.cyc = 300))#, method=mixed(method_temp[1],method_temp[2]))
  }
  if(class(reg_obj) != "try-error"){
    object_raw<-stepGAIC(reg_obj, k = log(n))
    reg_equ<-reg_equation
    table_for_stepaic<-capture_results(reg_equ, object_raw, file_name_aic, table_for_stepaic, colname_temp)
  }else{write(paste("\n PROBLEM NOT SOLVED!! "), file=file_name_aic, append=T)}
}

  ### 3. If target is binary: 
  if(min(target, na.rm=T) >=0 && max(target, na.rm=T)<=1){
  
  reg_equation<-eval(as.formula(paste("round(target) ~",paste(regressors_reg, collapse = "+"))))
  reg_obj<-gamlss(reg_equation, family=BI(mu.link="probit"))
  if(reg_obj$converged != T){reg_obj<-gamlss(reg_equation, family=BI(mu.link="probit"), method=mixed(method_temp[1],method_temp[2]))}
  #object_raw<-stepGAIC(probit)
  object_raw<-stepGAIC(reg_obj, k=log(n))
  reg_equ<-reg_equation
  table_for_stepaic<-capture_results(reg_equ, object_raw, file_name_aic, table_for_stepaic, colname_temp)
  
  reg_call_target_probit<-(as.character(object_raw$call)[2])
  vars_reg_temp_target_probit<-as.character(as.formula(reg_call_target_probit))[3]
  
  
  if(exists("families_temp")&& use_fitDist_models && length(families_temp)>1 && families_temp[2]!="NET"){
    reg_obj<-gamlss(reg_equation, family = families_temp[2], method=mixed(method_temp[1],method_temp[2]))
    object_raw<-stepGAIC(reg_obj, k = log(n))
    reg_equ<-reg_equation
    table_for_stepaic<-capture_results(reg_equ, object_raw, file_name_aic, table_for_stepaic, colname_temp)
  } # if use fitDist
} # if binary target
  
} # end it in iterate_temp (iterate_temp == 2 if 2sls manually)
# If iv approach for type, use "best" model in IV model. Both, first and second stage model 
# are specified using stepGAIC
if(exists("iv_reg_for_type") && iv_reg_for_type){
  
  write(paste("\n \n ### \n REPLACE TYPE BY TYPE_ORIG IN IVREG vars \n"), file=file_name_aic, append=T)
  type<-type_orig
  write(paste(unique(type)), file=file_name_aic, append=T)
  

  # Check whether type is in reg_call. If not, iv regression not needed
  reg_call<-reg_call_target_ols
  if(exists("regressors_reg_fix")){
    regressors_in_call<-regressors_reg_fix
  }else if(length(grep("type", reg_call)) == 0 && add_type_manually){
    write(paste("\n \n ### \n TYPE NOT IN REG-EQUATION BUT MANUALLY ADDED\n"), file=file_name_aic, append=T)
    regressors_in_call<-c("type")
  }else{
    regressors_in_call<-c()
  }
    
    for(r in regressors_reg){if(length(grep(r, reg_call)>0)){regressors_in_call<-c(regressors_in_call,r)}}
    reg_call<-paste("target ~ ",paste(regressors_in_call, collapse ="+ "))
    #vars_reg_temp<-as.character(as.formula(reg_call))[3]
  
    # add to type_instruments those variables that are part of reg_call except those that are expected 
    # to be endogenous (esp. type and its interaction terms)
    instruments<-c(type_instruments_ols); endog<-c("type")
    #for(r in c(regressors_reg)){
    for(r in regressors_in_call){
      if(length(grep("type",r)) == 0){
        instruments<-c(instruments, r)
      }else{endog<-c(endog, r)}
    }
    instruments<-unique(instruments)
    instruments<-instruments[which(instruments %in% regressors_type)]
    # Adjust call: Only if more endog. than instruments
    if(length(instruments)<length(regressors_in_call)){
      warning("reg call adjusted because too few instruments")
      instruments<-unique(c(instruments, controls))
    }
    #adjust_reg_call<-0 -> I think it is better to add further exog. vars (e.g. controls)
    #for(i in type_instruments){if(length(grep(i, reg_call)) > 0){adjust_reg_call<-adjust_reg_call+1}}
    #if(adjust_reg_call > 0){
    #  temp<-c(endog, instruments[-which(instruments %in% type_instruments)])
    #  reg_call<-paste("target ~",paste(temp, collapse = "+"))}
    
    # Restrict set of endog. vars to type (only relevant if interaction terms included)
    if(exists("use_only_type_endog") && use_only_type_endog){endog<-"type"}
    endog<-unique(endog)
    reg_equation_iv<-eval(as.formula(paste(reg_call, "| . - ", paste(endog, collapse = "-"), " + ", paste(instruments, collapse = "+"))))
    object_raw<-ivreg(reg_equation_iv)
    reg_equ<-reg_equation_iv
    table_for_stepaic<-capture_results(reg_equ, object_raw, file_name_aic, table_for_stepaic, colname_temp)
  #}else{write(paste("\n \n ### \n NO IVREG BECAUSE TYPE NOT IN REG-EQUATION \n"), file=file_name_aic, append=T)
  #  write(paste("\n Report full model used as stepAIC input \n"), file=file_name_aic, append=T)
  #  capture.output(summary(reg_obj_temp1))
  #}
  
  
   # Check whether type is in reg_call. If not, iv regression not needed
  reg_call<-reg_call_target_ols
  if(exists("regressors_reg_fix")){
    regressors_in_call<-regressors_reg_fix
  }else if(length(grep("type", reg_call))==0 && add_type_manually){
    write(paste("\n \n ### \n NOT IN REG-EQUATION BUT MANUALLY ADDED\n"), file=file_name_aic, append=T)
    regressors_in_call<-c("type")
  }else{
    regressors_in_call<-c()
  }
  
  for(r in regressors_reg){if(length(grep(r, reg_call)>0)){regressors_in_call<-c(regressors_in_call,r)}}
  reg_call<-paste("target ~ ",paste(regressors_in_call, collapse ="+ "))
  
    #vars_reg_temp<-as.character(as.formula(reg_call))[3]
    
    # add to type_instruments those variables that are part of reg_call except those that are expected 
    # to be endogenous (esp. type and its interaction terms)
    instruments<-c(type_instruments_ols); endog<-c("type")
    #for(r in c(regressors_reg)){
    for(r in regressors_in_call){
      if(length(grep("type",r)) == 0){
        instruments<-c(instruments, r)
      }else{endog<-c(endog, r)}
    }
    instruments<-unique(instruments)
    instruments<-instruments[which(instruments %in% regressors_type)]
    # Adjust call: Only if more endog. than instruments
    #if(length(instruments)<length(regressors_in_call)){stop("THIS SHOULD BE TESTED")}
    #adjust_reg_call<-0 -> I think it is better to add further exog. vars (e.g. controls)
    #for(i in type_instruments){if(length(grep(i, reg_call)) > 0){adjust_reg_call<-adjust_reg_call+1}}
    #if(adjust_reg_call > 0){
    #  temp<-c(endog, instruments[-which(instruments %in% type_instruments)])
    #  reg_call<-paste("target ~",paste(temp, collapse = "+"))}
    
    # Restrict set of endog. vars to type (only relevant if interaction terms included)
    if(exists("use_only_type_endog") && use_only_type_endog){endog<-"type"}
    endog<-unique(endog)
    if(length(instruments)<length(regressors_in_call)){
      warning("reg call adjusted because too few instruments")
      instruments<-unique(c(instruments, controls))
    }
    reg_equation_iv<-eval(as.formula(paste(reg_call, "| . - ", paste(endog, collapse = "-"), " + ", paste(instruments, collapse = "+"))))
    object_raw<-ivreg(reg_equation_iv)
    reg_equ<-reg_equation_iv
    table_for_stepaic<-capture_results(reg_equ, object_raw, file_name_aic, table_for_stepaic, colname_temp)
    
    # Repeat everything using ivglm, i.e. probit regression on first stage.
    instruments<-c(type_instruments_probit); endog<-c("type")
    #for(r in c(regressors_reg)){
    for(r in regressors_in_call){
      if(length(grep("type",r)) == 0){
        instruments<-c(instruments, r)
      }else{endog<-c(endog, r)}
    }
    instruments<-unique(instruments)
    instruments<-instruments[which(instruments %in% regressors_type)]
    # Restrict set of endog. vars to type (only relevant if interaction terms included)
    if(exists("use_only_type_endog") && use_only_type_endog){endog<-"type"}
    endog<-unique(endog)
    
    
    write(paste("\n \n ### IV via IVGLM (using probit model on first stage) \n"), file=file_name_aic, append=T)
    if(class(data_reg_temp[,match("type", colnames(data_reg_temp))])!="numeric"){stop("why did num conversion not work?")}
    data_reg_temp[,match("type", colnames(data_reg_temp))]<-as.numeric(type)
    if(is.element(NA, data_reg_temp)){dt<-as.data.frame(data_reg_temp[-which(is.na(data_reg_temp)),])
    }else{dt<-as.data.frame(data_reg_temp)}
    for(c in 1:ncol(dt)){dt[,c]<-as.numeric(as.character(dt[,c]))}

    if(max(dt$type)>1){dt$type<-dt$type-1}
    
    type_model_temp<-eval(as.formula(paste("type ~ ", paste(instruments, collapse = "+"))))
    fit_type <- glm(type_model_temp,data=dt, family=binomial(link="probit"), maxit=100)
    capture.output(summary(fit_type), file=file_name_aic, append=T)
    # fitY.LX = glm(formula = target ~ regression_variables (including type and type-interaction) + controls, 
    # family = distribution of choice)
    if(min(target, na.rm=T) >=0 && max(target, na.rm=T)<=1){
      #reg_call_target_probit<-(as.character(object_raw$call)[2])
      #vars_reg_temp_target_probit<-as.character(as.formula(reg_call))[3]
      temp_call<-reg_call_target_probit
      #d$target<-round(d$target)
      #fit_target = glm(target_model_temp,data=dt, familiy = binomial(link="probit"))
      fit_target <- glm(target_model_temp,data=dt, family = binomial(link="probit"),maxit=100)
    }else{
      target_model_temp<-eval(as.formula(reg_call)) # This is the "optimal" equation obtained from stegGAIC -> Make binary/non-binary target check
      fit_target <- glm(target_model_temp,data=dt, family = gaussian(link = "identity"), maxit=100)
    }
    capture.output(summary(fit_target), file=file_name_aic, append=T)
    #fit_ivglm_temp<-ivglm(estmethod="ts", fitX.LZ=fit_type, fitY.LX=fit_target, data = dt, ctrl=T) 
    a<-try(fit_ivglm_temp<-ivglm(estmethod="ts", fitX.LZ=fit_type, fitY.LX=fit_target, data = dt, ctrl=F) )
    #summary(fit_ivglm_temp)
    if(class(a) != "try-error"){
      object_raw<-fit_ivglm_temp
      reg_equ<-target_model_temp
      #stop("ivglm")
      table_for_stepaic<-capture_results(reg_equ, object_raw, file_name_aic, table_for_stepaic)
    }
    #rm(dt)
    #phtest(fit_target,fit_ivglm_temp)
    #stop("ADD HAUSMANN TEST FÜR IVGLM")
    
    #stop("Confirm ivglm_experiment and think about clustered Std Err.")
    #
    # Hier: 
    # ivglm(estmethod = "ts", fitZ.L = , fitX.LZ = , fitX.L = , fitY.LX = )
    # fitIV <- ivglm(estmethod="ts", fitX.LZ=fitX.LZ, fitY.LX=fitY.LX, ctrl=TRUE) 
    #summary(fitIV)
    # 
    # method: ts -> two stage, ctrl = T -> use control fct R=X-X_hat when refitting fitY.LX... Not sure what 
    # this means. 
    # 
    # Optionally: clusterid -> could be used to compute clustered standard errors (cluster as regime type)
    # 
    
    
  #}else{write(paste("\n \n ### \n NO IVREG BECAUSE TYPE NOT IN REG-EQUATION \n"), file=file_name_aic, append=T)
  #  write(paste("\n Report full model used as stepAIC input \n"), file=file_name_aic, append=T)
  #  capture.output(summary(reg_obj_temp1))
  #}
  
  
  
  
} # if iv_reg_for_type



#if(use_tax_spill_skill == 0 && is.element("skill_macro", regressors_reg) || is.element("I(skill_macro * skill_macro)", regressors_reg)){stop("Excluding skill macro did not work. WHY??")}

if(targ=="variance_share_full" && agent == "Firm" && r==2){stop("Hopefully, this is the end and I can make manual expers with variance share on spills")}



rm(list=ls(pattern="fit_"))
rm(list=ls(pattern="model_temp"))
rm(list=ls(pattern="stepgaic"))
rm(object_raw)
