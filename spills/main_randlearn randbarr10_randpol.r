#if(1==2){
#delete.all<-function()
rm(list=ls(pos=.GlobalEnv),pos=.GlobalEnv)
#delete.all()
graphics.off()
rm(list=ls(all=TRUE))

# # Settings for R analysis
# 
revise_some_variables<-1
GET_DATA<-1 # Activate if you do not have pre-compiled RData set
USE_SUBSET_OF_DATA<-1 # Here you may chose which data to study. 
USE_OWN_COLOR_SETTINGS<-0
# If you also run the get data routine, you will generate only a subset of 
# RData. For transparency reasons, also all other tracked variables are 
# available, but not included in this default setting because it takes 
# a lot of time to run the code on the full data set, but you may change this 
# setting manually. 
# 
use_experiment_directory<-0 # You may write results to other directory to 
# avoid that previous results are overwritten.

# 
with_policy<-1
with_rand_policy<-1
with_rand_learn<-1
with_rand_barr<-1
#
VALIDATION <- 0
TS<-0
MTS<-0
DENS<-0
REG<-0
SNAPSHOT<-0
WILC_TYPES<-0
wilcox_average<-1
minisnapshot<-0
CORR<-0
SCAT <- 1 # Scatterplot if some variables are init. at random (e.g. random barriers)
SUMMARY <- 1
REG_SPILL<-1
VAR_ANALYSIS<-0

#
PWILC<-1
PTS<-1
PREG<-0

par_rand_learn<-0
par_rand_barr<-0

# Remove data after use: 
REMOVE_VALID_DATA <- 1
REMOVE_RAW_DATA <- 1

script_dir<-paste("/home/kerstin/Schreibtisch/docs/rcode/scripts",sep="")
script_dir<-paste("/home/kerstin/Schreibtisch/eurace/own_R/scripts",sep="")

if(file.exists(paste(script_dir))!=TRUE){
  stop("Set the paths to your directories first! Read the readme...")
}

#experiment_directory<-paste("/home/kerstin/Schreibtisch/learn_spill/rand_exper/14/")
experiment_directory<-paste("//home/kerstin/Schreibtisch/learn_spill/rand_exper/rand_learn_rand_pol_rand_barr10/")
parameter_bau<-"" 
#exper_dir_bau<-paste("PATH TO BASELINE IF YOU COMPARE SIMULS ACROSS DIFF. DIRECTORIES")
exper_dir_bau<-paste("/home/kerstin/Schreibtisch/learn_spill/rand_exper/both_learning_at_random_random_barr_10/")
exper_name_bau<-paste("Batch")

source(paste(experiment_directory,"/Configure.r", sep=""))
source(paste(script_dir,"/aux_all_agents/libs.r", sep=""))



#study_all_agents<-1
study_all_agents<-0 # if 0 study only set of selected agents (see below)
# Alternative: 
selected_agent<-c("Eurostat", "Government", "Firm")
#<-c("Eurostat")
#selected_agent<-c("Eurostat")#, "Firm")
#selected_agent<-c("Government")
#selected_agent<-"Firm"

# For some var, transition phase (i.e. time before market entry is cutted or manually reset (esp. those var related 
# to green technology (skill level etc.)))
cut_transition<-1

# Whether to use log data for e.g. output etc. 
# (see var_log in /aux_all_agents/variables_settigs_for_agents.r)
take_logs<-1
kind_of_data<-"logs"

# Only routine testing. But you may also restrict your analysis to subset of var 
# (no guarantee that it works througout all subfiles, did not test it comprehensively)
use_test_sample<-0

# Which parameter values? If you want to restrict your analysis to subset of avail. 
# paratmeter settings in exper. folder
parameters_tmp<-parameter_values

# Define "eco-regime" (here <0.5 conv. technology util. in last period):
threshold_eco_scen<-0.5

# Allow different cases and set data directory accordingly. 
# This is only relevant if you have created the corresponding data sets in data_handling.r
# routine. For most analysis, only data in levels is relevant. 
# By default, only "levels" data is created by this script. This can be 
# changed manually in the /aux_all_agents/data_handling.r script
#data_types_available<-c("levels", "ann_growth_rates", "normalized_by_init_avg", "normalized_by_per-run_avg", "Time_series", "Trend", "Cycle")
data_types_available<-c("levels")
# Select data type for analysis: 
data_type<-data_types_available[1]


# Make a selection of the data set to be analyzed, default: all
aggregation_levels<-c("single_large", "batch", "eco_conv_batch", "eco_conv_switch")
#if(exists("with_rand_learn") && with_rand_learn == 1){ 
  # only relevant if randomized learning parameters
#  available_aggregation_levels<-c(available_aggregation_levels, "batch_Learn_Type", "batch_Spill", "batch_Ease")
#}
# Make a selection for the type of time series plots that 
# should be generated. 
aggregation_levels_for_ts<-c("batch", "eco_conv_batch", "eco_conv_switch")
#aggregation_levels_for_ts<-c("single_large")
# Use for time series only subset. Data disaggr. by run is only in 
# specific cases needed. 
if(exists("with_rand_learn") && with_rand_learn == 1){
  aggregation_levels_for_ts<-c(aggregation_levels_for_ts, "batch_Learn_Type", "batch_Spill", "batch_Ease")
}

# Add gov. agent to set of analyzed agents if policy is studied. 
if(with_policy){
  list_of_agents_tmp<-unique(c(list_of_agents, "Government"))
}else{
  list_of_agents_tmp<-list_of_agents
}

###
### The call routines script does: 
### 1. Creates a data set in R format (if not yet created before)
###   out of the raw data in SQL format. 
### 2. Creates some aggregated data sets, esp. aggregation for different 
###   scenario types. 
### 3. Perform statistical analyses that are specified above (e.g. 
###   time series, wilcoxon tests, regressions, generation of 
###   validation output, etc. )
source(paste(script_dir,"/aux_all_agents/call_routines.r", sep=""))



