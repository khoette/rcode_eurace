load(paste(data_directory,"data_work.RData", sep=""))


list_of_var_for_validation<-c("monthly_output", "total_consumption_budget", "monthly_sold_quantity", 
                              "monthly_investment_quantity", "unemployment_rate", 
                              "no_vacancies", "monthly_investment_value", 
                              "price_index", "average_wage", "total_debt", 
                              "monthly_inflation_rate", 
                              "firm_average_productivity_conv", "firm_average_productivity_eco", 
                              "monthly_real_investments_conv","monthly_real_investments_eco", 
                              "ig_sold_quantity", "price_eco", "firm_average_wage", 
                              "firm_average_productivity", "output_growth_rate_annual", 
                              "gdp_growth_rate_annual", "real_investment", "average_mark_up"
)

list_of_var_for_validation<-list_of_var_for_validation[which(!is.na(match(list_of_var_for_validation, colnames(data_work))))]


vars_temp<-list_of_var_for_validation
cols_temp<-match(vars_temp, colnames(data_work))
cols_temp<-cols_temp[!is.na(cols_temp)]
data_work_temp<-data_work[c(1,2,3,cols_temp)]
cols_temp<-match(vars_temp, colnames(data_work_temp))

trend_full<-array(NA, dim=c(dim(data_work_temp)))
cycle_full<-array(NA, dim=c(dim(data_work_temp)))
series_full<-array(NA, dim=c(dim(data_work_temp)))
colnames(trend_full)<-colnames(data_work_temp)
colnames(cycle_full)<-colnames(data_work_temp)
colnames(series_full)<-colnames(data_work_temp)

trend_full<-as.data.frame(trend_full)
cycle_full<-as.data.frame(cycle_full)
series_full<-as.data.frame(cycle_full)

trend_full[,1:3]<-as.matrix(data_work_temp[,1:3])
cycle_full[,1:3]<-as.matrix(data_work_temp[,1:3])
series_full[,1:3]<-as.matrix(data_work_temp[,1:3])


for(c in cols_temp){
  print(paste("get ts data for:", colnames(data_work_temp)[c]))
  runs_temp<-unique(data_work_temp$Run)
  data_temp<-cbind(Run= data_work_temp$Run, data_work_temp[c])
  var_temp<-as.numeric(data_temp[,2])
  run_temp<-data_temp[,1]
  data_temp<-cbind(data_temp, ts=NA)
  cycle_temp<-array(NA, dim=c(length(run_temp), 1))
  trend_temp<-array(NA, dim=c(length(run_temp), 1))
  series_temp<-array(NA, dim=c(length(run_temp), 1))
  for(r in runs_temp){
    #print(r)
    index<-which(run_temp == r)
    data_subset<-var_temp[index]
    ts_data<-ts(as.numeric(data_subset), frequency = 12)
    ts_data_temp<-mFilter(ts_data,filter="BK", pl = 8, pu= 32, type = "variable", drift=TRUE)

    #ts_data_temp<-mFilter(ts_data,filter="HP", drift = TRUE, frequency=80)
    #ts_data_temp<-mFilter(ts_data,filter="HP", drift = TRUE, frequency=400)
    cycle_temp[index]<-ts_data_temp$cycle
    trend_temp[index]<-ts_data_temp$trend
    series_temp[index]<-ts_data
  }
  cycle_full[,c]<-as.numeric(cycle_temp)
  trend_full[,c]<-as.numeric(trend_temp)
  series_full[,c]<-as.numeric(series_temp)
} # c in cols_temp
vars_ts<-vars_temp
rm(c,r)

# create back up for later work
save(cycle_full, file=paste(data_directory,"cycle_bk_var_8_32.RData", sep=""))
save(trend_full, file=paste(data_directory,"trend_bk_var_8_32.RData", sep=""))
save(series_full, file=paste(data_directory,"series_bk_var_8_32.RData", sep=""))
               
# Create plots for a 10 year period before day of market entry of an arbitrary run: 
runs_temp<-unique(cycle_full$Run)
r <- sample(runs_temp, 1)
vars_temp_plot<-c("monthly_output", "total_consumption_budget", "ig_sold_quantity")
cols_temp<-match(c("Periods", "Run", "Type", vars_temp_plot), colnames(cycle_full))
if(is.element(NA, cols_temp)){
  print(paste("One of your variables of interest is not available: ", c("Periods", "Run", "Type", vars_temp_plot)[which(is.na(cols_temp))]))
  cols_temp<-cols_temp[which(!is.na(cols_temp))]
}

periods_temp<-seq(10220, 12600, 20)
#periods_temp<-seq(361, 600, 20)
inds_temp<-which(cycle_full$Run == r)
data_temp1<-cycle_full[inds_temp,cols_temp]
data_temp1<-data_temp1[which(data_temp1$Periods %in% periods_temp),]
data_temp<-data_temp1

inds_temp<-which(trend_full$Run == r)
trend_temp1<-trend_full[inds_temp,cols_temp]
trend_temp1<-trend_temp1[which(trend_temp1$Periods %in% periods_temp),]


cols<-match(vars_temp_plot, colnames(data_temp1))
cols<-cols[which(!is.na(cols))]
# Normalize by trend_full: 
for(c in cols){
  data_temp[,c]<-data_temp1[,c]/trend_temp1[,c]
}


vec_temp<-(as.matrix(data_temp[,cols]))
ymax<-max(vec_temp, na.rm=TRUE)
ymin<-min(vec_temp, na.rm=TRUE)
names<-c()
c1<-1
pdf(paste(plot_dir_validation,"/I_DO_NOT_KNOW.pdf", sep=""))
for(c1 in 1:length(cols)){
  c<-cols[c1]
  names<-c(names,colnames(data_temp)[c])
  if(c1 == 1){
    plot(data_temp[,c], col=c1, ylab="", xlab="Periods", ylim=range(ymin, ymax), type="l")
  }else{
    lines(data_temp[,c],col=c1)
  }
}
legend("top",xpd=TRUE, legend=c(names), col=c(1:length(cols)), lty=rep(1,length(cols)), 
       inset=c(0,-0.4), horiz=FALSE)
dev.off()
rm(list = ls(pattern="_temp"))

