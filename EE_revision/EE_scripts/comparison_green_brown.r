
# Steps: 
# 1. load pre-computed growth rates, share conv cap and raw data
# 2. compute growth rate manually for subsets eco, conv, switch
# 3. compute variance of diffusion measure. 
# 4. box-plots and manual comparison of growth rates. 
# 5. Mini regression: Output growth explained by type, by variance share, by interaction. 
#
# Further below: Compute business cylce volatility (avg. std 
# across runs and sd of avg sd across runs)
if(exists("data_directory")!=T){
  d<-1
  kind_of_data<-"logs"
  parameter<-parameters_tmp[1]
  data_directory<-paste(experiment_directory,"/rdata/",kind_of_data,"/",data_type[d],"/",sep="")
}
load(file=paste(data_directory,"data_single_switch_",experiment_name,"_",parameter,"_Eurostat.RData",sep=""))
object<-object[order(as.numeric(as.character(object$Run)), as.numeric(as.character(object$Periods))),]
share<-object$share_conv_capital_used

# # Use bandpass filtered data, i.e. trend_full
if(file.exists(paste(data_directory,"series_bk_var_8_32.RData", sep=""))){
  load(paste(data_directory,"series_bk_var_8_32.RData", sep=""))
  #load(paste(data_directory,"cycle_bk_var_8_32.RData", sep=""))
  #load(paste(data_directory,"trend_bk_var_8_32.RData", sep=""))
  data_temp<-series_full
  rm(list=ls(pattern = "_full"))
}else{
  print("Bandpass filtered time series data not existing. Unfiltered data is used")
  data_temp<-object
}
vars_temp<-c("monthly_output")
cols_temp<-match(c("Run", "Type", "Periods", vars_temp), colnames(data_temp))

temp<-data_temp[cols_temp]
rm(data_temp)
temp<-temp[order(as.numeric(as.character(temp$Run)), as.numeric(as.character(temp$Periods))),]
temp<-cbind(temp, share)
rm(share)

types<-unique(temp$Type)

# 1. Get output growth rate
temp<-temp[order(as.numeric(as.character(temp$Run)), as.numeric(as.character(temp$Periods))),]
if(max(temp$monthly_output, na.rm=TRUE) < 1000){
  print("Most likely, you are using log(output). This is now 
          retransformed to compute growth rates")
  temp$monthly_output<-exp(temp$monthly_output)
}
temp0<-temp
runs_temp<-unique(temp$Run)
output_temp<-temp$monthly_output
# Multiply by 100 (tranform into pct), otherwise values get numerically very small
share_temp<-100*temp$share   
r1<-1
i<-2

gr_temp<-rep(NA, length(runs_temp))
gr_sd_temp<-rep(NA, length(runs_temp))
share_sd_temp<-rep(NA, length(runs_temp))
temp<-cbind(temp, ann_gr=rep(NA,nrow(temp)))
temp<-cbind(temp, share_sd=rep(NA,nrow(temp)))

# Compute growth rate, geometric mean and standard dev. and sd of share conv cap for each run
# 
for(r1 in 1:length(runs_temp)){ 
  r <-runs_temp[r1]
  inds_temp<-which(temp$Run == r)

  out_r_temp<-output_temp[inds_temp]
  # monthly gr rate (index shifted)
  #diff_r_temp<-diff(out_r_temp,1)
  # Use yearly
  diff_r_temp<-diff(out_r_temp,12) # difference across year, i.e. (y_t - y_{t-12}) for all t
  # divide differences by level of output excluding the last year 
  gr_r_temp<-diff_r_temp/out_r_temp[-c((length(out_r_temp)-11):length(out_r_temp))] 
  # if monthly
  gr_r_temp<-gr_r_temp[which(!is.na(gr_r_temp))] # remove NAs
  gr_r_temp<-gr_r_temp + 1 # add one to compute geometric mean
  # add to data frame temp
  temp$ann_gr[inds_temp]<-c(rep(NA,12),gr_r_temp)
  
  # Compute std of share conv cap and add to data frame: 
  share_r_temp<-share_temp[inds_temp]
  sd_r_share<-rep(NA,length(share_r_temp))
  # ignore first 600 periods (30 observations) (time until market entry)
  # compute sd_share across time window of 2.5 years
  for(t in c(60:length(share_r_temp))){
    s<-sd(share_r_temp[(t-30):t])
    if(is.na(s)){stop("Chekc this, sd is na")}
    sd_r_share[t]<-s
  }
  temp$share_sd<-sd_r_share
  
  # Compute averages per run: 
  # Arithmetic mean for std of share conv cap: 
  #share_sd_temp<-rep(NA, length(runs_temp))
  share_sd_temp[r1]<-mean(sd_r_share,na.rm=T)
  
  # Compute geometric mean for growth rate
  
  mean_temp<-((prod(gr_r_temp))**(1/length(gr_r_temp))-1) 
  if(is.na(mean_temp)){stop("What happened?")}
  gr_temp[r1]<-mean_temp
  
  # Take simple std to compute deviations of yearly growth 
  # rates from long term geometric mean of growth rate
  # (first, transform factor back into rate)
  gr_r_temp<-gr_r_temp-1
  sd_temp<-sum(((gr_r_temp)-(mean_temp))**2)/length(gr_r_temp)
  sd_temp<-sqrt(sd_temp)
  gr_sd_temp[r1]<-sd_temp
}

# transform into pct. growth: 
temp$ann_gr<-(temp$ann_gr-1)*100

share<-temp$share[which(temp$Periods == 15000)]
conv<-as.factor(round(share))
t<-temp$Type[which(temp$Periods == 600)]
ols<-lm(gr_temp~share_sd_temp*t)
summary(ols)
ols<-lm(gr_temp~share_sd_temp)
summary(ols)
ols<-lm(gr_temp~share_sd_temp+t)
summary(ols)

ols<-lm(gr_temp~share_sd_temp*conv)
summary(ols)
ols<-lm(gr_temp~share_sd_temp+conv)
summary(ols)

ols<-lm(share~share_sd_temp)
summary(ols)

type<-glm(as.factor(t)~share_sd_temp,family=binomial(link="probit"))
summary(type)

gr<-temp$ann_gr
sd<-temp$share_sd
r<-temp$Run
ty<-temp$Type
ols<-lm(gr~sd+ty)
summary(ols)
ols<-lm(gr~sd*ty)
summary(ols)
ols<-lm(gr~sd*ty+r)
summary(ols)

require(plm)
require(lmtest)  # for waldtest()

temp$Periods<-as.numeric(as.character(temp$Periods))
panel<-pdata.frame(temp, index=c("Run", "Periods"), drop.index = F, row.names = T)
head(panel)

# fit pooled OLS
m1 <- lm(ann_gr ~ share_sd, data = panel)
# fit same model with plm (needed for clustering)
pm1 <- plm(ann_gr ~ share_sd, data = panel, model = "pooling")

coeftest(m1)
waldtest(m1)
coeftest(m1, vcov = vcovHC(m1, type = "HC1"))
waldtest(m1, vcov = vcovHC(m1, type = "HC1"))


G <- length(unique(panel$Run))
N <- length(panel$Run)
dfa <- (G/(G - 1)) * (N - 1)/pm1$df.residual
run_c_vcov <- dfa * vcovHC(pm1, type = "HC0", cluster = "group", adjust = T)
coeftest(pm1, vcov = run_c_vcov)
waldtest(pm1, vcov = run_c_vcov, test = "F")

G <- length(unique(panel$Periods))
N <- length(panel$Periods)
dfa <- (G/(G - 1)) * (N - 1)/pm1$df.residual
per_c_vcov <- dfa * vcovHC(pm1, type = "HC0", cluster = "time", adjust = T)
coeftest(pm1, vcov = per_c_vcov)
waldtest(pm1, vcov = per_c_vcov, test = "F")


