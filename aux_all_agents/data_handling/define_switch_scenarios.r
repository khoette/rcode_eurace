if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/", data_type[d] ,"/",paste("data_single_switch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))!=TRUE){

if(exists("switch_ids") != TRUE){
  if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/", data_type[d] ,"/",paste("data_single_switch_",experiment_name,"_",parameter,"_Eurostat",sep=""),".RData",sep=""))==TRUE){
    load(file=paste(experiment_directory,"/rdata/logs/levels/data_single_switch_",experiment_name,"_",parameter,"_Eurostat.RData",sep=""))
    temp<-object[which(object$Type == "switch"),]
    if(length(temp$Run) > 0){
      switch_ids<-unique(temp$Run)
    }
  }else{ # single switch file Eurostat does not yet exist. 
agent<-"Eurostat"
 
load(file=paste(experiment_directory,"/rdata/logs/levels/data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
eval(call("<-",(paste("temp_mat_single",sep="")),object))
rm(object)

halftime<-as.integer(max(as.numeric(temp_mat_single$Periods))/2)
endtime<-as.integer(max(as.numeric(temp_mat_single$Periods)))
#halftime<-floor(((15000)/2)/20)
#endtime<-floor(15000/20)

runs_temp<-unique(temp_mat_single$Run)

test<-temp_mat_single
test$Type<-as.character(test$Type)
rm(switch_ids)
if(exists("switch_ids") != TRUE){
  switch_ids<-c()
}
for(r in runs_temp){
  temp<-temp_mat_single[which(temp_mat_single$Run == r), ]
  #if((temp$Type[1] == "conv" && as.numeric(temp$share_conv_capital_used[halftime]) < 0.5)||(temp$Type[1] == "eco" && as.numeric(temp$share_conv_capital_used[halftime]) > 0.5))
  if((temp$Type[1] == "conv" && min(as.vector(temp$share_conv_capital_used)) < 0.80) || (temp$Type[1] == "eco" && min(as.vector(temp$share_conv_capital_used)) > 0.20)  ||
     (temp$Type[1] == "eco" && as.numeric(min(as.vector(temp$share_conv_capital_used[1:halftime]))) < 0.75 && max(as.vector(temp$share_conv_capital_used[halftime:endtime])) > 0.75 ))
  {
    temp$Type<-"switch"
    test[which(test$Run == r), ] <- temp
    if(exists("switch_ids")){
      switch_ids<-c(switch_ids,floor(as.numeric(r)))
    }else{
      switch_ids<-floor(as.numeric(r))
    }
  }
}

switch_ids<-unique(switch_ids)
eval(call("<-",(paste("data_single_switch_",experiment_name,"_",parameter,"_",agent,sep="")),test))

object<-eval(as.name(paste("data_single_switch_",experiment_name,"_",parameter,"_",agent,sep="")))
save(object, file=paste(experiment_directory,"/rdata/logs/levels/",paste("data_single_switch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))

rm(test)
rm(list=ls(pattern="temp"))
} # file Eurostat switch already exists
}else{ # switch ids already exist. 
  load(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  object$Type<-as.character(object$Type)
  if(length(switch_ids)>0){
  temp<-object[which(is.element(floor(as.numeric(object$Run)), floor(as.numeric(switch_ids)))),]
  temp$Type<-"switch"
  object[which(is.element(round(as.numeric(object$Run)), switch_ids)),]<-temp
  }
  
  
  eval(call("<-",(paste("data_single_switch_",experiment_name,"_",parameter,"_",agent,sep="")),object))
  
  object<-eval(as.name(paste("data_single_switch_",experiment_name,"_",parameter,"_",agent,sep="")))
  save(object, file=paste(experiment_directory,"/rdata/",kind_of_data,"/", data_type[d] ,"/",paste("data_single_switch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
  
  rm(object)
  rm(list=ls(pattern="temp"))
  
}
}else{
  print("Data already exists")
}

