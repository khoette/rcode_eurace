if(GET_DATA==1){
 
# (1) Get data from sql-database
# (2) Transform data to pdata.frame for ggplotting
# (3) Make some normalizations with data
# (4) Get also bandpass data
if(exists("study_all_agents") == TRUE && study_all_agents == 1){
  list_of_agents<-c("Eurostat", "Government", "Firm")
  print(paste("Study all agents: ", paste(list_of_agents, collapse = "; ")))
}else{
  list_of_agents<-selected_agent
  print(paste("Study only selected agent: ", paste(list_of_agents, collapse=" ")))
}

for(agent_temp in list_of_agents){
  print(paste("Get data for: ", agent_temp))
  agent<-agent_temp
  if(agent == "Firm"){
    no_agents<-120
  }else{
    no_agents<-1
  }
  # Settings for variables, e.g. which var to log. 
  source(paste(script_dir,"/aux_all_agents/variables_settings_for_agents.r",sep=""))
  
  ts_data<-0
  # Data for which parameters? 
  if(exists("parameters_tmp") != TRUE){
    parameters_tmp<-parameter_values
  }
  # Get raw data for all variables that are available in sql data base: 
  for(p in 1:length(parameters_tmp)){
    parameter<-(parameters_tmp[p])
  if(file.exists(file=paste(experiment_directory,"/rdata/raw_data/data_all_",experiment_name,"_",parameter,"_",agent,".RData",sep="")) == FALSE ){ 
    if(file.exists(file=paste(experiment_directory,"/rdata/", kind_of_data, "/levels/data_batch_",experiment_name,"_",parameter,"_",agent,".RData",sep="")) == FALSE){ 
      print(paste("Get raw data for parameter: ", parameter, sep=""))
      source(paste(script_dir,"/aux_all_agents/data_handling/get_data.r",sep=""))
    }
  }else{
    print("Data already exists.")
  }
}
# Data for which variables? 
if(exists("variables_selection") == FALSE ||is.element(NA, match(variables_selection, list_of_var)) ){
  source(paste(script_dir,"/aux_all_agents/variables_settings_for_agents.r",sep=""))
}
if(exists("take_logs") && take_logs==1){
  kind_of_data<-"logs"
}else{
  kind_of_data<-"levels"
}
  
# Transform data into pdata.frame format. 
for(p in 1:length(parameters_tmp)){
  parameter<-parameters_tmp[p]
  if(exists("logs_taken")){
    rm(logs_taken)
  }
  # This is only relevant if you want to make ex-post manipulation of data, e.g. 
  # run a second type of aggregateion. Should be in the long run integrated in 
  # switch definition script. 
  name<-"levels"
  cases_temp<-c()
  if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/",name,"/",paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep="")) == FALSE){
    cases_temp<-c(cases_temp, 1)
  }
  if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/",name,"/",paste("data_batch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep="")) == FALSE){
    cases_temp<-c(cases_temp, 2)
  }
  if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/",name,"/",paste("data_eco_conv_batch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep="")) == FALSE){
    cases_temp<-c(cases_temp, 3)
  }
  if((exists("ts_data") && ts_data==1) || length(cases_temp) > 0){ 
    cases_temp<-c()
    if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/",name,"/",paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep="")) == FALSE){
      cases_temp<-c(cases_temp, 1)
    }
    if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/",name,"/",paste("data_batch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep="")) == FALSE){
      cases_temp<-c(cases_temp, 2)
    }
    if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/",name,"/",paste("data_eco_conv_batch_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep="")) == FALSE){
      cases_temp<-c(cases_temp, 3)
    }
    
    print(paste("Transform data to pdata.frame for parameter: ", parameter, sep=""))
    variables<-list_of_var
    if(length(variables_selection) > length(list_of_var)){
      variables_selection<-list_of_var[is.na(match(variables_selection, list_of_var)) == FALSE]
    }
    
    source(paste(script_dir,"/aux_all_agents/data_handling/get_data_df.r",sep=""))
    aggr<-"single_large"
    data_directory<-paste(experiment_directory,"/rdata/logs/levels/",sep="")
    source(paste(script_dir, "/aux_all_agents/data_handling/compute_eco_conv_switch_subsets.r", sep=""))
    
    }else{
    print(paste("Panel transformed data already existing for ",agent, parameter))
    print(paste(experiment_directory,"/rdata/",kind_of_data,"/levels/data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
    load(file=paste(experiment_directory,"/rdata/",kind_of_data,"/levels/data_batch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
    variables<-colnames(object)
    temp<-c("Periods", "Run", "Type", "Parameter")
    variables<-variables[!variables %in% grep(paste0(temp, collapse = "|"), variables, value = T)]
    if(agent=="Firm"){
      variables<-variables[which(variables!="id")]
    }
  }
  if(p==1){
  # Back up parameter values: 
  available_variables_in_panel<-variables
  eval(call("<-",(paste("variables_available_in_panel_",agent,sep="")),variables))
  
  # Data for which variables? 
  if(length(list_of_var) == length(variables_selection)){
    variables_selection<-variables
  }
  }
}# end parameter
  
if(1==2){
# Normalizations: 
for(p in 1:length(parameters_tmp)){
  ts_data<-0
  parameter<-parameters_tmp[p]
  print(paste("Normalize data for parameter: ", parameter, sep=""))
  source(paste(script_dir,"/aux_all_agents/data_handling/get_data_normalized.r",sep=""))
}
}
  take_ts<-0
if(take_ts == 1){
for(p in 1:length(parameters_tmp)){
  parameter<-parameters_tmp[p]
  print(paste("Get bandpass data for parameter: ", parameter, sep=""))
  source(paste(script_dir,"/aux_all_agents/data_handling/bandpassfilter.r",sep=""))
}
}
  
}

data_type<-c("levels", "ann_growth_rates", "normalized_by_init_avg", "normalized_by_per-run_avg", "Time_series", "Trend", "Cycle")
data_type<-data_type[1]
for(p in 1:length(parameters_tmp)){
  parameter<-parameters_tmp[p]
  for(d in 1:length(data_type)){
    data_directory<-paste(experiment_directory,"/rdata/",kind_of_data,"/",data_type[d],"/",sep="")
    for(agent in list_of_agents){
      if(file.exists(paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))){
        }
    }
  }
}


for(p in 1:length(parameters_tmp)){
  parameter<-parameters_tmp[p]
  if(exists("switch_ids")){
    rm(switch_ids)
  }
  for(d in 1:length(data_type)){
    data_directory<-paste(experiment_directory,"/rdata/",kind_of_data,"/",data_type[d],"/",sep="")
    for(agent in list_of_agents){
      if(file.exists(paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))){
        print(paste("Define switch scenarios: ", agent, parameter, kind_of_data, data_type[d], sep=" "))
        source(paste(script_dir,"/aux_all_agents/data_handling/define_switch_scenarios_new_definition.r",sep=""))
        aggr<-"single_switch"
        source(paste(script_dir, "/aux_all_agents/data_handling/compute_eco_conv_switch_subsets.r", sep=""))
        
        #if(exists("with_rand_learn")&&with_rand_learn == 1){
        #if((is.element("learning_spillover_strength_var", list_of_var_complete) || is.element("learning_spillover_strength_var", list_of_var_Eurostat) )&& (exists("with_rand_learn") && with_rand_learn == 1)){
        #  source(paste(script_dir,"/aux_all_agents/data_handling/define_learning_subsets.r",sep=""))
        #}
        #}
        #source(paste(script_dir,"/aux_all_agents/data_handling/define_switch_scenarios.r",sep=""))
        #
      }else{
        print(paste("Data type not available: ", kind_of_data, data_type[d], agent))
      }
      
      if(file.exists(paste(data_directory,"data_single_switch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))){
        print(paste("Define switch scenarios: ", parameter,kind_of_data, data_type[d], sep=" "))
        source(paste(script_dir,"/aux_all_agents/data_handling/define_switch_scenarios_for_batch.r",sep=""))
        if(exists("with_rand_learn")&&with_rand_learn == 1){
        if(is.element("learning_spillover_strength_var", list_of_var_Eurostat) && exists("with_rand_learn") && with_rand_learn==1){
          source(paste(script_dir,"/aux_all_agents/data_handling/define_batch_scenarios_for_subsets.r", sep=""))
        }
        }
      }else{
        print(paste("Data type not available: ", kind_of_data, data_type[d], agent, parameter))
      }
    }
  }
}


}else{ # 
  print("You have set: GET_DATA == 0")
}
rm(list=ls(pattern="temp"))

# Remove raw data: 
if(REMOVE_RAW_DATA == 1 && file.exists(paste(experiment_directory,"/rdata/raw_data", sep=""))){
  print("Remove raw data")
  unlink(paste(experiment_directory,"/rdata/raw_data", sep=""), recursive = TRUE, force = FALSE)
}

