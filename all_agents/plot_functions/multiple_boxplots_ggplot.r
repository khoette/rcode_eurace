if(exists("MULTI_BOX") && MULTI_BOX == 1){


#  Color settings: 
high<-"navyblue"
low<-"orangered"
highhigh<-"navyblue"
highlow<-"skyblue"
lowhigh<-"orange"
lowlow<-"orangered"
eco<-"limegreen"
conv<-"salmon"
switch<-"royalblue"

# interaction: 
# high: dark, low: bright
ecol<-"yellowgreen"
ecoh<-"darkgreen"
convl<-"orange"
convh<-"chocolate4"
switchl<-"skyblue"
switchh<-"darkblue"

# linetype settings: 
ltyhigh<-"dashed"
ltylow<-"dotted"


fix_vars_temp<-c("Periods", "id")
plot_by_type_temp<-c("Type", "Spill", "Ease", "Learn_Type", "Firm_Type")

fix_vars_temp<-c(fix_vars_temp, plot_by_type_temp)

aggr<-"single_large"

if(exists("par_analyis") && par_analysis==1){
  pars_temp<-parameters_tmp
}else{
  pars_temp<-parameter
}
p1<-parameter
for(p1 in pars_temp){
  load(file=paste(data_directory,"data_",aggr,"_",experiment_name,"_",p1,"_",agent,".RData",sep=""))
  source(paste(script_dir,"/aux_all_agents/transform_data_for_boxplot.r",sep=""))    
  if(length(pars_temp)>1){
    data_temp<-cbind(Parameter = p1, data_temp)
    if(p1 == pars_temp[1]){
      data_temp0<-data_temp
    }else{
      data_temp0<-rbind(data_temp0, data_temp)
    }
    if(p1 == pars_temp[length(pars_temp)]){
      data_temp<-data_temp0
      rm(data_temp0)
    }
  }
}

source(paste(script_dir, "/aux_all_agents/plot_settings/plot_settings.r", sep=""))

plot_dir_temp0<-paste(plot_dir_selection, "/Multi_boxplot/", sep="")
if(dir.exists(plot_dir_temp0)!=1){
  dir.create(plot_dir_temp0)
}
plot_dir_temp<-paste(plot_dir_temp0)

# 
# Which variables? 
# 


cols<-match(c(fix_vars_temp, variables_temp), colnames(data_temp))
cols<-cols[is.na(cols)!=TRUE]
data_temp<-data_temp[cols]

cols<-match(variables_temp, colnames(data_temp)) # all cols except fix vars
cols<-cols[is.na(cols)!= TRUE]

data<-data_temp
rm(data_temp)
ind<-match(fix_vars_temp, colnames(data))
ind<-ind[which(is.na(ind)==FALSE)]
df_data0<-data[ind]
increment<-0
for(type in plot_by_type_temp){
  ind<-match(type, colnames(data))
  if(is.na(ind)==FALSE && length(data[which(is.na(data[,ind])==FALSE),ind])>0.5*length(data[,ind])){
for(v in cols){
  increment<-increment+1
  var_name<-colnames(data)[v]
  # Reshape the data, i.e. extract column data and use melt to bring it to plot 
  # format! 
  df_data<-cbind(df_data0, data[v])
  df_data<-melt(df_data, measure.vars = var_name)
  df_data<-df_data[which(is.infinite(df_data$value) == FALSE),]
  df_data<-df_data[which(is.na(df_data$value) == FALSE),]
  #
  if(length(df_data[,1]) < no_agents * runs * 0.1 || is.numeric(df_data$value)==FALSE){
    print(paste("df_data element either not numeric or very few remaining observations after removing inf and na. In hist_and_dens_temp_mat_firm", var_name, v, sep=" "))
    break
  }
  pdf(paste(plot_dir_temp,"/",agent,"_",var_name,"_",type,"_boxplots.pdf",sep=""))

  if(type=="Type"){
    p1<-ggplot(subset(df_data, Type %in% c("eco", "conv")), aes(x=reorder(Periods, as.numeric(Periods)), y=value, color=Type))
  }else if(type=="Spill"){
    p1<-ggplot(subset(df_data, Spill %in% c("high", "low")), aes(x=reorder(Periods, as.numeric(Periods)), y=value, color=Type))
  }else if(type=="Ease"){
    p1<-ggplot(subset(df_data, Ease %in% c("high", "low")), aes(x=reorder(Periods, as.numeric(Periods)), y=value, color=Type))
  }else if(type=="Learn_Type"){
    p1<-ggplot(subset(df_data, Learn_Type %in% c("high", "low")), aes(x=reorder(Periods, as.numeric(Periods)), y=value, color=Type))
  }else if(type=="Firm_Type"){
    p1<-ggplot(subset(df_data, Firm_Type %in% c("eco", "conv", "switch-c2e", "switch-e2c")), aes(x=reorder(Periods, as.numeric(Periods)), y=value, color=Type))
  }else if(type=="Adopter_Type"){
    p1<-ggplot(subset(df_data, Adopter_Type %in% c("early_adopter", "laggard")), aes(x=reorder(Periods, as.numeric(Periods)), y=value, color=Type))
  }
  plot<- p1 + geom_point()
  plot<-p1 + geom_boxplot() + xlab("Periods") + ylab(paste(var_name)) + scale_x_discrete(breaks=seq(0,number_xml*20,5000)) 

  plot<-plot + scale_colour_manual(name='Regime', values=c('eco'=eco, 'conv'=conv, 'switch'=switch)) #+ scale_linetype_manual(name='Spill', values=c('high'=ltyhigh, 'low'=ltylow))
  
  print(plot)
  dev.off()

  if(agent=="Firm"){
    pdf(paste(plot_dir_temp,"/Firm_type",var_name,"_eco_conv_boxplots.pdf",sep=""))
    
    p1<-ggplot(subset(df_data[which(is.na(df_data$Firm_Type)==FALSE),], Firm_Type %in% c("eco", "conv","switch-c2e","switch-e2c")), aes(x=reorder(Periods, as.numeric(Periods)), y=value, color=Type))
    
    plot<- p1 + geom_point()
    plot<-p1 + geom_boxplot() + xlab("Periods") + ylab(paste(var_name)) + scale_x_discrete(breaks=seq(0,number_xml*20,5000)) 
    
    print(plot)
    dev.off()
    
  }
  rm(plot, p1, var_name, value)
}
  }# v in cols
  

} # type in types
rm( cols,type,  data)
} # if "MULTI_BOX"
rm(list=ls(pattern="temp"))
rm(list=ls(patter="df_data"))

on.exit(if(is.null(dev.list()) == F){ dev.off()}, add=T)
