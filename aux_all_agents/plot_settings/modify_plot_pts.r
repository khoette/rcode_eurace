## Modify plot
## 
## 
## 
if(draw_line != 0){
  if(as.character(value) != "monthly_budget_balance_gdp_fraction"){
    plot<-plot + geom_hline(yintercept = draw_line, linetype = "dashed", color = "gray30")
  }else{
    plot<-plot + geom_hline(yintercept = 0, linetype = "dashed", color = "gray30")
  }
}

pt_values<-c('eco' = pt_eco, 'conv'=pt_conv, 'switch'=pt_switch) 

if(exists("USE_OWN_COLOR_SETTINGS") && USE_OWN_COLOR_SETTINGS){
  if(setequal(parameters_tmp, c("0.0", "0.5", "2.0"))){
    col_values<-c('0.0' = "gray50", '0.5' = "gray25", '2.0' = "gray0", 
                  #'eco.0.0' = "palegreen1", 'eco.0.5' = "palegreen3", 'eco.2.0' = "palegreen4", 
                  'eco.0.0' = "chartreuse1", 'eco.0.5' = "chartreuse3", 'eco.2.0' = "chartreuse4", 
                  'conv.0.0' = "tomato1", 'conv.0.5' = "tomato3", 'conv.2.0' = "tomato4", 
                  'switch.0.0' = "skyblue1", 'switch.0.5' = "skyblue3", 'switch.2.0' = "skyblue4")
  }else{
    #'0.0' = "red", '0.5' = "blue", '1.0' = "black", 
    col_values<-c('0.0' = "gray50", '0.5' = "gray25", '1.0' = "gray0",
                  #'eco.0.0' = "palegreen1", 'eco.0.5' = "palegreen3", 'eco.1.0' = "palegreen4", 
                  'eco.0.0' = "chartreuse1", 'eco.0.5' = "chartreuse3", 'eco.1.0' = "chartreuse4", 
                  
                  'conv.0.0' = "tomato1", 'conv.0.5' = "tomato3", 'conv.1.0' = "tomato4", 
                  'switch.0.0' = "skyblue1", 'switch.0.5' = "skyblue3", 'switch.1.0' = "skyblue4")
  }
  lty_value<-c(1,1,1,1,1,1,1,1,1,1,1,1)
}else if(BLACK_WHITE){
  col_values<-c(1,1,1,1,1,1,1,1,1,1,1,1)
  lty_values<-c('Experiment'=lty0, 'Baseline'=lty1, 'Both' = lty2, '0' = lty1, '1' = lty0, '2' = lty2, '3' = lty3, '4' = lty4, 
                'None'=lty1, 'Skills'=lty3, 'Productivity'=lty2, 'Consumption'=lty0, 'Investment'=lty3, 'None'=lty0, 
                'Skills'=lty3, 'Productivity'=lty2, 
                '0.0' = lty0, '0.5' = lty1, '1.0' = lty2, '1.5' = lty3, '2.0' = lty4
  ) 
  # points used in black white to distinguish scenario types
}else{
  col_values<-c('Experiment' = "black", 'Baseline' = 'gray50',
                'eco.Experiment'=eco,'eco.Both'=ecoh, 'eco.Baseline'=baseline_col, 
                'conv.Experiment'=conv,'conv.Both'=convh, 'conv.Baseline'=baseline_col, 
                'switch.Experiment'=switch,'switch.Both'=switchh, 'switch.Baseline'=baseline_col,
                'eco'=eco, 'conv'=conv, 'switch'=switch,
                'high-high'=highhigh, 'high-low'=highlow, 'low-low'='lowlow', 'low-high'=lowhigh,
                'high'=high, 'low'=low,
                'eco.low'=ecol,'eco.high'=ecoh, 'conv.low'=convl, 'conv.high'=convh,'switch.low'=switchl, 'switch.high'=switchh, 
                '0.0' = 'gray0', '0.5' = 'gray15', '1.0' = 'gray30', '1.5' = 'gray30', '2.0' = 'gray45'
  )
  if((length(parameter_names)>2 && aggr == "eco_conv_switch" )|| length(parameter_names)>3  ){
    lty_values<-c(1,1,1,1,1,1,1,1,1,1,1,1) # distinction only by color
  }else{
    lty_values<-c('Experiment'=1, 'Baseline'=2, 'Both' = 3, '0' = 1, '1' = 2, '2' = 3, '3' = 4, '4' = 5) 
  }
}
if(1==2){  
  if(experiment_name == "switch_diffusion_barrier_type"){
    plot<-plot+ scale_linetype_manual(name="", values=c('None'=lty1, 'Skills'=lty3, 'Productivity'=lty2, 'Both'=lty0)) #+ scale_shape_manual(name="", values=c('0'=0, '1'=1, '2'=2, '3'=3))
  }else if(experiment_name == "eco_policy_switch" || length(parameters_tmp) == 2){
    plot<-plot+ scale_linetype_manual(name="", values=c('Baseline'=lty1, 'Experiment'=lty0, 'Both' = lty0, 'Consumption'=lty2, 'Investment'=lty3))# + scale_shape_manual(name="", values=c('0'=0, '3'=1, '4'=2))
  }else if(experiment_name == "switch_diffusion_barrier_type" || length(parameters_tmp) == 2){
    plot<-plot+ scale_linetype_manual(name="", values=c('None'=lty0, 'Skills'=lty3, 'Both' = lty1, 'Productivity'=lty2))# + scale_shape_manual(name="", values=c('0'=0, '3'=1, '4'=2))
  }else if(aggr!="batch" && is.element(experiment_name, c("learning_spillover_strength","min_learning_coefficient_const"))==FALSE){
    plot<-plot+ scale_linetype_manual(name="", values=c('0'=lty0, '3'=lty1, '4'=lty2))# + scale_shape_manual(name="", values=c('0'=0, '3'=1, '4'=2))
  }
  if(aggr == "batch"){
    if(experiment_name == "switch_diffusion_barrier_type"){
      plot<-plot+ scale_linetype_manual(name="", values=c('None'=lty1, 'Skills'=lty3, 'Productivity'=lty2, 'Both'=lty0)) #+ scale_shape_manual(name="", values=c('0'=0, '1'=1, '2'=2, '3'=3))
    }else if(experiment_name == "eco_policy_switch" || length(parameters_tmp) == 2){
      plot<-plot+ scale_colour_manual(name="", values=c('Baseline'=baseline, 'Experiment'=experiment, 'Both'=experiment, 'Consumption'=cons, 'Investment'=invest))# + scale_shape_manual(name="", values=c('0'=0, '3'=1, '4'=2))
    }#else if(experiment_name == "learning_spillover_strength" || experiment_name == "min_learning_coefficient_const" ){
    #  plot<-plot+ scale_colour_manual(name="", values=c('1.0'= col1, '0.5'= col2, '0.0'=col3, '1.5' = , '2.0' = ))# + scale_shape_manual(name="", values=c('0'=0, '3'=1, '4'=2))
    #}
    else{
      plot<-plot+ scale_color_brewer(type='seq', palette='Reds')
    }
  }else if(length(col_values)>=length(parameters_tmp)){
    plot<-plot+ scale_colour_manual(name="", values=col_values) 
  }else{
    plot<-plot+ scale_color_brewer(type='seq', palette='Reds')
  }
}

if(BLACK_WHITE ){
  if(no_parameters_temp == 2){
    #col_values<-c(rep("gray5", no_types_temp), rep("black", no_types_temp))
    col_values<-c(rep("gray60", (no_types_temp)), rep("black", (no_types_temp)))
    #pt_values<-c(rep(pt_eco, no_types_temp), rep(pt_conv, no_types_temp), rep(pt_switch, no_types_temp))
  }
}#else{ # end BLACK_WHITE
#  col_values<-c("gray60", "gray60",conv,eco,conv,eco)

#}
if(aggr == "batch"){
  length_temp<-1
}else if(is.element(aggr, c("single_large", "eco_conv_batch","Spill", "Ease"))){
  length_temp<-2
}else if(is.element(aggr, c("single_switch", "eco_conv_switch"))){
  length_temp <- 3
}else{
  length_temp <- 4
}
if(is.element(aggr, c("single_large", "single_switch")) == FALSE){
  if(is.element("Type", colnames(temp)) && aggr != "batch"){
    if(BLACK_WHITE || (exists("ADD_POINTS") && ADD_POINTS)){
      plot <- plot + geom_point(aes(shape = Type, color=Parameter), size = 3)
    }else if(is.numeric(temp$Parameter) == FALSE){
      plot <- plot + geom_point(aes(shape = Type, color=interaction(Type, Parameter), group=interaction(Type,Parameter)), size = 3)
    }else if(is.numeric(temp$Parameter) == TRUE){
      plot <- plot + geom_point(aes(shape = Type, color=Parameter), size = 3)
    }
    plot<-plot+scale_shape_manual(name='', values=pt_values, guide=FALSE)
  }else if(is.element("Spill", colnames(temp))){
    plot <- plot + geom_point(aes(shape = Spill))
    plot<-plot+scale_shape_manual(name='', values=c('high'=15, 'low'=1))
  }else if(is.element("Ease", colnames(temp))){
    plot <- plot + geom_point(aes(shape = Ease))
    plot<-plot+scale_shape_manual(name='', values=c('high'=15, 'low'=1))
  }else if(is.element("Learn_Type", colnames(temp))){
    plot <- plot + geom_point(aes(shape = Learn_Type))
    plot<-plot+scale_shape_manual(name='', values=c('high-high'=15, 'high-low'=16, 'low-low'='1', 'low-high'=0))
    
  }
}else if(1==2){ # else aggr is single run data
  # Here I do not plot different point types for different scenario types. 
  # Looks too confounding...
  if(is.element("Type", colnames(temp)) && agent!= "Firm"){
    plot <- plot + geom_point(aes(shape = Type, group=interaction(Type,Run)))
    plot<-plot+scale_shape_manual(name='', values=pt_values)
  }else if(is.element("Spill", colnames(temp))){
    plot <- plot + geom_point(aes(shape = Spill, group=interaction(Spill,Run)))
    plot<-plot+scale_shape_manual(name='', values=c('high'=15, 'low'=1))
    
    #'eco.low'=0,'eco.high'=15, 'conv.low'=1, 'conv.high'=16,'switch.low'=2, 'switch.high'=17))
    
  }else if(is.element("Ease", colnames(temp))){
    plot <- plot + geom_point(aes(shape = Ease, group=interaction(Ease,Run)))
    plot<-plot+scale_shape_manual(name='', values=c('high'=15, 'low'=1))
  }else if(is.element("Learn_Type", colnames(temp))){
    plot <- plot + geom_point(aes(shape = Learn_Type, group=interaction(Learn_Type,Run)))
    plot<-plot+scale_shape_manual(name='', values=c('high-high'=15, 'high-low'=16, 'low-low'='1', 'low-high'=0))
  }
}

if((length(setdiff(parameter_names, names(col_values))) == 0)|| (exists("USE_OWN_COLOR_SETTINGS") && USE_OWN_COLOR_SETTINGS == 1)){
  if(aggr == "batch"){
    plot <- plot+scale_color_manual(name='', values=col_values, guide=FALSE)
  }else{
    plot <- plot + scale_color_manual(name='', values=col_values,guide = FALSE)
  }
  plot <- plot + scale_linetype_manual(name='', values=c(1,1,1,1,1,1,1,1,1,1), guide=FALSE)
  
}else if(is.numeric(temp$Parameter) == TRUE){
  #plot <- plot + scale_color_gradient(low = gradient_low, high = gradient_high)
  plot<-plot+ scale_color_brewer(type='seq', palette='Reds')
  #plot <- plot + scale_linetype_manual(name='', values=lty_values, guide=FALSE)
  #plot <- plot + scale_linetype_continuous(name='', guide=TRUE)
}else if((length(parameter_names)>2 && aggr == "eco_conv_switch" )|| length(parameter_names)>3 || (length(unique(temp$Type)) * length(parameter_names) > 10)  ){
  plot <- plot + scale_linetype_manual(name='', values=lty_values, guide=FALSE)
  plot<-plot + scale_colour_manual(name='', values=col_values,guide = FALSE) 
}

#plot <- plot + guides(shape = guide_legend(override.aes = list(linetype = lty_values, color=col_values)))
#if(aggr != "batch"){
#  plot <- plot + guides(colour = guide_legend(override.aes = list(shape = pt_values, linetype = lty_values)))
#  plot <- plot + scale_shape(guide = FALSE)
#  plot <- plot + scale_linetype(guide = FALSE)
#}else{
#  plot <- plot + guides(colour = guide_legend(override.aes = list(linetype = lty_values)))
#  plot <- plot + scale_linetype(guide = FALSE)
#}


#}else{ # else BLACK_WHITE
#  plot <- plot + scale_linetype_manual(name='', values=lty_values, guide = FALSE)
#  plot<-plot + scale_colour_manual(name='', values=col_values) 
#}

xmin_temp<-"120"
xmax_temp<-"15000"
plot <- plot + xlab("Periods") + aes(ymin=min, ymax=max)#, xmin=xmin_temp, xmax=xmax_temp)


plot<-plot + scale_x_discrete(breaks=factor(c("3000","6000","9000","12000"))) + scale_y_continuous(limits=c(min,max))
plot<-plot + ylab(ylabel)

#plot<- plot+ theme(legend.position="none")
#plot<-plot+ scale_linetype_manual(name='', name='Regime', values=c('eco'='dashed', 'conv'='dotted', 'switch'='lines'))


#if(exists("legend_pos_temp")){
#  plot <- plot+ theme(legend.position=legend_pos_temp)
#}else{
#  plot <- plot+ theme(legend.position=c(0.1, 0.9))
#}
plot <- plot+ theme(legend.position="bottom")

if(WHITE_BACKGROUND){
  plot<- plot + theme_bw() + theme(panel.border = element_rect(color = "black"), 
                                   #axis.line = element_line(colour = "black"), 
                                   #axis.line.x = element_line(colour = "black"), 
                                   axis.ticks.x = element_line())
  
  #plot<- plot + theme_bw() + theme(panel.border = element_rect(), panel.grid.major = element_blank(),panel.grid.minor = element_blank())
  #, axis.line = element_line(colour = "black"))
}

plot<-plot + theme(axis.text.x = element_text(size=16),
                   axis.text.y = element_text(size=16),
                   axis.title.x = element_text(size=18),
                   axis.title.y = element_text(size=18))

ADD_POINTS<-FALSE

