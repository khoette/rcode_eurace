fct_temp<-"PTS"
if(exists("aggregation_levels_for_ts")){
  aggregation_levels_temp<-aggregation_levels_for_ts
}

source(paste(script_dir, "/aux_all_agents/plot_settings/plot_settings.r", sep=""))

fix_vars_temp0<-c("Type", "id", "Periods", "Firm_Type", "Adopter_Type", 
                 "Parameter", "Run", "min_learning_coefficient_var", 
                 "learning_spillover_strength_var", "Number_Runs", "Spill", "Ease", "Learn_Type")

if(dir.exists(paste(plot_dir_parameter,"/Time_series/",sep=""))!=1){
  dir.create(paste(plot_dir_parameter,"/Time_series/",sep=""))
}
plot_dir_temp0<-paste(plot_dir_parameter,"/Time_series/",sep="")

no_parameters_temp<-length(parameters_tmp)

for(aggr in aggregation_levels_temp){
  
  if(file.exists(paste(data_directory,"data_", aggr, "_",experiment_name,"_",parameters_tmp[2],"_",agent,".RData",sep=""))){

  print(paste("TS for ", aggr))
  
  fix_vars_temp0<-c("Type", "id", "Periods", "Firm_Type", "Adopter_Type", 
                   "Parameter", "Run", "min_learning_coefficient_var", 
                   "learning_spillover_strength_var", "Number_Runs", "Spill", "Ease", "Learn_Type")
  
  #for(c in cases){
  for(p in 1:length(parameters_tmp)){
    parameter<-parameters_tmp[p]
    
# Get data: 
    if(exists("bau_in_diff_folder") && bau_in_diff_folder == 1 && p==1){ #&& parameter == parameter_bau){
      load(file=paste(data_dir_bau,"data_", aggr, "_",exper_name_bau,"_",parameter,"_",agent,".RData",sep=""))
    }else{
      load(file=paste(data_directory,"data_", aggr, "_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
    }
    #eval(call("<-",(paste("temp_mat_single",sep="")),object))
    
    if(p==1){
      fix_vars_temp<-fix_vars_temp0[which(is.element(fix_vars_temp0, colnames(object)))]
      plot_dir_temp<-paste(plot_dir_temp0,"/", aggr, "/",sep="")
      if(dir.exists(plot_dir_temp)!=1){
        dir.create(plot_dir_temp)
      }
    }
    
  if(exists("variables") != TRUE || is.element(NA, match(variables, colnames(object)))){
    variables<-colnames(object)[which(is.element(colnames(object), fix_vars_temp0)!=TRUE)]
  }

    if(exists("bau_in_diff_folder") && bau_in_diff_folder == 1){
      # Check consistence of fix_vars_temp
      if(p==2){
        temp_fix<-fix_vars_temp0[which(is.element(fix_vars_temp0, colnames(object)))]
        if(setequal(temp_fix, fix_vars_temp) != 0){
          temp<-setdiff(fix_vars_temp, temp_fix)
          if(length(temp)!= 0){
            print("WARNING: More fix vars in BAU than in exper file")
            remove<-match(temp, colnames(df_data_temp))
            df_data_temp<-df_data_temp[,-remove]
          }
          temp<-setdiff(temp_fix, fix_vars_temp)
          if(length(temp)!= 0){
            print("WARNING: More fix vars in exper than in BAU file")
            if(is.element("learning_spillover_strength_var", temp)){
              with_rand_learn<-0
            }
          }
        } # same fix vars?
      } # parameter != parameter_bau
    }# parameter in diff folder check

      cols<-match(c(fix_vars_temp, variables), colnames(object))
      cols<-cols[which(is.na(cols) != TRUE)]

      object <-object[cols]
      
    parameter_temp<-c(rep(parameter_names[p], times=dim(object)[1]))
    
    object<-add_column(object, Parameter=parameter_temp, .after = length(fix_vars_temp))
    if(p>1 && length(colnames(df_data_temp))!=length(colnames(object))){

      if(length(colnames(df_data_temp))>length(colnames(object))){
        remove_temp<-match(setdiff(colnames(df_data_temp), colnames(object)), colnames(df_data_temp))
        df_data_temp<-df_data_temp[-remove_temp]
      }
      if(length(colnames(df_data_temp))<length(colnames(object))){
        remove_temp<-match(setdiff(colnames(object), colnames(df_data_temp)), colnames(object))
        object<-object[-remove_temp]
      }
    }
    #all_cols_temp<-intersect(colnames(object), colnames(df_data_temp))
    #cols_temp<-match(colnames(object), all_cols_temp)
    #object<-object[which(!is.na(cols_temp))]
    #cols_temp<-match(all_cols_temp, colnames(df_data_temp))
    #df_data_temp<-df_data_temp[which(!is.na(cols_temp))]
    if(p == 1){
      df_data_temp<-object
      #cols_object_temp<-colnames(object)
    }else{
      df_data_temp<-rbind(df_data_temp, object)
    }
  }# End parameter loop: Now, df_data_temp is a data frame of selected variables for each parameter observation. 
  # Number of rows should be no_agents x (runs || batch || types) x number_xml x length(parameters_tmp)
rm(object)

if(aggr!="batch"){
  no_types_temp<-length(unique(df_data_temp$Type))
}else{
  no_types_temp<-1
}
if(exists("USE_OWN_COLOR_SETTINGS") && USE_OWN_COLOR_SETTINGS == 0){
  if(is.element(NA, (as.numeric(as.character(df_data_temp$Parameter)))) == FALSE){
    df_data_temp$Parameter<-as.numeric(as.character(df_data_temp$Parameter))
  }
}
  fix_vars_temp<-c(fix_vars_temp, "Parameter")
  fix_indices_temp<-match(fix_vars_temp, colnames(df_data_temp))
  fix_vars_temp<-fix_vars_temp[which(!is.na(fix_indices_temp))]
  fix_indices_temp<-fix_indices_temp[which(!is.na(fix_indices_temp))]
  df_data0_temp<-df_data_temp[fix_indices_temp]
  
  data<-df_data_temp
  
  cols<-match(variables, colnames(data))
  fix_vars<-match(colnames(data)[which(is.element(colnames(data), variables) ==FALSE)], colnames(data))
  if(exists(paste("cut_transition_phase_",agent, sep=""))){
    eval(call("<-",(paste("cut_transition_phase",sep="")),as.name(paste("cut_transition_phase_",agent,sep=""))))
    cut_cols<-match(cut_transition_phase, colnames(data))
    cut_cols<-cut_cols[is.na(cut_cols) != TRUE]
  }
  cols<-cols[is.na(cols) != TRUE]


  for(v in 1:length(variables)){
    
    value<-variables[v]
    var<-match(value,colnames(data))
    if(is.na(var)){
      print("var match is NA in parameter_multiple_ts_eurostat.r")
      break
    }
    
    temp<-cbind(df_data0_temp, data[var])
    source(paste(script_dir,"/all_agents/parameter/parameter_ts.r", sep=""))
  }# end v in variables
  }# if file.exists(DATA)
  else{
    print(paste("Data not existing: ", data_directory,"data_", aggr, "_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  }
}# end aggr in aggregation levels

rm(data, ylabel, lwd, alpha, value, v, aggr)
rm(list=ls(pattern="df"))
rm(list=ls(pattern="temp"))
on.exit(if(is.null(dev.list()) == F){ dev.off()}, add=T)
