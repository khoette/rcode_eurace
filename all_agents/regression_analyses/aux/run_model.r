# Inputs: c_ as matrix of controls, target as target variable
#
if((reg_level == "Eurostat" && targ!= "share_conv_capital_used") || 
   (reg_level == "Firm" && targ != "share_conventional_vintages_used")){
  add_type_dummy<-1
  d_<-type
}else{
  d_<-rep(0, length(target))
}
#
if(reg_level == "Firm"){use_normal_reg<-0}else{use_normal_reg<-1}
if(exists("use_normal_reg") && use_normal_reg){
write("\n \n ############################################## \n \n NEXT MODEL \n \n",file=file_name,append=T)
write("\n \n ############################################## \n \n NEXT MODEL \n \n",file=file_name_tex,append=T)
counter<-0
if(with_rand_learn){
  descr_con_temp<-c(descr_temp, paste("\n Predictors", paste(colnames(c_), collapse=", "), "measured in ",p2))
  write(descr_con_temp,file=file_name,append=T)
  # table_for_tex<-regr_and_capt_output = 
  # function(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE){
  title_text<-paste("Target", targ,"in",p, "on", paste(colnames(c_),collapse=", "), "in", p2)
  model_ols<-target ~ d_ + c_
  model_binary<-round(target) ~ d_ +c_
  table_for_tex<-regr_and_capt_output(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE, use_gamlss = TRUE, family=families_temp)
  counter<-counter+1
  model_ols<-target ~ d_ + c_ + macro_
  model_binary<-round(target) ~ d_ +c_ + macro_
  table_for_tex<-regr_and_capt_output(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE, use_gamlss = TRUE, family=families_temp)
  counter<-counter+1
}
if(with_rand_barr){
  descr_con_temp<-c(descr_temp, paste("\n Predictors", paste(colnames(b_), collapse=", "), "measured in ",p2))
  write(descr_con_temp,file=file_name,append=T)
  # table_for_tex<-regr_and_capt_output = 
  # function(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE){
  title_text<-paste("Target", targ,"in",p, "on", paste(colnames(b_),collapse=", "), "in", p2)
  model_ols<-target ~ d_ + b_
  model_binary<-round(target) ~ d_ +b_
  table_for_tex<-regr_and_capt_output(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE, use_gamlss = TRUE, family=families_temp)
  counter<-counter+1
  model_ols<-target ~ d_ + b_ + macro_
  model_binary<-round(target) ~ d_ +b_ + macro_
  table_for_tex<-regr_and_capt_output(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE, use_gamlss = TRUE, family=families_temp)
  counter<-counter+1
  
}

if(with_rand_barr && with_rand_learn){
  model_ols<-target ~ d_ + c_ * b_
  model_binary<-round(target) ~ d_ +c_ * b_
  table_for_tex<-regr_and_capt_output(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE, use_gamlss = TRUE, family=families_temp)
  counter<-counter+1
  model_ols<-target ~ d_ + c_ * b_ + macro_
  model_binary<-round(target) ~ d_ +c_ * b_ + macro_
  table_for_tex<-regr_and_capt_output(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE, use_gamlss = TRUE, family=families_temp)
  counter<-counter+1
}

if(with_policy && with_rand_policy){
  if(p_reg_simple){
    model_ols<-target ~ d_ + p_
    model_binary<-round(target) ~ d_ +p_
    table_for_tex<-regr_and_capt_output(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE, use_gamlss = TRUE, family=families_temp)
    counter<-counter+1
    model_ols<-target ~ d_ + p_ + macro_
    model_binary<-round(target) ~ d_ +p_ + macro_
    table_for_tex<-regr_and_capt_output(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE, use_gamlss = TRUE, family=families_temp)
    counter<-counter+1
    }  # policy reg simple
  
    descr_con_temp<-c(descr_temp, paste("\n Predictors", paste(c(colnames(p_), colnames(b_)), collapse=", "), "measured in ",p2))
    write(descr_con_temp,file=file_name,append=T)
    title_text<-paste("Target", targ,"in",p, "on", paste(c(colnames(p_), colnames(b_)),collapse=", "), "in", p2)
    if(with_rand_barr && with_rand_learn){
      model_ols<-target ~ d_ + p_+b_+c_
      model_binary<-round(target) ~ d_ +p_+b_+c_
    }else if(with_rand_barr){
      model_ols<-target ~ d_ + p_+b_
      model_binary<-round(target) ~ d_ +p_+b_
    }else if(with_rand_learn){
      model_ols<-target ~ d_ + p_+c_
      model_binary<-round(target) ~ d_ +p_+c_
    }
    table_for_tex<-regr_and_capt_output(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE, use_gamlss = TRUE, family=families_temp)
    counter<-counter+1
    
    descr_con_temp<-c(descr_temp, paste("\n Predictors", paste(c(colnames(p_), colnames(b_), colnames(macro_)), collapse=", "), "measured in ",p2))
    write(descr_con_temp,file=file_name,append=T)
    title_text<-paste("Target", targ,"in",p, "on", paste(c(colnames(p_), colnames(b_), colnames(macro_)),collapse=", "), "in", p2)
    if(with_rand_barr && with_rand_learn){
      model_ols<-target ~ d_ + p_+b_+c_ + macro_
      model_binary<-round(target) ~ d_ +p_+b_+c_ + macro_
    }else if(with_rand_barr){
      model_ols<-target ~ d_ + p_+b_ + macro_
      model_binary<-round(target) ~ d_ +p_+b_+ macro_
    }else if(with_rand_learn){
      model_ols<-target ~ d_ + p_+c_ + macro_
      model_binary<-round(target) ~ d_ +p_+c_ + macro_
    }
    table_for_tex<-regr_and_capt_output(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE, use_gamlss = TRUE, family=families_temp)
    counter<-counter+1
} # with policy


  if(exists("pbc_inter") && pbc_inter){
    descr_con_temp<-c(descr_temp, paste("\n Predictors", paste(c(colnames(p_), colnames(b_)), collapse=", "), "measured in ",p2))
    write(descr_con_temp,file=file_name,append=T)
    title_text<-paste("Target", targ,"in",p, "on", paste(c(colnames(p_), colnames(b_)),collapse=", "), "in", p2)
    if(with_rand_barr && with_rand_learn){
      model_ols<-target ~ d_ + p_+b_+c_ + p_:b_ + p_:c_ + b_:c_
      model_binary<-round(target) ~ d_ +p_+b_+c_ + p_:b_ + p_:c_ + b_:c_
    }else if(with_rand_barr){
      model_ols<-target ~ d_ + p_+b_ + p_:b_ 
      model_binary<-round(target) ~ d_ +p_+b_+ p_:b_ 
    }else if(with_rand_learn){
      model_ols<-target ~ d_ + p_+c_ + p_:c_
      model_binary<-round(target) ~ d_ +p_+c_ + p_:c_ 
    }
    table_for_tex<-regr_and_capt_output(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE, use_gamlss = TRUE, family=families_temp)
    counter<-counter+1
    
    descr_con_temp<-c(descr_temp, paste("\n Predictors", paste(c(colnames(p_), colnames(b_), colnames(macro_)), collapse=", "), "measured in ",p2))
    write(descr_con_temp,file=file_name,append=T)
    title_text<-paste("Target", targ,"in",p, "on", paste(c(colnames(p_), colnames(b_), colnames(macro_)),collapse=", "), "in", p2)
    if(with_rand_learn && with_rand_barr){
      model_ols<-target ~ d_ + p_+b_+c_ + p_:b_ + p_:c_ + b_:c_ + macro_
      model_binary<-round(target) ~ d_ +p_+b_+c_ + p_:b_ + p_:c_ + b_:c_ + macro_
    }else if(with_rand_learn){
      model_ols<-target ~ d_ + p_+c_  + p_:c_  + macro_
      model_binary<-round(target) ~ d_ +p_+c_ +  p_:c_ + macro_
    }else if(with_rand_barr){
      model_ols<-target ~ d_ + p_+b_+ p_:b_ + macro_
      model_binary<-round(target) ~ d_ +p_+b_ + p_:b_ + macro_
    }
    table_for_tex<-regr_and_capt_output(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE, use_gamlss = TRUE, family=families_temp)
    counter<-counter+1
    

  } # if pbc interaction
rm(model_ols, model_binary)
rm(title_text)

} # if use_normal_reg


