# This scripts is called by time series potting. Need "temp" as data frame 
# input. Activated if BLACK_WHITE

# If black and white plot, points are added to the lines. 
# If there are many observations, the only a subset of data should be used 
# (otherwise too many points). 
if(is.element(aggr, c("eco_conv_batch", "eco_conv_switch"))){
  if((exists("PTS")&&PTS==0)){
    temp<-temp[seq(12,length(temp[,1]), 12),]
  }else{ 
    i<-0
    for(type_temp in unique(temp$Type)){
      for(par_temp in unique(temp$Parameter)){
        i<-i+1
        inds_temp<-intersect(which(temp$Type == type_temp), which(temp$Parameter == par_temp))
        if(length(inds_temp)!= length(unique(temp$Periods[inds_temp]))){stop("Indexing did not work as intended")}
        temp1<-temp[inds_temp,]
        if(i==1){temp2<-temp1[seq(12,length(temp1[,1]), 12),]
        }else{temp2<-rbind(temp2,temp1[seq(12,length(temp1[,1]), 12),])}
      }
    }
    temp<-temp2
    rm(temp2, temp1, type_temp, par_temp, i, inds_temp)
    
  }
}else{
  temp<-temp[seq(3,length(temp[,1]), 3),]
}
#print("Black and white, plot of subset of data")
# 
