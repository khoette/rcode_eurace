#aggregation_levels_for_ts<-c("eco_conv_batch", "eco_conv_switch")
#aggregation_levels_for_ts<-c("batch")
#aggregation_levels_for_ts<-c("single_switch")
#
if(is.element("Firm", selected_agent) && (GET_DATA == 1 ||REG == 1)){
  print("Do not wonder, it will take a while to compile the data 
        at firm level -> 120 agents x 750 periods x 210 runs x XXvariables")
}
if(exists("study_all_agents") == TRUE && study_all_agents == 1){
  list_of_agents<-c("Eurostat", "Government", "Firm")
  print(paste("Study all agents: ", paste(list_of_agents, collapse = "; ")))
}else{
  list_of_agents<-selected_agent
  print(paste("Study only selected agent: ", paste(list_of_agents, collapse="; ")))
}
#
source(paste(script_dir,"/all_agents/regression_analyses/aux/functions_reg.r",sep=""))

if(setequal(parameters_tmp, parameter_values) == FALSE){
  print(paste("You have chosen the following selection of parameters, but not the full available set: ", paste(parameters_tmp, collapse=" ")))
}

if(exists("script_dir") == FALSE){
  stop(paste("You have forgotten to set the script dir"))
}
source(paste(experiment_directory,"/Configure.r", sep=""))
rm(Bank, batch_analysis, ClearingHouse, colored_lines, correlation, 
   cross_correlation_function, correlation_distribution,
   Eurostat, Firm, FirmFounder, frequency, Government, growth_rate, heat_maps, 
   heat_maps_2V, herfindahl, histogram, histogram_iteration_vector, IGFirm, 
   Mall, make_legend, multiple_time_series, parameter_analysis, quantiles, ratio, 
   root_directory, save_snapshots, scatter, time_series, transition_phase)


if(cut_transition==1){
  source(paste(script_dir,"/aux_all_agents/reset_some_variables_before_market_entry.r",sep=""))
}
# Get data: 
source(paste(script_dir,"/aux_all_agents/data_handling/data_handling.r",sep=""))

if(exists("VAR_ANALYSIS") && VAR_ANALYSIS){
  for(agent in list_of_agents){
    source(paste(script_dir,"/aux_all_agents/data_handling/add_user_defined_variables.r",sep=""))
  }
}
# # 
# # # START ANALYSIS # # # # # # # #
# # 
# 
# Plot: 
# 

if(exists("kind_of_data")!=TRUE){
  kind_of_data<-"logs" # Use logs as default
}
# 
if(exists("study_all_agents") == TRUE && study_all_agents == 1){
  list_of_agents<-c("Eurostat", "Government", "Firm")
  print(paste("Study all agents: ", paste(list_of_agents, collapse = "; ")))
}else{
  list_of_agents<-selected_agent
  print(paste("Study only selected agent: ", list_of_agents, collapse="; "))
}
#
if(exists("kind_of_data")!=TRUE){
  kind_of_data<-"logs" # Use logs as default
}
# Which data types are available? 
data_types_available<-c()
if(exists("parameter_names")!=TRUE){
  parameter_names<-parameters_tmp
}

for(i in c("levels", "ann_growth_rates", "normalized_by_init_avg", "normalized_by_per-run_avg", "Time_series", "Trend", "Cycle")){
  if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/",i, sep=""))){
    data_types_available<-c(data_types_available, i)
  }
}
rm(i)
# Make a selection of the data set to be analyzed, default: all
available_aggregation_levels<-c("single_large", "batch", "eco_conv_batch", "single_switch", "eco_conv_switch")
if(exists("with_rand_learn") && with_rand_learn == 1){
  available_aggregation_levels<-c(available_aggregation_levels, "batch_Learn_Type", "batch_Spill", "batch_Ease")
}
if(exists("aggregation_levels")==FALSE){
  aggregation_levels<-available_aggregation_levels
}
if(exists("aggregation_levels_for_ts") == FALSE){
  aggregation_levels_for_ts<-c("batch", "eco_conv_batch", "eco_conv_switch")
  if(exists("with_rand_learn") && with_rand_learn == 1){
    aggregation_levels_for_ts<-c(aggregation_levels_for_ts, "batch_Learn_Type", "batch_Spill", "batch_Ease")
  }
}
which_data_types<-match(c("levels"), data_types_available)
data_type<-data_types_available

# Simple time series, density, MTS
if(with_policy){
  list_of_agents_tmp<-unique(c(list_of_agents, "Government"))
}else{
  list_of_agents_tmp<-list_of_agents
}
d<-which_data_types[1]
p<-1
agent<-list_of_agents[1]
if(is.element(1, c(VALIDATION, TS, MTS, REG, DENS, SNAPSHOT, WILC_TYPES, CORR, SCAT, SUMMARY, REG_SPILL))){
for(d in which_data_types){
  if(is.element("normalized_by_per-run_avg", data_type) && data_type[d]=="normalized_by_per-run_avg"){
    list_of_agents_old<-list_of_agents_tmp
    aggregation_levels_old<-aggregation_levels
    aggregation_levels<-"single_large"
    if(is.element("Firm", list_of_agents_old) == TRUE){
      list_of_agents_tmp<-"Firm"
    }
  }
  
  for(p in 1:length(parameters_tmp)){
    par_analysis<-0
    parameter<-parameters_tmp[p]
    folder_name<-data_type[d]
    data_directory<-paste(experiment_directory,"/rdata/",kind_of_data,"/",data_type[d],"/",sep="")
    
    source(paste(script_dir, "/aux_all_agents/variables_settings_for_agents.r", sep=""))
    # in the validation.r script you may specify the variables 
    # you would like to study. Some default variables are already 
    # specified, i.e. those used in the paper. 
    if(VALIDATION){
      source(paste(script_dir, "/all_agents/validation.r", sep=""))
    }
    
    if(SUMMARY){
      source(paste(script_dir,"/aux_all_agents/create_directories_for_plotting.r",sep=""))
      
      source(paste(script_dir, "/all_agents/summary_info/summary.r", sep=""))
    }
    
    
    for(agent in list_of_agents_tmp){
      print(paste("Start analysis for: ", agent, parameter, data_type[d], sep=" "))
      if(agent == "Firm"){
        no_agents<-120
      }else{
        no_agents<-1
      }
      
      source(paste(script_dir,"/aux_all_agents/get_var_available_in_panel.r", sep=""))
      
      if(file.exists(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep="")) == FALSE){
        print(paste("Required data does not exist: ",data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
      }else{
        source(paste(script_dir,"/aux_all_agents/create_directories_for_plotting.r",sep=""))
        #
        
        if(exists("SNAPSHOT") && SNAPSHOT!=0){
          print(paste("Start snapshot analysis, e.g. distib. and boxplots: ", agent, parameter, data_type[d], sep=" "))
          source(paste(script_dir, "/all_agents/snapshot_analysis_wilcox_distr_etc.r", sep=""))
          source(paste(script_dir, "/all_agents/plot_functions/multiple_boxplots_ggplot.r", sep=""))
        } #  
        
        if(exists("TS") && TS==1){
          print(paste("Start TS:", parameter, data_type[d], sep=" "))
          source(paste(script_dir,"/all_agents/plot_functions/simple_time_series.r",sep=""))
          rm(list=ls(pattern="temp"))
        }
        #
        if(exists("DENS") && DENS!=0){
          print(paste("Start Hist Dens:", parameter, data_type[d], sep=" "))
          # Plot distributions: 4 types of plots for each data set: time series, density, density per run, histogram (relative frequency)
          source(paste(script_dir,"/all_agents/hist_and_dens_ggplot_temp_mat_eurostat.r", sep=""))
          rm(list=ls(pattern="temp"))
        }
        # 
        if(exists("MTS") && MTS!=0){
          # Multiple time series: 
          print(paste("Start MTS:", parameter, data_type[d], sep=" "))
          source(paste(script_dir,"/aux_all_agents/multiple_time_series_settings.r",sep=""))
          if(exists("mts_table")){
            source(paste(script_dir,"/all_agents/plot_functions/multiple_timeseries_eurostat.r",sep=""))
            rm(mts_table)
            rm(list=ls(pattern="temp"))
          }
        }
        
        
        if(exists("SCAT") && SCAT != 0){
          if((exists("with_rand_barr") && with_rand_barr == 1)||(exists("with_rand_policy") && with_rand_policy == 1 )|| (exists("with_rand_learn") && with_rand_learn == 1)){
            source(paste(script_dir,"/all_agents/plot_functions/scatterplot_rand_barrs_full.r", sep=""))
          }
        }
        
        if(exists("REG") && REG!=0){
          # Start simple regression analysis: 
          print(paste("Start simple reg:", parameter, data_type[d], sep=" "))
          par_analysis<-0
          if(exists("EE_revision") && EE_revision){
            #source(paste(script_dir,"/EE_revision/EE_scripts/init_cond_regr_new_test.r",sep=""))
            source(paste(script_dir,"/EE_revision/EE_scripts/init_cond_regr_new_.r",sep=""))
          }else{
            source(paste(script_dir,"/all_agents/init_cond_regr_new_test.r",sep=""))
          }
        }
        
        if(exists("REG_SPILL") && REG_SPILL!=0){
          # Start simple regression analysis: 
          print(paste("Start spill reg:", parameter, data_type[d], sep=" "))
          aggr<-"single_large"
          source(paste(script_dir,"/all_agents/regression_analyses/reg_test_spill_effect.r",sep=""))
        }

        
        if(exists("WILC_TYPES") && WILC_TYPES!=0){
          print(paste("Start wilcox type test: ", agent, parameter, data_type[d], sep=" "))
          source(paste(script_dir, "/all_agents/wilcox_types.r", sep=""))
        } 
        
        if(exists("CORR") && CORR!=0){
          print(paste("Start correlation analysis: ", agent, parameter, data_type[d], sep=" "))
          source(paste(script_dir, "/aux_all_agents/set_variable_pairs_for_correl.r", sep=""))
          for(pair in 1:3){
            if(pair==1 && exists("selected_var_pairs_macro")){
              selected_var_pairs<-selected_var_pairs_macro
              pair_caption<-"Selected macro economic variables"
              pair_title<-"macro"
              add_gov<-0
              rm(selected_var_pairs_macro)
            }else if(pair == 2 && exists("selected_var_pairs_eco")){
              selected_var_pairs<-selected_var_pairs_eco
              pair_caption<-"Selected technological variables"
              pair_title<-"techn"
              add_gov<-0
              rm(selected_var_pairs_eco)
            }else if(pair == 3 && exists("selected_var_pairs_policy")){
              selected_var_pairs<-selected_var_pairs_policy
              pair_caption<-"Selected policy variables"
              pair_title<-"policy"
              add_gov<-1
              rm(selected_var_pairs_policy)
            }
            if(exists("selected_var_pairs") && length(selected_var_pairs)>0){
              source(paste(script_dir, "/all_agents/correlation_betw_variable_pairs_temp_mat_eurostat.r", sep=""))
            }
            rm(pair_title, pair_caption, selected_var_pairs)
          }
        } 
        
        
      } # else if data exists
    } # end agent loop
  } # end parameter loop
  if(is.element("normalized_by_per-run_avg", data_type) && data_type[d]=="normalized_by_per-run_avg"){
    list_of_agents_tmp<-list_of_agents_old
    aggregation_levels<-aggregation_levels_old
    rm(list_of_agents_old, aggregation_levels_old)
  }
} # end data type
} # end if (is.element(1, c(VALIDATION, TS, MTS, REG, DENS, SNAPSHOT, WILC_TYPES, CORR, SCAT)))
#
#

# 
# Parameter analysis: 
# 
if(exists("parameter_bau")){
  parameters_tmp<-c(parameter_bau, parameters_tmp)
}

if(exists("exper_dir_bau") && exper_dir_bau != experiment_directory && exists("exper_name_bau") &&  exper_name_bau != experiment_name){
  parameters_tmp<-unique(parameters_tmp)
}
if((exists("exper_dir_bau") && exper_dir_bau != experiment_directory) || (exists("exper_name_bau") &&  exper_name_bau != experiment_name)){
  bau_in_diff_folder<-1
}else{
  bau_in_diff_folder<-0
}

if(with_policy){
  list_of_agents_tmp<-unique(c(list_of_agents, "Government"))
}else{
  list_of_agents_tmp<-list_of_agents
}

if(length(parameters_tmp)>1 && is.element(1, c(PWILC, PTS, PREG))){

  if(exists("plot_dir_selection")!=TRUE){
    source(paste(script_dir, "/aux_all_agents/create_directories_for_plotting.r", sep=""))
  }
  plot_dir_parameter<-paste(plot_dir_selection,"Parameter/", sep="")
  if(dir.exists(plot_dir_parameter)!=1){
    dir.create(plot_dir_parameter)
  }
  
  # create scenario names
  parameter_names<-rep(NA, length(parameters_tmp))
  for(p1 in 1:length(parameters_tmp)){
    p0<-parameters_tmp[p1]
    if(exists("bau_in_diff_folder")&&bau_in_diff_folder==1){
      if(p0 == parameter_bau && p1 == 1){
        parameter_names[p1]<-"Baseline"
      }else if(length(parameters_tmp) == 2){
        parameter_names[p1]<-"Experiment"
      }else{
        parameter_names[p1]<-parameter
      }
    }else if(experiment_name == "eco_policy_switch"){
      if(p0 == "3"){
        parameter_names[p1]<-"Investment"
      }else if(p0 == "4"){
        parameter_names[p1] <-"Consumption"
      }else if(p0 == "0"){
        parameter_names[p1] <-"Baseline"
      }else{
        parameter_names[p1] <- p0
      }
    }else{
      parameter_names[p1]<-p0
    }
  }
  rm(p0, p1)
  
  for(d in which_data_types){
    for(agent in list_of_agents_tmp){
      if(is.element("normalized_by_per-run_avg", data_type) && data_type[d]=="normalized_by_per-run_avg"){
        list_of_agents_old<-list_of_agents_tmp
        aggregation_levels_old<-aggregation_levels
        list_of_agents_tmp<-"Firm"
        aggregation_levels<-"single_large"
      } # end chekc normalized by per run avg
      folder_name<-data_type[d]
      data_directory<-paste(experiment_directory,"/rdata/",kind_of_data,"/",data_type[d],"/",sep="")
      if(exists("bau_in_diff_folder") && bau_in_diff_folder == 1){
        data_dir_bau<-paste(exper_dir_bau,"/rdata/",kind_of_data,"/",data_type[d],"/",sep="")
      }
      print(paste("Start parameter analysis for: ", agent))
      if(agent == "Firm"){
        no_agents<-120
      }else{
        no_agents<-1
      }
      print(paste("You study parameters ",paste(parameters_tmp,collapse=", "), "which are named as: ", paste(parameter_names,collapse="; ")))
      #stop("pts check")
      if(exists("parameter_bau") && length(parameter_bau)>0){
        parameter<-parameters_tmp[2]
      }else{
        parameter<-parameters_tmp[1]
      }
      if(file.exists(file=paste(data_directory,"data_batch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))) {
        load(file=paste(data_directory,"data_batch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
        
        if(exists(paste("variables_available_in_panel_",agent,sep=""))!=TRUE && file.exists(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))){
          load(file=paste(data_directory,"data_batch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
          variables<-colnames(object)[which(colnames(object) != "Periods")]
          variables<-variables[which(variables != "Run")]
          variables<-variables[which(variables != "Type")]
          variables<-variables[which(variables != "Number_Runs")]
          variables<-variables[which(variables != "id")]
          variables<-variables[which(variables != "Adopter_Type")]
          variables<-variables[which(variables != "min_learning_coefficient_var")]
          variables<-variables[which(variables != "learning_spillover_strength_var")]
          variables<-variables[which(variables != "Spill")]
          variables<-variables[which(variables != "Ease")]
          variables<-variables[which(variables != "Learn_Type")]
          eval(call("<-",(paste("variables_available_in_panel_",agent,sep="")),as.name(paste("variables"))))
          
        }
        if(exists("variables_selection") == FALSE || is.element(NA, match(variables_selection, colnames(object)))){
          variables_selection<-variables
        }
        rm(object)
      }
      if(exists("bau_in_diff_folder") && bau_in_diff_folder == 1){
        load(file=paste(data_dir_bau,"data_batch_",exper_name_bau,"_",parameter_bau,"_",agent,".RData",sep=""))
        vars_avail<-colnames(object)
        load(file=paste(data_directory,"data_batch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
        variables<-intersect(vars_avail, colnames(object))
        
        fix_vars_temp0<-c("Type", "id", "Periods", "Firm_Type", "Adopter_Type", 
                          "Parameter", "Run", "min_learning_coefficient_var", 
                          "learning_spillover_strength_var", "Number_Runs", "Spill", "Ease", "Learn_Type")
        
        variables<-variables[which(is.element(variables, fix_vars_temp0)==FALSE)]
        rm(object)
      }
      
      
      
      
      source(paste(script_dir,"/aux_all_agents/create_directories_for_plotting.r",sep=""))
      
      if(exists("PTS") && PTS==1){
        if(file.exists(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep="")) == FALSE){
          print(paste("Required data does not exist: ",data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
        }else{
          print("Parameter time series")
          source(paste(script_dir,"/all_agents/parameter/parameter_multiple_ts_eurostat.r",sep=""))
        }  
      }else{  # if PTS (parameter time series) == 1
        print("No parameter TS.")
      }
      
      if(exists("PWILC") && PWILC == 1){
        if(file.exists(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep="")) == FALSE){
          print(paste("Required data does not exist: ",data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
        }else{
          print("Start parameter wilcox analysis.")
          source(paste(script_dir,"/all_agents/parameter/wilcox_parameter.r",sep=""))
        }
      }else{
        print("No parameter wilcox.")
      }
      
      if(exists("PREG") && PREG == 1){
        if(file.exists(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep="")) == FALSE){
          print(paste("Required data does not exist: ",data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
        }else{
          par_analysis<-1
          print("Start parameter simple regression analysis.")
          source(paste(script_dir,"/all_agents/init_cond_regr_new_test.r",sep=""))
        }
      }else{
        print("No parameter regression.")
      }
      
      if(is.element("normalized_by_per-run_avg", data_type) && data_type[d]=="normalized_by_per-run_avg"){
        list_of_agents_tmp<-list_of_agents_old
        aggregation_levels<-aggregation_levels_old
        rm(list_of_agents_old, aggregation_levels_old)
      }
    } # agent in list of agents temp
  } # d in data types
} # if length parameters tmp > 1
#rm(list_of_agents_tmp)





