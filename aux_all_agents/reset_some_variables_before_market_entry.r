# Set eco technology related variables to NA for time before market entry
# 
day_market_entry<-600
if(exists("day_market_entry") == FALSE){
  stop("You should first define the day of market entry before resetting variables.... ")
}

for(agent in list_of_agents){
  
  if(agent=="Eurostat"){
    
    set_to_na_temp<-c("average_s_skill_eco","firm_average_productivity_eco", "market_share_eco", 
    "avg_ecoefficiency", "techn_frontier_eco", "total_capital_stock_units_eco", 
    "capital_price_index_eco", "total_units_used_capital_eco","capacity_utilization_eco",
    "avg_productivity_gap", "avg_age_eco_firms","avg_age_conv_firms", 
    "avg_output_eco_firms", "avg_output_conv_firms", "proxy_using_costs_eco_techn", "proxy_total_costs_eco_techn", 
    "price_highest_vintage_eco", "price_lowest_vintage_eco", "using_cost_difference_eco_conv_capital", 
    "price_diff_lowest_vintage", "price_per_productivity_unit_eco", "frontier_price_difference", 
    "price_per_productivity_unit_difference", "price_per_productivity_unit_difference_wage_ratio", 
    "price_per_productivity_unit_eco_wage_ratio", "price_per_productivity_unit_conv_wage_ratio", "price_diff_highest_vintage",
    "total_cost_difference_eco_conv_capital", "lowest_productivity_eco", "price_diff_highest_vintage_wage_ratio", 
    "price_diff_lowest_vintage_wage_ratio","avg_productivity_gap", "s_skill_ratio","frontier_productivity_ratio", 
    "price_ratio_frontier", "price_per_prod_unit_ratio_frontier", "degree_of_novelty_ratio", "base_tax_rate", 
    "skill_divergence", "frontier_divergence")
  
    set_to_value_at_entry_day_temp<-c("specific_skill_gap", "frontier_gap", 
                                      "skill_divergence", "frontier_divergence")    
  
    #cut_transition_phase<-c(set_to_na_temp, set_to_value_at_entry_day_temp)
    cut_transition_phase<-c(set_to_na_temp)
    
    eval(call("<-",(paste("cut_transition_phase_",agent,sep="")),as.name(paste("cut_transition_phase",sep=""))))
     
  }else if(agent=="Firm"){
    
    set_to_na_temp<-c()
    
    set_to_value_at_entry_day_temp<-c()    
    
    #cut_transition_phase<-c(set_to_na_temp, set_to_value_at_entry_day_temp)
    cut_transition_phase<-c(set_to_na_temp)
    
    eval(call("<-",(paste("cut_transition_phase_",agent,sep="")),as.name(paste("cut_transition_phase",sep=""))))
    
  }else if(agent == "Government"){
    
    set_to_na_temp<-c("base_tax_rate")
    
    #set_to_value_at_entry_day_temp<-c()    
    
    #cut_transition_phase<-c(set_to_na_temp, set_to_value_at_entry_day_temp)
    cut_transition_phase<-c(set_to_na_temp)
    
    eval(call("<-",(paste("cut_transition_phase_",agent,sep="")),as.name(paste("cut_transition_phase",sep=""))))
    
  }
  #else if(agent == "Firm")
  #  set_to_na_temp <- TBD NO ACCOUNT YET FOR MUTLIPLE AGENTS!! 
}
  




