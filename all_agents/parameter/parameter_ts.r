# Inputs: aggr, temp, value

if(exists(paste("cut_transition_phase_",agent, sep="")) && is.element(value, cut_transition_phase)){
  temp[which(as.vector(as.numeric(as.character(temp$Periods)))<(day_market_entry+20)),match(value, colnames(temp))]<-NA
} 

temp<-melt(temp, measure.vars = value)
temp<-temp[which(is.na(temp$value)==FALSE),]
if(length(temp[,1]) == 0){
  print(paste("Only NAs in data for var: ", value,aggr,agent))
  next
}
if(agent == "Eurostat" && (aggr == "single_large" || aggr == "single_switch") && is.element(NA, temp$Type) == TRUE){
  print(paste("That's strange: Why is type NA for Eurostat? ", aggr, data_type[d]))
}
if(is.element("Type", colnames(temp))){
  temp<-temp[which(is.na(temp$Type)==FALSE),]
}

min<-min(temp$value, na.rm=TRUE)
max<-max(temp$value, na.rm=TRUE)
median<-median(temp$value, na.rm=TRUE)
source(paste(script_dir,"/aux_all_agents/plot_settings/settings_axis_labels.r", sep=""))

if(exists("BLACK_WHITE") && exists("DIFFERENT_LINE_SHAPES") 
   && (BLACK_WHITE || DIFFERENT_LINE_SHAPES)){
  source(paste(script_dir,"/aux_all_agents/plot_settings/plot_subset_of_data.r", sep=""))
}

if(is.na(median)==FALSE && (abs(max)<abs(median)*1e+100 && abs(min) < abs(median)*1e+100) 
   || is.element(value, c("eco_tax_rate", "eco_investment_subsity", "eco_consumption_subsidy"))){
  
  pdf(paste(plot_dir_temp,"/",pdf_name_temp,sep=""))
  p1<-ggplot(temp, aes(x=reorder(Periods, as.numeric(Periods)), y=value))
  
  if(aggr == "single_large" || aggr ==  "single_switch"){
    plot<-p1+geom_path(aes(linetype=as.factor(Parameter), color = Type,group=interaction(Parameter,Run,value,Type)), alpha=alpha, lwd=lwd)
    #plot<-plot + geom_point(aes(shape = Parameter,group=interaction(Parameter,Run,value,Type)), alpha=alpha, lwd=lwd)
  }else if(aggr == "eco_conv_batch" || aggr == "eco_conv_switch"){
    if(exists("USE_OWN_COLOR_SETTINGS") && USE_OWN_COLOR_SETTINGS){
      plot<-p1+geom_path(aes(color=interaction(Type, Parameter), group=interaction(Type,Parameter)))
    }else if((length(parameter_names)>2 && aggr == "eco_conv_switch" )|| length(parameter_names)>=3  ){
      # If more than 3 parameter experiments, distinguish by color, not by line type. 
      # Line type is best if color indicates Type and disinction between only 
      # baseline and experiment. 
      # Line type is by default 1, if BLACK_WHITE it is used for parameter distinction. 
      if(is.numeric(temp$Parameter)){
        plot<-p1+geom_path(aes(color=Parameter,linetype=Type, group=interaction(Type,Parameter)))
        ADD_POINTS<-1
      }else if(length(unique(temp$Parameter))*length(unique(temp$Type)) > 5){
        plot<-p1+geom_path(aes(color=as.numeric(Type),linetype=as.factor(Parameter), group=interaction(Type,Parameter)))
      }else{
        plot<-p1+geom_path(aes(color=interaction(as.factor(Parameter), as.numeric(Type)),linetype=as.factor(Parameter), group=interaction(Type,Parameter)))
      }
    }else{
      if(is.numeric(temp$Parameter[1])){
        plot<-p1+geom_path(aes(color=Type, group=interaction(Type,as.factor(Parameter)), linetype = as.factor(Parameter)))
      }else{
        plot<-p1+geom_path(aes(color=Type, group=interaction(Type,Parameter), linetype = Parameter))
      }
    }
    #plot<-plot + geom_point(aes(shape = Parameter,group=interaction(Parameter,value,Type)), alpha=alpha, lwd=lwd)
  }else{ # aggr == batch
    if(is.numeric(temp$Parameter[1])){
      plot<-p1+geom_path(aes(color=as.factor(Parameter), linetype = as.factor(Parameter), group=as.factor(Parameter)))
    }else{
      plot<-p1+geom_path(aes(color=Parameter, linetype = Parameter, group=Parameter))
    }
    #plot<-plot + geom_point(aes(shape = Parameter,group=interaction(Parameter,value)), alpha=alpha, lwd=lwd)
  }
  
  source(paste(script_dir,"/aux_all_agents/plot_settings/modify_plot_pts.r", sep=""))
  print(plot)
  dev.off()
} # median test

