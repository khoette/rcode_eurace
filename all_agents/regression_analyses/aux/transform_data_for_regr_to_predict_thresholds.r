agent<-reg_level
if(file.exists(paste(experiment_directory,"/",agent,"data_on_last_sign_flip_in_diffusion_",parameter,".RData", sep="")) == F){
#source(paste(script_dir,"/all_agents/regression_analyses/aux/functions_reg.r",sep=""))

if(agent == "Eurostat"){
  share_var_name_temp<-"share_conv_capital_used"
  variables_temp<-c("share_conv_capital_used","s_skill_ratio","frontier_productivity_ratio")
}else if(agent=="Firm"){
  share_var_name_temp<-"share_conventional_vintages_used"
  variables_temp<-c("share_conventional_vintages_used")
}

if(file.exists(paste(data_from,agent,"_data_avg_",parameter,".RData",sep=""))){
  load(file= paste(data_from,agent,"_data_avg_",parameter,".RData",sep=""))
  load(file=paste(data_from,agent,"_diffs_",parameter,".RData",sep=""))
  
  if(agent=="Firm"){
    variables_temp<-c("skill_ratio", "technology_ratio", "productivity_ratio", "share_conventional_vintages_used")
  }
  variables_temp<-variables_temp[which(!is.na(match(variables_temp, colnames(data_avg))))]
  cols<-match(variables_temp, colnames(data_avg))
  
}else{
#p0<-rep[p00]
#aggr<-"single_large"
#if(exists("bau_in_diff_folder") && bau_in_diff_folder == 1 && p00 == 1 && par_analysis){
#  data_from<-paste(data_dir_bau,"data_", aggr, "_",exper_name_bau,"_",p0,"_",sep="")
#}else{
  if(exists("aggr") != T){aggr<-"single_large"}
  if(exists("p0") != T){p0<-parameters_tmp[1]}
  
  data_from<-paste(data_directory,"data_", aggr, "_",experiment_name,"_",p0,"_",sep="")
#}
  load(file=paste(data_from,agent,".RData",sep=""))

if(agent=="Firm"){
  skill_ratio<-object$mean_spec_skills_conv/object$mean_spec_skills_eco
  technology_ratio<-object$technology_conv/object$technology_eco
  productivity_ratio<-object$firm_productivity_conv/object$firm_productivity_eco
  variables_temp<-c("skill_ratio", "technology_ratio", "productivity_ratio", "share_conventional_vintages_used")
  object<-cbind(object, skill_ratio)
  object<-cbind(object, technology_ratio)
  object<-cbind(object, productivity_ratio)
  rm(list=ls(pattern=("_ratio")))
}


## Input: object (data frame in single run format)
## Output: reg_data_temp
## This file computes year running averages for variables_temp
variables_temp<-variables_temp[which(!is.na(match(variables_temp, colnames(object))))]
vars<-match(c("id","Type","Firm_Type", "Adopter_Type", "Run", "Periods", variables_temp), colnames(object))
vars<-unique(vars[is.na(vars)==FALSE])
temp_data<-object[,vars]
rm(object)
vars<-match(c(variables_temp), colnames(temp_data))
vars<-unique(vars[is.na(vars)==FALSE])
for(col in vars){
  temp_data[,col]<-as.numeric(temp_data[,col])
}
rm(col, vars)

fix_vars_temp<-match(c("Parameter","id", "Periods", "Type", "Firm_Type", "Adopter_Type", "Run"), colnames(temp_data))
fix_vars_temp<-unique(fix_vars_temp[which(is.na(fix_vars_temp)== FALSE) ])

if(no_agents==1 || is.element("id", colnames(temp_data)) == FALSE){
  temp_data<-temp_data[order(as.numeric(as.character(temp_data$Run)), as.numeric(as.character(temp_data$Periods))),]
}else{
  temp_data<-temp_data[order(as.numeric(as.character(temp_data$Run)), as.numeric(as.character(temp_data$id)), as.numeric(as.character(temp_data$Periods))),]
}
runs_temp<-temp_data$Run
unique_runs_temp<-unique(runs_temp)
if(agent == "Firm"){
  ids_temp<-as.numeric(as.character(temp_data$id))
}
    
# Smooth everything as moving avarage over freq_temp window (e.g. if 240 ~ 1 year)
cols<-match(variables_temp, colnames(temp_data))
cols<-unique(cols[!is.na(cols)])
c<-cols[1]
r<-unique_runs_temp[1]
for(c in cols){
  if(agent == "Firm"){
    print(paste("data_avg for ", colnames(temp_data)[c]))
  }
  temp_data_col<-temp_data[,c]
  empty_temp_data_col<-rep(NA, length(temp_data_col))
  for(r in unique_runs_temp){
    inds_r_temp<-which(!is.na(match(runs_temp, r)))
    if(agent == "Firm"){
      #print(r)
      data0<-as.numeric(as.character(temp_data_col[inds_r_temp]))
      iter_temp<-length(unique(ids_temp[inds_r_temp]))
      inds_id_temp<-unique(ids_temp[inds_r_temp])
    }else{
      data<-as.numeric(as.character(temp_data_col[inds_r_temp]))
      iter_temp<-1
    }
    i_temp<-1
    for(i_temp in c(1:iter_temp)){
      if(agent == "Firm" && iter_temp > 1){
        id_temp<-inds_id_temp[i_temp]
        inds_id_r_temp<-inds_r_temp[which(ids_temp[inds_r_temp] == id_temp)]
        data<-temp_data_col[inds_id_r_temp]
      }else{inds_id_r_temp <- inds_r_temp}
      if(length(data) == number_xml){
        replace_temp<-c(rep(NA, 41)) # first entries (until market entry) as NA
        for(i in 31:(number_xml-11)){ # Period 620 until 750-11
          mean_temp<-mean(data[c(i:(i+11))], na.rm=TRUE) # Average of following 12 months. 
          replace_temp<-c(replace_temp, mean_temp)
        }
        empty_temp_data_col[inds_id_r_temp] <- replace_temp
      }else{
        stop("length of inds r temp != number xml. Error")
      }
    } # end i_temp in c(1:iter_temp) # should be number of agents
  } # end r in runs_unique_temp
  if(c == cols[1]){
    data_avg<-cbind(temp_data[,fix_vars_temp], empty_temp_data_col)
  }else{
    data_avg<-cbind(data_avg, empty_temp_data_col)
  }
} # c in cols
colnames(data_avg)<-colnames(temp_data)[c(fix_vars_temp, cols)]
rm(temp_data)
cols<-match(variables_temp, colnames(data_avg))
cols<-cols[which(!is.na(cols))]
# Transform NaN to NA
for(c in cols){data_avg[which(is.nan(data_avg[,c])),c]<-NA}
# Get first differences (obs_t - obs_{t-1}). 
# Needed to get sign of FD and to extract data with sign-flips 
# which is an indication of unstable patterns of technology use
# 
  diffs<-data_avg
  for(c in cols){
    if(agent == "Firm"){
      print(paste("diff_data for ", colnames(diffs)[c]))
    }
    data_temp<-diffs[,c]
    data[2:length(data_temp)]<-data_temp[2:length(data_temp)]-data_temp[1:(length(data_temp)-1)]
    diffs[,c]<-data_temp
  }
  rm(data_temp)
  # Save data if to be used later
  save(data_avg, file = paste(data_from,agent,"_data_avg_",parameter,".RData",sep=""))
  save(diffs, file= paste(data_from,agent,"_diffs_",parameter,".RData",sep=""))
}# end if file.exists == FALSE


share_inds<-match(share_var_name_temp, colnames(data_avg))
# Find sign-flips in diffusion data. 
signs_temp<-sign(diffs[,share_inds])

sign_flip<-signs_temp[1:(length(signs_temp)-1)]-signs_temp[2:(length(signs_temp))]
inds_flip<-which(sign_flip != 0)
rm(sign_flip,signs_temp)

# Extract run ids of sign flip -> target: Use "sign-flip" time as dependent variable for 
# regression on spillover parameters. 
# Further: Use threshold level of barriers as var to be explained by combi of initial 
# barriers and spill parameters. 
# 
data_avg_flip<-data_avg
pers_temp<-unique(data_avg$Periods)
# Last obs. is NA because in computation of inds_flip not account for run-id data subsets, 
# i.d. also diff's between diff. runs and ids computed.
data_avg_flip[which(data_avg_flip$Periods == pers_temp[length(pers_temp)]),cols]<-NA
if(length(inds_flip)>0){data_avg_flip<-data_avg_flip[inds_flip,]}
#
#
# Reduce data set to those where not yet convergence, i.e. remove all data with 
# less than 97.5 pct. of one technology type. 
# That is done because I am interested in the "last sign flip" in the diffusion 
# curve that is observed and where the technological state has not yet stabilized. 
inds_remove<-c(which(is.na(data_avg_flip[,share_inds])), 
               which(data_avg_flip[,share_inds] > 0.975),
               which(data_avg_flip[,share_inds] < 0.025))
#View(data_avg_flip[inds_remove,])
data_flip_not_converged<-data_avg_flip[-inds_remove,]
# Diffs are "within-run" diffs (and for Firm agent "within-agent" diff's)
#diffs_not_converged<-diffs[-inds_remove,]
rm(inds_remove, diffs)

data_temp<-data_flip_not_converged
#data_temp<-data_avg_flip
# Get data frame with "last-sign-flip" data: 
runs_temp<-unique(data_temp$Run)
r<-runs_temp[1]
i<-1
if(agent=="Firm"){
  iterate_temp<-unique(data_temp$id)
}
if(is.element(NA, runs_temp)){stop("NA is element of runs_temp")}
for(r in runs_temp){
  inds_r<-which(data_temp$Run == r)
  if(agent == "Firm"){
    print(r)
    data_temp0<-data_temp[inds_r,]
    ids_temp<-unique(data_temp0$id)
    iter_temp<-length(ids_temp)
  }else{
    iter_temp<-1
    temp<-data_temp[inds_r,]
  }
  for(i in 1:iter_temp){
    if(agent=="Firm"){
      temp<-data_temp0[which(data_temp0$id == ids_temp[i]),]
    }
    share_temp<-temp[,share_inds]
    type_temp<-temp$Type[1]
    if(!is.na(type_temp)){
      if(type_temp == "conv"){
        inds_select<-which(share_temp == min(share_temp,na.rm=T))
      }else if(type_temp == "eco"){
        inds_select<-which(share_temp == max(share_temp,na.rm=T))
      }
      inds_select<-inds_select[length(inds_select)]
      #last_entry_temp<-inds_id_r[length(inds_id_r)]
    }else{# na check, i.e. firm not active
      stop()
      inds_select<-inds_id_r[1]
    } 
      if(i == 1 && r == runs_temp[1]){
        data_last_flip<-temp[inds_select, ]
      }else{
        data_last_flip<-rbind(data_last_flip,temp[inds_select, ])
      }
    
   
    #if(as.numeric(as.character(data_last_flip$Periods[nrow(data_last_flip)])) < max(as.numeric(as.character(data_temp$Periods[inds_id_r])), na.rm=T)){
    #  stop("You did not select the last period element")
    #}
  }# in in iter_temp
}# r in runs_temp
rm(list=ls(pattern="inds_"))

# Now, I have a data frame with the observations of the last sign flip in 
# data. 
# It might happen that data is not complete, i.e. not every run-id is covered. 
# This occurs if the diffusion curve is "flat", i.e. there is no sign flip in 
# the "non-convergence-subset" defined by 2.5% stability threshold. 
# This may also occur when using firm data and firms are not active. 
# This needs to be corrected.
if((agent=="Eurostat" && length(unique(data_last_flip$Run)) < runs)
   ||(agent == "Firm" && length(unique(data_last_flip$id)) < length(unique(data_avg$id)))){
  if(agent=="Firm"){
    ids_temp<-unique(data_last_flip$id)
    all_ids_temp<-unique(data_avg$id)
    missing_temp<-setdiff(all_ids_temp,ids_temp)
    add_temp<-data_avg[which(data_avg$id %in% missing_temp),]
    missing_runs_temp<-unique(add_temp$Run)
  }else if(agent=="Eurostat"){
    runs_temp<-unique(data_last_flip$Run)
    all_runs_temp<-unique(data_avg$Run)
    missing_runs_temp<-setdiff(all_runs_temp,runs_temp)
    rm(all_runs_temp, runs_temp)
    add_temp<-data_avg[which(data_avg$Run %in% missing_runs_temp),]
  }
  if(length(missing_runs_temp)>0){
    for(r in missing_runs_temp){
      inds_r_temp<-which(add_temp$Run == r)
      temp0<-add_temp[inds_r_temp,]
      if(agent=="Eurostat"){iterate<-1
      }else if(agent=="Firm"){
        print(r)
        ids_temp<-unique(temp0$id)
        iterate<-length(ids_temp)
      }
      for(i in 1:iterate){
        if(agent=="Firm"){
          id_temp<-ids_temp[i]
          inds_id_r_temp<-which(temp0$id == id_temp)
          temp<-temp0[inds_id_r_temp,]
          share_temp<-temp[, share_inds]
        }else if(agent=="Eurostat"){
          temp<-temp0
          share_temp<-temp[,share_inds]
        }
        if(length(share_temp[which(!is.na(share_temp))]) > 0){
          if(temp$Type[1] == "conv"){
            # if regime type conv: Select period with min share, which corresponds 
            # to last turning point.  
            min_temp<-min(share_temp, na.rm=T)
            ind_temp<-which(share_temp == min_temp)
          }else if(temp$Type[1] == "eco"){
            #stop("You need to define a strategy for eco regimes...")
            max_temp<-max(share_temp, na.rm = T)
            ind_temp<-which(share_temp == max_temp)
          }else if(temp$Type[1] == NA){
            ind_temp<-1
          }else{
            stop("What happened now?")
          }
          ind_temp<-max(ind_temp)
        }else{ # If share_temp consists only of NA
          ind_temp<-1
        }
        if(i == 1 && r == missing_runs_temp[1]){
          to_add_temp<-temp[ind_temp,]
        }else{
          to_add_temp<-rbind(to_add_temp, temp[ind_temp,])
        }
      } # i in iterate
    }#   r in missing_runs_temp
    data_last_flip<-rbind(data_last_flip, to_add_temp)
    rm(to_add_temp)
  } # if length(missing_runs_temp) > 0
  if(agent=="Eurostat"){data_last_flip<-data_last_flip[order(data_last_flip$Run),]
  }else if(agent=="Firm"){data_last_flip<-data_last_flip[order(data_last_flip$Run, data_last_flip$id),]}
}
rm(data_avg)

save(data_last_flip, file=paste(experiment_directory,"/",agent,"data_on_last_sign_flip_in_diffusion_",parameter,".RData", sep=""))

# Interesting now: Threshold levels of skill and prod ratio, time period. 
# These values can be used in regression analyses to be explained by spillover 
# parameters and by policy. 
rm(list=ls(pattern="temp"))
rm(list=ls(pattern="inds"))
}else{
  print(paste("Transformed data already existing: ", paste(experiment_directory,"/",agent,"data_on_last_sign_flip_in_diffusion_",parameter,".RData", sep="")))
  load(paste(experiment_directory,"/",agent,"data_on_last_sign_flip_in_diffusion_",parameter,".RData", sep=""))
}