
# Now, compute table with cross correl betw. cycle_full columns 
# and output_temp at diff. time lags
# Structure of the data: Long table with Run * Periods entries. I need to 
# ensure that correl tables are computed pair wise, i.e. match same runs. 
# 
# 
vars_temp<-c("monthly_output", "total_consumption_budget","monthly_sold_quantity",
             "unemployment_rate", "no_vacancies", "monthly_investment_value", 
             "price_index", "average_wage", "total_debt", "monthly_inflation_rate", 
             "firm_average_productivity", "real_investment", 
             "ig_sold_quantity", "price_eco", "firm_average_wage", 
             "average_mark_up"  )

load(paste(data_directory,"cycle_bk_var_8_32.RData", sep=""))
load(paste(data_directory,"trend_bk_var_8_32.RData", sep=""))
load(paste(data_directory,"series_bk_var_8_32.RData", sep=""))

## Choose variables to compute cross correl table: 
vars_temp<-c("monthly_output", "total_consumption_budget","monthly_sold_quantity",
             "unemployment_rate", "no_vacancies", "monthly_investment_value", 
             "price_index", "average_wage", "total_debt", "monthly_inflation_rate", 
             "firm_average_productivity", "real_investment", 
             "ig_sold_quantity", "price_eco", "firm_average_wage", 
             "average_mark_up"  )

rownames_temp<-c("Output","Consumption", "Sold Quantity", "Unemployment", "Vacancies", 
                 "Investment (value)", "Price", "Wage", "Debt", 
                 "Inflation", "Productivity", "Investment (real)", 
                 "Investment (quantity)", "Price eco", "Firm avg. wage", 
                 "Mark up")

cols_temp<-match(vars_temp, colnames(cycle_full))
vars_temp<-vars_temp[which(!is.na(cols_temp))]
rownames_temp<-rownames_temp[which(!is.na(cols_temp))]
cols_temp<-cols_temp[which(!is.na(cols_temp))]

temp<-cycle_full
temp<-temp[order(as.numeric(as.character(temp$Run)), as.numeric(as.character(temp$Periods))),]
output_temp<-temp[,match("monthly_output", colnames(temp))]

lags <- c(-4:4)
empty_table <- array(NA, dim = c(length(vars_temp), length(lags)))
rownames(empty_table) <- rownames_temp
colnames_temp <- c("t-4", "t-3", "t-2", "t-1", "0", "t+1", "t+2", "t+3", "t+4")
colnames(empty_table) <- colnames_temp
empty_table<-as.data.frame(empty_table)
empty_table_sd<-empty_table
t1<-1
len_temp<-length(output_temp)
runs_temp<-unique(temp$Run)

v<-vars_temp
# Besser: Get run wise average and std! 
runs_data_temp<-temp[,match(c("Run"), colnames(temp))]
periods_data_temp<-temp[,match("Periods", colnames(temp))]
output_temp<-temp[,match("monthly_output", colnames(temp))]
len_temp<-number_xml
for(t1 in 1:length(lags)){
  t<-lags[t1]
  corrs<-c()
  sds<-c()
  for(v in vars_temp){
    col <- match(v, colnames(temp))
    x_temp<-temp[,col]
    correls_r<-c()
    for(r1 in 1:length(runs_temp)){
      r<-runs_temp[r1]
      inds_temp<-which(runs_data_temp == r)
      inds_output_temp<-inds_temp+t
      inds_x_temp<-inds_temp
      if(t<0){
        use_temp<-which(inds_output_temp > len_temp*(r1-1))
      }else if(t > 0){
        use_temp<-which(inds_output_temp <= (len_temp*r1))
      }
      inds_output_temp<-inds_output_temp[use_temp]
      inds_x_temp<-inds_x_temp[use_temp]
      x <- x_temp[inds_x_temp]
      output_lag_temp<-output_temp[inds_output_temp]
      correl_r<-cor(x, output_lag_temp, use="pairwise.complete.obs")
      correls_r<-c(correls_r, correl_r)
    }
    correl<-mean(correls_r, na.rm=TRUE)
    correl_sd<-sd(correls_r, na.rm=TRUE)
    corrs<-c(corrs, correl)
    sds<-c(sds, correl_sd)
  }
  empty_table[,t1]<-paste(round_customized(corrs, digit=3))
  empty_table_sd[,t1]<-paste("(",round_customized(sds,digit=3),")",sep="")
} 

rm(t, t1, x, col, correl, corrs)
#rm(list=ls(pattern="_temp"))
rownames(empty_table_sd)<-NULL
rownames(empty_table)<-rownames_temp

for(row in 1:length(empty_table[,1])){
  if(row == 1){
    table<-empty_table[row,]
    table<-rbind(table, empty_table_sd[row,])
  }else{
    table<-rbind(table, empty_table[row,])
    table<-rbind(table, empty_table_sd[row,])
  }
}
tab<-xtable(table, digits=4)
filename_temp<-paste(plot_dir_validation,"cross_correl_table_with_lagged_output_cycle.tex")
write(paste("documentclass{standalone} \n begin{document}"), file = filename_temp, append=FALSE)

write(paste("Table with cross correl betw. macro vars and lagged output, all data as cyle element of bandpass filtered TS data\n\n"), file = filename_temp, append=TRUE)
write(print(tab), file = filename_temp, append=TRUE)
write(paste("end{document}"), file = filename_temp, append=TRUE)
rm(empty_table, empty_table_sd, tab, table)
if(1==2){
# Besser: Get run wise average and std! 
for(t1 in 1:length(lags)){
  t<-lags[t1]
  if(t!=0){
    if(t<0){
      remove<-c(0:(-t))
      remove_2<-c((len_temp+1+t):len_temp)
    }else{
      remove<-c((len_temp+1-t):len_temp)
      remove_2<-c(0:t)
    }
    output_lag_temp<-output_temp[-remove]
    cycle_temp<-temp[-remove_2,]
  }else{
    output_lag_temp<-output_temp
    cycle_temp<-temp
  }
  
  v<-vars_temp[1]
  corrs<-c()
  for(v in vars_temp){
    col <- match(v, colnames(temp))
    x <- cycle_temp[,col]
    correl<-cor(x, output_lag_temp, use="pairwise.complete.obs")
    corrs<-c(corrs, correl)
  }
  empty_table[,t1]<-corrs
} 

}
