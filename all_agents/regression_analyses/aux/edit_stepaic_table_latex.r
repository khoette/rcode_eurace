OTHER_NAMES<-T
use_rounded_type_hat<-0
if(with_rand_policy && with_rand_barr && use_tax_spill_skill!=T){manual_selection<-T
}else{manual_selection<-F} # make manual column selection
manual_selection<-T
source(paste(script_dir,"/aux_all_agents/plot_settings/settings_axis_labels.r", sep=""))

# transform table_for_stepaic to latex table
reg_level<-agent
source(paste(script_dir, "/aux_all_agents/plot_settings/settings_axis_labels.r", sep=""))
#if(exists("plot_dir_temp_aic")!=T){
  if(exists("plot_dir_selection")!=T){source(paste(script_dir,"/aux_all_agents/create_directories_for_plotting.r", sep=""))}
  plot_dir_temp_aic<-paste(plot_dir_selection,"Regression_on_spills/",agent,"_level_of_analysis/stepAIC_results/",sep="")
#}


name_extension<-c()
if(exists("use_2sls_manually") && use_2sls_manually){name_extension<-c(name_extension, "manual_2sls")}
if(exists("use_rounded_type_hat") && use_rounded_type_hat){name_extension<-c(name_extension, "rounded_type")}

use_tax_spill_skill<-F
if(use_tax_spill_skill){
  filename_temp<- paste(plot_dir_temp_aic,"/",exp_temp,"latex_tab_",agent,"_tax_skill_spill",paste("_",paste(name_extension,collapse="_")),"_sq",sep="")
}else{
  filename_temp<-paste(plot_dir_temp_aic,"/",exp_temp,"latex_tab_",agent,"_regr",paste("_",paste(name_extension,collapse="_")),".tex",sep="")
}

data_temp<-paste(plot_dir_temp_aic,exp_temp,reg_level,"_testAIC_",paste(name_extension,collapse="_"),".RData",sep="")
if(reg_level == "Firm"){
  data_temp<-paste(plot_dir_temp_aic,reg_level,"_testAIC_",paste(name_extension,collapse="_"),".RData",sep="")
}
if(file.exists(data_temp)!=T){
  print("You do not have the data you want to work with. Run reg_test_spill_effect.r first!")
}else{
  # load table_for_stepaic
  load(data_temp)
  
  if(agent == "Firm" && is.element("skill_micro", rownames(table_for_stepaic))){
    # Use of skill_macro and micro is mutually exclusive per model specification. 
    # Here, I exclude skill_micro-models from table, i.e. use only skill_macro
    cols_macro_barr_temp<-which(is.na(table_for_stepaic[which(rownames(table_for_stepaic) == "skill_micro"),]))
    table_for_stepaic_all<-table_for_stepaic
    if(length(cols_macro_barr_temp)%%2!=0){stop("Revise cols_macro_barr_temp manually")}
    cols_macro_barr_temp<-cols_macro_barr_temp[-which(cols_macro_barr_temp == 25)]
    table_for_stepaic<-table_for_stepaic[,cols_macro_barr_temp]
  }
  nrows_temp<-nrow(table_for_stepaic)
  ncols_temp<-ncol(table_for_stepaic)
  
  info_on_cols<-(colnames(table_for_stepaic))
  
  # Rsq col is empty for ivglm. I replace NA by rsq from manual 2sls, i.e. type fitted by probit regression 
  # and inserted in main equation. Residuals and variation of target var used to compute rsq. 
  r<-match("Rsq", rownames(table_for_stepaic))
  for(s in c("share_conv", "Time", "frontier_ratio", "prod_ratio", "techn_ratio", "skill_ratio", "cap_ratio", "variance_share_full")){
    cols<-which(grepl(s, colnames(table_for_stepaic)))
    if(length(cols)>0){
      t<-table_for_stepaic[,cols]
      na_cols<-which(is.na(t[r,]))
      lm_cols<-which(grepl("lm ", colnames(t)))
      counter<-0
      while(length(na_cols)>0){
        c<-lm_cols[which(lm_cols<na_cols[1])]
        t[r,na_cols[1]]<-t[r,c[length(c)]]
        lm_cols<-lm_cols[which(lm_cols > na_cols[1])]
        na_cols<-which(is.na(t[r,]))
        counter<-counter+1
        if(counter>10){
          stop("Most likely error")
        }
      }
      table_for_stepaic[,cols]<-t
    }
  }
    
  na_cols<-which(is.na(table_for_stepaic[r,]))
  
  lm_cols<-which(grepl("lm ", colnames(table_for_stepaic)))
  lm_cols<-which(!grepl("ivglm", colnames(table_for_stepaic)[lm_cols]))
  
  #stop("Make a decision about the columns you want to have")
  # 
  
  if(agent=="Firm"){
    cols_temp<-c()
    cols_to_select<-c("lm  share_conventional_vintages_used", 
                      "gamlss  share_conventional_vintages_used", "ivglm  Time", 
                      "ivglm  abs_crit_prod_ratio", "ivglm  abs_crit_skill_ratio", 
                      "ivglm  variance_share_full")
    cols_to_select_test<-c("lm  share_conventional_vintages_used", 
                           "gamlss  share_conventional_vintages_used", 
                           "ivglm  Time", "ivreg  Time",
                           "ivglm  abs_crit_prod_ratio", "ivreg  abs_crit_prod_ratio",
                           "ivglm  abs_crit_skill_ratio", "ivreg  abs_crit_skill_ratio", 
                           "ivglm  variance_share_full","ivreg  variance_share_full")
    if(use_rounded_type_hat){
      cols_to_select<-c("lm  share_conventional_vintages_used", 
                        "gamlss  share_conventional_vintages_used", 
                        "ivreg  Time","ivreg  abs_crit_prod_ratio",
                        "ivreg  abs_crit_skill_ratio", "ivreg  variance_share_full")
    }
    if(exists("use_tax_spill_skill") && use_tax_spill_skill){
      cols_to_select<-c(
       # "lm spill only share_conventional_vintages_used", 
      #  "lm int only share_conventional_vintages_used", 
      #  "lm skill only share_conventional_vintages_used", 
      #  "lm tax only share_conventional_vintages_used", 
                        "gamlss spill only share_conventional_vintages_used", 
                        "gamlss int only share_conventional_vintages_used", 
                        "gamlss skill only share_conventional_vintages_used", 
                        "gamlss tax only share_conventional_vintages_used", 
                        "ivglm spill only variance_share_full", 
                        "ivglm int only variance_share_full",
                        "ivglm skill only variance_share_full", 
                        "ivglm tax only variance_share_full", 
                        
                        "ivglm spill only Time",
                        "ivglm int only Time",
                        "ivglm skill only Time",
                        "ivglm tax only Time",
                        "ivglm spill only abs_crit_cap_ratio",
                        "ivglm int only abs_crit_cap_ratio",
                        "ivglm skill only abs_crit_cap_ratio",
                        "ivglm tax only abs_crit_cap_ratio",
                        "ivglm spill only abs_crit_prod_ratio",
                        "ivglm int only abs_crit_prod_ratio",
                        "ivglm skill only abs_crit_prod_ratio",
                        "ivglm tax only abs_crit_prod_ratio",
                        "ivglm spill only abs_crit_skill_ratio",
                        "ivglm int only abs_crit_skill_ratio",
                        "ivglm skill only abs_crit_skill_ratio",
                        "ivglm tax only abs_crit_skill_ratio")
    }
    for(c in 1:length(info_on_cols)){
      c1<-info_on_cols[c]
      if(is.element(c1, cols_to_select)){cols_temp<-c(cols_temp, c)}
    }
    #stop("col selection")
    if(length(cols_temp) != length(cols_to_select) && use_tax_spill_skill != T){
      warning("Adjust you col selection")
      if(exists("manual_selection") && manual_selection){
        if(with_rand_policy && with_rand_barr){
          cols_temp<-c(3,4,24,59,129,164)
        }
        warning(print("Verify whether you want the following:", paste(cols_temp, collapse=" ")))
        
      }else if(length(cols_temp) == (3*length(cols_to_select)-2)){
        if(length(cols_temp) == 16){
          cols_temp<-cols_temp[c(3,4,7,10,13,16)]
        }else{stop()}
      }else if(setequal(unique(info_on_cols[cols_temp]), cols_to_select)){
        c<-cols_temp[order(cols_temp, decreasing=T)]
        c1<-c[match(cols_to_select,info_on_cols[c])]
        cols_temp<-c1[order(c1)]
      }else if(length(cols_temp) == 2*length(cols_to_select)){
        if(length(info_on_cols) == 24){
          cols_temp<-cols_temp[c(3,4,6,8,10,12)]
        }else if(length(cols_temp) == 12){
          cols_temp<-cols_temp[c(3,4,6,8,10,12)]
        }
      }
    }
    #if(with_rand_barr && with_rand_policy && with_rand_learn){
    #  cols_temp<-c(3,4,14,24,44,49)
    #  if(!setequal(info_on_cols[cols_temp], cols_to_select)){stop("You really should adjust your col selection.")}
    #}
    if(exists("use_tax_spill_skill") && use_tax_spill_skill){
      cols_temp<-cols_temp[match(cols_to_select, colnames(table_for_stepaic)[cols_temp])]
    }
    rm(cols_to_select, c, c1)
  }else if(agent=="Eurostat"){
    cols_temp<-c()
    cols_to_select<-c("lm  share_conv_capital_used", 
                      "gamlss  share_conv_capital_used", "ivglm  Time", 
                      "ivglm  abs_crit_frontier_ratio", "ivglm  abs_crit_skill_ratio", 
                      "ivglm  variance_share_full")
    if(is.element("ivglm abs_crit_frontier_ratio", info_on_cols)){
      cols_to_select<-c(cols_to_select, "lm_abs_crit_frontier_ratio")}
    if(is.element("ivglm abs_crit_skill_ratio", info_on_cols)){
      cols_to_select<-c(cols_to_select, "lm_abs_crit_skill_ratio")}
    
    if(exists("use_tax_spill_skill") && use_tax_spill_skill){
      cols_to_select<-c(
        #"lm spill only share_conv_capital_used", 
        #                "lm skill only share_conv_capital_used", 
        #                "lm tax only share_conv_capital_used", 
                        "gamlss spill only share_conv_capital_used", 
                        "gamlss int only share_conv_capital_used", 
                        "gamlss skill only share_conv_capital_used", 
                        "gamlss tax only share_conv_capital_used", 
                        "ivglm spill only abs_crit_cap_ratio",
                        "ivglm int only abs_crit_cap_ratio",
                        "ivglm skill only abs_crit_cap_ratio",
                        "ivglm tax only abs_crit_cap_ratio", 
                        "ivglm spill only variance_share_full", 
                        "ivglm int only variance_share_full", 
                        "ivglm skill only variance_share_full", 
                        "ivglm tax only variance_share_full", 
                        "ivglm spill only Time",
                        "ivglm int only Time",
                        "ivglm skill only Time",
                        "ivglm tax only Time",
                        "ivglm spill only abs_crit_frontier_ratio", 
                        "ivglm int only abs_crit_frontier_ratio", 
                        "ivglm skill only abs_crit_frontier_ratio",
                        "ivglm tax only abs_crit_frontier_ratio",
                        "ivglm spill only abs_crit_skill_ratio",
                        "ivglm int only abs_crit_skill_ratio",
                        "ivglm skill only abs_crit_skill_ratio",
                        "ivglm tax only abs_crit_skill_ratio")
    }
    for(c in 1:length(info_on_cols)){
      c1<-info_on_cols[c]
      if(is.element(c1, cols_to_select)){cols_temp<-c(cols_temp, c)}
    }

    if(length(cols_temp) != length(cols_to_select)){warning("Adjust you col selection")}
    if(exists("manual_selection") && manual_selection){
      #stop("Verify whether you want the following:")
      if(with_rand_policy && with_rand_barr){
        cols_temp<-c(3,4,24,59,94,164)
      }
      warning(print("Verify whether you want the following:"))
      print(cols_temp)
    }else if(setequal(unique(info_on_cols[cols_temp]), cols_to_select)){
      c<-cols_temp[order(cols_temp, decreasing=T)]
      c1<-c[match(cols_to_select,info_on_cols[c])]
      cols_temp<-c1[order(c1)]
    }else if(length(cols_temp) == 2*length(cols_to_select)){
      if(use_tax_spill_skill){
        if(setequal(info_on_cols[cols_temp[c(1,2,seq(5,length(cols_temp),2))]],info_on_cols[cols_temp[c(3,4,seq(6,length(cols_temp),2))]])){
          cols_temp<-cols_temp[c(3,4,seq(6,length(cols_temp),2))]
        }
      }else{stop("you should really check your col selection")}
    }else if(length(cols_temp) != length(cols_to_select)){
      if(use_tax_spill_skill != T){
        warning("Adjust your col_selection: Now manually")
        cols_temp<-cols_temp[c(5,6,7,10,13,16)]
      }else if(length(cols_temp) != length(cols_to_select)){
        stop()
      }
    }
     rm(cols_to_select, c)
  }
  if(manual_selection){
    warning("Verify your col-selection")
    if(agent == "Eurostat"){
      cols_temp<-c(3,4,16,41,66,116)
      cols_temp<-c(3,4,14,24,34,54)
    }else if(agent == "Firm"){
      cols_temp<-c(3,4,16,41,91,116)
      cols_temp<-c(3,4,14,24,44,54)
    }
    if(with_rand_barr!=T){
      if(agent=="Firm"){
        cols_temp<-c(3,4,10,20,40,50)
      }else if(agent == "Eurostat"){
        cols_temp<-c(3,4,10,20,30,50)
      }
    }
    if(with_rand_policy){
      if(agent=="Firm"){
        cols_temp<-c(3,4,20,40,50)
      }else if(agent == "Eurostat"){
        cols_temp<-c(3,4,25)
      }
    }
  }
  cols_temp<-cols_temp[which(!is.na(cols_temp))]
  ### FINALLY CHOSEN (in paper):
  #cols_temp<-cols_temp[c(1,2,4,6,8,10)]
  warning(print("Verify whether you want the following:"))
  print(paste(cols_temp))
  print(paste(info_on_cols[cols_temp]))
  
  # Row 1:4 contain AIC, BIC, Rsq and convergence info. 
  info_rows<-table_for_stepaic[1:4,cols_temp]
  info_rows[4,which(info_rows[4,] ==1)]<-"TRUE"
  result_rows<-table_for_stepaic[5:nrows_temp,cols_temp]
  remove_r<-c()
  for(r in 1:nrow(result_rows)){
    count<-0
    for(c in 1:ncol(result_rows)){
      if(is.na(result_rows[r,c])){
        count<-count+1
      }
    }
    if(count == ncol(result_rows)){
      remove_r<-c(remove_r, r)
    }
  }
  
  #View(result_rows[remove_r,])
  if(!is.null(remove_r)){result_rows<-result_rows[-remove_r,]}
  rm(remove_r)
  tab_temp<-rbind(result_rows, info_rows)
  name_col_temp<-rownames(tab_temp)
  order_temp<-rep(100, length(name_col_temp))
  # Order: spills [0,1) -> policy [1,2) -> barriers [2,3) 
  # -> spill-pol [3,4) -> spill-barr [4,5) -> pol:barr [5,6) 
  # -> type [6-7-8,9) -> micro_contr -> macro_contr.
  if(exists("use_tax_spill_skill") && use_tax_spill_skill){pol<-2;  sk<-1; tp<-6
  }else{pol <-1; sk<-2; tp<-6}
  for(i in 1:length(name_col_temp)){
    n <- name_col_temp[i]
    if(n == "(Intercept)"){order_temp[c(i,i+1)]<--1
    }else if(n == "spill" || n == "chi_spill"){name_col_temp[i]<-"DOLESCchiUPBLdistBRDOL"
    order_temp[c(i,i+1)]<-0
    }else if(grepl("I",n) && length(gregexpr("spill",n)[[1]]) == 2 && grepl("type",n)!=T){name_col_temp[i]<-"DOL (ESCchiUPBLdistBR)UP2 DOL"
    order_temp[c(i,i+1)]<-0.01
    }else if( length(gregexpr("spill",n)[[1]]) == 2 && grepl("I",n) && grepl("type",n)){name_col_temp[i]<-"DOL ESCmathbbmBLIBR(eco) ESCcdot (ESCchiUPBLdistBR)UP2 DOL"
    order_temp[c(i,i+1)]<-0.012+tp
    }else if(grepl("spill",n) && grepl("I",n) &&  grepl("type",n)){name_col_temp[i]<-"DOL ESCmathbbmBLIBR(eco) ESCcdot ESCchiUPBLdistBR DOL"
    order_temp[c(i,i+1)]<-0.011+tp
    }else if(n == "int" || n == "chi_int"){name_col_temp[i]<-"DOLESCchiUPBLintBRDOL"
    order_temp[c(i,i+1)]<-0.1
    }else if(grepl("I",n) && length(gregexpr("int",n)[[1]]) == 2 && grepl("type",n)!=T){name_col_temp[i]<-"DOL (ESCchiUPBLintBR)UP2 DOL"
    order_temp[c(i,i+1)]<-0.11
    }else if(grepl("I",n) && length(gregexpr("int",n)[[1]]) == 2 && grepl("type",n)){name_col_temp[i]<-"DOL ESCmathbbmBLIBR(eco)  (ESCchiUPBLintBR)UP2 DOL"
    order_temp[c(i,i+1)]<-0.13 + tp
    }else if(grepl("I",n) && grepl("int",n)&&  grepl("type",n)){name_col_temp[i]<-"DOL ESCmathbbmBLIBR(eco)  ESCchiUPBLintBR DOL"
    order_temp[c(i,i+1)]<-0.12 + tp
    }else if(n == "int:spill" || n == "chi_int:chi_spill"){name_col_temp[i]<-"DOL ESCchiUPBLdistBR ESCcdot ESCchiUPBLintBR DOL"
    order_temp[c(i,i+1)]<-0.2
    }else if(grepl("I",n) && length(gregexpr("frontier",n)[[1]]) == 2 && grepl("type",n)!=T){name_col_temp[i]<-"DOL(ESCbetaUPBLABR)UP2DOL"
    order_temp[c(i,i+1)]<-sk + .01
    }else if(n == "frontier"){name_col_temp[i]<-"DOLESCbetaUPBLABRDOL"
    order_temp[c(i,i+1)]<-sk
    }else if(grepl("I",n) &&  length(gregexpr("frontier",n)[[1]]) == 2 && grepl("type",n)){name_col_temp[i]<-"DOLESCmathbbmBLIBR(eco) (ESCbetaUPBLABR)UP2DOL"
    order_temp[c(i,i+1)]<-sk + .03 + tp
    }else if(grepl("I",n) && grepl("frontier",n) && grepl("type",n)){name_col_temp[i]<-"DOL ESCmathbbmBLIBR(eco) ESCbetaUPBLABRDOL"
    order_temp[c(i,i+1)]<-sk + .02 + tp
    }else if(is.element(n, c("skill", "skill_macro", "skill_micro"))){name_col_temp[i]<-"DOLESCbetaUPBLbBRDOL"
    order_temp[c(i,i+1)]<-sk + .1
    }else if(grepl("I",n) && length(gregexpr("skill",n)[[1]]) == 2 && grepl("type",n)!=T){name_col_temp[i]<-"DOL(ESCbetaUPBLbBR)UP2DOL"
    order_temp[c(i,i+1)]<-sk + .11
    }else if(grepl("I",n) &&  length(gregexpr("skill",n)[[1]]) == 2 && grepl("type",n)){name_col_temp[i]<-"DOL ESCmathbbmBLIBR(eco) ESCcdot (ESCbetaUPBLbBR)UP2DOL"
    order_temp[c(i,i+1)]<-sk + .13 + tp
    }else if(grepl("I",n) && grepl("skill",n) &&  grepl("type",n)){name_col_temp[i]<-"DOL ESCmathbbmBLIBR(eco) ESCcdot ESCbetaUPBLbBRDOL"
    order_temp[c(i,i+1)]<-sk + .12 + tp
    }else if(grepl("skill",n) && grepl("frontier",n)){name_col_temp[i]<-"DOLESCbetaUPBLABR ESCcdot ESCbetaUPBLbBRDOL"
    order_temp[c(i,i+1)]<-sk + .2
    #spill-barr [4,5)
    }else if(grepl("int",n) && grepl("frontier",n)){name_col_temp[i]<-"DOLESCchiUPBLintBR ESCcdot ESCbetaUPBLABRDOL"
    order_temp[c(i,i+1)]<-4.2
    }else if(grepl("spill",n) && grepl("frontier",n)){name_col_temp[i]<-"DOLESCchiUPBLdistBR ESCcdot ESCbetaUPBLABRDOL"
    order_temp[c(i,i+1)]<-4.0
    }else if(grepl("spill",n) && grepl("skill",n)){name_col_temp[i]<-"DOLESCchiUPBLdistBR ESCcdot ESCbetaUPBLbBRDOL"
    order_temp[c(i,i+1)]<-4.1
    }else if(grepl("int",n) && grepl("skill",n)){name_col_temp[i]<-"DOLESCchiUPBLintBR ESCcdot ESCbetaUPBLbBRDOL"
    order_temp[c(i,i+1)]<-4.3
    }else if(n == "tax"){name_col_temp[i]<-"DOLESCthetaDOL"
    order_temp[c(i,i+1)]<-pol
    }else if(n == "cons"){name_col_temp[i]<-"DOLESCsigmaUPcDOL"
    order_temp[c(i,i+1)]<-pol + .1
    }else if(n == "inv"){name_col_temp[i]<-"DOLESCsigmaUPiDOL"
    order_temp[c(i,i+1)]<-pol + .2
    }else if(n == "inv:cons" || n == "cons:inv"){name_col_temp[i]<-"DOLESCsigmaUPi ESCcdot ESCsigmaUPcDOL"
    order_temp[c(i,i+1)]<-pol + .7
    }else if(n == "tax:cons"){name_col_temp[i]<-"DOLESCtheta ESCcdot ESCsigmaUPcDOL"
    order_temp[c(i,i+1)]<-pol + .5
    }else if(n == "tax:inv"){name_col_temp[i]<-"DOLESCtheta ESCcdot ESCsigmaUPiDOL"
    order_temp[c(i,i+1)]<-pol + .6
    
    }else if(grepl("I", n)){
      if(length(gregexpr("tax",n)[[1]]) == 2 && grepl("type",n)!=T){name_col_temp[i] <- "DOLESCthetaUP2DOL"
      order_temp[c(i,i+1)]<-pol +.011
      }else if(length(gregexpr("tax",n)[[1]]) == 2 &&  grepl("type",n)){name_col_temp[i] <- "DOL ESCmathbbmBLIBR(eco) ESCcdot ESCthetaUP2DOL"
      order_temp[c(i,i+1)]<-pol +.012 + tp
      }else if(length(gregexpr("cons",n)[[1]]) == 2){name_col_temp[i] <- "DOL(ESCsigmaUPc)UP2DOL"
      order_temp[c(i,i+1)]<-pol +.111 
      }else if(length(gregexpr("cons",n)[[1]]) == 2&& grepl("type",n)){name_col_temp[i] <- "DOL ESCmathbbmBLIBR(eco)  ESCcdot (ESCsigmaUPc)UP2DOL"
      order_temp[c(i,i+1)]<-pol +.112 + tp
      }else if(length(gregexpr("inv",n)[[1]]) == 2){name_col_temp[i] <- "DOL(ESCsigmaUPi)UP2DOL"
      order_temp[c(i,i+1)]<-pol +.211
      }else if(length(gregexpr("inv",n)[[1]]) == 2 && grepl("type",n)){name_col_temp[i] <- "DOL ESCmathbbmBLIBR(eco)  ESCcdot (ESCsigmaUPi)UP2DOL"
      order_temp[c(i,i+1)]<-pol +.212 + tp 
      }
    }else if(n == "tax:frontier"){name_col_temp[i]<-"DOLESCtheta ESCcdot ESCbetaUPADOL"
    order_temp[c(i,i+1)]<-5.0
    }else if(is.element(n,c("tax:skill", "tax:skill_macro"))){name_col_temp[i]<-"DOLESCtheta ESCcdot ESCbetaUPbDOL"
    order_temp[c(i,i+1)]<-5.1
    }else if(n == "inv:frontier"){name_col_temp[i]<-"DOLESCsigmaUPi ESCcdot ESCbetaUPADOL"
    order_temp[c(i,i+1)]<-5.4
    }else if(is.element(n, c("inv:skill", "inv:skill_macro"))){name_col_temp[i]<-"DOLESCsigmaUPi ESCcdot ESCbetaUPbDOL"
    order_temp[c(i,i+1)]<-5.5
    }else if(n == "cons:frontier"){name_col_temp[i]<-"DOLESCsigmaUPc ESCcdot ESCbetaUPADOL"
    order_temp[c(i,i+1)]<-5.2
    }else if(is.element(n,c("cons:skill", "cons:skill_macro"))){name_col_temp[i]<-"DOLESCsigmaUPc ESCcdot ESCbetaUPbDOL"
    order_temp[c(i,i+1)]<-5.3
    
    #-> spill-pol [3,4)
    }else if(is.element(n,c("tax:chi_spill", "tax:spill"))){name_col_temp[i]<-"DOL ESCchiUPBLdistBR ESCcdot ESCtheta DOL"
    order_temp[c(i,i+1)]<-3
    }else if(is.element(n,c("tax:chi_int", "tax:int"))){name_col_temp[i]<-"DOL ESCchiUPBLintBR ESCcdot  ESCtheta DOL"
    order_temp[c(i,i+1)]<-3.1
    }else if(is.element(n,c("inv:chi_spill", "inv:spill"))){name_col_temp[i]<-"DOL ESCchiUPBLdistBR ESCcdot ESCsigmaUPiDOL"
    order_temp[c(i,i+1)]<-3.4
    }else if(is.element(n,c("inv:chi_int", "inv:int"))){name_col_temp[i]<-"DOL ESCchiUPBLintBR ESCcdot ESCsigmaUPi DOL"
    order_temp[c(i,i+1)]<-3.5
    }else if(is.element(n,c("cons:chi_spill", "cons:spill"))){name_col_temp[i]<-"DOLESCchiUPBLdistBR ESCcdot ESCsigmaUPcDOL"
    order_temp[c(i,i+1)]<-3.2
    }else if(is.element(n,c("cons:chi_int","cons:int"))){name_col_temp[i]<-"DOL ESCchiUPBLintBR ESCcdot ESCsigmaUPc DOL"
    order_temp[c(i,i+1)]<-3.3
    #-> type [6-7-8,9)
    }else if(grepl("type",n)){
      if(grepl("int",n)){name_col_temp[i]<-"DOLESCmathbbmBLIBR(eco) ESCcdot ESCchiUPBLintBRDOL"
      order_temp[c(i,i+1)]<-.1 +tp
      }else if(grepl("spill", n)){name_col_temp[i]<-"DOLESCmathbbmBLIBR(eco) ESCcdot ESCchiUPBLdistBRDOL"
      order_temp[c(i,i+1)]<-tp+.01
      }else if(grepl("skill", n)){name_col_temp[i]<-"DOLESCmathbbmBLIBR(eco) ESCcdot ESCbetaUPBLbBRDOL"
      order_temp[c(i,i+1)]<-tp + sk + .11
      }else if(grepl("frontier", n)){name_col_temp[i]<-"DOLESCmathbbmBLIBR(eco) ESCcdot ESCbetaUPBLABRDOL"
      order_temp[c(i,i+1)]<-tp + sk + .1
      }else if(grepl("tax",n)){name_col_temp[i]<-"DOLESCmathbbmBLIBR(eco) ESCcdot ESCthetaDOL"
      order_temp[c(i,i+1)]<-tp + pol
      }else if(grepl("inv",n)){name_col_temp[i]<-"DOLESCmathbbmBLIBR(eco) ESCcdot ESCsigmaUPiDOL"
      order_temp[c(i,i+1)]<-tp + pol + .2
      }else if(grepl("cons",n)){name_col_temp[i]<-"DOLESCmathbbmBLIBR(eco) ESCcdot ESCsigmaUPcDOL"
      order_temp[c(i,i+1)]<-tp + pol + .1
      }else{name_col_temp[i]<-"DOLESCmathbbmBLIBR(eco)DOL"
      order_temp[c(i,i+1)]<-tp
      }
    }else if(n == "mean_spec_skills_conv"){name_col_temp[i]<-"DOLBUPcDOWNBLiBRDOL"
    order_temp[c(i,i+1)]<-9.01
    }else if(n == "price"){name_col_temp[i]<-"DOLpriceDOWNBLiBRDOL"
    order_temp[c(i,i+1)]<-9.13
    }else if(n == "technology_conv"){name_col_temp[i]<-"DOLAUPBLEff(c)BRDOWNBLiBRDOL"
    order_temp[c(i,i+1)]<-9.02
    }else if(n == "output"){name_col_temp[i]<-"DOLoutputDOWNBLiBRDOL"
    order_temp[c(i,i+1)]<-9.12
    }else if(n == "no_employees"){name_col_temp[i]<-"DOL#employeesDOWNBLiBRDOL"
    order_temp[c(i,i+1)]<-9.10
    }else if(n == "unit_costs"){name_col_temp[i]<-"DOLUnitCostsDOWNBLiBRDOL"
    order_temp[c(i,i+1)]<-9.11
    }else if(n == "price"){name_col_temp[i]<-"DOLUnitCostsDOWNBLiBRDOL"
    order_temp[c(i,i+1)]<-9.121
    }else if(n == "age"){name_col_temp[i]<-"DOLAgeDOWNBLiBRDOL"
    order_temp[c(i,i+1)]<-9.14
    }else if(n == "no_active_firms"){name_col_temp[i]<-"DOL#firmsDOL"
    order_temp[c(i,i+1)]<-10.1 
    }else if(n == "techn_frontier_conv"){name_col_temp[i]<-"DOLAUPBLVBRDOWNcDOL"
    order_temp[c(i,i+1)]<-9
    }else if(n == "monthly_output"){name_col_temp[i] <- "DOLYDOL"
    order_temp[c(i,i+1)]<-10.0
    }else if(n == "average_s_skill_conv"){name_col_temp[i] <- "DOLBUPcDOL"
    order_temp[c(i,i+1)]<-9.015
    }else if(n == "eco_price_wage_ratio"){name_col_temp[i] <- "DOLBLpUPBLecoBRBR/BLwUPrBRDOL"
    order_temp[c(i,i+1)]<-10.2
    }else if(n == "tax:eco_price_wage_ratio"){name_col_temp[i] <- "DOLBLpUPBLecoBRBR/BLwUPrBR ESCcdot ESCthetaDOL"
    order_temp[c(i,i+1)]<-10.25
    }else if(n == "Rsq"){name_col_temp[i]<-"DOLRUP2DOL"}
  }
  order_temp[seq(2,length(order_temp),2)]<-order_temp[seq(2,length(order_temp),2)] + 0.0001
  temp<-order_temp[which(order_temp<99)]
  if(length(temp) != length(unique(temp))){stop("There is a double entry!")}
  others<-(which(order_temp>=99))
  tab_temp<-cbind(name_col_temp,tab_temp)
  tab_temp<-tab_temp[c(order(temp),others),]
  add_col_temp<-rep("", length(colnames(tab_temp)))
  for(i in 1:length(colnames(tab_temp))){
    n<-colnames(tab_temp)[i]
    if(grepl("ivglm", n)){
      add_col_temp[i]<-"IV"
    }else if(grepl("lm", n)){
      add_col_temp[i]<-"OLS"
    }else if(grepl("gamlss", n)){
      add_col_temp[i]<-"Probit"
    }else if(grepl("ivreg", n)){
      add_col_temp[i]<-"IV (lin.)"
    }
    
    if(i == 1){colnames(tab_temp)[i]<-""
    }else if(n == "name_col_temp"){colnames(tab_temp)[i]<-""
    }else if(grepl("share_conventional_vintages_used",n)){colnames(tab_temp)[i]<-"DOLESCnuUPcDOWNBLiBRDOL"
    }else if(grepl("share_conv_capital_used",n)){colnames(tab_temp)[i]<-"DOLESCnuUPcDOWNBLtBRDOL"
    }else if(reg_level == "Eurostat"){
      if(grepl("Time",n)){colnames(tab_temp)[i]<-"DOLtUPBL*BRDOL"
      }else if(grepl("abs_crit_prod_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLAUPBL+BRBR/BLAUPBL-BRBRESCbigr)UPBL*BRDOL"
      }else if(grepl("abs_crit_techn_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLAUPBLEff(+)BRBR/BLAUPBLEff(-)BRBRESCbigr)UPBL*BRDOL"
      }else if(grepl("abs_crit_frontier_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLAUPVDOWN+BR/BLAUPVDOWN-BRESCbigr)UPBL*BRDOL"
      }else if(grepl("abs_crit_skill_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLBUPBL+BRBR/BLBUPBL-BRBRESCbigr)UPBL*BRDOL"
      }else if(grepl("abs_crit_share",n)){colnames(tab_temp)[i]<-"DOLESCnuUPBL*BRDOL"
      }else if(grepl("abs_crit_cap_ratio",n)){colnames(tab_temp)[i]<-"DOLESCkappaUPBL*BRDOL"
      }else if(grepl("crit_share", n)){colnames(tab_temp)[i]<-"DOLESCnuUPBL*BRDOL"
      }else if(grepl("crit_prod_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLAUPBLcBRBR/BLAUPBLgBRBRESCbigr)UPBL*BRDOL"
      }else if(grepl("crit_techn_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLAUPBLEff(c)BRBR/BLAUPBLEff(g)BRBRESCbigr)UPBL*BRDOL"
      }else if(grepl("crit_frontier_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLAUPVDOWNcBR/BLAUPVDOWNgBRESCbigrESCbigr)UPBL*BRDOL"
      }else if(grepl("crit_skill_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLBUPBLcBRBR/BLBUPBLgBRBRESCbigr)UPBL*BRDOL"
      }else if(grepl("crit_share",n)){colnames(tab_temp)[i]<-"DOLESCnuUPBL*BRDOL"
      }else if(grepl("crit_cap_ratio",n)){colnames(tab_temp)[i]<-"DOLESCkappaUPBL*BRDOL"
      }else if(grepl("crit_share", n)){colnames(tab_temp)[i]<-"DOLESCnuUPBL*BRDOL"
      }else if(grepl("variance_share_full", n)){colnames(tab_temp)[i]<-"DOL(ESCsigmaUPBLESCnuBR)UP2DOL"
      }
    }else if(reg_level == "Firm"){
      if(grepl("Time",n)){colnames(tab_temp)[i]<-"DOLtDOWNiUPBL*BRDOL"
      }else if(grepl("abs_crit_prod_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLADOWNiUPBL+BRBR/BLADOWNiUPBL-BRBRESCbigr)UPBL*BRDOL"
      }else if(grepl("abs_crit_techn_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLADOWNiUPBLEff(+)BRBR/BLADOWNiUPBLEff(-)BRBRESCbigr)UPBL*BRDOL"
      }else if(grepl("abs_crit_frontier_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLAUPVDOWN+BR/BLAUPVDOWN-BRESCbigr)UPBL*BRDOL"
      }else if(grepl("abs_crit_skill_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLBDOWNiUPBL+BRBR/BLBDOWNiUPBL-BRBRESCbigr)UPBL*BRDOL"
      }else if(grepl("abs_crit_share",n)){colnames(tab_temp)[i]<-"DOLESCnuDOWNBLiBRUPBL*BRDOL"
      }else if(grepl("abs_crit_cap_ratio",n)){colnames(tab_temp)[i]<-"DOLESCkappaDOWNBLiBRUPBL*BRDOL"
      }else if(grepl("crit_share", n)){colnames(tab_temp)[i]<-"DOLESCnuDOWNBLiBRUPBL*BRDOL"
      }else if(grepl("crit_prod_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLADOWNiUPBLcBRBR/BLADOWNiUPBLgBRBRESCbigr)UPBL*BRDOL"
      }else if(grepl("crit_techn_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLADOWNiUPBLEff(c)BRBR/BLADOWNiUPBLEff(g)BRBRESCbigr)UPBL*BRDOL"
      }else if(grepl("crit_frontier_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLAUPVDOWNcBR/BLAUPVDOWNgBRESCbigrESCbigr)UPBL*BRDOL"
      }else if(grepl("crit_skill_ratio",n)){colnames(tab_temp)[i]<-"DOLESCbigl(BLBDOWNiUPBLcBRBR/BLBDOWNiUPBLgBRBRESCbigr)UPBL*BRDOL"
      }else if(grepl("crit_share",n)){colnames(tab_temp)[i]<-"DOLESCnuDOWNBLiBRUPBL*BRDOL"
      }else if(grepl("crit_cap_ratio",n)){colnames(tab_temp)[i]<-"DOLESCkappaDOWNBLiBRUPBL*BRDOL"
      }else if(grepl("crit_share", n)){colnames(tab_temp)[i]<-"DOLESCnuDOWNBLiBRUPBL*BRDOL"
      }else if(grepl("variance_share_full", n)){colnames(tab_temp)[i]<-"DOL(ESCsigmaDOWNiUPBLESCnuBR)UP2DOL"
      }
    }
    
  }
  tab_temp0<-rbind(add_col_temp, tab_temp)
  if(use_tax_spill_skill){
    split_by<-sum(with_rand_barr,with_rand_learn, with_rand_policy)
    if(length(which(grepl("int",rownames(tab_temp))))>0){
      split_by<-sum(1,split_by)
    }
    if(split_by == 4){
      #stop("Adjust your code!")
      first<-c(2:(2*split_by + 1),(4*split_by + 2):((5*split_by + 1)))
    }else{
      first<-c(1:(2*split_by),(5*split_by + 1):(6*split_by))
    }
    tab_temp1<-tab_temp0[,c(1,first)]
    tab_temp2<-tab_temp0[,-first]
    for(i in 1:2){
      if(i == 1){tab_temp3<-tab_temp1
      }else if(i == 2){tab_temp3<-tab_temp2}
      caption_temp<-paste("Experiment: ",exp_temp,paste(name_extension,collapse=" "),"ESCmulticolumnBL",ncol(tab_temp3),"BRBLlBRBLESCscriptsize Significance codes: 0 ‘***’ .001 ‘**’ .01 ‘*’ .05 
‘.’ .1 
‘ ’ 1.BRESCESC")
      tab<-xtable(tab_temp3, caption = caption_temp, include.rownames=F, inlude.colnames=FALSE)
      print(tab, include.rownames=F, inlude.colnames=FALSE, file=paste(filename_temp,i,".tex",sep=""), append = F)
    }
  }else{
    #caption_temp<-paste(info_on_cols, collapse = "; ")
    caption_temp<-paste(reg_level,". Experiment: ",exp_temp,paste(name_extension,collapse=" "),"ESCmulticolumnBL",ncol(tab_temp0),"BRBLlBRBLESCscriptsize Significance codes: 0 ‘***’ .001 ‘**’ .01 ‘*’ .05 
‘.’ .1 
‘ ’ 1.BRESCESC")
    tab<-xtable(tab_temp0, caption = caption_temp, include.rownames=F, inlude.colnames=FALSE)
    print(tab, include.rownames=F, inlude.colnames=FALSE, file=filename_temp, append = F)
  }
  
  



} # if file.exists()
rm(tab,i,r,n)
rm(list=ls(pattern="temp"))
