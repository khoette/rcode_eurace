plot_dir_temp<-paste(plot_dir_validation,"/", sep="")
if(dir.exists(plot_dir_temp)!=TRUE){
  dir.create(plot_dir_temp)
}
# Steps: 
# 1. load trend data
# 2. compute annual output growth rate for each run manually
# 3. compute geom mean per run and arith mean across runs
#
# Further below: Compute business cylce volatility (avg. std 
# across runs and sd of avg sd across runs)
# 
#load(paste(data_directory,"data_work.RData", sep=""))
load(file=paste(data_directory,"data_single_switch_",experiment_name,"_",parameter,"_Eurostat.RData",sep=""))
object<-object[order(as.numeric(as.character(object$Run)), as.numeric(as.character(object$Periods))),]
ann_growth_rates_temp<-c()
if(is.element("gdp_growth_rate_annual", colnames(object))){
  ann_growth_rates_temp<-cbind(ann_growth_rates_temp, gdp_gr_ann = object$gdp_growth_rate_annual)
}
if(is.element("output_growth_rate_annual", colnames(object))){
  ann_growth_rates_temp<-cbind(ann_growth_rates_temp, output_gr_ann = object$output_growth_rate_annual)
}
# # Use bandpass filtered data, i.e. trend_full
load(paste(data_directory,"series_bk_var_8_32.RData", sep=""))
load(paste(data_directory,"cycle_bk_var_8_32.RData", sep=""))
load(paste(data_directory,"trend_bk_var_8_32.RData", sep=""))

data_temp<-series_full
vars_temp<-c("monthly_output")
cols_temp<-match(c("Run", "Type", "Periods", vars_temp), colnames(data_temp))

temp<-data_temp[cols_temp]
rm(data_temp)
temp<-temp[order(as.numeric(as.character(temp$Run)), as.numeric(as.character(temp$Periods))),]
if(!is.null(ann_growth_rates_temp)){
  temp<-cbind(temp, ann_growth_rates_temp)
}
rm(ann_growth_rates_temp)

# 1. Get output growth rate
temp<-temp[order(as.numeric(as.character(temp$Run)), as.numeric(as.character(temp$Periods))),]
runs_temp<-unique(temp$Run)
output_temp<-temp$monthly_output
avail_rates<-c()
if(is.element("output_gr_ann", colnames(temp))){
  output_gr_temp<-temp$output_gr_ann
  avail_rates<-c(avail_rates, 2)
}
if(is.element("gdp_gr_ann", colnames(temp))){
  gdp_gr_temp<-temp$gdp_gr_ann
  avail_rates<-c(avail_rates, 3)
}
if(max(output_temp, na.rm=TRUE) < 1000){
  print("Most likely, you are using log(output). This is now 
        retransformed to compute growth rates")
  output_temp<-exp(output_temp)
}
avg_temp<-c()
sd_temp<-c()
avg_sd_temp<-c()
sd_sd_temp<-c()
r1<-1
i<-2
avail_rates<-c(avail_rates,1)
for(i in avail_rates){ # There are some built in growth rates in the eurace model
  
  empty_temp<-rep(NA, length(runs_temp))
  empty_temp_sd<-rep(NA, length(runs_temp))
  # Compute growth rate, geometric mean and standard dev. for each run
  for(r1 in 1:length(runs_temp)){ 
    r <-runs_temp[r1]
    inds_temp<-which(temp$Run == r)
    
    if(i == 1){
      out_r_temp<-output_temp[inds_temp]
      # monthly gr rate (index shifted)
      #diff_r_temp<-diff(out_r_temp,1)
      #gr_temp<-diff_r_temp/out_r_temp[-length(out_r_temp)]
      # Use yearly
      diff_r_temp<-diff(out_r_temp,12) # difference across year, i.e. (y_t - y_{t-12}) for all t
      # divide differences by level of output excluding the last year 
      gr_temp<-diff_r_temp/out_r_temp[-c((length(out_r_temp)-11):length(out_r_temp))] 
      
      # if monthly
      gr_temp<-gr_temp[which(!is.na(gr_temp))] # remove NAs
      gr_temp<-gr_temp + 1 # add one to compute geometric mean
    }else if(i==2){
      gr_temp<-output_gr_temp[inds_temp]
    }else if(i==3){
      gr_temp<-gdp_gr_temp[inds_temp]
    }
    mean_temp<-((prod(gr_temp))**(1/length(gr_temp))-1) # Compute geometric mean
    # Take simple std to compute deviations of yearly growth 
    # rates from long term geometric mean of growth rate
    # (first, transform factor back into rate)
    gr_temp<-gr_temp-1
    sd_temp<-sum(((gr_temp)-(mean_temp))**2)/length(gr_temp)
    sd_temp<-sqrt(sd_temp)
    #sd_temp<-exp(sqrt(sum((gr_temp/mean_temp)**2)/length(gr_temp)))
    empty_temp[r1]<-mean_temp 
    empty_temp_sd[r1]<-sd_temp
  }
  avg_temp<-c(avg_temp, mean(empty_temp, na.rm=TRUE)) # Average growth rate across runs
  sd_temp<-c(sd_temp, sd((empty_temp), na.rm=TRUE)) # Standard dev. across runs
  avg_sd_temp<-c(avg_sd_temp, mean(empty_temp_sd, na.rm=TRUE)) # Average within-run standard dev.
  sd_sd_temp<-c(sd_sd_temp, sd(empty_temp_sd, na.rm=TRUE)) # Standard dev. of within-run std.
}
avg_output_gr_temp<-avg_temp[1]
sd_output_gr_temp<-avg_sd_temp[1]

deviation_gr_across_runs<-sd_temp[1]
deviation_sds_across_runs<-sd_sd_temp[1]




filename<-paste(plot_dir_validation,"/growth_rate.txt", sep="")
write(paste("Output growth rate and std for bandpass filtered TS data: ", avg_output_gr_temp, " sd:", sd_output_gr_temp), file = filename, append = FALSE)
write(paste("Deviations across runs: GR:", deviation_gr_across_runs, " sd:", deviation_sds_across_runs), file = filename, append = TRUE)

write(paste("Growth rates and std are computed as geom. mean per run 
            (resp. std as deviation of annual growth rate from long term 
            geo mean growth rate WITHIN a run. Then, the average across 
            runs is taken and shown her. \n Further, I show the deviations 
            in these two values ACROSS runs."), file = filename, append = TRUE)


# For any reason, precomputed growth rates are strange. This is 
# not relevant here and rather any documentation mistake during the 
# simuls but without influence on simulation runs themselves. See 
# also c code and model documentation. 
# 
# 
# Compute std dev. of business cylces (check avg. within runs 
# and avg. across runs, normalize first into pct. by dividing 
# by trend). 
# 
temp<-trend_full
vars_temp<-c("monthly_output")
cols_temp<-match(c("Run", "Type", "Periods", vars_temp), colnames(temp))
temp<-temp[cols_temp]
temp<-temp[order(as.numeric(as.character(temp$Run)), as.numeric(as.character(temp$Periods))),]
trend_temp<-as.vector(temp$monthly_output)
temp<-cycle_full
vars_temp<-c("monthly_output")
cols_temp<-match(c("Run", "Type", "Periods", vars_temp), colnames(temp))
temp<-cycle_full[cols_temp]
temp<-temp[order(as.numeric(as.character(temp$Run)), as.numeric(as.character(temp$Periods))),]
cycle_vola_temp<-temp
out_temp<-as.vector(cycle_vola_temp$monthly_output)
col_temp<-match("monthly_output", colnames(cycle_vola_temp))
cycle_vola_temp[,col_temp]<-100*(out_temp/trend_temp)

runs_temp<-unique(cycle_vola_temp$Run)
means_temp<-rep(NA, length(runs_temp))
sds_temp<-rep(NA, length(runs_temp))
for(r1 in 1:length(runs_temp)){
  r<-runs_temp[r1]
  temp<-cycle_vola_temp[which(cycle_vola_temp$Run == r),col_temp]
  mean_temp<-mean(abs(temp), na.rm=TRUE)
  means_temp[r1]<-mean_temp
  sd_temp<-sd(temp, na.rm=TRUE)
  sds_temp[r1]<-sd_temp
}
if(mean(temp,na.rm=TRUE) > 0.5){
  print("This is strange: Mean busi-cycle vola should be close to zero. 
        Probably mistake in ts computation.... ")
}
busi_mean_abs_fluctuation_temp<-mean(means_temp, na.rm=TRUE)
busi_sd_abs_fluctuation_temp<-mean(sds_temp, na.rm=TRUE)
busi_mean_acr_runs_fluct_temp<-sd(means_temp, na.rm=TRUE)
busi_sd_acr_runs_fluct_temp<-sd(sds_temp, na.rm=TRUE)

write(paste("\n \n Avg. size of fluctuations (abs): ",busi_mean_abs_fluctuation_temp, busi_sd_abs_fluctuation_temp), file = filename, append = TRUE)
write(paste("Devations across runs: in mean:", busi_mean_acr_runs_fluct_temp," in sd: ", busi_sd_acr_runs_fluct_temp), file = filename, append = TRUE)

write(paste("First: Average size of output fluctuations (means. as pct. 
            share, i.e. normalized by trend) measured in absolute terms, 
            here shown as average of run averages. Std. is avg. std. of 
            within-run fluctuation (not abs!). \n Below, I show the 
            deviation across simulation runs, i.e. simpel standard dev. 
            computed across runs."), file = filename, append = TRUE)

rm(r, r1, i)
rm(list=ls(pattern="temp"))
rm(list=ls(pattern="_full"))

