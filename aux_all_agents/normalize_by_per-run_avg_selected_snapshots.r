
# This normalization only makes sense if more than 1 agent. 
if(no_agents>1){
  if(file.exists(file=paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_per-run_avg/data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep="")) == FALSE){ 
    
    load(file=paste(experiment_directory,"/rdata/",kind_of_data,"/levels/",paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
    data_single_large<-object
    
    print(paste("Normalize by average agent in t"))
    # 1: Normalize by average agent in t.  This normalization by period run average only makes sense 
    # if more than one agent! 
    data<-data_single_large#[which(temp_mat_single_large$active == 1),]
    runs<-max(as.numeric(data$Run))
    data_temp<-data
    variables_temp<-variables[which(variables != "id")]
    variables_temp<-variables_temp[which(variables_temp != "active")]
    cols<-match(variables_temp, colnames(data))
    
    temp<-max(as.numeric(data_temp$Periods))
    selected_its_temp<-array(NA, dim=(temp*20/1000))
    for(i in 1:length(selected_its_temp)){
      selected_its_temp[i]<-1000 * i
    }
    data<-data_temp[which(is.element(data_temp$Periods, selected_its_temp)),]
    data_temp<-data
    
    #its<-seq(20, 20*number_xml,1000)
    its<-selected_its_temp
    #for(it in 1:number_xml){
    for(it in (selected_its_temp)){
      if(it %% 10){
        print(paste("Period: ", it, length(data[,1])))
      }
      per<-it
      subset0<-data[which(data$Periods == per),]
      subset0_temp<-subset0
      for(r in 1:runs){
        if(r %% 10 == 0){
          print(paste("Run: ",r, length(subset0_temp[,1])))
        }
        subset<-subset0_temp[which(floor(as.numeric(subset0_temp$Run)) == r),] # take floor because run accounts for 
        # parameter identity in decimal place
        subset0_temp<-subset0_temp[which(floor(as.numeric(subset0_temp$Run)) != r),]
        for(v in cols)
        {
          # Divisor (mean agent or initial value) -> better: period-run average agent! 
          div<-mean(subset[,v], na.rm=TRUE)
          if(div==0){
            print(paste("Error: Mean agent has value zero???? get_data_firm_normalize", v, colnames(subset)[v], r))
          }
          if(div!=0){
            subset[,v]<-subset[,v]/div
          }else{
            subset[,v]<-NA
          }
        }
        subset0[which(floor(as.numeric(subset0$Run)) == r),]<-subset
      }
      data_temp[which(data_temp$Period == per),]<-subset0
      data<-data[which(data$Periods != per),]
      
    }
    
    data_single_normalized<-data_temp
    if(dir.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_per-run_avg/",sep=""))!=1){
      dir.create(paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_per-run_avg/",sep=""))
    }
    eval(call("<-",(paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep="")),data_temp))
    object<-eval(as.name(paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep="")))
    save(object, file=paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_per-run_avg/",paste("data_single_large_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
  }else{
    print(paste("Normalized by per-run avg data already existing for ",agent, parameter))
    print(paste(experiment_directory,"/rdata/",kind_of_data,"/normalized_by_per-run_avg/data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  } # else data file.exists
}# if no_agents > 1


rm(list=ls(pattern="temp"))
