
## Check some properties at a given period. 
## 
## How many green regimes? 

aggregation_levels_temp<-c("single_large", "single_switch")

for(aggr in aggregation_levels_temp){
  if(aggr == aggregation_levels_temp[1] && parameter == parameters_tmp[1] && agent == "Eurostat" && d == which_data_types[1]){
    append <- FALSE
  }else{
    append <- TRUE
  }
  
  write(paste("\n \n \n AGGREGATION TYPE:  ", aggr, " for parameter: ", parameter, sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = append)
  
if(agent=="Eurostat" && folder_name == "levels"){
  load(file=paste(data_directory,"data_",aggr,"_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  if(is.element("Learn_Type", colnames(object))){
    count<-object[which(object$Periods == number_xml*20),c("Run", "Type", "Learn_Type", "Spill", "Ease")]
  }else{
    count<-object[which(object$Periods == number_xml*20),c("Run", "Type")]
  }
  count_eco<-length(count[which(count$Type == "eco"),"Type"])
  count_conv<-length(count[which(count$Type == "conv"),"Type"])
  if(is.element("switch", unique(count$Type))){
    count_switch<-length(count[which(count$Type == "switch"),"Type"])
    write(paste("How many regimes of each type in switch disaggr.? eco: ", count_eco,", conv: ", count_conv, ", switch: ", count_switch, " out of ", runs, " for parameter: ", parameter, sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
  }else{
    write(paste("How many regimes of each type WITHOUT SWITCH disaggr.? eco: ", count_eco,", conv: ", count_conv, "out of ", runs , sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
  }
  if(aggr == aggregation_levels_temp[1]){
    if(exists("with_rand_learn") && with_rand_learn==1 && is.element("Learn_Type", colnames(count))){
      count_high_high<-length(count[which(count$Learn_Type == "high-high"),"Learn_Type"])
      count_high_low<-length(count[which(count$Learn_Type == "high-low"),"Learn_Type"])
      count_low_high<-length(count[which(count$Learn_Type == "low-high"),"Learn_Type"])
      count_low_low<-length(count[which(count$Learn_Type == "low-low"),"Learn_Type"])
      write(paste("\n \n How many Learn-type scens: high-high, high-low, low-high, low-low? ", count_high_high, count_high_low, count_low_high, count_low_low," out of ", runs , sep=" "), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
      for(type in unique(count$Type)){
        type_count<-count[which(count$Type == type),]
        count_high_high<-length(type_count[which(type_count$Learn_Type == "high-high"),"Learn_Type"])
        count_high_low<-length(type_count[which(type_count$Learn_Type == "high-low"),"Learn_Type"])
        count_low_high<-length(type_count[which(type_count$Learn_Type == "low-high"),"Learn_Type"])
        count_low_low<-length(type_count[which(type_count$Learn_Type == "low-low"),"Learn_Type"])
        write(paste("Within class: ", type,"\n How many Learn-type scens: high-high, high-low, low-high, low-low? ", count_high_high, count_high_low, count_low_high, count_low_low," out of ", length(type_count$Type) , sep=" "), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
      }
    
      count_low<-length(count[which(count$Ease == "low"),"Ease"])
      count_high<-length(count[which(count$Ease == "high"),"Ease"])
      write(paste("\n \n How many low (high) spill_scens? ", count_low, "(",count_high,") out of ", runs , sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
      for(type in unique(count$Type)){
        type_count<-count[which(count$Type == type),]
        count_high<-length(type_count[which(type_count$Spill == "high"),"Spill"])
        count_low<-length(type_count[which(type_count$Spill == "low"),"Spill"])
        write(paste("Within class: ", type,"\n How many low (high) spill_scens? ", count_low, "(",count_high,") out of ", length(type_count$Type) , sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
      }
      count_low<-length(count[which(count$Ease == "low"),"Ease"])
      count_high<-length(count[which(count$Ease == "high"),"Ease"])
      write(paste("\n \n How many low (high) ease_scens? ", count_low, "(",count_high,") out of ", runs , sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
      for(type in unique(count$Type)){
        type_count<-count[which(count$Type == type),]
        count_high<-length(type_count[which(type_count$Ease == "high"),"Ease"])
        count_low<-length(type_count[which(type_count$Ease == "low"),"Ease"])
        write(paste("Within class: ", type,"\n How many low (high) ease_scens? ", count_low, "(",count_high,") out of ", length(type_count$Type) , sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
      }
    }
  }
  
  if(exists("with_policy") && with_policy == 1){
    write(paste("\n \n Policy experiments:", sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
    fix_vars<-c("Type", "Periods", "Learn_Type", "Spill", "Ease")
    vars<-c("eco_tax_rate", "eco_investment_subsidy", 
            "eco_consumption_subsidy", "gov_monthly_eco_tax_revenue", 
            "gov_monthly_eco_subsidy_payment", "base_tax_rate",
            "monthly_budget_balance")
    cols<-match(c(fix_vars, vars), colnames(object))
    cols<-cols[which(is.na(cols)==FALSE)]
    object_pol<-object[which(as.numeric(as.character(object$Periods))>=1200),]
    object_pol<-object_pol[which(as.numeric(as.character(object_pol$Periods))<=3600),cols]
    cols<-match(vars, colnames(object_pol))
    cols<-cols[which(is.na(cols)==FALSE)]
    
    for(col in cols){
      write(paste("\n Variable:", colnames(object_pol)[col], sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
      mean_rval<-mean(object_pol[,col], na.rm=TRUE)
      sd_rval<-sd(object_pol[,col], na.rm=TRUE)
      min_rval<-min(object_pol[,col], na.rm=TRUE)
      max_rval<-max(object_pol[,col], na.rm=TRUE)
      write(paste("Mean: ", mean_rval, "SD: ", sd_rval, "Min: ", min_rval, "Max: ", max_rval, sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
          for(type in unique(object_pol$Type)){
            object_pol_type<-object_pol[which(object_pol$Type == type),]
            write(paste("By type: ", type, sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
            mean_rval<-mean(object_pol_type[,col], na.rm=TRUE)
            sd_rval<-sd(object_pol_type[,col], na.rm=TRUE)
            min_rval<-min(object_pol_type[,col], na.rm=TRUE)
            max_rval<-max(object_pol_type[,col], na.rm=TRUE)
            write(paste("Mean: ", mean_rval, "SD: ", sd_rval, "Min: ", min_rval, "Max: ", max_rval, sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
          }
      }
    rm(list=ls(pattern="_rval"))
    rm(object_pol, object_pol_type)
  }
  
  if(exists("with_rand_barr") && with_rand_barr == 1){
    write(paste("\n \n Random barrier:", sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
    fix_vars<-c("Type", "Periods", "Learn_Type", "Spill", "Ease")
    vars<-c("frontier_gap", "specific_skill_gap")
    cols<-match(c(fix_vars, vars), colnames(object))
    cols<-cols[which(is.na(cols)==FALSE)]
    
    object_pol<-object[which(as.numeric(as.character(object$Periods)) %in% c(600, 3000, 6000, 15000)),cols]
    cols<-match(vars, colnames(object_pol))
    cols<-cols[which(is.na(cols)==FALSE)]
    
  
    for(col in cols){
      write(paste("\n\n Variable:", colnames(object_pol)[col], sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
      for(per in unique(object_pol$Periods)){
        write(paste("In Period: ", per, sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
        object_pol_barr<-object_pol[which(object_pol$Periods == per),]
        mean_rval<-mean(object_pol_barr[,col], na.rm=TRUE)
        sd_rval<-sd(object_pol_barr[,col], na.rm=TRUE)
        min_rval<-min(object_pol_barr[,col], na.rm=TRUE)
        max_rval<-max(object_pol_barr[,col], na.rm=TRUE)
        write(paste("Mean: ", mean_rval, "SD: ", sd_rval, "Min: ", min_rval, "Max: ", max_rval, sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
        for(type in unique(object_pol_barr$Type)){
          object_pol_barr_type<-object_pol_barr[which(object_pol_barr$Type == type),]
          write(paste("By type: ", type, sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
          mean_rval<-mean(object_pol_barr_type[,col], na.rm=TRUE)
          sd_rval<-sd(object_pol_barr_type[,col], na.rm=TRUE)
          min_rval<-min(object_pol_barr_type[,col], na.rm=TRUE)
          max_rval<-max(object_pol_barr_type[,col], na.rm=TRUE)
          write(paste("Mean: ", mean_rval, "SD: ", sd_rval, "Min: ", min_rval, "Max: ", max_rval, sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
        }
        if(exists("with_rand_learn")&&with_rand_learn==1){
          for(type in unique(object_pol_barr$Learn_Type)){
            object_pol_barr_type<-object_pol_barr[which(object_pol_barr$Learn_Type == type),]
            write(paste("By type: ", type, sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
            mean_rval<-mean(object_pol_barr_type[,col], na.rm=TRUE)
            sd_rval<-sd(object_pol_barr_type[,col], na.rm=TRUE)
            min_rval<-min(object_pol_barr_type[,col], na.rm=TRUE)
            max_rval<-max(object_pol_barr_type[,col], na.rm=TRUE)
            write(paste("Mean: ", mean_rval, "SD: ", sd_rval, "Min: ", min_rval, "Max: ", max_rval, sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
          }
        }
      }
    }
      rm(list=ls(pattern="_rval"))
      rm(list=ls(pattern="object"))
  
  }
  
  rm(list=ls(pattern="count"))
  rm(list=ls(pattern="object"))
}


if(agent=="Government" && folder_name == "levels" && 1==2){
  load(file=paste(data_directory,"data_",aggr,"_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))

  object0<-object[which(as.numeric(as.character(object$Periods))>=600),]
  
  vars<-c("base_tax_rate", "monthly_budget_balance", "monthly_budget_balance_gdp_fraction", 
          "eco_policy_budget", "eco_policy_budget_per_gdp")
  cols<-match(vars, colnames(object))
  cols<-cols[which(is.na(cols)==FALSE)]
  
  if(exists("with_rand_learn") && with_rand_learn==1 && is.element("Learn_Type", colnames(object))){
    for(col in cols){
      write(paste("\n \n Variable: ", colnames(object0)[col], sep=" "), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
    
       for(type in unique(object0$Learn_Type)){
          object<-object0[which(object0$Learn_Type == type),]
          mean_rval<-mean(object[,col], na.rm = TRUE)
          sd_rval<-sd(object[,col], na.rm=TRUE)
          
          write(paste(type, ": mean ", mean_rval, "SD: ", sd_rval, sep=" "), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
        }
        for(type in unique(object0$Ease)){
          object<-object0[which(object0$Ease == type),]
        
          mean_rval<-mean(object[,col], na.rm = TRUE)
          sd_rval<-sd(object[,col], na.rm=TRUE)
          write(paste(type, ": mean ", mean_rval, "SD: ", sd_rval, sep=" "), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
        }
        for(type in unique(object0$Spill)){
          object<-object0[which(object0$Spill == type),]
          mean_rval<-mean(object[,col], na.rm = TRUE)
          sd_rval<-sd(object[,col], na.rm=TRUE)
          write(paste(type, ": mean ", mean_rval, "SD: ", sd_rval, sep=" "), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
        }
      }
    }
  
    write(paste("By scenario type: ", sep=" "), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
    for(col in cols){
      write(paste("Variable: ", colnames(object)[col], sep=" "), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
    
      for(type in unique(object0$Type)){
        object<-object0[which(object0$Type == type),]
        mean_rval<-mean(object[,col], na.rm = TRUE)
        sd_rval<-sd(object[,col], na.rm=TRUE)
        write(paste(type, ": mean ", mean_rval, "SD: ", sd_rval, sep=" "), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = TRUE)
      }
      
    }
  rm(type, col)
  rm(list=ls(pattern="object"))
}
if(agent=="Firm"&& folder_name == "levels" && 1==2){
  load(file=paste(data_directory,"data_",aggr,"_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  count<-object[which(object$Periods == number_xml*20),c("Run", "Firm_Type", "id")]
  count<-count[which(is.na(count$Firm_Type)==FALSE),]
  count_eco<-length(count[which(count$Firm_Type == "eco"),2])
  count_conv<-length(count[which(count$Firm_Type == "conv"),2])
  count_switch_ce<-length(count[which(count$Firm_Type == "switch-c2e"),2])
  count_switch_ec<-length(count[which(count$Firm_Type == "switch-e2c"),2])
  
  write(paste("\n \n How many firms of each type? eco: ", count_eco,", conv: ", count_conv, ", switch: c2e: ", count_switch_ce, " e2c: ", count_switch_ec, " out of ", length(count$Firm_Type), "observations for parameter: ", parameter, sep=""), file = paste(experiment_directory, "/own_Plots/Info", sep = ""), append = append)
  
  rm(list=ls(pattern="count"), object)
  
}


} # end aggr in aggregation levels
## 
## 
## 
if(agent=="Firm" && 1==2){
# Firm size distribution
# 
#agent<-"Firm"
if(exists("data_directory")!=TRUE){
  data_directory<-paste(experiment_directory,"/rdata/logs/levels/",sep="")
#  data_directory<-paste(experiment_directory,"/rdata/",kind_of_data,"/",data_type[d],"/",sep="")
}

if(file.exists(paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))){
  load(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  eval(call("<-",(paste("temp_mat_single",sep="")),object))
  select<-sample(unique(temp_mat_single$Run), 1)
  temp_mat_single<-temp_mat_single[which(temp_mat_single$Run == select),]
  rm(object, select)
  if(agent=="Firm"){
    temp_mat_single<-temp_mat_single[which(as.numeric(temp_mat_single$output ) > 0),]
  }
}
  temp<-max(as.numeric(temp_mat_single$Periods))
  selected_its_temp<-array(NA, dim=floor(temp*20/1000))
  for(i in 1:length(selected_its_temp)){
    selected_its_temp[i]<-1000 * i
  }
if(agent == "Firm" && folder_name == "normalized_by_per-run_avg" && file.exists(paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))){
  source(paste(script_dir, "/aux_all_agents/normalize_by_per-run_avg_selected_snapshots.r", sep=""))
  load(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  eval(call("<-",(paste("temp_mat_single",sep="")),object))
  rm(object)
  temp_mat_selection<-temp_mat_single
  
}else{

  temp_mat_selection<-temp_mat_single[which(is.element(temp_mat_single$Periods, selected_its_temp)),]
}
rm(temp_mat_single)

# 
# Which variables? 
# 
if(exists("variables_temp")!= TRUE){
  eval(call("<-",(paste("variables_temp",sep="")),as.name(paste("variables_available_in_panel_",agent,sep=""))))
}
fix_vars_temp<-c("Periods", "Type")
if(agent=="Firm"){
  fix_vars_temp<-c(fix_vars_temp, "id", "Firm_Type")
}

temp_mat_selection<-temp_mat_selection[c(fix_vars_temp, variables_temp)]

if(exists("plot_dir_selection")!=TRUE){
  source(paste(script_dir,"/aux_all_agents/create_directories_for_plotting.r",sep=""))
}
plot_dir_temp0<-paste(plot_dir_selection,"/Snapshot/", sep="")
if(dir.exists(plot_dir_temp0)!=1){
  dir.create(plot_dir_temp0)
}
plot_dir_temp<-paste(plot_dir_temp0,"/Distribution_",agent,"/", sep="")
if(dir.exists(plot_dir_temp)!=1){
  dir.create(plot_dir_temp)
}
if(dir.exists(paste(plot_dir_temp,"Density_",agent,"/",sep=""))!=1){
  dir.create(paste(plot_dir_temp,"Density_",agent,"/",sep=""))
}
if(dir.exists(paste(plot_dir_temp,"Summary_tables_",agent,"/",sep=""))!=1){
  dir.create(paste(plot_dir_temp,"Summary_tables/",sep=""))
}
if(dir.exists(paste(plot_dir_temp,"Histograms_",agent,"/",sep=""))!=1){
  dir.create(paste(plot_dir_temp,"Histograms_",agent,"/",sep=""))
}
    
lines_temp<-length(variables_temp)
summary_table<-data.frame(array(NA, dim=c(lines_temp, 7)))
colnames(summary_table)<-c("Mean", "Variance", "Variation coeff.",  "Skewness", "Kurtosis", "Min", "Max")
rownames(summary_table)<-variables_temp

cols<-match(variables_temp, colnames(temp_mat_selection)) # all cols except fix vars
cols<-cols[is.na(cols)!= TRUE]

for(it in selected_its_temp){
  data<-temp_mat_selection[which(temp_mat_selection$Periods == as.character(it)),]
  df_data0<-data[match(fix_vars_temp, colnames(data))]
  types<-unique(data$Type)
  increment<-0
  for(v in cols){
    increment<-increment+1
    var_name<-colnames(data)[v]
    # Reshape the data, i.e. extract column data and use melt to bring it to plot 
    # format! 
    df_data<-cbind(df_data0, data[v])
    df_data<-melt(df_data, measure.vars = var_name)
    df_data<-df_data[which(is.infinite(df_data$value) == FALSE),]
    df_data<-df_data[which(is.na(df_data$value) == FALSE),]
    #
    if(length(df_data[,1]) < no_agents * runs * 0.1 || is.numeric(df_data$value)==FALSE){
      print(paste("df_data element either not numeric or very few remaining observations after removing inf and na. In hist_and_dens_temp_mat_firm", var_name, v, sep=" "))
      break
    }
    #if(1==2){
    #
    # Write summary table:  
    for(type in types){
      df_data_temp<-df_data[which(df_data$Type == type),]
      mean<-mean(df_data_temp$value, na.rm = TRUE)
      vari<-var(df_data_temp$value, na.rm = TRUE)
      var_coeff<-sd(df_data_temp$value, na.rm = TRUE)/mean * 100
      skew<-skewness(df_data_temp$value, na.rm = TRUE)
      kurt<-kurtosis(df_data_temp$value, na.rm = TRUE)
      min<-min(df_data_temp$value, na.rm = TRUE)
      max<-max(df_data_temp$value, na.rm = TRUE)
      if(v==cols[1]){
        eval(call("<-",(paste("table_temp_", type,sep="")),as.name(paste("summary_table",sep=""))))
      }
      
      eval(call("<-",(paste("temp",sep="")),as.name(paste("table_temp_", type,sep=""))))
      temp[increment,]<-c(mean,vari, var_coeff, skew, kurt, min, max)
      
      eval(call("<-",(paste("table_temp_", type,sep="")),temp))
      
    # Write summary table to file: 
      if(v==cols[length(cols)]){
        caption<-paste("Table of moments:", type, data_type[d], sep=" ")
        tab<-xtable(temp, caption = caption)
        digits(tab) <- 3
        #align(tab)<-paste(rep("lp{2.5cm}",min(limit+1, 1+length(indices)-(cols-1)*limit)), sep="")
        print(tab, file=paste(plot_dir_temp,"Summary_tables/",var_name,".tex",sep=""))
      }
    }
    
      if(abs(max-min)> 1e-4){
        
        #Plot density, density per run and relative frequency histogram
        pdf(paste(plot_dir_temp,agent,"_",it, "_",parameter,"_",var_name,"_density.pdf",sep=""))
        
        limits<-c(mean-3*sqrt(vari),mean+3*sqrt(vari))
        p1<-ggplot(df_data, aes(x=value))
        #plot<-p+geom_path(aes(group=interaction(variable,Run)))+ xlab("Periods")+scale_colour_discrete("") + ylab("")
        plot<-p1 + geom_density(aes(col=Type))

        plot<-plot  +scale_x_continuous(limits = limits)
        plot<- plot   +  stat_function(fun = dnorm, args = list(mean = mean, sd = sqrt(vari)), col="grey", linetype="dashed")
        plot<- plot + ylab("Density") + xlab(var_name) +  theme(legend.justification=c(0.95,0.95), legend.position=c(0.95,0.95), legend.title = element_blank())#, plot.margin = unit(c(2, 2, 1, 1), "cm"))
        print(plot)
        
        dev.off()
        
        pdf(paste(plot_dir_temp,agent,"_",it,"_",parameter,"_",var_name,"_histogram.pdf",sep=""))
        plot<-p1+geom_freqpoly(aes(y = (..count..)/sum(..count..)), bins = 80, alpha=.5)#+ xlab("Periods")+scale_colour_discrete("") + ylab("")
        plot<-plot + geom_histogram(aes(y = (..count..)/sum(..count..), col=Type), bins = 80, alpha=.1)  +  stat_function(fun = dnorm, args = list(mean = mean, sd = sqrt(vari)), col="grey", linetype="dashed")

        plot<- plot +scale_x_continuous(limits = limits) + ylab("Relative frequency") + xlab(var_name)
        print(plot)
        
        dev.off()
      } # if abs(max-min)>1e-5
    #}
    if(agent == "Firm"){
    if(abs(max-min)> 1e-4){
      
      df_data<-df_data[which(is.na(df_data$Firm_Type)==FALSE),]
      
      #Plot density, density per run and relative frequency histogram
      pdf(paste(plot_dir_temp,"Firm_type_",it, "_",parameter,"_",var_name,"_density.pdf",sep=""))
      
      limits<-c(mean-3*sqrt(vari),mean+3*sqrt(vari))
      p1<-ggplot(df_data, aes(x=value))
      #plot<-p+geom_path(aes(group=interaction(variable,Run)))+ xlab("Periods")+scale_colour_discrete("") + ylab("")
      plot<-p1 + geom_density(aes(col=Firm_Type))
      
      plot<-plot  +scale_x_continuous(limits = limits)
      plot<- plot   +  stat_function(fun = dnorm, args = list(mean = mean, sd = sqrt(vari)), col="grey", linetype="dashed")
      plot<- plot + ylab("Density") + xlab(var_name) +  theme(legend.justification=c(0.95,0.95), legend.position=c(0.95,0.95), legend.title = element_blank())#, plot.margin = unit(c(2, 2, 1, 1), "cm"))
      print(plot)
      
      dev.off()
      
      pdf(paste(plot_dir_temp,"Firm_type_",parameter,"_",var_name,"_histogram.pdf",sep=""))
      plot<-p1+geom_freqpoly(aes(y = (..count..)/sum(..count..)), bins = 80, alpha=.5)
      plot<-plot + geom_histogram(aes(y = (..count..)/sum(..count..), col=Type), bins = 80, alpha=.1)  +  stat_function(fun = dnorm, args = list(mean = mean, sd = sqrt(vari)), col="grey", linetype="dashed")
      
      plot<- plot +scale_x_continuous(limits = limits) + ylab("Relative frequency") + xlab(var_name)
      print(plot)
      
      dev.off()
    }
    }
    } # v in cols
  }
rm(list=ls(pattern="temp"), plot, p1, df_data, tab, df_data0, summary_table, vari, var_coeff, var_name, v, skew, mean, min, max)
}
on.exit(if(is.null(dev.list()) == F){ dev.off()}, add=T)
