# Input: 
# Output: reg_data_temp
# 
# 1. Specify variables of interest
# 2. Get required data
# 3. Make transformations if desired (e.g. smoothing)
# 
if(agent=="Eurostat" || agent=="Government"){
  periods_temp<-c("600","1800","3000","9000","12000","14780")
}else if(agent == "Firm"){ 
  # For firm agent use 620 as first period due to some delay in the percolation of 
  # market entry effects (esp. important for accounting of techn. variables.)
  periods_temp<-c("620","1800","3000","9000","12000","14780")
}

if(reg_level == "Eurostat"){
  vars_temp<-c("specific_skill_gap", "frontier_gap", 
               "average_s_skill_conv", "techn_frontier_conv",  
               "monthly_output", "no_active_firms", "eco_price_wage_ratio", 
               "share_conv_capital_used", 
               "min_learning_coefficient_var", "learning_spillover_strength_var", 
               "base_tax_rate", "unemployment_rate")
}else if(reg_level == "Firm"){
  vars_temp<-c("specific_skill_gap", "frontier_gap", 
               "technology_conv" , "mean_spec_skills_conv", 
               "no_employees", "output", "age", "price", "unit_costs",
               "share_conventional_vintages_used", "technology_gap", 
               "mean_specific_skill_gap", "min_learning_coefficient_var", 
               "learning_spillover_strength_var", 
               "base_tax_rate")
}
if(with_policy){
  vars_temp<-c(vars_temp, c("eco_tax_revenue_per_gdp","eco_subsidy_payment_per_gdp", 
               "eco_tax_rate", "eco_consumption_subsidy", "eco_investment_subsidy"))
}

transform_to_pct_points_temp<-c("eco_tax_rate", "eco_consumption_subsidy", "eco_investment_subsidy",
                                "specific_skill_gap", "frontier_gap",
                                "technology_gap", "mean_specific_skill_gap")
vars_in_600_temp<-c("specific_skill_gap", "frontier_gap",
                    "technology_gap", "mean_specific_skill_gap")

if(exists("DATA_DIRECTORY") == F){
  if(exists("parameter")==F){parameter<-parameters_tmp[1]}
  print(paste("Get regression data for parameter ", parameter))
  p_temp<-parameter
  if(exists("data_directory")!=T){data_directory<-paste(experiment_directory,"/rdata/logs/levels/",sep="")}
  data_from<-paste(data_directory,"data_single_large_",experiment_name,"_",p_temp,"_",sep="")
  if(exists("data_temp")){
    rm(data_temp)
  }
  for(agent in c("Eurostat", "Firm", "Government")){
    if(exists(paste("variables_available_in_panel_",agent,sep="")) != T){
      source(paste(script_dir,"/aux_all_agents/get_var_available_in_panel.r", sep=""))
    }else{print(paste("avail", agent))}
    vars_to_get_temp<-intersect(vars_temp, eval(as.name(paste("variables_available_in_panel_",agent,sep=""))))
    if(length(vars_to_get_temp) > 0){
      if(file.exists(paste(data_from,agent,".RData",sep=""))){
        load(file=paste(data_from,agent,".RData",sep=""))
        if(agent == "Firm"){
          object<-object[order(as.numeric(as.character(object$Run)), as.numeric(as.character(object$id)), as.numeric(as.character(object$Periods))),]
        }else{        
          object<-object[order(as.numeric(as.character(object$Run)), as.numeric(as.character(object$Periods))),]
        }
        if(agent != "Firm"){
          if(exists("data_temp") == FALSE){
            data_temp<-object[match(c("Periods", "Run","Type",vars_to_get_temp), colnames(object))]
          }else{
            data_temp<-cbind(data_temp, object[match(vars_to_get_temp, colnames(object))])
          }
        }else{
          data_firm<-object[match(c("Periods", "Run","Type","id",vars_to_get_temp), colnames(object))]
        }
        rm(object)
      }else{
        stop("Data not existing. get_data_spill.r")
      }
    } # end if length var to get > 0
  }# end load data for each agent
  
  if(exists("data_firm") && reg_level == "Firm"){
    rep_temp<-2
  }else{
    rep_temp<-1
  }
  
  for(i in rep_temp){
    if(i == 1){
      work<-data_temp
    }else if(i==2){
      work<-data_firm
      work$id<-round(as.numeric(as.character(work$id)))
      rm(data_firm)
      #stop("i == 2")
    }
    work$Periods<-as.numeric(as.character(work$Periods))
    periods_temp<-as.numeric(as.character(periods_temp))
    periods_select_temp<-c()
    for(p in periods_temp){
      periods_select_temp<-c(periods_select_temp,seq(p, (p+220), 20))
    }
    inds_temp<-which(work$Periods %in% periods_select_temp)
    work<-work[inds_temp,]
    runs_temp<-work$Run
    pers_temp<-work$Periods
    #types_temp<-work$Type
    cols_temp<-(match(vars_temp, colnames(work)))
    cols_temp<-cols_temp[which(!is.na(cols_temp))]
    c_temp<-colnames(work)
    for(c in cols_temp){
      work[,c]<-as.numeric(as.character(work[,c]))
    }
    r_temp<-unique(runs_temp)
    if(i == 2){
      ids_temp<-work$id
    }
    # Loop: per run aggregate one year of observations
    r<-r_temp[1]
    p<-1
    for(r in r_temp){
      r_inds<-which(runs_temp == r)
      print(r)
      for(p in 1:length(periods_temp)){
        p1<-12*(p-1)+1
        p_temp<-periods_select_temp[p1:(p1+11)]
        p_inds<-which(pers_temp[r_inds] %in% p_temp)
        w_inds<-r_inds[p_inds]
        w_temp<-work[w_inds,]
        if(i == 1){
          str_temp<-w_temp[12,]
          for(c in cols_temp){
            if(is.na(match(c_temp[c], vars_in_600_temp))){
              m_temp<-mean(w_temp[,c], na.rm=TRUE)
              if(is.na(m_temp) || is.infinite(m_temp)){
                stop("m_temp is inf or na")
              }
            }else{
              m_temp<-w_temp[1,c]
            }
            str_temp[,c]<-m_temp
          } # end c in cols_temp
          if(p == 1){
            temp0<-str_temp
          }else{
            temp0<-rbind(temp0,str_temp)
          }
        }else if(i == 2){
          #not_na_inds<-which(!is.na(w_temp$output))
          #w_temp<-w_temp[not_na_inds,]
          #not_na_inds<-which(!is.infinite(w_temp$output))
          #w_temp<-w_temp[not_na_inds,]
          id_temp<-unique(w_temp$id)
          id<-id_temp[2]
          for(id in id_temp){
            inds_temp<-which(w_temp$id == id)
            if(length(inds_temp) < 12){
              print(paste("warning: less than 12 obs", id, p, r))
            }else{
              wrk_temp<-w_temp[inds_temp,]
              str_ag_temp<-wrk_temp[12,]
              for(c in cols_temp){
                if(is.na(match(c_temp[c],vars_in_600_temp))){
                  m_temp<-mean(wrk_temp[,c])#, na.rm=TRUE)
                  #if(is.na(m_temp) || is.infinite(m_temp)){
                  #  stop("m_temp is na or inf")
                  #}
                  if(is.infinite(m_temp)){
                    m_temp<-NA
                  }
                }else{m_temp<-wrk_temp[1,c]}
                str_ag_temp[c]<-m_temp
              }
              if(id == id_temp[1]){
                str_temp<-str_ag_temp
              }else{
                str_temp<-rbind(str_temp, str_ag_temp)
              }
            } # else length obs == 12
          }# end id in id temp
          if(p == 1){
            temp0<-str_temp
          }else{
            temp0<-rbind(temp0, str_temp)
          }
          #stop("get id temp")
        } # end if i == 2 (if i == 2: firm data and aggr. sens. to id)
      } # end p in pers_temp
      if(r == r_temp[1]){
        temp<-temp0
      }else{
        temp<-rbind(temp, temp0)
      }
    }# end r in r_temp
    # 
    # Transform selected var to pct. points (barriers and policy)
    transf_temp<-match(transform_to_pct_points_temp, colnames(temp))
    transf_temp<-transf_temp[which(!is.na(transf_temp))]
    temp[,transf_temp]<-100*temp[,transf_temp]
    
    # Separate into main controls and target variable and additional controls in matrix. 
    if(i == 1){
      data_euro<-temp
      save(data_euro, file = paste(data_from,"data_Eurostat_",parameter,".RData",sep=""))
      
      rm(data_temp)
    }else if(i == 2){
      inds_na<-which(is.na(temp$output))
      data_firm<-temp[-inds_na,]
      save(data_firm, file = paste(data_from,"data_Firm_",parameter,".RData",sep=""))
      
      rm(id)
    }
    
  } # end for i in rep_temp
  rm(i, p, c, work, r, p1)
  
  
} # exists pre-compiled regression data. 

rm(list=ls(pattern="temp"))
rm(list=ls(pattern="_inds"))

