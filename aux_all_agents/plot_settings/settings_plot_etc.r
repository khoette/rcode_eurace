eco<-"limegreen"
conv<-"salmon"
switch<-"royalblue"

baseline<-"black"
experiment<-"red"
invest<-"blue"
cons<-"orange"

if(agent=="Eurostat" || agent == "Government"){
  make_plots <- 1
  no_agents<-1
}else if(agent == "Firm"){
  make_plots <- 0
  no_agents<-120
}
