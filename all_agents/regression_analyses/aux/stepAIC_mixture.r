## stepAIC reg routine
capture_results <- function(reg_equ, object_raw, file_name_aic, table_for_stepaic, colname_temp){
  write(paste("\n \n ### \n ", paste(targ, paste(reg_equ)[c(1,3)], collapse=" "), "\n"), file=file_name_aic, append=T)
  capture.output(object_raw, file=file_name_aic, append=T)
  capture.output(summary(object_raw, diagnostics=T), file=file_name_aic, append=T)
  table_for_stepaic<-write_results_to_table(object_raw, table_for_stepaic, file_name_aic)
  return(table_for_stepaic)
}


### Test diff. distrib. families: 
#library(gamlss.mx)
if(targ=="Time"){
  target_mix<-target-min(target)
  zero_comp<-T
  k_temp<-4
  pr_temp<-NULL
  w_temp<-NULL
  
}else{
  target_mix<-target
  zero_comp<-F
  k_temp<-2
  pr_temp<-c(sum(type)/length(type), (1-sum(type)/length(type)))
  w_temp<-type
  
}


modelNO_rm <- gamlssMX(target_mix~1,family=NO,weights = w_temp, prob=pr_temp,zero.component = zero_comp,K=k_temp,data=na.omit(data_temp_mix))
modelGA_rm <- gamlssMX(target_mix~1,family=GA,weights = w_temp, prob=pr_temp,zero.component = zero_comp,K=k_temp,data=na.omit(data_temp_mix))
modelIG_rm <- gamlssMX(target_mix~1,family=IG,weights = w_temp, prob=pr_temp,zero.component = zero_comp,K=k_temp,data=na.omit(data_temp_mix))
modelWEI_rm <- gamlssMX(target_mix~1,family=WEI,weights = w_temp, prob=pr_temp,zero.component = zero_comp,K=k_temp,data=na.omit(data_temp_mix))
modelRG_rm <- gamlssMX(target_mix~1,family=RG,weights = w_temp, prob=pr_temp,K=k_temp,data=na.omit(data_temp_mix))
modelLO_rm <- gamlssMX(target_mix~1,family=LO,weights = w_temp, prob=pr_temp,K=k_temp,data=na.omit(data_temp_mix))
aic_list<-AIC(modelNO_rm,modelGA_rm,modelIG_rm,modelRG_rm,modelLO_rm)

choice<-as.name(rownames(aic_list)[1])
# In targ == share_conv_capital_used -> WEI performs best. 
model_choice<-eval(choice)
family_temp<-model_choice$family
rm(list=ls(pattern="_rm"))
## Make test whether global maximum achieved: 
model0<-gamlssMXfits(n=4, target_mix~1,family=family_temp,K=k_temp,data=na.omit(data_temp_mix))

## Check mixture model
## 
pdf(paste(plot_dir_temp_aic,"/histogram_",targ,"_multimode.pdf", sep=""))
plot(model_choice)
dev.off()

reg_obj_both<-gamlssMX(formula = reg_equation,pi.fomula = pi_equation,prob=pr_temp,weights = w_temp, family=family_temp,K=k_temp, data=data_temp_mix)
reg_obj_main<-gamlssMX(formula = reg_equation,pi.formula = ~1,prob=pr_temp,weights = w_temp, family=family_temp,K=k_temp, data=data_temp_mix)
reg_obj_pi<-gamlssMX(formula = target_mix~1,pi.fomula = pi_equation,prob=pr_temp,weights = w_temp, family=family_temp,K=k_temp, data=data_temp_mix)

test_aic<-AIC(reg_obj_both, reg_obj_main, reg_obj_pi)
reg_obj<-eval(as.name(rownames(test_aic)[1]))
if(rownames(test_aic)[1] != "reg_obj_pi"){
  object_raw<-stepGAIC(reg_obj, k=log(n))
  reg_equ<-reg_equation
}else{
  object_raw<-reg_obj_pi
  reg_equ<-reg_obj_pi
}
table_for_stepaic<-capture_results(reg_equ, object_raw, file_name_aic, table_for_stepaic, colname_temp)

reg_call_target_probit<-(as.character(object_raw$call)[2])
vars_reg_temp_target_probit<-as.character(as.formula(reg_call_target_probit))[3]
