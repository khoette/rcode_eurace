if(no_agents == 1){
  if(file.exists(file=paste(experiment_directory,"/rdata/",kind_of_data,"/Cycle/data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep="")) == FALSE){ 
    
  # Bandpass detrending
  # 
  # # Data input: Format data_all_eco_id
  # i.e. no_agents x number xml x runs x variables
  # 
  #  Apply filter to 1:number_xml separately per run and per agent, then normalize. 
  # 
  load(file=paste(experiment_directory,"/rdata/raw_data/data_all_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  data<-object
  rm(object)

  use_ts_data<-1

  #Find out which data is available. 
  eval(call("<-",(paste("list_of_var",sep="")),as.name(paste("list_of_var_",agent,sep=""))))
  #if(exists("variables_for_bandpass") != TRUE){
  vars <- list_of_var
  #}else{
  # vars<-variables_for_bandpass
  # } 
  #
    # Take logs but check that logs not yet taken... 
    # 
  source(paste(script_dir,"/aux_all_agents/variables_settings_for_agents.r", sep=""))  
  if(exists("variables_selection")==FALSE || is.element(NA, match(variables_selection, list_of_var)) || length(variables_selection) > length(list_of_var)){
    variables_selection<-list_of_var
  }
    if(agent == "Eurostat"){
      temp_test<-"monthly_output"
    }else if(agent=="Government"){
      temp_test<-"monthly_income"
      if(is.na(match(temp_test, variables_selection)) == TRUE){
        temp_test<-var_log[length(var_log)]
      }
    }else if(agent=="Firm"){
      temp_test<-"output"
    }else{
      take_logs<-0
    }
    if(is.element(NA, match(var_log, variables_selection)) == TRUE){
      var_log<-var_log[is.na(match(var_log, variables_selection)) == FALSE]
    }
    

  if(take_logs == 1 && max(data[1,,,match(temp_test, variables_selection)], na.rm = TRUE) > 100) {
    indices<-match(var_log, variables_selection)
    data_temp<-data
    for(i in 1:no_agents){
      for(r in 1:runs){
        for(t in 1:number_xml){
          for(v_temp in indices){
            if(is.na(data[i,t,r,v_temp])==FALSE){
              if(data[i,t,r,v_temp]>0){
                data_temp[i,t,r,v_temp]<-log(data[i,t,r,v_temp])
              }else{
                data_temp[i,t,r,v_temp]<-(-1)*log(abs(data[i,t,r,v_temp]))
              }
            }else{
              data[i,t,r,v_temp]<-NA
            }
          }
        }
      }
      data<-data_temp
      #data_temp[1:no_agents,,,indices]<-log(data[1:no_agents,,,indices])
      #data_temp[is.nan(data_temp)]<-(-1)*log(abs(data[1:no_agents, ,,indices]))
    }
    kind_of_data<-"logs"
  }else{
    kind_of_data<-"levels"
  }

  v<-match(vars, list_of_var)
  data_detrended_bandpass<-data[,,,v, drop=FALSE]

  rm(data,v)
  data_mFilter_ts<-array(0,dim=c(3,number_xml, runs, length(vars)))
  info_table<-data.frame(ur=rep(0,length(vars)), drift=rep(0,length(vars)), trend=rep(0,length(vars)))
  rownames(info_table)<-vars
  print(paste("Compute table to determine utilization of UR and drift."))
  for(v in 1:length(vars)){ # If data set too large, apply filter vector-wise
    for(r in 1:runs){
      for(i in 1:no_agents){
        ts<-as.ts(as.matrix(data_detrended_bandpass[1,,r,v], frequency=12))
        #ts[which(is.infinite(ts) == TRUE)]<-NA
        ts<-ts[which(is.infinite(ts) == FALSE)]
        if(length(na.omit(ts)) > 12){
          ts<-na.contiguous(ts)
          if(length(ts)>120){
            df1<-ur.df(ts, type="trend", lags=12, selectlags = "AIC")
            #df2<-ur.df(ts, type="drift", lags=12)
            #df3<-ur.df(ts, type="none", lags=12)
            root<-df1@teststat>df1@cval[,2] # chose root and drift on 5% level
            info_table[v,]<-info_table[v,]+ (1*root)
          }
        }
      }
    }
  }
  print(paste("Apply bandpass filter."))
  for(v in 1:length(vars)){ 
    print(paste(vars[v]))
    for(r in 1:runs){
      for(i in 1:no_agents){
        #ts<-as.ts(data_detrended_bandpass[1,,r,v], deltat=1/12)
        ts<-as.ts(as.matrix(data_detrended_bandpass[1,,r,v], frequency=12))
        ts[which(is.infinite(ts) == TRUE)]<-NA
        ts<-ts[which(is.infinite(ts) == FALSE)]
        root<-c(info_table[v,1]>0.5 , info_table[v,2]>0.5)
        root[which(is.na(root) == TRUE)]<-FALSE
        if(length(ts)>120){
          test<-mFilter(ts, filter="BK", root = root[1], drift=root[2], nfix=12)
          data_mFilter_ts[2,,r,v]<-as.vector(test$trend)
          data_mFilter_ts[3,,r,v]<-as.vector(test$cycle)
        }else{
          data_mFilter_ts[2,,r,v]<-NA
          data_mFilter_ts[3,,r,v]<-NA
        }
        data_mFilter_ts[1,,r,v]<-as.vector(ts)

      }
    }
  }
  rm(test, root, df1, i, v, r, ts, data_detrended_bandpass)

  ts_names<-c("Time_series", "Trend", "Cycle")
  variables<-vars
  variables_orig<-vars
  for(ts in 1:3){
    name<-ts_names[ts]
    if(dir.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/",name,sep=""))!=1){
      dir.create(paste(experiment_directory,"/rdata/",kind_of_data,"/",name,sep=""))
    }
    ts_data<-1
    print(paste(ts_names[ts]))
    source(paste(script_dir,"/aux_all_agents/get_data_df.r",sep=""))
  }
  rm(data, data_mFilter_ts)
  rm(list=ls(pattern="temp"))
  } # if data file.exists
  else{
    print(paste("Bandpass data already exists: ", agent, parameter, sep=" "))
  }
}# if no agents == 1
