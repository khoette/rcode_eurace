#parameter<-bau_value
cases<-match(aggregation_levels, available_aggregation_levels)
cases<-cases[which(is.na(cases) != TRUE)]
if(is.element("normalized_by_per-run_avg", data_type) && data_type[d] == "normalized_by_per-run_avg"){
  cases<-1
}
if(is.element(1, cases)){
  load(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  eval(call("<-",(paste("temp_mat_single_large",sep="")),object))
}
if(folder_name!="normalized_by_per-run_avg"){
  if(is.element(2, cases)){
    load(file=paste(data_directory,"data_batch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
    eval(call("<-",(paste("temp_mat_batch",sep="")),object))
  }
  if(is.element(3, cases)){
    load(file=paste(data_directory,"data_eco_conv_batch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
    eval(call("<-",(paste("temp_mat_eco_conv_batch",sep="")),object))
  }
  if(is.element(4, cases)){
    load(file=paste(data_directory,"data_single_switch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
    eval(call("<-",(paste("temp_mat_single_switch",sep="")),object))
  }
  if(is.element(5, cases)){
    load(file=paste(data_directory,"data_eco_conv_switch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
    eval(call("<-",(paste("temp_mat_eco_conv_switch",sep="")),object))
  }
}


source(paste(script_dir, "/aux_all_agents/plot_settings/plot_settings.r", sep=""))


if(dir.exists(paste(plot_dir_distr,"Summary_tables/",sep=""))!=1){
  dir.create(paste(plot_dir_distr,"Summary_tables/",sep=""))
}
plot_dir_temp0<-paste(plot_dir_distr,"Summary_tables/",sep="")

if(is.element(TRUE,(is.element(c("id", "active"), variables)))){
  print(paste("Warning: id and/or active not removed from variables array in hist_dens_ggplot", agent))
}
if(agent=="Firm"){
  temp_mat_single_large<-temp_mat_single_large[which(as.numeric(temp_mat_single_large$output ) > 0),]
}

#if(exists("data_mFilter_ts") == TRUE && use_ts_data == 1){
if(exists("temp_mat_single_large") == FALSE){
  print("Required data set not existing. Run get data ggplot firm version to get panel data set!")
}else{
# Get index to identify eco scenarios: 
#eco_share<-match("share_conv_capital_used", list_of_var_with_eco)
for(it in cases){
  if(it == 1){
    data<-temp_mat_single_large
    fix_vars<-c("Type", "Periods", "Run")
    if(agent=="Firm"){
      fix_vars<-c(fix_vars, "id")
    }
    name<-"Single_large"
  }else if(it == 2){
    data<-temp_mat_eco_conv_batch
    fix_vars<-c("Type", "Periods")
    name<-"Eco_conv_batch"
  }else if(it == 3){
    data<-temp_mat_batch
    fix_vars<-c("Periods")
    name<-"Batch"
  }else if(it == 4){
    data<-temp_mat_single_switch
    fix_vars<-c("Type", "Periods", "Run")
    if(agent=="Firm"){
      fix_vars<-c(fix_vars, "id")
    }
    name<-"switch"
  }
  else if(it == 5){
    data<-temp_mat_single_switch
    fix_vars<-c("Type", "Periods", "Run")
    if(agent=="Firm"){
      fix_vars<-c(fix_vars, "id")
    }
    name<-"eco_conv_switch"
  }
if(agent == "Firm"){
  data<-data[which(data$active == 1),]
  data<-data[which(data$output > 1e-3),]
}

plot_dir_temp<-paste(plot_dir_temp0,"/",name,"/", sep="")
if(dir.exists(plot_dir_temp)!=1){
  dir.create(plot_dir_temp)
}
if(dir.exists(paste(plot_dir_temp,"Density/",sep=""))!=1){
  dir.create(paste(plot_dir_temp,"Density/",sep=""))
}

if(dir.exists(paste(plot_dir_temp,"Histograms/",sep=""))!=1){
  dir.create(paste(plot_dir_temp,"Histograms/",sep=""))
}

 
  lines<-length(variables)
  summary_table<-data.frame(array(NA, dim=c(lines, 7)))
  colnames(summary_table)<-c("Mean", "Variance", "Variation coeff.",  "Skewness", "Kurtosis", "Min", "Max")
  rownames(summary_table)<-variables

  cols<-match(fix_vars, colnames(data))
  df_data0<-data[,cols]
  cols<-match(variables, colnames(data))
  cols<-cols[is.na(cols)!= TRUE]
for(v in cols){
  var_name<-colnames(data)[v]
  # Reshape the data, i.e. extract column data and use melt to bring it to plot 
  # format! 
  df_data<-cbind(df_data0, data[v])
  if(it==3){
    colnames(df_data)[1:length(fix_vars)]<-fix_vars
  }
  df_data<-melt(df_data, measure.vars = var_name)
  #df_data<-df_data[1:100000,]
  df_data<-df_data[which(is.infinite(df_data$value) == FALSE),]
  df_data<-df_data[which(is.na(df_data$value) == FALSE),]
  
  # 
  if(length(df_data[,1]) < number_xml || is.numeric(df_data$value)==FALSE){
    print(paste("df_data element either not numeric or very few remaining observations after removing inf and na. In hist_and_dens_temp_mat_firm", var_name, v, sep=" "))
    break
  }
  #
  # Write summary table:  
  mean<-mean(df_data$value, na.rm = TRUE)
  vari<-var(df_data$value, na.rm = TRUE)
  var_coeff<-sd(df_data$value, na.rm = TRUE)/mean * 100
  skew<-skewness(df_data$value, na.rm = TRUE)
  kurt<-kurtosis(df_data$value, na.rm = TRUE)
  min<-min(df_data$value, na.rm = TRUE)
  max<-max(df_data$value, na.rm = TRUE)
  summary_table[v,]<-c(mean,vari, var_coeff, skew, kurt, min, max)
  
  # Write summary table to file: 
  if(v==cols[length(cols)]){
  caption<-paste("Table of moments:", name, data_type[d], sep=" ")
  tab<-xtable(summary_table, caption = caption)
  digits(tab) <- 3
  #align(tab)<-paste(rep("lp{2.5cm}",min(limit+1, 1+length(indices)-(cols-1)*limit)), sep="")
  print(tab, file=paste(plot_dir_temp0,"Summary_tables/",name,".tex",sep=""))
  }
  # Plot time series: Variation over time 
  pdf(paste(plot_dir_temp,agent,"_",parameter,"_",var_name,"_variation_over_time.pdf",sep=""))
  if(it<3 || it == 4 || it == 5){
    p1<-ggplot(df_data, aes(x=reorder(Periods, as.numeric(Periods)), y=value, color =Type))
  }else{
    p1<-ggplot(df_data, aes(x=reorder(Periods, as.numeric(Periods)), y=value))
  }
  plot<- p1+ geom_point(shape = ".", alpha = 0.01) 
  plot<-plot +scale_x_discrete(breaks=seq(0,number_xml*20,5000)) + scale_y_continuous(limits=c(min,max)) 
  plot<-plot+ guides(colour = guide_legend(override.aes = list(shape = 15, alpha=1)))
  plot<- plot + theme(legend.justification=c(0.05,0.95), legend.position=c(0.05,0.95), legend.title = element_blank()) + ylab(var_name) + xlab("Time")
  
  print(plot)
  #+ theme(legend.justification=c(0.95,0.95), legend.position=c(0.95,0.95), legend.title = element_blank())
  dev.off()

  if(abs(max-min)> 1e-4){
    
    #Plot density, density per run and relative frequency histogram
    pdf(paste(plot_dir_temp,"Density/",agent,"_",parameter,"_",var_name,"_density.pdf",sep=""))
  
    limits<-c(mean-3*sqrt(vari),mean+3*sqrt(vari))
    p1<-ggplot(df_data, aes(x=value))
    #plot<-p+geom_path(aes(group=interaction(variable,Run)))+ xlab("Periods")+scale_colour_discrete("") + ylab("")
    if(it==1 || it == 2 || it ==4 || it == 5){
      plot<-p1 + geom_density(aes(col=Type))
    }else if(it==3){
      plot<-p1 + geom_density(aes(col=variable))
    }
    plot<-plot  +scale_x_continuous(limits = limits)
    plot<- plot   +  stat_function(fun = dnorm, args = list(mean = mean, sd = sqrt(vari)), col="grey", linetype="dashed")
    plot<- plot + ylab("Density") + xlab(var_name) +  theme(legend.justification=c(0.95,0.95), legend.position=c(0.95,0.95), legend.title = element_blank())#, plot.margin = unit(c(2, 2, 1, 1), "cm"))
    print(plot)

    dev.off()
  
    if(it==1 || it == 4){
    pdf(paste(plot_dir_temp,"Density/",agent,"_",parameter,"_",var_name,"_density_eco_conv_single_runs.pdf",sep=""))
    plot<-p1 + geom_density(aes(col=Type, group=interaction(variable, Run)), alpha=alpha, lwd=lwd) 
    plot<- plot   +  stat_function(fun = dnorm, args = list(mean = mean, sd = sqrt(vari)), col="grey", linetype="dashed")
    plot<- plot +scale_x_continuous(limits = limits) + ylab("Density") + xlab(var_name) +  theme(legend.justification=c(0.95,0.95), legend.position=c(0.95,0.95), legend.title = element_blank())#, plot.margin = unit(c(2, 2, 1, 1), "cm"))
    plot<-plot + guides(colour = guide_legend(override.aes = list(shape = 15, alpha=1)))
    print(plot)
    dev.off()
    
    pdf(paste(plot_dir_temp,"Density/",agent,"_",parameter,"_",var_name,"_density_batch_like_single_runs.pdf",sep=""))
    plot<-p1 + geom_density(aes(group=interaction(variable, Run)), alpha=alpha, lwd=lwd) 
    plot<- plot   +  stat_function(fun = dnorm, args = list(mean = mean, sd = sqrt(vari)), col="grey", linetype="dashed")
    plot<- plot +scale_x_continuous(limits = limits) + ylab("Density") + xlab(var_name) +  theme(legend.justification=c(0.95,0.95), legend.position=c(0.95,0.95), legend.title = element_blank())#, plot.margin = unit(c(2, 2, 1, 1), "cm"))
    plot<-plot + guides(colour = guide_legend(override.aes = list(shape = 15, alpha=1)))
    print(plot)
    dev.off()
    }
  
    pdf(paste(plot_dir_temp,"Histograms/",agent,"_",parameter,"_",var_name,"_histogram.pdf",sep=""))
    #plot<-p+geom_histogram(bins = 80, alpha=.1)#+ xlab("Periods")+scale_colour_discrete("") + ylab("")
    if(it==1 || it ==2 || it>=4){
      plot<-p1+geom_freqpoly(aes(y = (..count..)/sum(..count..)), bins = 80, alpha=.5)#+ xlab("Periods")+scale_colour_discrete("") + ylab("")
      plot<-plot + geom_histogram(aes(y = (..count..)/sum(..count..), col=Type), bins = 80, alpha=.1)  +  stat_function(fun = dnorm, args = list(mean = mean, sd = sqrt(vari)), col="grey", linetype="dashed")
    }else if(it == 3){
      plot<-p1+geom_freqpoly(aes(y = (..count..)/sum(..count..)), bins = 80, alpha=.5)#+ xlab("Periods")+scale_colour_discrete("") + ylab("")
      plot<-plot + geom_histogram(aes(y = (..count..)/sum(..count..)), bins = 80, alpha=.1)  +  stat_function(fun = dnorm, args = list(mean = mean, sd = sqrt(vari)), col="grey", linetype="dashed")
    }
    plot<- plot +scale_x_continuous(limits = limits) + ylab("Relative frequency") + xlab(var_name)
    print(plot)

    dev.off()
  } # if abs(max-min)>1e-5
} # v in cols
} # end cases (use disaggr micro data, eco_conv_batch and batch (for growth rates))
rm(df_data, df_data0, name, var_name, min, max, kurt, vari, mean, skew)
}
rm(list=ls(pattern="temp"))
