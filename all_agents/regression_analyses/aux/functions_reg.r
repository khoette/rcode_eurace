round_customized <- function(x, digit = 4, ...){
  x1<-c()
  for(x0 in x){
    if(!is.na(x0)){
      if(abs(x0)<1){d <- digit
      }else if(abs(x0)<10){d<- max(0,(digit-1))
      }else if(abs(x0)<100){d<- max(0,(digit-2))
      }else if(abs(x0)<1000){d<- max(0,(digit - 3))
      }else{d<-max(0, digit-4)}
      x1<-c(x1,round(x0,d))
    }else{
      x1<-c(x1,NA)
    }
  }
  return(x1)
}

#table_for_stepaic<-write_results_to_table(object_raw, table_for_stepaic, file_name_aic)
#write_results_to_table(ols,table_old, file_name_tex)
write_results_to_table = function(obj_raw, table_old_all, file_name_temp, ...){
  #ols,table_old, file_name_tex

  if(exists("digit")!=T){digit<-4}
  table_notes<-c()
  
  if(ncol(table_old_all)>0){
    c_names<-colnames(table_old_all)
    table_doc_old<-as.matrix(table_old_all[1:4,])
    table_old<-as.matrix(table_old_all[5:length(table_old_all[,1]),])
  }else{
    table_old<-as.matrix(table_old_all)
    c_names<-c()
  }
  # Should do: 
  # Write descriptive text (Model description etc.) . 
  # Capture AIC, SBC or equiv. 
  # Use indicative column name. 
  # For GAMLSS obj: How to present shape parameters?? 
  clss<-class(obj_raw)[1]
  obj<-summary(obj_raw)
  if(clss == "gamlss"){
    clss_det<-paste(clss,targ)
    #clss_det<-paste(substr(as.character(obj_raw$call)),1:5) # alternative obj_raw$family
    rsq<-round_customized(as.numeric(as.character(Rsq(obj_raw, type="both"))),digit)
    est0<-obj_raw$mu.coefficients
    est<-round_customized(as.numeric(as.character(obj_raw$mu.coefficients)),digit)
    err<-round_customized(as.numeric(as.character(obj[,"Std. Error"])),digit)
    pval<-round_customized(as.numeric(as.character(obj[,"Pr(>|t|)"])),digit)
    add_info<-c()
    converged<-obj_raw$converged
    #if(obj_raw$converged != T){
      table_notes<-c(table_notes, paste("GAMLSS converged? obj_raw$converged: ", obj_raw$converged))
    #}
  }else{
    #clss_det<-obj$call
    clss_det<-paste(clss,targ)
    if(clss == "ivglm"){rsq<-NA
    }else{rsq<-round_customized(as.numeric(as.character(obj$adj.r.squared)),digit)
    }
    est<-round_customized(as.numeric(as.character(obj$coefficients[,"Estimate"])),digit)
    est0<-obj$coefficients[,"Estimate"]
    err<-round_customized(as.numeric(as.character(obj$coefficients[,"Std. Error"])),digit)
    if(clss == "ivglm"){
      pval<-round_customized(as.numeric(as.character(obj$coefficients[,"Pr(>|z|)"])),digit)
    }else{pval<-round_customized(as.numeric(as.character(obj$coefficients[,"Pr(>|t|)"])),digit)}
    add_info<-c()
    converged<-NA
  }
  
  if(exists("colname_temp")){
    clss_det<-paste(clss,colname_temp,targ)
  }
  rnames<-names(est0)
  if(is.null(rnames)&&length(err)==1){rnames<-"(Intercept)"}
  code<-c()
  if(length(pval)>0){
    for(p in pval){
      if(is.na(p)){code<-c(code,"NA")}
      else if(p<0.001){code<-c(code,"***")}
      else if(p<0.01){code<-c(code,"**")}
      else if(p<0.05){code<-c(code,"*")}
      else if(p<0.1){code<-c(code,".")}
      else{code<-c(code,"")}
    }
  }else{code<-NA}
  rentries<-matrix(NA, nrow=(2*length(est)), ncol=1)
  rownames(rentries)<-rep("name me",(2*length(est)) )
  i<-1
  rnames_tmp<-c()
  for(r in 1:length(rentries)){
    if(r %% 2 == 1){
      rentries[r,1]<-paste(est[i], code[i],sep="")
      rnames_tmp<-c(rnames_tmp,rnames[i])
    }else{
      rentries[r,1]<-paste("(",err[i],")", sep="")
      rnames_tmp<-c(rnames_tmp,paste(""))
      i<-i+1
    }
  }

  rownames(rentries)<-rnames_tmp
  if(ncol(table_old) > 0){
    names_old<-rownames(table_old)
    names_old<-names_old[which(names_old != "")]
  }
  if(exists("names_old")){
    all_names<-union(names_old, rnames)
    new_data<-as.matrix(rep(NA, (2*length(all_names))), ncol=1)
    order_old<-match(rnames, all_names) * 2
    ord_old<-c()
    for(o in order_old){
      ord_old<-c(ord_old, (o-1), o)
    }
    all_n<-c()
    for(n in all_names){
      all_n<-c(all_n, n, "")
    }
    new_data[ord_old]<-as.vector(rentries)
    rownames(new_data)<-all_n
    
    #print(rownames(new_data))
    #print(rownames(table_old))
    if(setequal(rownames(table_old), rownames(new_data)[1:length(rownames(table_old))])!=T){
      warning("Are names equally ordered?")
      stop("Are names equally ordered?")}
  }else{
    new_data<-rentries
  }
  emergency_brake<-0
  while(length(new_data) > nrow(table_old) && emergency_brake < 200){
    new_row<-rep(NA, ncol(table_old))
    table_old<-rbind(table_old, new_row)
    emergency_brake<-emergency_brake+1
    if(emergency_brake==199){stop("Emergency brake in while loop (functions_reg.r)")}
  }
  
  new_result<-new_data

  if(clss != "ivreg" && clss != "ivglm"){
    aic<-round_customized(as.numeric(AIC(obj_raw)), digit)
    bic<-round_customized(as.numeric(BIC(obj_raw)), digit)
  }else{aic<-NA; bic<-NA}
  vec_dat<-matrix(c(aic,bic,rsq[length(rsq)], converged),nrow=4,ncol=1)
  if(ncol(table_old)>0){
    table_new<-cbind(table_old, new_data)
    rownames(table_new)<-all_n
    table_doc<-cbind(table_doc_old, vec_dat)
    table_merge<-rbind(table_doc, table_new)
  }else{
    table_merge<-rbind(vec_dat, new_result)
    table_merge<-as.matrix(table_merge, nrow=length(table_merge), ncol=1)
    rownames(table_merge)[1:4]<-c("AIC", "BIC", "Rsq", "Converged?")
  }
  
  colnames(table_merge)<-c(c_names, paste(as.character(clss_det), collapse=":"))
  table_notes<-c(paste("Col", ncol(table_merge)),table_notes, paste("AIC:", 
                     aic, "BIC:", bic, 
                     "Adj.Rsq:", rsq, add_info,"\n"))
  write(table_notes, file=file_name_temp, append = TRUE)
  
  return(table_merge)
}
#tab_old<-matrix(NA, ncol = 1, nrow=1)
#tab<-write_results_to_table(c(1:4), tab_old)
#table_for_tex<-regr_and_capt_output(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE, use_gamlss = TRUE, family=families_temp)

regr_and_capt_output = function(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE, use_gamlss=FALSE, family = NULL){
  
  if(exists("table_for_tex")){
    table_old<-table_for_tex
  }else{
    table_old<-matrix(NA, ncol=0, nrow=0)
    if(counter>0){
      stop()
    }
  }
  
  write(paste(title_text), file = paste(file_name), append = app)
  ols<-lm(model_ols)
  table_old<-write_results_to_table(ols,table_old, file_name_tex)
  capture.output(summary(ols, digits=4), file=file_name, append = TRUE)
  write(paste("AIC: ",paste(AIC(ols)), "BIC: ",paste(BIC(ols))), file=file_name, append = TRUE)
  if(1==2 && (min_targ>0 || max_targ<0)){
    gama<-glm(model_ols, family=Gamma(link = "log"), maxit = 100)
    table_old<-write_results_to_table(gama,table_old, file_name_tex)
    capture.output(summary(gama, digits=4), file=file_name, append = TRUE)
    capture.output(PseudoR2(gama), file=file_name, append = TRUE)
  }
  if(min_targ >= 0 && max_targ <= 1){
    if(use_gamlss == FALSE){
      logit<-glm(model_binary, family=binomial(link="logit"), maxit = 100)
      probit<-glm(model_binary, family=binomial(link="probit"), maxit = 100)
      table_old<-write_results_to_table(logit,table_old, file_name_tex)
      table_old<-write_results_to_table(logit,table_old, file_name_tex)
      
      capture.output(summary(logit, digits=4), file=file_name, append = TRUE)
      #capture.output(PseudoR2(logit), file=file_name, append = TRUE)
      write(paste("AIC: ",paste(AIC(logit)), "BIC: ",paste(BIC(logit))), file=file_name, append = TRUE)
      
      print(paste("WHY AM I HERE??", use_gamlss))
      capture.output(summary(probit, digits=4), file=file_name, append = TRUE)
      #capture.output(PseudoR2(probit), file=file_name, append = TRUE)
      write(paste("AIC: ",paste(AIC(probit)), "BIC: ",paste(BIC(probit))), file=file_name, append = TRUE)
    }else{
      if(exists("families_temp[2]") && families_temp[2] != "NET" && exists("use_fitDist_models") && use_fitDist_models){
        gamlss_bin<-gamlss(model_binary, family = families_temp[2], method=mixed(5,200))
      }
      if(exists("use_logit") && use_logit){
        logit<-gamlss(model_binary, family=BI(mu.link="logit"))
        if(logit$converged != T){logit<-gamlss(model_binary, family=BI(mu.link="logit"), method=mixed(5,200))}
        capture.output(summary(logit, digits=4), file=file_name, append = TRUE)
        capture.output(Rsq(logit, type="both"), file=file_name, append = TRUE)
        write(paste("AIC: ",paste(AIC(logit)), "BIC: ",paste(BIC(logit))), file=file_name, append = TRUE)
        write(paste("Converged??? ", logit$converged), file=file_name, append = TRUE)
        table_old<-write_results_to_table(logit,table_old, file_name_tex)
        print("logit done")
      }
      if(exists("use_probit") && use_probit){
        probit<-gamlss(model_binary, family=BI(mu.link="probit"))
        if(probit$converged != T){probit<-gamlss(model_binary, family=BI(mu.link="probit"), method=mixed(5,200))}
        
        capture.output(summary(probit, digits=4), file=file_name, append = TRUE)
        capture.output(Rsq(probit, type="both"), file=file_name, append = TRUE)
        write(paste("AIC: ",paste(AIC(probit)), "BIC: ",paste(BIC(probit))), file=file_name, append = TRUE)
        write(paste("Converged??? ", probit$converged), file=file_name, append = TRUE)
      
        table_old<-write_results_to_table(probit,table_old, file_name_tex)
        print("probit done")
      }
    }
    #mlogit<-margins(logit)
    #mprobit<-margins(probit)
    #capture.output(smmary(mlogit), file=file_name, append = TRUE)
    #capture.output(summary(mprobit), file=file_name, append = TRUE)
  } # end binary
  
  if(use_gamlss){
    if(exists("families_temp[1]") && families_temp[1] != "NET"&& exists("use_fitDist_models") && use_fitDist_models){
      if(use_gamlss && families_temp[1] != "NET"&& exists("use_fitDist_models") && use_fitDist_models){
        gamlss_obj<-gamlss(model_ols, family = families_temp[1], method=mixed(5,200))
        #if(gamlss_obj$converged != TRUE){
        #  print("Warning: gamlss_obj not converged after 20 its")
        #  counter_temp<-0
        #  while(gamlss_obj$converged != TRUE && counter_temp<10){
        #    gamlss_obj<-refit(gamlss_obj)
        #    counter_temp<-counter_temp+1
        #    print(counter_temp)
        #  }
        #  print(counter_temp)
        #}
        
      }
      
      capture.output(summary(gamlss_obj, digits=4), file=file_name, append = TRUE)
      write(paste("Converged? ",gamlss_obj$converged),file=file_name, append = TRUE)
      capture.output(Rsq(gamlss_obj, type="both"), file=file_name, append = TRUE)
      table_old<-write_results_to_table(gamlss_obj,table_old, file_name_tex)
      print("gamlss done")
    }
    if(exists("families_temp[2]") && families_temp[2] != "NET"&& exists("use_fitDist_models") && use_fitDist_models){
      capture.output(summary(gamlss_bin, digits=4), file=file_name, append = TRUE)
      write(paste("Converged? ",gamlss_bin$converged),file=file_name, append = TRUE)
      capture.output(Rsq(gamlss_bin, type="both"), file=file_name, append = TRUE)
      table_old<-write_results_to_table(gamlss_bin,table_old, file_name_tex)
      print("gamlss bin done")
    }
  } # if use_gamlss
  
  return(table_old)
  
}














# Functions for clustering
# model <- knn3(Type ~ ., data=x, k = 1)
# decisionplot(model, x, class = "Type", main = "kNN (1)")
# 
#decisionplot(model_temp, data_dec_temp, file_name, class = "Type", labels_temp = c(xlabel, ylabel),
#             main = title, col = c(eco, conv), range_temp = range_temp, 
#             color_temp = color_temp)
#data<-data_dec_temp
#model<-model_temp
#class="Type"
#predict_type<-"class"
#labels_temp<-c(xlabel, ylabel)
#main=title
#col_temp<-c(eco, conv)


decisionplot <- function(model, data, file_name, 
                         class = NULL, predict_type = "class",
                         resolution = 50, showgrid = TRUE, col_temp=NULL, 
                         pch_temp=NULL , alpha_temp=NULL, color_temp=NULL,
                         range_temp = NULL, labels_temp = NULL, ...) {
  if(is.null(range_temp)){
    r <- sapply(data[,c(1,2)], range, na.rm = TRUE)
    #print(paste("Set default range for boundaryies of plot, i.e. inferred by data.", paste(r, collapse=", ")))
  }else{
    r<-range_temp
    inds_temp<-unique(c(which(data[,1] < r[1,1]), which(data[,1] > r[2,1])))
    inds_temp<-unique(c(inds_temp, which(data[,2] < r[1,2]), which(data[,2] > r[2,2])))
    if(length(inds_temp)>0){data<-data[-inds_temp,]}
  }
  
  if(!is.null(class)) cl <- data[,class] else cl <- 1
  
  if(!is.null(color_temp)){
    ind<-match(color_temp, colnames(data))
    if(ind != 4){stop("Check whether the function decisionplot() is implemented correctly")}
    col_column<-data[,ind]
    if(length(unique(col_column)) > 3){ # If more than three types, discrete scale 
      #color becomes messy, transform to numeric and use gradient color.
      col_column_temp<-as.numeric(as.character(col_column))
      #rbPal <- colorRampPalette(c('red', 'blue'), alpha=T) # red is low, blue is high
      #col_column <- rbPal(10)[as.numeric(cut(col_column_temp,breaks = 10))]
      # Darker color suggests higher value
      col_column<-brewer.pal(9, "Greys")
      col_colum<-col_column[as.numeric(cut(col_column_temp,breaks = 9))]
    }else{
      col_column<-as.integer(cl) + 1L
    }
  }else if(!is.null(col_temp)){
    col_column<-(as.character(cl))
    col_column[which(col_column=="conv")]<-col_temp[2] # "salmon
    col_column[which(col_column=="eco")]<-col_temp[1] # "limegreen
  }else{
    col_column<-as.integer(cl) + 1L
  }
  
  data <- data[,1:2]
  k <- length(unique(cl))
  
  if(is.null(pch_temp)){pch_temp<-16}
  if(is.null(alpha_temp)){alpha_temp<-0.5}
  if(!is.null(labels_temp)){
    labs <- labels_temp
  }else{
    labs<-colnames(data)
  }
  
  pdf(file_name)
  plot(data, col = alpha(col_column, alpha_temp), pch = pch_temp
       , xlab=labs[1], ylab=labs[2], xlim=r[,1], ylim=r[,2])#, ...)
  #plot(data, col = as.integer(cl)+1L, pch = as.integer(cl)+1L, ... )
  
  # make grid
  
  xs <- seq(r[1,1], r[2,1], length.out = resolution)
  ys <- seq(r[1,2], r[2,2], length.out = resolution)
  g <- cbind(rep(xs, each=resolution), rep(ys, time = resolution))
  colnames(g) <- colnames(r)
  
  g <- as.data.frame(g)
  
  ### guess how to get class labels from predict
  ### (unfortunately not very consistent between models)
  p <- predict(model, g, type = predict_type)
  if(is.list(p)) p <- p$class
  p <- as.factor(p)
  if(!is.null(col_temp)){
    col_column<-(as.character(p))
    col_column[which(col_column=="conv")]<-conv
    col_column[which(col_column=="eco")]<-eco
  }else{
    col_column<-as.integer(p) + 1L
  }
  
  if(showgrid) points(g, col = col_column, pch = ".")
  
  
  z <- matrix(as.integer(p), nrow = resolution, byrow = TRUE)
  contour(xs, ys, z, add = TRUE, drawlabels = FALSE,
          lwd = 2, levels = (1:(k-1))+.5)
  
  invisible(z)
  dev.off()
}  

