if(exists(paste("cut_transition_phase_",agent, sep=""))){
  eval(call("<-",(paste("cut_transition_phase",sep="")),as.name(paste("cut_transition_phase_",agent,sep=""))))
  cut_cols<-match(cut_transition_phase, colnames(data))
  cut_cols<-cut_cols[is.na(cut_cols) != TRUE]
}
# 
# 
if(exists("value")){
  if(length(grep("ratio", as.character(value)))>0){ ## Draw a horizontal line at y=1 
    draw_line<-1
  }else{
    draw_line<-0
  }
  
  if(as.character(value)=="share_conv_capital_used"){
    min <- 0
    max <- 1
    ylabel<-"Share conventional capital used"
    val_name_temp<-"share_conv"
  }else if(as.character(value)  == "frontier_gap"){
    ylabel<-"% frontier productivity difference"
    index_pair<-match("specific_skill_gap", colnames(data))
  }else if(as.character(value)  == "specific_skill_gap"){
    ylabel<-"% specific skill difference"
    index_pair<-match("frontier_gap", colnames(data))
  }else if(as.character(value)  == "s_skill_ratio"){
    ylabel<-"Specific skill ratio"
    val_name_temp<-"skill_ratio"
    index_pair<-match("frontier_productivity_ratio", colnames(data))
  }else if(as.character(value)  == "frontier_productivity_ratio"){
    ylabel<-"Frontier productivity ratio"
    val_name_temp<-"frontier_ratio"
    index_pair<-match("s_skill_ratio", colnames(data))
  }else if(as.character(value)  == "monthly_output"){
    ylabel<-"Monthly output"
    val_name_temp<-"mon_out"
  }else if(as.character(value)  == "unemployment_rate"){
    ylabel<-"Unemployment rate"
    val_name_temp<-"unempl"
  }else if(as.character(value)  == "price_per_productivity_unit_difference_wage_ratio"){
    ylabel<-"Price difference for productivity units wage ratio"
  }else if(as.character(value)  == "price_per_productivity_unit_difference"){
    ylabel<-"Price per productivity unit difference"
    val_name_temp<-"prcpr_ratio"
  }else if(as.character(value)  == "no_active_firms"){
    ylabel<-"# active firms"
    val_name_temp<-"no_firms"
  }else if(as.character(value)  == "price_eco"){
    ylabel<-"Natural resource price"
    val_name_temp<-"price_eco"
  }else if(as.character(value)  == "price_eco"){
    ylabel<-"Real natural resource price"
    val_name_temp<-"price_eco_wage_ratio"
  }else if(as.character(value)  == "avg_productivity_gap"){
    ylabel<-"% productivity difference (avg)"
  }else if(as.character(value)  == "avg_productivity_ratio"){
    ylabel<-"Avg productivity ratio"
    val_name_temp<-"prod_ratio"
  }else if(as.character(value)  == "proxy_using_cost_ratio"){
    ylabel<-"Proxy for using cost ratio"
  }else if(as.character(value)  == "capital_price_index_eco"){
    ylabel<-"Green capital price index"
  }else if(as.character(value)  == "capital_price_index_conv"){
    ylabel<-"Conventional capital price index"
  }else if(as.character(value)  == "average_s_skill_conv"){
  ylabel<-"Average specific skill level (conv)"
  }else if(as.character(value)  == "average_s_skill_eco"){
    ylabel<-"Average specific skill level (eco)"
  }else if(as.character(value)  == "techn_frontier_eco"){
    ylabel<-"Frontier productivity (eco)"
}else if(as.character(value)  == "techn_frontier_conv"){
  ylabel<-"Frontier productivity (conv)"
}else if(as.character(value)  == "aggr_environmental_impact"){
  ylabel<-"Aggregate environmental impact"
  val_name_temp<-"aggr_env_imp"
}else if(as.character(value)  == "avg_ecoefficiency"|| as.character(value)  == "ecoefficiency" ){
  ylabel<-"Average eco-efficiency"
  val_name_temp<-"eco_eff"
  if(as.character(value)  == "avg_ecoefficiency"){
    val_name_temp<-"eco_eff"
  }
}else if(value == "skill_divergence"){
  ylabel<-"Skill divergence"
  val_name_temp<-"skill_div"
}else if(value == "Frontier divergence"){
  ylabel<-"Frontier divergence"
  val_name_temp<-"front_div"
}else if(as.character(value) == "variance_share"){
  ylabel<-"Variance of diffusion"
  if(agent=="Eurostat"){val_name_temp<-"var_share"
  }else if(agent == "Firm"){val_name_temp<-"var_share_firm"}
}else if(as.character(value) == "sd_share"){
  ylabel<-"Standard dev. of diffusion"
  if(agent=="Eurostat"){val_name_temp<-"sd_share"
  }else if(agent == "Firm"){val_name_temp<-"sd_share_firm"}
}else if(as.character(value)  == "gov_monthly_eco_tax_revenue"){
  ylabel<-"Monthly eco tax revenue"
}else if(as.character(value)  == "gov_monthly_eco_subsidy_payment"){
  ylabel<-"Monthly eco subsidy payment"
}else if(as.character(value)  == "degree_of_novelty_ratio"){
  ylabel<-"Degree of techn. novelty ratio"
  val_name_temp<-"novelty_ratio"
}else if(as.character(value)  == "price_diff_highest_vintage_wage_ratio"){
  ylabel<-"Frontier price difference wage ratio "
}else if(as.character(value)  == "price_ratio_frontier"){
  ylabel<-"Capital price ratio"
  val_name_temp<-"fr_prc_ratio"
  index_pair<-match("price_per_prod_unit_ratio_frontier", colnames(data))
}else if(as.character(value)  == "price_per_prod_unit_ratio_frontier"){
  ylabel<-"Price per productivity unit ratio"
  val_name_temp<-"prcpr_u_ratio"
  index_pair<-match("price_ratio_frontier", colnames(data))
}else if(as.character(value)  == "no_employees"){
  ylabel<-"# employees"
  val_name_temp<-"no_empl"
}else if(as.character(value)  == "costs_eco"){
  ylabel<-"costs_eco"
}else if(as.character(value)  == "price"){
  ylabel<-"Firm avg. price"
  val_name_temp<-"price"
}else if(as.character(value)  == "effective_investments"){
  ylabel<-"Effective investment by firms"
  val_name_temp<-"eff_invest"
}else if(as.character(value)  == "unit_costs"){
  #stop()
  ylabel<-"Unit costs"
  val_name_temp<-"unit_costs"
}else if(as.character(value)  == "actual_mark_up"){
  ylabel<-"Actual mark up"
  val_name_temp<-"actual_mark_up"
}else if(as.character(value)  == "output"){
  ylabel<-"Firm avg. output"
  val_name_temp<-"output"
}else if(as.character(value)  == "base_tax_rate"){
    ylabel<-"Base tax rate"
    val_name_temp<-"base_tax"
}else if(as.character(value) == "monthly_budget_balance_gdp_fraction"){
  min <- -0.05
  max <- 0.05
  draw_line<-1
  ylabel<-"Budget balance in % GDP"
  val_name_temp<-"budget_bal_pct_gdp"
  }else if(as.character(value) == "eco_price_wage_ratio"){
    min <- 0.09
    max <- 0.10
    draw_line<-1
    ylabel<-"Real price of natural resource "
    val_name_temp<-"price_eco_wage"
  }else if(as.character(value)  == "eco_policy_budget"){
    ylabel<-"Budget from eco-policy"
  }else if(as.character(value)  == "eco_policy_budget_per_gdp"){
    ylabel<-"Budget from eco-policy per GDP"
  }else if(as.character(value)  == "monthly_eco_tax_revenue"){
    ylabel<-"Monthly revenue from eco-tax"
  }else if(as.character(value)  == "monthly_eco_subsidy_payment"){
    ylabel<-"Monthly payment for eco-subsidies"
  }else if(as.character(value)  == "price_index"){
    ylabel<-"Average firm price"
  }else if(as.character(value) == "share_conventional_vintages_used"){
    ylabel<-"Share conventional capital used at firm level"
  }else if(as.character(value) == "eco_costs"){
    ylabel<-"Resource costs"
  }else if(as.character(value) == "mean_wage"){
    ylabel<-"Mean wage"
  }else{
    ylabel<-as.character(value)
  }
if(exists("val_name_temp") != TRUE){
  val_name_temp<-paste("z_",as.character(value),sep="_")
}

if(exists("index_pair")){
  if(exists(paste("cut_transition_phase_",agent, sep="")) && length(cut_cols)>0 && is.element(var, cut_cols)){
    #temp0<-cbind(data$Periods, data[,index_pair])
    temp0<-data[which(as.numeric(as.character(data$Periods))>=(day_market_entry+20)),index_pair]
    min1<-min(as.numeric(temp0, na.rm=TRUE))
    max1<-max(as.numeric(temp0, na.rm=TRUE))
    min<-min(min1, min, na.rm=TRUE)
    max<-max(max1, max, na.rm=TRUE)
    rm(temp0)
  }else{
    min<-min(min, min(data[, index_pair], na.rm = TRUE), na.rm = TRUE)
    max<-max(max, max(data[, index_pair], na.rm = TRUE), na.rm = TRUE)
  }
  rm(index_pair)
}





if(as.character(value) == "share_conv_capital_used" && is.element(aggr, c("single_large", "single_switch"))){
  #legend_pos_temp<-"none")
  legend_pos_temp<-c(0.9, 0.5)
}else if(aggr=="batch" && is.element(as.character(value), c("share_conv_capital_used", "aggr_environmental_impact", "price_per_productivity_unit_difference_wage_ratio", "ecoefficiency"))){
  #legend_pos_temp<-"none")
  legend_pos_temp<-c(0.9, 0.9)
}else if(is.element(as.character(value), c("share_conv_capital_used", "aggr_environmental_impact", "price_per_productivity_unit_difference_wage_ratio"))){
  #legend_pos_temp<-"none")
  legend_pos_temp<-c(0.9, 0.5)
}else if(is.element(as.character(value), c("no_active_firms"))){
  #legend_pos_temp<-"none")
  legend_pos_temp<-c(0.1, 0.1)
}else{
  legend_pos_temp<-c(0.1, 0.9)
}
} # if exists("value")

### HERE YOU MAY MODIFY THE NAME OF YOUR PLOTS
# plot name: "/",agent,"_",value,"_mts.pdf",sep=""
# Settings for axes labeling
# with_policy<-1 with_rand_policy<-1 with_rand_learn<-0 with_rand_barr<-1
if(exists("OTHER_NAMES")){
  if(exists("fct_temp")){
    exp_temp<-paste(fct_temp,"_",sep="")
  }else if(experiment_name == "BAU"){
    exp_temp<-"BAU_"
  }else if(with_policy && with_rand_policy && with_rand_barr && with_rand_learn==0){
    exp_temp<-"RAND_POL_RAND_"
  }else if(with_policy && with_rand_policy && with_rand_barr == 0&& with_rand_learn==0){
    exp_temp<-"RAND_POL_FIX_"
  }else if(with_policy == 0 && with_rand_policy == 0 && with_rand_barr&& with_rand_learn==0){
    exp_temp<-"RAND_BARR_"
  }else if(with_policy == 0 && with_rand_policy == 0 && with_rand_barr == 0&& with_rand_learn){
    exp_temp<-"RLFB_"
  }else if(with_policy == 0 && with_rand_policy == 0 && with_rand_barr&& with_rand_learn){
    exp_temp<-"RLRB_"
  }else if(with_policy == 1 && with_rand_policy == 0 && with_rand_barr&& with_rand_learn){
    exp_temp<-"RLRBFP_"
  }else if(with_policy == 1 && with_rand_policy == 0 && with_rand_barr==0&& with_rand_learn){
    exp_temp<-"RLFBFP_"
  }else if(with_policy == 1 && with_rand_policy == 1 && with_rand_barr==0&& with_rand_learn){
    exp_temp<-"RLFBRP_"
  }else if(with_policy == 1 && with_rand_policy && with_rand_barr && with_rand_learn){
    exp_temp<-"RLRBRP_"
  }else if(experiment_name == "min_learning_coeff_const"){
    exp_temp<-"LEARN_"
  }else if(experiment_name == "learning_spillover_strength"){
    exp_temp<-"SPILL_"
  }else if(with_policy == 0 && with_rand_policy == 0 && with_rand_barr == 0){
    exp_temp<-"BAU_"
  }else{
    exp_temp<-""
  }
}

if(exists("value") && exists("exp_temp")){
  if(aggr == "eco_conv_switch"){
    aggr_name_temp<-"econvs"
  }else if(aggr == "eco_conv_batch"){
    aggr_name_temp<-"econv"
  }else if(aggr == "batch"){
    aggr_name_temp<-"batch"
  }else if(is.element(aggr, c("single_large", "single_switch"))){
    aggr_name_temp<-"single"
  }else{
    aggr_name_temp<-aggr
  }
  pdf_name_temp<-paste("/",exp_temp,val_name_temp,"_",aggr_name_temp,".pdf" , sep="")
  rm(val_name_temp, aggr_name_temp)
}else if(exists("value") && is.element(value, c("skill_divergence", "frontier_divergence"))){
  pdf_name_temp<-paste("/",agent,"_",value, ".pdf", sep="")
}else if(exists("value")){ # Use default name
  pdf_name_temp<-paste("/",agent,"_",value,"_",parameter,".pdf",sep="")
}