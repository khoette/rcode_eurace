## Extract growth rates per run
## 
agent<-"Eurostat"
aggr<-"single_large"
kind_of_data<-"logs"
d<-1
data_directory<-paste(experiment_directory,"/rdata/",kind_of_data,"/",data_type[d],"/",sep="")
data_from<-paste(data_directory,"data_", aggr, "_",experiment_name,"_",p0,"_",sep="")

load(file=paste(data_from,agent,".RData",sep=""))

# Restrict to relevant data: 

v_temp<-c("Type","Periods","Run", "monthly_output", "share_conv_capital_used","unemployment_rate")
o<-object[,match(v_temp, colnames(object))]
rm(object, v_temp, d)

# Compute growth rates per run
# 
run<-as.numeric(as.character(unique(o$Run)))
if(agent == "Firm"){
  id<-run + 0.001*round(as.numeric(as.character(o$id)))
  stop("REMOVE first incomplete firms! apply to whole object")
  sub<-unique(id)
  output<-o$output
}else{
  sub<-unique(run)
  id<-run
  output<-o$monthly_output
}
# sub: Variable to subset for compute avg. growth rate (within run or within firm)


growth_rate<-rep(NA, length(sub))
for(s in sub){
  indx<-which(id == s)
  out<-output[indx]
  diffs<-diff(out,lag=12)
  ### GEHT DAS NICHT EINFACHER??
}

