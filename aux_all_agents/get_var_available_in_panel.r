# Get variable settings for plotting
# 
# 
if(file.exists(file=paste(data_directory,"data_batch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))){
load(file=paste(data_directory,"data_batch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))

if(exists(paste("variables_available_in_panel_",agent,sep=""))==FALSE){
  variables<-colnames(object)[which(colnames(object) != "Periods")]
  variables<-variables[which(variables != "Run")]
  variables<-variables[which(variables != "Type")]
  variables<-variables[which(variables != "Number_Runs")]
  variables<-variables[which(variables != "id")]
  variables<-variables[which(variables != "Firm_Type")]
  variables<-variables[which(variables != "Adopter_Type")]
  #variables<-variables[which(variables != "min_learning_coefficient_var")]
  #variables<-variables[which(variables != "learning_spillover_strength_var")]
  variables<-variables[which(variables != "Spill")]
  variables<-variables[which(variables != "Ease")]
  variables<-variables[which(variables != "Learn_Type")]
  
  eval(call("<-",(paste("variables_available_in_panel_",agent,sep="")),as.name(paste("variables"))))
}

eval(call("<-",(paste("list_of_var",sep="")),as.name(paste("list_of_var_",agent,sep=""))))
eval(call("<-",(paste("variables",sep="")),as.name(paste("variables_available_in_panel_",agent,sep=""))))
eval(call("<-",(paste("variables_available_in_panel",sep="")),as.name(paste("variables_available_in_panel_",agent,sep=""))))



if(exists("variables_selection") == FALSE || is.element(NA, match(variables_selection, colnames(object)))){
  variables_selection<-variables
}
rm(object)
}
