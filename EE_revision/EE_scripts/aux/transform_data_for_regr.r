vars<-match(c("id","Type","Firm_Type", "Adopter_Type", "Run", "Periods", target_var_temp_const, variables_init_cond_temp, 
              variables_policy_temp, variables_init_cond_temp, variables_barr_temp), colnames(object))
vars<-unique(vars[is.na(vars)==FALSE])
temp_data<-object[,vars]
vars<-match(c(target_var_temp_const, variables_init_cond_temp, variables_policy_temp, variables_init_cond_temp, variables_barr_temp), colnames(temp_data))
vars<-unique(vars[is.na(vars)==FALSE])
for(col in vars){
  temp_data[,col]<-as.numeric(temp_data[,col])
}

if(compute_vars_temp == 1 &&   agent == "Eurostat"){# || agent=="Firm"){
  i<-match(c("Run", "Periods",target_var_temp_const), colnames(object))
  i<-i[which(is.na(i) == FALSE)]
  temp<-object[,i]
  i<-match(target_var_temp_const, colnames(temp))
  i<-i[which(is.na(i)==FALSE)]
}
rm(object)

# Compute variance over time within single run (takes a lot of time)
if(compute_vars_temp == 1 &&   agent == "Eurostat"){# || agent=="Firm"){
  #target_var_temp_const<-target_var_temp1
  for(r in unique(temp$Run)){
    subs_temp<-temp[which(temp$Run == r),]
    old_temp<-subs_temp
    for(t in 1:length(subs_temp$Periods)){ # Replacement of entry by its change from t-12 to t
      if(t<=12){
        subs_temp[t,i]<-NA
      }else{
        subs_temp[t,i]<-old_temp[t,i]-old_temp[t-12,i]
      }
    }
    temp[which(temp$Run == r),]<-subs_temp
    if(as.numeric(as.character(r)) %% 10 == 0){
      print(paste("Run: ", r))
    }
  }
  rm(subs_temp, old_temp)
  
  if(agent=="Eurostat"){
    target_var_temp1<-c("share_conv_capital_used", "no_active_firms", "monthly_output")
  }else if(agent=="Firm"){
    target_var_temp1<-c("share_conventional_vintages_used")
  }
  var1<-array(NA, dim=length(unique(temp$Run)))
  if(length(target_var_temp1)>1){
    var2<-array(NA, dim=length(unique(temp$Run)))
  }
  if(length(target_var_temp1)>2){
    var3<-array(NA, dim=length(unique(temp$Run)))
  }
  for(j in c(1:length(target_var_temp1))){
    idx<-match(target_var_temp1[j],colnames(temp))
    for(r in c(1:length(unique(temp$Run)))){
      if(j==1){
        var1[r]<-sd(temp[which(temp$Run==unique(temp$Run)[r]),idx],na.rm=TRUE)
      }else if(j==2){
        var2[r]<-sd(temp[which(temp$Run==unique(temp$Run)[r]),idx],na.rm=TRUE)
      }else if(j==3){
        var3[r]<-sd(temp[which(temp$Run==unique(temp$Run)[r]),idx],na.rm=TRUE)
      }
    }
    if(j==1){
      mean<-mean(var1,na.rm=TRUE)
      var1<-var1/mean
    }else if(j==2){
      mean<-mean(var2, na.rm=TRUE)
      var2<-var2/mean
    }else if(j==3){
      mean<-mean(var3, na.rm=TRUE)
      var3<-var3/mean
    }
  }
  
  rm(temp, j, r, idx, target_var_temp1)
  add_vars<-1
}else{
  add_vars<-0
}


if(agent =="Firm" && (with_policy||with_rand_barr)){
  iterate<-2
  add_variables_temp<-c()
  if(with_policy){
    add_variables_temp<-c(add_variables_temp, variables_policy_temp)
  }
  if(with_rand_barr){
    add_variables_temp<-c(add_variables_temp, variables_barr_temp)
  }
}else{
  iterate<-1
}
for (iter in 1:iterate){
  
  if(iter == 2 || ((agent=="Eurostat"||agent=="Government") && with_policy)){

    if(no_agents>1){
      
      if(iter != 2){
        print("ERROR iter != 2 and agent == Firm???")
        stop()
      }
 
        reg_data_temp_backup<-reg_data_temp
        rm(reg_data_temp)
        no_agents0<-no_agents
        no_agents<-1
        temp_data_backup<-temp_data
        rm(temp_data)

      load(file=paste(data_from,"Eurostat.RData",sep=""))

      vars<-match(c("id","Type","Firm_Type", "Adopter_Type", "Run", "Periods",add_variables_temp), colnames(object))
      vars<-unique(vars[is.na(vars)==FALSE])
      temp_data_pol<-object[,vars]
      rm(object)
    }
    if(agent != "Government" && with_policy && file.exists(paste(data_from,"Government.RData",sep=""))){
        load(file=paste(data_from,"Government.RData",sep=""))

    }else{
        load(file=paste(data_from,"Eurostat.RData",sep=""))
    }
  
    vars<-match(c(variables_policy_temp, variables_barr_temp,  variables_init_cond_temp), colnames(object))
    vars<-unique(vars[is.na(vars)==FALSE])
    
    if(agent=="Firm"){
      if(length(vars) > 0){
        temp_data_pol<-cbind(temp_data_pol, object[,vars])
      }
      temp_data<-temp_data_pol
      rm(temp_data_pol)
    }else{
      if(length(vars) > 0){
        temp_data<-cbind(temp_data, object[,vars])
      }
    }
    
    rm(object, vars)
  } # end if iter == 2 (Collect data from eurostat and gov and merge it with firm data)
    
  
var_of_interest<-unique(var_of_interest)
target_var_temp_const<-unique(target_var_temp_const)


fix_vars_temp<-match(c("Parameter","id", "Periods", "Type", "Firm_Type", "Adopter_Type", "Run"), colnames(temp_data))
fix_vars_temp<-unique(fix_vars_temp[which(is.na(fix_vars_temp)== FALSE) ])

reg_data_temp<-as.data.frame(array(NA, dim=c(length(time_temp)*runs*no_agents, length(temp_data[1,]))))
#reg_data_temp<-as.data.frame(array(NA, dim=c(length(time_temp)*runs*length(parameters_tmp)*no_agents, length(temp_data[1,]))))
colnames(reg_data_temp)<-colnames(temp_data)
vars<-c(1:length(temp_data))
vars<-vars[which(is.element(vars,fix_vars_temp)==FALSE)]

b<-c(1:(runs*no_agents))
c<-c(1:length(time_temp))
if(is.element("Parameter", colnames(temp_data))){
  temp_data$Parameter<-as.vector(as.character(temp_data$Parameter))
}
temp_data$Run<-floor(as.numeric(as.vector(as.character(temp_data$Run))))
temp_data$Periods<-as.vector(as.character(temp_data$Periods))
temp_data$Type<-(as.character(temp_data$Type))


if(is.element("Firm_Type", colnames(temp_data))){
  temp_data$Firm_Type<-as.character(temp_data$Firm_Type)
}
if(is.element("Adopter_Type", colnames(temp_data))){
  temp_data$Adopter_Type<-as.character(temp_data$Adopter_Type)
}
if(is.element("id", colnames(temp_data))){
  temp_data$id<-(as.character(temp_data$id))
}
for(i in (length(fix_vars_temp)+1):length(colnames(temp_data))){
  temp_data[,i]<-as.numeric(temp_data[,i])
}
i1<-1
#for(p0 in parameters_tmp){
  if(is.element("Parameter", colnames(temp_data))){
    temp_data2<-temp_data[which(temp_data$Parameter == p0),]
  }else{
    temp_data2<-temp_data
  }
  for(t in c){
    time_temp2<-seq(time_temp[t], (time_temp[t]+220), 20)
    temp2<-temp_data2[which(as.vector(as.character(temp_data2$Periods)) %in% time_temp2),]
    rm(time_temp2)
    temp2<-temp2[order(as.numeric(as.character(temp2$Run))),]
    if(no_agents>1){
      temp2<-temp2[order(as.numeric(as.character(temp2$id))),]
    }
    
    index<-1
    if(no_agents>1){
      print(paste("Time ", time_temp[t]))
      for(ag in unique(temp2$id)){
        temp4<-temp2[which(temp2$id == ag),]
        temp3<-as.matrix(temp4[,vars])
        a<-apply(temp3,FUN=mean, MARGIN=2, na.rm=TRUE)
        reg_data_temp[i1,vars]<-a
        reg_data_temp[i1,fix_vars_temp]<-temp4[1,fix_vars_temp]
        i1<-i1+1
      }
    }else{
      for(i in b){
        temp3<-as.matrix(temp2[c(index:(index+11)),vars])
        a<-apply(temp3,FUN=mean, MARGIN=2, na.rm=TRUE)
        reg_data_temp[i1,vars]<-a
        reg_data_temp[i1,fix_vars_temp]<-temp2[index,fix_vars_temp]
        index<-index+12
        i1<-i1+1
      }
    }
  }
#} # end p0 in parameter loop
#reg_data_temp<-reg_data_temp[which(is.na(reg_data_temp$Run)==FALSE),]

rm(a, b, c, index, i1, i, temp3, temp2, temp_data2, vars)
if(no_agents>1){
  rm(ag, temp4)
}

#reg_data_temp<-temp_data[which(as.vector(as.character(temp_data$Periods)) %in% time_temp),]
#temp_data$Parameter<-as.factor(temp_data$Parameter)
#reg_data_temp$Parameter<-as.factor(reg_data_temp$Parameter)
reg_data_temp$Type<-as.factor(reg_data_temp$Type)

if(iter == 2){
  reg_data_temp_policy<-reg_data_temp
  reg_data_temp<-reg_data_temp_backup
  rm(reg_data_temp_backup)
  no_agents<-no_agents0
  temp_data_pol<-temp_data
  temp_data<-temp_data_backup
  rm(temp_data_backup, no_agents0)
  #rm(temp_data, temp_data_pol)
}

}


if(add_vars==1){
  
  if(agent=="Eurostat"){
    target_var_temp_const<-c(target_var_temp_const, "std_share", "std_no_firms", "std_output")
    var_of_interest<-c(var_of_interest, "std_share", "std_no_firms", "std_output")
    reg_data_temp<-cbind(reg_data_temp, std_share = NA, std_no_firms = NA, std_output = NA)
    not<-match(c("std_share", "std_no_firms", "std_output"), colnames(reg_data_temp))
    reg_data_temp[which(as.numeric(reg_data_temp$Periods)==((20*number_xml)-220)), not[1]]<-var1
    reg_data_temp[which(as.numeric(reg_data_temp$Periods)==((20*number_xml)-220)), not[2]]<-var2
    reg_data_temp[which(as.numeric(reg_data_temp$Periods)==((20*number_xml)-220)), not[3]]<-var3
  }else if(agent=="Firm"){
    target_var_temp_const<-c(target_var_temp_const, "std_share")
    var_of_interest<-c(var_of_interest, "std_share")
    reg_data_temp<-cbind(reg_data_temp, std_share=NA)
    not<-match(c("std_share"), colnames(reg_data_temp))
    reg_data_temp[which(as.numeric(reg_data_temp$Periods)==((20*number_xml)-220)), not[1]]<-var1
  }
}

if(agent == "Firm" && (with_policy||with_rand_barr)){
  reg_data_temp<-reg_data_temp[order(as.numeric(as.character(reg_data_temp$id)), as.numeric(as.character(reg_data_temp$Periods))),]
  reg_data_temp<-reg_data_temp[order(as.numeric(as.character(reg_data_temp$Run)), as.numeric(as.character(reg_data_temp$Periods))),]
  len<-match(c(add_variables_temp), colnames(reg_data_temp_policy))
  len<-len[is.na(len)!=TRUE]
  
  #reg_data_temp_policy<-reg_data_temp_policy[order(as.numeric(as.character(reg_data_temp_policy$Periods))),]
  empty_col<-array(NA, dim=c(length(reg_data_temp[,1]), length(len)))
  empty_col<-as.data.frame(empty_col)
  colnames(empty_col)<-colnames(reg_data_temp_policy)[len]
  #id<-match("id", colnames(reg_data_temp))
  idx<-1
  for(r in unique(reg_data_temp_policy$Run)){
    #reg_data_temp2<-reg_data_temp[which(reg_data_temp$Run == r),]
    reg_data_temp_policy2<-reg_data_temp_policy[which(reg_data_temp_policy$Run == r),]
    for(per in unique(reg_data_temp_policy2$Periods)){
      fill<-reg_data_temp_policy2[which(reg_data_temp_policy2$Periods == per),len]
      #rep<-length(reg_data_temp2[which(reg_data_temp2$Periods == per),id])
      rep<-no_agents
      array<-t(array(fill, dim=c(length(len), rep)))
      empty_col[c(idx:(idx+rep-1)),]<-as.data.frame(array)
      idx<-idx+rep
    }
  }

  reg_data_temp<-cbind(reg_data_temp, empty_col)
 # rm(array, fill, rep, idx, empty_col, reg_data_temp_policy, reg_data_temp_policy2)
}

