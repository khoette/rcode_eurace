# Validation
# 
  source(paste(script_dir,"/aux_all_agents/plot_settings/plot_settings.r",sep=""))
  
list_of_var_for_validation<-c("monthly_output", "total_consumption_budget", "monthly_sold_quantity", 
                              "monthly_investment_quantity", "unemployment_rate", 
                              "no_vacancies", "productivity", "monthly_investment_value", 
                              "price_index", "average_wage", "total_debt", 
                              "leverage_ratio", "mark_up", "monthly_inflation_rate", 
                              "firm_average_productivity_conv", "firm_average_productivity_eco", 
                              "monthly_real_investments_conv","monthly_real_investments_eco", 
                              "ig_sold_quantity", "price_eco", "firm_average_wage", 
                              "firm_average_productivity", "output_growth_rate_annual", 
                              "gdp_growth_rate_annual"
)

if(exists("plot_dir_validation")!=TRUE){
  source(paste(script_dir,"/aux_all_agents/create_directories_for_plotting.r", sep=""))
}
agent<-"Eurostat"
if(agent=="Eurostat"){
  
  if(file.exists(paste(data_directory,"data_work.RData", sep=""))){
    print("Your validation data is already existing. If you want to create new data you must 
          have a look on the code or remove the existing data manually.")
    load(paste(data_directory,"data_work.RData", sep=""))
    print(list_of_var_for_validation[which(is.na(match(list_of_var_for_validation, colnames(data_work))))])
    list_of_var_for_validation<-list_of_var_for_validation[which(!is.na(match(list_of_var_for_validation, colnames(data_work))))]
    
  }else{
  plot_dir_temp0<-paste(plot_dir_validation)
  if(dir.exists(plot_dir_temp0)!=1){
    dir.create(plot_dir_temp0)
  }

  
  print(list_of_var_for_validation[which(is.na(match(list_of_var_for_validation, list_of_var_Eurostat)))])
  list_of_var_for_validation<-list_of_var_for_validation[which(!is.na(match(list_of_var_for_validation, list_of_var_Eurostat)))]
    
  load(file=paste(data_directory,"data_single_switch_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  
  cols<-match(list_of_var_for_validation, colnames(object))
  cols<-c(match(c("Type", "Periods", "Run"), colnames(object)), cols)
  data<-object[cols]
  data<-data[order(as.numeric(as.character(data$Run)), as.numeric(as.character(data$Periods))),]

  # get productivity (not considering technology type)
  prod_temp<-pmax(data$firm_average_productivity_conv, data$firm_average_productivity_eco, na.rm=TRUE)
  data_work<-cbind(data, firm_average_productivity=prod_temp)
  rm(data)
  prod_temp<-pmax(data_work$monthly_real_investments_conv, data_work$monthly_real_investments_eco, na.rm=TRUE)
  data_work<-cbind(data_work, real_investment=prod_temp)

  
if(file.exists(paste(data_directory,"data_single_switch_",experiment_name,"_",parameter,"_Firm.RData",sep=""))&& is.element("actual_mark_up", list_of_var_Firm)){
  # Get firm data for mark ups: 
  load(file=paste(data_directory,"data_single_switch_",experiment_name,"_",parameter,"_Firm.RData",sep=""))
  cols<-match(c("Type", "Periods", "Run", "output", "no_employees"), colnames(object))
  firm_level_data<-object[cols]
  cols<-match(c("Type", "Periods", "Run", "actual_mark_up"), colnames(object))
  data_temp<-object[cols]
  rm(object)
  
  data_temp0<-data_temp[order(as.numeric(as.character(data_temp$Run)), as.numeric(as.character(data_temp$Periods))),]
  rm(data_temp)
  mark_up_temp<-data_temp0$actual_mark_up
  run_temp<-data_temp0$Run
  period_temp<-data_temp0$Periods
  
  mark_up_temp_short<-c()
  per_temp<-c()
  runs_temp<-c()
  
  len_temp<-length(data_work[,1])
  index<-c(1:120)
  for(i in 1:len_temp){
    mark_up_temp_short<-c(mark_up_temp_short, mean(mark_up_temp[index], na.rm=TRUE))
    if(length(unique(run_temp[index]))!=1 || length(unique(period_temp[index]))!=1){
      stop()
    }
    per_temp<-c(per_temp, period_temp[index[1]])
    runs_temp<-c(runs_temp, run_temp[index[1]])
    index<-index+120
  }
  data_work<-cbind(data_work, average_mark_up=mark_up_temp_short)
  rm(list=ls(pattern="_temp"))
  rm(index, cols)
}else{ # else of if firm data exists
  print(paste("Cannot compute mark ups: Firm data not compiled... 
              or mark up not in firm data"))
}
  
  save(data_work, file=paste(data_directory,"data_work.RData", sep=""))
  
} # check if file exists


  # Transform data into time series data and get bandpass filtered data. 
  # in: data_work
  # out: data_work, cycle_full, trend_full, series_full,
  # vars_temp (list of filtered variables)
  # 
  ### Output of the validation script is available in txt and 
  ### pdf plots in plot_dir_validation
  start_manual <-0
  if(start_manual){
    print("Validation not started. You indicated that you do it manually...  
          Go to validation.r script if you want to change it")
  }
  start_manual <-0
  if(start_manual == 0){
  
  # This script produces the bandpassed time series data for the analysis. 
  # Running this script takes a while (if the number of runs is large)
  if(file.exists(paste(data_directory,"cycle_bk_var_8_32.RData", sep="")) != TRUE){
    source(paste(script_dir, "/all_agents/validation/validation_get_ts.r", sep=""))
  }else{
    print(paste("TS data already existing. Is this your desired type? cycle_bk_var_8_32"))
  }
  
  # Compute table of cross and autocorrelations of core macro variables 
  # with business cycle. 
  # in: cycle_full, vars_temp
  source(paste(script_dir, "/all_agents/validation/validation_cross_correl_lagged.r", sep=""))
    
  source(paste(script_dir, "/all_agents/validation/validation_relative_volatility_plots.r", sep=""))
  #source(paste(script_dir, "/all_agents/validation/validation_plot_cycle_comparison.r", sep=""))
  
  # Phillips and Beveridge curve. You may manually set within the script 
  # which snapshot in time you would like to consider. Output: Plots. 
  source(paste(script_dir, "/all_agents/validation/validation_phillips_beveridge.r", sep=""))

  # Compute output growth rate and standard dev, i.e. avg. strength 
  # of business cylces. Output is a txt file in validation directory. 
  source(paste(script_dir,"/all_agents/validation/validation_growth_rate.r",sep=""))
  
  if(file.exists(paste(data_directory,"data_single_switch_",experiment_name,"_",parameter,"_Firm.RData",sep=""))){
    # Generate plots of rank-size distribution of individual firms. 
    source(paste(script_dir, "/all_agents/validation/validation_rank_size.r", sep=""))
  }
  }
}# Call routine only once if agent == Eurostat
rm(data_work)

if(REMOVE_VALID_DATA == 1){
  for(i in c("data_work", "series_bk_var_8_32", "cycle_bk_var_8_32", "trend_bk_var_8_32")){
    filename_temp<-paste(data_directory,i,".RData", sep="")
    if(file.exists(filename_temp)){
      file.remove(filename_temp)
    }
  }
}
rm(list=ls(pattern="temp"))
