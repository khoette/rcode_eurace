# Set variable-combinations to be plotted as mts
if(agent=="Eurostat"){
  mts_table<-matrix(NA, 1, 10) # Default setting, max 10 vars per combi. 

mts_table[1,]<-c("share_conv_capital_used", "frontier_gap", "specific_skill_gap", rep(NA, 7))
mts_table<-rbind(mts_table, c("frontier_gap", "specific_skill_gap", rep(NA, 8)))
mts_table<-rbind(mts_table, c("frontier_gap", "specific_skill_gap", "avg_productivity_gap", "share_conv_capital_used", rep(NA, 6)))
mts_table<-rbind(mts_table, c("price_highest_vintage_eco","price_highest_vintage_conv", rep(NA, 8)))
mts_table<-rbind(mts_table, c("unemployment_rate","monthly_output", rep(NA, 8)))
mts_table<-rbind(mts_table, c("unemployment_rate","gdp", rep(NA, 8)))
mts_table<-rbind(mts_table, c("monthly_output","total_consumption_budget", "net_investment", rep(NA, 7)))
mts_table<-rbind(mts_table, c("monthly_output","total_consumption_budget", "total_capital_stock_units", rep(NA, 7)))
mts_table<-rbind(mts_table, c("monthly_output","total_consumption_budget", "total_capital_stock_units_conv", "total_capital_stock_units_eco", rep(NA, 6)))
mts_table<-rbind(mts_table, c("monthly_output","monthly_sold_quantity", "total_capital_stock_units", rep(NA, 7)))
mts_table<-rbind(mts_table, c("average_wage","techn_frontier", rep(NA, 8)))
mts_table<-rbind(mts_table, c("average_wage_skill_1","average_wage_skill_2", "techn_frontier", "monthly_output", rep(NA, 6)))
mts_table<-rbind(mts_table, c("annual_inflation_rate","monthly_output", rep(NA, 8)))
mts_table<-rbind(mts_table, c("monthly_output","total_consumption_budget", "ig_market_size", rep(NA, 7)))
mts_table<-rbind(mts_table, c("monthly_output","total_consumption_budget", "ig_sold_quantity", rep(NA, 7)))
mts_table<-rbind(mts_table, c("eco_tax_rate","eco_investment_subsidy", "eco_consumption_subsidy", "eco_r_and_d_subsidy", rep(NA, 6)))
mts_table<-rbind(mts_table, c("gov_monthly_eco_tax_revenue","gov_monthly_eco_subsidy_payment", "moving_avg_market_share_eco", rep(NA, 7)))
mts_table<-rbind(mts_table, c("gdp", "moving_avg_r_and_d_expenditure", rep(NA, 8)))
mts_table<-rbind(mts_table, c("gdp", "moving_avg_r_and_d_expenditure_conv", "moving_avg_r_and_d_expenditure_eco", rep(NA, 7)))
mts_table<-rbind(mts_table, c("average_wage", "price_eco", rep(NA, 8)))
mts_table<-rbind(mts_table, c("price_per_productivity_unit_difference_wage_ratio", "eco_price_wage_ratio", rep(NA, 8)))
mts_table<-rbind(mts_table, c("price_per_productivity_unit_conv_wage_ratio" , "price_per_productivity_unit_eco_wage_ratio" , rep(NA, 8)))
mts_table<-rbind(mts_table, c("price_per_productivity_unit_conv" , "price_per_productivity_unit_eco" , rep(NA, 8)))
mts_table<-rbind(mts_table, c("s_skill_ratio", "frontier_productivity_ratio", rep(NA, 8)))
mts_table<-rbind(mts_table, c("price_per_prod_unit_ratio_frontier", "price_ratio_frontier", rep(NA, 8)))
mts_table<-rbind(mts_table, c("eco_tax_rate","eco_investment_subsidy", "eco_consumption_subsidy", rep(NA, 7)))

}else if(agent == "Firm"){
  mts_table<-matrix(NA, 1, 10) # Default setting, max 10 vars per combi. 
  
  mts_table[1,]<-c("share_conventional_vintages_used", "output", "price", rep(NA, 7))
  mts_table<-rbind(mts_table, c("output", "price", rep(NA, 8)))
  mts_table<-rbind(mts_table, c("share_conventional_vintages_used", "unit_costs", "price", rep(NA, 7)))
  mts_table<-rbind(mts_table, c("mean_spec_skills_eco","mean_spec_skills_conv", rep(NA, 8)))
  mts_table<-rbind(mts_table, c("technology_eco","technology_conv", rep(NA, 8)))
  mts_table<-rbind(mts_table, c("mean_spec_skills_eco","mean_spec_skills_conv", "technology_eco","technology_conv", rep(NA, 6)))
  mts_table<-rbind(mts_table, c("used_units_capital_stock_eco","used_units_capital_stock_conv", rep(NA, 8)))
  mts_table<-rbind(mts_table, c("firm_productivity_eco","firm_productivity_conv", rep(NA, 8)))
  mts_table<-rbind(mts_table, c("firm_productivity_eco","firm_productivity_conv", "output", "price", rep(NA, 6)))
  
}else if(agent == "Government"){
  mts_table<-matrix(NA, 1, 10) # Default setting, max 10 vars per combi. 
  mts_table[1,]<-c("eco_subsidy_payment", "eco_tax_revenue", rep(NA, 8))
  mts_table<-rbind(mts_table, c("monthly_eco_subsidy_payment", "monthly_eco_tax_revenue", "monthly_budget_balance", rep(NA, 7)))

  mts_table<-rbind(mts_table, c("eco_policy_budget", "monthly_eco_tax_revenue", "monthly_eco_subsidy_payment", rep(NA, 7)))
  mts_table<-rbind(mts_table, c("base_tax_rate", "monthly_budget_balance_gdp_fraction", rep(NA, 8)))
  mts_table<-rbind(mts_table, c("monthly_tax_revenues", "monthly_budget_balance","eco_policy_budget", rep(NA, 7)))
  mts_table<-rbind(mts_table, c("monthly_income", "monthly_expenditure", rep(NA, 8)))
  mts_table<-rbind(mts_table, c("monthly_eco_tax_revenue", "monthly_eco_subsidy_payment", rep(NA, 8)))
  
}else{
  break
}
