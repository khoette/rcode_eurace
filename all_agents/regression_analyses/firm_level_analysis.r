# Data given: data_firm, data_euro
# Preprocessing: 
## - Chekc that id-pairs (i.e. only pairwise complete observations), 
## - get some macro-level controls from eurostat data and transform 
##    it to firm-level-dimensionality
#stop("firm level analyis")
use_fitDist_models<-0

# Transform ids to unique id-run identifiers
data_work$id<-as.numeric(as.character(data_work$Run))+0.001*as.numeric(as.character(data_work$id))

vars_temp<-colnames(data_work)
periods_temp<-unique(data_work$Periods)
periods_temp_target<-periods_temp[order(periods_temp, decreasing = T)][1] # take only end of time
periods_temp_controls<-periods_temp[order(periods_temp)][1] # take only first obs.
target_vars_temp<-c("share_firm" = "share_conventional_vintages_used")
if(exists("macro_control_regression") && macro_control_regression){
  periods_temp_target<-periods_temp[order(periods_temp,decreasing = T)]
}

if(exists("reg_spill_threshold") && reg_spill_threshold){
  if(exists("use_type_adjusted_crit_levels") && use_type_adjusted_crit_levels){
    # add additional target vars if avaiable: 
    target_vars_temp<-c(target_vars_temp, "crit_time" = "Time", 
                      "abs_crit_prod_ratio" = "abs_crit_prod_ratio", 
                      "abs_crit_techn_ratio" = "abs_crit_techn_ratio",
                      "abs_crit_skill_ratio" = "abs_crit_skill_ratio", 
                      "abs_crit_cap_ratio" = "abs_cap_ratio", 
                      "variance_share_full" = "variance_share_full")
  }else{
    target_vars_temp<-c(target_vars_temp, "crit_time" = "Time", 
                        "crit_prod_ratio" = "crit_prod_ratio", 
                        "crit_techn_ratio" = "crit_techn_ratio",
                        "crit_skill_ratio" = "crit_skill_ratio", 
                        "crit_share" = "crit_share")
  }
  target_vars_temp<-unique(target_vars_temp[which(!is.na(match(target_vars_temp, colnames(data_firm))))])
}
if(exists("macro_control_regression") && macro_control_regression){
  target_vars_temp<-c("output", "unit_costs", "no_employees")
  target_vars_temp<-unique(target_vars_temp[which(!is.na(match(target_vars_temp, colnames(data_firm))))])
}

if(with_policy && with_rand_policy){
  macro_contr_temp<-data_work[match(c("techn_frontier_conv", "no_active_firms"), 
                                  colnames(data_work))]
}else{
  macro_contr_temp<-data_work[match(c("techn_frontier_conv", "no_active_firms", "eco_price_wage_ratio"), 
                                    colnames(data_work))]
}
#micro_contr_temp<-data_work[match(c("technology_conv","mean_spec_skills_conv","no_employees", 
#                    "output", "price", "unit_costs"),colnames(data_work))]
micro_contr_temp<-data_work[match(c("mean_spec_skills_conv","no_employees", 
                                    "output", "price", "unit_costs"),colnames(data_work))]


barr_macro_temp<-data_work[match(c("frontier_gap", "specific_skill_gap"),colnames(data_work))]
barr_micro_temp<-data_work[match(c("frontier_gap", "mean_specific_skill_gap"),colnames(data_work))]

spills_temp<-data_work[match(c("min_learning_coefficient_var", "learning_spillover_strength_var" ),colnames(data_work))]

if(with_policy){
  pol_temp<-data_work[match(c("eco_tax_rate", "eco_consumption_subsidy", "eco_investment_subsidy"), colnames(data_work))]
}
targ<-target_vars_temp[2]
p<-periods_temp_target

if(exists("exp_temp")!=T){exp_temp<-""}
if(exists("name_extension") != T){name_extension<-""}
if(exists("macro_control_regression")&&macro_control_regression){name_extension<-c(name_extension,"macro")}

file_temp<-paste(plot_dir_temp_aic, exp_temp,reg_level,"_testAIC_",paste(name_extension, collapse="_"),".RData",sep="")


if(exists("use_2sls_manually") && use_2sls_manually){
  name_extension<-c(name_extension,"use_2sls_manually")
  if(exists("use_rounded_type_hat") && use_rounded_type_hat){
    name_extension<-c(name_extension,"use_rounded_type")
  }
}else{
  use_2sls_manually<-0
}


file_name_aic<-paste(plot_dir_temp_aic, "StepAIC_ordered_list_",paste(name_extension,collapse="_"),sep="")
write(paste("Results of stepAIC tests"), file=file_name_aic, append=F)


table_for_stepaic<-matrix(NA,nrow=0,ncol=0)

if(use_truncated_data && is.element("Time", colnames(data_work))){
  # Remove data with almost 100% of only one technology type 
  # This concerns critical-levels: If last sign conversion in first or last period, conv. techn. 
  # use is almost 100 pct. 
  pers_temp<-unique(data_work$Periods)
  times_temp<-unique(data_work$Time)
  if(is.element(min(pers_temp, na.rm=T), times_temp) ||is.element(max(pers_temp, na.rm=T), times_temp)){
  inds_temp<-which(data_work$Time %in% c(min(pers_temp, na.rm=T), (max(pers_temp, na.rm=T))))
  #if(is.element(max(pers_temp, na.rm=T), times_temp)){
  #  inds_temp<-which(data_work$Time %in% c((max(pers_temp, na.rm=T))))
    if(exists("use_type_adjusted_crit_levels") && use_type_adjusted_crit_levels){
      c<-match(c("Time", "abs_crit_prod_ratio", "abs_crit_techn_ratio", "abs_crit_skill_ratio", "abs_crit_cap_ratio", "variance_share_full"), colnames(data_work))
    }else{
      c<-match(c("Time", "crit_prod_ratio", "crit_techn_ratio", "crit_skill_ratio", "crit_share"), colnames(data_work))
    }
    if(is.element(NA,c)){
      print(paste("Warning: Are you using the desired data?", paste(colnames(data_work), collapse=" ")))
      
      c<-c[which(!is.na(c))]
    }
    data_work[inds_temp,c]<-NA
  }
  rm(pers_temp, times_temp, inds_temp, c)
}

for(targ in target_vars_temp){
  
  inds_t<-match(targ, colnames(data_work))
  print(targ)
  for(p in periods_temp_target){
    inds_targ_temp<-which(data_work$Periods == p)
    target <- data_work[inds_targ_temp, inds_t]
    if(is.element(abs(Inf), target)){
      target[which(is.infinite(target))]<-NA
    }
    inds_keep<-which(!is.na(target))
    target<-target[inds_keep]
    inds_targ_temp<-inds_targ_temp[inds_keep]
    type<-data_work[inds_targ_temp,match("Type", colnames(data_work))]
    ids_target<-data_work$id[inds_targ_temp] # needed to check pair-wise completeness
    descr_temp<-c(paste("Target variable ",names(targ), "measured in ",p))
    p2<-periods_temp_controls[1]
    ### Variance share is too small and causes numerical problems in regression. Multiply by 100. 
    if(targ == "variance_share_full" && abs(max(target, na.rm=T)) < 0.5){
      target<-100*target
    }

    for(p2 in periods_temp_controls){
      #table_old<-matrix(NA,nrow=0,ncol=0)
      # pair-wise completeness check
      data_temp<-data_work[which(data_work$Periods == p2),]
      ids_contr<-data_work$id[which(data_work$Periods == p2)]
      ids_both<-intersect(ids_target, ids_contr)
      target<-as.vector(target[match(ids_both, ids_target)])
      min_targ<-min(target, na.rm=T)
      max_targ<-max(target, na.rm=T)
      
      type<-as.vector(type[match(ids_both, ids_target)])
      type<-as.character(type)
      type_named<-type
      type[which(as.character(type)=="conv")]<-0
      type[which(as.character(type)=="eco")]<-1
      type<-as.numeric(type)
      
      data_temp<-data_temp[match(ids_both, ids_contr),]
      inds_pairs<-which(data_work$Periods == p2)[match(ids_both, ids_contr)]
      
      # Get diff. sets of control vars
      macro0<-macro_contr_temp[inds_pairs,]
      micro0<-micro_contr_temp[inds_pairs,]
      macro_b_<-barr_macro_temp[inds_pairs,]
      micro_b_<-barr_micro_temp[inds_pairs,]
      chi0<-spills_temp[inds_pairs,]
      if(with_policy && with_rand_policy){
        p0<-as.matrix(pol_temp[inds_pairs,])
        colnames(p0)<-c("tax", "cons", "inv")
      }else{p0<-NULL}
    
      colnames(chi0)<-c("int", "spill")
      colnames(macro_b_)<-c("frontier", "skill_macro")
      colnames(micro_b_)<-c("frontier", "skill_micro")
      macro_<-cbind(macro0, micro0)
      macro_<-as.matrix(macro_)
      
      if(use_fitDist_models){
        fitdist_temp<-fitDist(target)
        families_temp<-c(fitdist_temp$family[1])
        if(min_targ>=0 && max_targ<=1 && targ != "variance_share_full"){
          fitdist_temp_bin<-fitDist(round(target))
          #write(paste("\n \n Results of fitDist(round(target)) for binary \n \n "),file=file_name,append=T)
          #capture.output(fitdist_temp_bin, file=file_name, append=T)
          families_temp<-c(families_temp, fitdist_temp_bin$family[1])
        }
      }
      chi_<-chi0
      c0<-as.matrix(chi0)
      
      if(with_rand_barr){
        barrs_temp<-c("micro", "macro")
      }else{
        barrs_temp<-c("none")
      }
      for(b in "macro"){#c("micro", "macro")){
        use_stepAIC<-1
        if(b == "micro"){
          b0<-micro_b_
        }else if(b == "macro"){
          b0<-macro_b_
        }else if(b == "none"){
          b0<-c()
        }
        if(with_policy && with_rand_policy){
          p_ <- p0
          pbc_inter<-1
        }
        file_name<-paste(plot_dir_temp, targ,"_in_",p,"_contr_in_",p2,"_",b,sep="")
        if(use_fitDist_models){
          write(paste("\n \n Results of fitDist(target) \n \n "),file=file_name,append=T)
          capture.output(fitdist_temp, file=file_name, append=T)
        }
        file_name_tex<-paste(plot_dir_temp, targ,"_in_",p,"_contr_in_",p2,"_",b,"_table_for_tex",sep="")
        write(paste("\n \n \n #### REGRESSION ON TARGET VAR: ", targ, "IN", p, b), file=file_name_aic, append = T)
        write(paste("\n \n \n #### REGRESSION ON TARGET VAR: ", targ, "IN", p, b), file=file_name_tex, append = F)
        write(paste("\n \n \n #### REGRESSION ON TARGET VAR: ", targ, "IN", p, b), file=file_name, append = F)
        if(use_fitDist_models){
          write(paste("\n \n Results of fitDist(target) \n \n "),file=file_name,append=T)
          capture.output(fitdist_temp, file=file_name, append=T)
          if(min_targ>=0 && max_targ<=1 && targ != "variance_share_full"){
            write(paste("\n \n Results of fitDist(round(target)) for binary \n \n "),file=file_name,append=T)
            capture.output(fitdist_temp_bin, file=file_name, append=T)
          }
        }

        b_<-as.matrix(b0)
        # Inputs for run_model.r script: target, c_ as matrix of controls, b_ as barrier controls
        c_<-c0
        p_reg_simple<-1
        
        #source(paste(script_dir,"/all_agents/regression_analyses/aux/run_model.r", sep=""))
        
        p_reg_simple<-F
        interact_int_spill<-data_temp$min_learning_coefficient_var*data_temp$learning_spillover_strength_var
        c_ <- cbind(c0, interact_int_spill)
        interact_skill_front<-data_temp$specific_skill_gap*data_temp$frontier_gap        
        b_ <- cbind(b0, interact_skill_front)
        b_<-as.matrix(b_)
        source(paste(script_dir,"/all_agents/regression_analyses/aux/run_model.r", sep=""))
        chi_int_sq<-data_temp$min_learning_coefficient_var*data_temp$min_learning_coefficient_var
        chi_spill_sq<-data_temp$learning_spillover_strength_var*data_temp$learning_spillover_strength_var
        chi_sq<-cbind(chi_int_sq, chi_spill_sq)
        c_ <- cbind(c0, chi_sq)
        skill_sq<-data_temp$specific_skill_gap*data_temp$specific_skill_gap
        front_sq<-data_temp$frontier_gap*data_temp$frontier_gap
        barr_sq<-cbind(skill_sq, front_sq)
        rm(skill_sq, front_sq)
        b_ <- cbind(b0, barr_sq)
        b_<-as.matrix(b_)
        #source(paste(script_dir,"/all_agents/regression_analyses/aux/run_model.r", sep=""))
        c_ <- cbind(c_, interact_int_spill)
        b_ <- cbind(b_, interact_skill_front)
        b_<-as.matrix(b_)
        source(paste(script_dir,"/all_agents/regression_analyses/aux/run_model.r", sep=""))
        if((exists("use_stepAIC") && use_stepAIC) || (exists("use_mixture_model") && use_mixture_model)){
          print("Make stepAIC tests")
          source(paste(script_dir,"/all_agents/regression_analyses/aux/stepAIC_test.r", sep=""))
        }
        
      } # for barriers in c(micro, macro) (refers to difference in type of skill measurement, 
      # i.e. whether on firm or macro level)
      if(exists("table_for_tex")){
        save(table_for_tex, file=paste(plot_dir_temp, targ,"_in_",p,"_contr_in_",p2,"_table_for_tex_data.RData",sep=""))
        rm(table_for_tex)
      }
      #stop("Check whether table notest created and table old")
      rm(list=ls(pattern="interact"))
    } # end for p2 in periods control
  }# end p in periods_temp_target
  rm(p, p2, min_targ, max_targ)
}# end targ in target_vars_temp
if(exists("exp_temp")!=T){exp_temp<-""}
if(exists("name_extension")){
  file_temp<-paste(plot_dir_temp_aic, exp_temp,reg_level,"_testAIC_",paste(name_extension, collapse="_"),".RData",sep="")
}else{
  file_temp<-paste(plot_dir_temp_aic, exp_temp,reg_level,"_testAIC.RData",sep="")
}
save(table_for_stepaic, file=file_temp)
rm(table_for_stepaic, file_temp, exp_temp)

rm(targ)
rm(b_, c_, barr_sq, chi_sq, macro_, chi_)
rm(list=ls(pattern="_b_"))
rm(list=ls(pattern="temp"))
rm(b0, c0, micro0, macro0, chi0, p0)
rm(data_work, file_name_tex)
