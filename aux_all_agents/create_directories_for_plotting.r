if(exists("reg_data_temp") && agent=="Firm"){
  backup<-reg_data_temp
}
# create plot directories

if(exists("take_logs") == TRUE && take_logs==1){
  kind_of_data<-"logs"
}else{
  kind_of_data<-"levels"
}
# 
# 
#plot_directory<-paste("/home/kerstin/Schreibtisch/database/Plots/",agent,"/", sep="")
plot_directory_prelim<-paste(experiment_directory,"own_Plots/", sep="")
if(dir.exists(plot_directory_prelim)!=TRUE){
  dir.create(plot_directory_prelim)
}
if(exists("kind_of_data")== TRUE){
  plot_directory_prelim<-paste(experiment_directory,"own_Plots/",kind_of_data,"/", sep="")
}
if(dir.exists(plot_directory_prelim)!=1){
  dir.create(plot_directory_prelim)
}

if(exists("data_type")== TRUE){
  plot_directory_prelim<-paste(plot_directory_prelim,"/",data_type[d],"/", sep="")
}
if(dir.exists(plot_directory_prelim)!=1){
  dir.create(plot_directory_prelim)
}
if(TS == 1){
plot_dir_time_series<-paste(plot_directory_prelim,"Time_series/", sep="")
if(dir.exists(plot_dir_time_series)!=1){
  dir.create(plot_dir_time_series)
}
if(parameter != "Batch"){
  plot_dir_time_series<-paste(plot_dir_time_series,"/", parameter,"/", sep="")
  if(dir.exists(plot_dir_time_series)!=1){
    dir.create(plot_dir_time_series)
  }
}

}
if(CORR==1){
plot_dir_validation_cor<-paste(plot_directory_prelim,"Correlations/", sep="")
if(dir.exists(plot_dir_validation_cor)!=1){
  dir.create(plot_dir_validation_cor)
}
}
if(DENS==1){
plot_dir_distr<-paste(plot_directory_prelim,"Distributions/", sep="")
if(dir.exists(plot_dir_distr)!=1){
  dir.create(plot_dir_distr)
}
}
if(VALIDATION == 1){
plot_dir_validation<-paste(plot_directory_prelim,"Validation/", sep="")
if(dir.exists(plot_dir_validation)!=1){
  dir.create(plot_dir_validation)
}
}
if(MTS == 1){
plot_dir_multi_ts<-paste(plot_directory_prelim,"Multi_time_series/", sep="")
if(dir.exists(plot_dir_multi_ts)!=1){
  dir.create(plot_dir_multi_ts)
}
}

if(WILC_TYPES){
plot_dir_wilcox_type<-paste(plot_directory_prelim,"Wilcox_type/", sep="")
if(dir.exists(plot_dir_wilcox_type)!=1){
  dir.create(plot_dir_wilcox_type)
}
}

plot_dir_selection<-paste(plot_directory_prelim)
if(exists("use_experiment_directory") && use_experiment_directory){plot_dir_selection<-paste(plot_directory_prelim,"/EXPERIMENTS/",sep="")}
if(dir.exists(plot_dir_selection)!=1){
  dir.create(plot_dir_selection,recursive=T)
}




rm(plot_directory_prelim)
