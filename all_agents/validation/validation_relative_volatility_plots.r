# Create relative volatility plots for single run drawn at random. 
# 
plot_dir_temp<-paste(plot_dir_validation,"/relative_volatility_plots/", sep="")
if(dir.exists(plot_dir_temp)!=TRUE){
  dir.create(plot_dir_temp)
}

# Steps: 
# 1. Get and normalize data (by avg. in considered period) -> Compare "relative" volatility. 
# 2. Compute also time series data elements for these plots
# 
# 
#load(paste(data_directory,"data_work.RData", sep=""))
load(file=paste(data_directory,"data_single_switch_",experiment_name,"_",parameter,"_Eurostat.RData",sep=""))
vars_all_temp<-c("Type", "Periods", "Run", "monthly_output", 
            "no_vacancies", "unemployment_rate", "total_consumption_budget", 
            "monthly_investment_value")
cols_temp<-match(vars_all_temp, colnames(object))
if(is.element(NA, cols_temp)){
  print(vars_all_temp[which(is.na(cols_temp))])
  #stop()
  cols_temp<-cols_temp[!is.na(cols_temp)]
}
# Select random run: 
runs_temp<-unique(object$Run)
r <- sample(runs_temp, 1)
inds_temp<-which(object$Run == r)
# Use only data on required vars: 
data_temp<-object[inds_temp, cols_temp]
rm(object)
# Transform into time series data: 
vars_temp<-c("monthly_output", "no_vacancies", "unemployment_rate", 
             "total_consumption_budget", "monthly_investment_value")

if(mean(data_temp[,match("monthly_investment_value", colnames(data_temp))], na.rm=TRUE) > 10*mean(data_temp[,match("monthly_output", colnames(data_temp))], na.rm=TRUE)){
  print("Most likely, you took logs of output, but not of investment value, transform to ensure consistency")
  data_temp1<-data_temp
  data_temp1[,match("monthly_output", colnames(data_temp))]<-exp(data_temp[,match("monthly_output", colnames(data_temp))])
  data_temp1[,match("total_consumption_budget", colnames(data_temp))]<-exp(data_temp[,match("total_consumption_budget", colnames(data_temp))])
  data_temp<-data_temp1
  if(is.na(match("no_vacancies", colnames(data_temp)))){
    print("no_vacancies not available...")
  }else{
    print("Transform vacancies to vacancy rate (manually, i.e.  * 100/1600)")
    data_temp[,match("no_vacancies", colnames(data_temp))]<-data_temp[,match("no_vacancies", colnames(data_temp))]/16
  }
}
cols_temp<-match(vars_temp, colnames(data_temp))
cols_temp<-cols_temp[which(!is.na(cols_temp))]
# Create empty data frames 
trend_full<-array(NA, dim=c(dim(data_temp)))
cycle_full<-array(NA, dim=c(dim(data_temp)))
series_full<-array(NA, dim=c(dim(data_temp)))
colnames(trend_full)<-colnames(data_temp)
colnames(cycle_full)<-colnames(data_temp)
colnames(series_full)<-colnames(data_temp)
trend_full<-as.data.frame(trend_full)
cycle_full<-as.data.frame(cycle_full)
series_full<-as.data.frame(cycle_full)
trend_full[,1:3]<-as.matrix(data_temp[,1:3])
cycle_full[,1:3]<-as.matrix(data_temp[,1:3])
series_full[,1:3]<-as.matrix(data_temp[,1:3])

for(c in cols_temp){
  print(paste(colnames(data_temp)[c]))
  var_temp<-as.numeric(data_temp[,c])
  ts_data<-ts(as.numeric(var_temp), frequency = 12)
#  ts_data_temp<-mFilter(ts_data,filter="BK", pl = 8, pu= 32, type = "variable", drift=TRUE)
  #ts_data_temp<-mFilter(ts_data,filter="BK", pl =6, pu= 32, type = "variable", drift=TRUE)
  ts_data_temp<-mFilter(ts_data,filter="HP", drift = TRUE, frequency=160)
  #ts_data_temp<-mFilter(ts_data,filter="HP", drift = TRUE, frequency=400)
  cycle_full[,c]<-ts_data_temp$cycle
  trend_full[,c]<-ts_data_temp$trend
  series_full[,c]<-ts_data
} # c in cols_temp

i<-1
p<-1
for(i in 1:2){
  if(i==1){
    vars_temp1<-c("monthly_output", "total_consumption_budget", "monthly_investment_value")
    legend_names_temp<-c("Output", "Consumption", "Investment")
  }else if(i == 2){
    vars_temp1<-c("monthly_output", "no_vacancies", "unemployment_rate")
    legend_names_temp<-c("Output", "Vacancies", "Unemployment")
  }
  if(is.element(NA, match(vars_temp1, colnames(cycle_full)))){
    vars_temp1<-vars_temp1[!is.na(match(vars_temp1, colnames(cycle_full)))]
  }
  cols_temp1<-match(c("Periods", "Run", "Type", vars_temp1), colnames(cycle_full))
  data_temp_1<-cycle_full[,cols_temp1]
  trend_temp_1<-trend_full[,cols_temp1]
  p<-1
  for(p in 1:2){
    if(p == 1){
      periods_temp<-seq(10220, 12600, 20)
      name_temp<-paste(paste(vars_temp1, collapse="_"),"_10220_12600", sep="")
    }else{
      periods_temp<-seq(5000,7380, 20)
      name_temp<-paste(paste(vars_temp1, collapse="_"),"_5000_5220", sep="")
    }
    inds_temp<-which(data_temp_1$Periods %in% periods_temp)
    data_temp<-data_temp_1[inds_temp,]
    trend_temp<-trend_temp_1[inds_temp,]
    
    cols<-match(vars_temp1, colnames(data_temp))
    # Normalize by trend_full: 
    for(c in cols){
      data_temp[,c]<-data_temp[,c]/trend_temp[,c]
    }

    vec_temp<-(as.matrix(data_temp[,cols]))
    ymax<-max(vec_temp, na.rm=TRUE)
    ymin<-min(vec_temp, na.rm=TRUE)
    names<-c()
    
    pdf(paste(plot_dir_temp,name_temp,".pdf",sep=""))
    
    c1<-1
    for(c1 in 1:length(cols)){
      c<-cols[c1]
      names<-c(names,colnames(data_temp)[c])
      if(BLACK_WHITE != 1){
        if(c1 == 1){
          plot(data_temp[,c], col=c1, ylab="", xlab="Periods", ylim=range(ymin, ymax), type="l")
          legend("topright",xpd=FALSE, legend=legend_names_temp, col=c(1:length(cols)), lty = rep(1,length(cols)), #lty=c(1:length(cols)),#rep(1,length(cols)), 
                 inset=c(0,0), horiz=FALSE)
          
        }else{
          lines(data_temp[,c],col=c1)#, lty=c1)
        }
      }else{ # if black-white plot
        stop()
        if(c1 == 1){
          plot(data_temp[,c], col="black",lty=c1, ylab="", xlab="Periods", ylim=range(ymin, ymax), type = "l")
          legend("topright",xpd=FALSE, legend=legend_names_temp, lty = c(1:length(cols)),col=rep(1,length(cols)),#rep(1,length(cols)), 
                 inset=c(0,0), horiz=FALSE)
        }else{
          lines(data_temp[,c],col="black", lty=c1)
        }
      } 
    }

    dev.off()
  }







} # end i in 1:2 (two types of relative vol. plot)

#rm(list = ls(pattern="temp"))
#rm(ymin, ymax, r, i, p)
#rm(list=ls(pattern="_full"))
