pdf(paste(plot_dir_temp_aic,"/histogram_",targ,"_",use_truncated_data, sep=""))
plot(hist(target, breaks=50))
dev.off()

if(exists("families_temp") && agent!="Firm"){
  use_fitDist_models<-1
}else{
  use_fitDist_models<-0
}

if(exists("use_only_type_endog")!=T){use_only_type_endog<-1} # If use only type endog in iv regression
if(exists("use_2sls_manually")!=T){use_2sls_manually<-0} # 
if(use_only_type_endog && use_2sls_manually){print("stop: Chose only one of both use_only_type_endog || use_2sls_manually")}


if(use_mixture_model == 0 && !is.element(targ, c("share_conv_capital_used", "share_conventional_vintages_used"))){
  iv_reg_for_type<-1
}else{iv_reg_for_type<-0}


#if(reg_level == "Firm"){simple_stepAIC <- 0}else{
simple_stepAIC<-1
  #}
type<-d_


type_orig<-type

regressors<-c()
exclude_vars<-c()
exclude_vars_type<-c() # Only used if some vars should be excluded, e.g. skill barrier excluded from main regression if 
# iv reg is used
# Then use in loop: if(grepl("PATTERN_TO_EXCLUDE",c)){exclude_vars_type<-c(exclude_vars_type, c)}
policy_vars_temp1<-c()
n<-length(type)
if(with_policy && with_rand_policy){
  policy_vars_temp1<-colnames(p0)
  for(c in colnames(p0)){
    if(scale_data){
      eval(call("<-",paste(as.name(c)),scale(p0[,match(c, colnames(p0))], center=T)))
    }else{
      eval(call("<-",paste(as.name(c)),p0[,match(c, colnames(p0))]))
    }
    regressors<-c(regressors,c)
  }
}
spill_pars<-c()
if(with_rand_learn){
  for(c in colnames(c0)){
    if(scale_data){
      eval(call("<-",paste(as.name(c)),scale(c0[,match(c, colnames(c0))], center=T)))
    }else{
      eval(call("<-",paste(as.name(c)),c0[,match(c, colnames(c0))]))
    }
    regressors<-c(regressors, c)
    if(length(grep("spill",c))>0){spill_pars<-c(spill_pars, c)}
  } # for c in colnames
}# if with rand learn
barr_names_temp<-c()

if(with_rand_barr){
  for(c in colnames(b0)){
    if(scale_data){
      eval(call("<-",paste(as.name(c)),scale(b0[,match(c, colnames(b0))], center=T)))
    }else{
      eval(call("<-",paste(as.name(c)),b0[,match(c, colnames(b0))]))
    }
    barr_names_temp<-c(barr_names_temp, c)
    regressors<-c(regressors, c)
    if(frontier_as_instrument && with_rand_barr && ((targ != "share_conv_capital_used" && reg_level == "Eurostat") 
       || (targ != "share_conventional_vintages_used" && reg_level == "Firm")) ){
      if(grepl("frontier",c)){exclude_vars<-c(exclude_vars, c)}
    }
  }
}

#if(with_rand_policy && is.element("tax", colnames(p0)) && is.element("eco_price_wage_ratio", colnames(macro_))){
#  macro_<-macro_[,-match("eco_price_wage_ratio", colnames(macro_))]
#}
controls<-colnames(macro_)

for(c in colnames(macro_)){
  if(scale_data){
    eval(call("<-",paste(as.name(c)),scale(macro_[,match(c, colnames(macro_))])))
  }else{
    eval(call("<-",paste(as.name(c)),macro_[,match(c, colnames(macro_))]))
  }
}
if(agent == "Firm" && is.element("technology_conv", controls) 
   && is.element("mean_spec_skills_conv", controls)){
  # Both variabls capture same effect
  controls<-controls[which(controls != "technology_conv")]
}
if(with_policy && with_rand_policy){
  controls<-controls[which(controls != "eco_price_wage_ratio")]
}
regressors<-unique(regressors)
if(length(exclude_vars_type)>0){regressors_type<-unique(c(regressors[-which(regressors %in% exclude_vars_type)], controls))
}else{regressors_type<-unique(c(regressors,controls))}
if(length(exclude_vars)>0){regressors_reg<-unique(c("type", regressors[-which(regressors %in% exclude_vars)], controls))
}else{regressors_reg<-unique(c("type", regressors,controls))}

data_reg_temp<-cbind(target, type)
for(i in unique(c(regressors, controls))){
  data_reg_temp<-cbind(data_reg_temp, eval(as.name(i)))
}
colnames(data_reg_temp)<-c("target", "type", unique(c(regressors, controls)))

if(exists("simple_stepAIC") && simple_stepAIC){
  
  if(use_ols_procedure){
    regressors_reg<-unique(c("type", regressors, controls))
    regressors_type<-unique(c(regressors_type, controls))
 
    reg_equation<-eval(as.formula(paste("target ~",paste(regressors_reg, collapse = "+"))))
    source(paste(script_dir,"/all_agents/regression_analyses/aux/step_aic_fct.r",sep=""))
  }
  if(use_mixture_model && targ!="share_conv_capital_used" && targ!="Time"){
    stop("mix test")
    regressors_mix<-unique(regressors_reg[which(regressors_reg!="type")])
    reg_equation<-eval(as.formula(paste("target ~",paste(regressors_mix, collapse = "+"))))
    regressors_pi<-regressors_mix
    pi_equation<-eval(as.formula(paste("~",paste(regressors_mix, collapse = "+"))))
    data_temp_mix<-as.data.frame(data_reg_temp[,which(colnames(data_reg_temp) != "type")])
    if(exists("family_temp")!=T){family_temp<-"WEI"}
    attach(data_temp_mix)
    #reg_obj<-gamlss(reg_equation, family=BI(mu.link="probit"))
    #if(reg_obj$converged != T){reg_obj<-gamlss(reg_equation, family=BI(mu.link="probit"), method=mixed(method_temp[1],method_temp[2]))}
    source(paste(script_dir,"/all_agents/regression_analyes/aux/stepAIC_mixture.r",sep=""))
  }
} # else: simple lin terms stepAIC

# Add squared and interaction terms

reg_temp<-regressors
#if(agent == "Firm" && targ != "share_conventional_vintages_used"){stop()}
reg_temp1<-c(); ex_type_temp1<-c(); ex_reg_temp1<-c()
for(r11 in 1:length(c("type",reg_temp))){
  r1<-c("type",reg_temp)[r11]
  for(r22 in r11:length(c("type",reg_temp))){
    r2<-c("type",reg_temp)[r22]
    
    if(!is.element(r1, policy_vars_temp1) || !is.element(r2, policy_vars_temp1)){
      if(r1!=r2) reg_temp1<-c(reg_temp1, paste(r1,":",r2,sep=""))
      else if(r1==r2 && r1 != "type") reg_temp1<-c(reg_temp1, paste("I(",r1,"*",r2,")",sep=""))
    }
    
    if(r1==r2){
      ex_reg_temp1<-c(ex_reg_temp1,paste("I(",r1,"*",r2,")",sep=""))
    }
    if(length(intersect(c(r1,r2), barr_names_temp)) != 0 
    && length(intersect(c(r1,r2), policy_vars_temp1)) != 0){
      if(r1 != r2){
        ex_reg_temp1<-c(ex_reg_temp1,paste(r1,":",r2,sep=""))
      }
    }
    
    
    if(length(intersect(c(r1,r2), exclude_vars)) != 0){
      if(r1==r2) ex_reg_temp1<-c(ex_reg_temp1, paste("I(",r1,"*",r2,")",sep=""))
      else ex_reg_temp1<-c(ex_reg_temp1, paste(r1,":",r2,sep=""))
    }
    if(length(intersect(c(r1,r2),barr_names_temp)) == 2 && r1!=r2){
      ex_reg_temp1<-c(ex_reg_temp1, paste(r1,":",r2,sep=""))
    }
     
    if(length(intersect(c(r1,r2), exclude_vars_type)) != 0 || (r1 =="type" || r2 == "type")){
      if(r1==r2 && use_sq_in_type){
        ex_type_temp1<-c(ex_type_temp1, paste("I(",r1,"*",r2,")",sep=""))
      }else{
       ex_type_temp1<-c(ex_type_temp1, paste(r1,":",r2,sep=""))
      }
    }
  } # for r22
} # for r11
#if(with_policy){reg_temp1<-c(reg_temp1, paste("tax:eco_price_wage_ratio",sep=""))}
regressors<-unique(c(reg_temp, reg_temp1))
#regressors_type<-c(regressors_type, reg_temp1)
regressors_reg<-c()
regressors_inter<-c()
regressors_type<-c()
for(r in regressors){
  if(!is.element(r, c(exclude_vars, ex_reg_temp1))|| is.element(r, controls)){
    if((length(grep("type", r)) == 0 || length(grep("frontier", r)) == 0) || (length(grep("type", r)) == 1 && length(grep("frontier", r)) == 1))
      regressors_reg<-c(regressors_reg, r)
  }
  if((!is.element(r, c(exclude_vars_type, ex_type_temp1)))|| is.element(r, controls)){
    regressors_type<-c(regressors_type, r)
  }
  #if((length(grep("spill", r)) == 0 && length(grep("type", r)) == 0 && length(grep("tax", r)) == 0)|| is.element(r, controls)  ){
  #  regressors_type<-c(regressors_type, r)
  #}
  if(with_policy){
     if((grepl("skill",r) || grepl("frontier",r)) && (grepl("spill",r) || grepl("int",r)) || 
     ((grepl("tax",r) || grepl("inv",r) || grepl("cons",r)) && (grepl("spill",r) || grepl("int",r) || (grepl("skill",r) || grepl("frontier",r))))){
      regressors_inter<-c(regressors_inter, r)
     }
    
  }# if with policy
}

regressors_reg<-unique(c("type", regressors_reg, controls))
regressors_type<-unique(c(regressors_type, controls))
regressors_inter<-unique(regressors_inter)

if(exists("test_no_exclude") && test_no_exclude){
  regressors_reg<-unique(c("type", regressors_reg, controls))
  regressors_type<-regressors_reg[which(grepl("type", regressors_reg) == F)]
}

write(paste("\n Regressors reg:", paste(regressors_reg, collapse="+ ")), file=file_name_aic, append=T)
write(paste("\n Regressors type:", paste(regressors_type, collapse="+ ")), file=file_name_aic, append=T)
write(paste("\n Regressors inter:", paste(regressors_inter, collapse="+ ")), file=file_name_aic, append=T)
if(length(regressors_inter)>0){iterate<-2}else{iterate<-1}

for(r in 1:iterate){
  if(iterate==2){
    if(r == 2){
      regressors_type<-unique(c(regressors_type, regressors_inter))
      regressors_reg<-unique(c(regressors_reg, regressors_inter))
      if(1==2 && with_policy && 
         ( is.element("tax:spill", regressors_reg) && is.element("I(tax*tax)", regressors_reg) || is.element("spill:skill_macro", regressors_reg))){
        if(is.element("tax:spill", regressors_reg)){
          if(is.element("I(tax*tax)", regressors_reg)) regressors_reg<-regressors_reg[-which(regressors_reg == "I(tax*tax)")]
          if(is.element("spill:skill_macro", regressors_reg)) regressors_reg<-regressors_reg[-which(regressors_reg == "spill:skill_macro")]
        }
      }
    
    for(f in 1:2){
      if(f == 1){regs<-regressors_type}else if(f == 2){regs<-regressors_reg}
      #rem<-c(); counter<-0
      #for(q in regs){
      #  if(!is.element(q, controls) && !is.element(q, regressors_inter)){
      #    if(sum(c(grepl("spill", q), grepl("skill", q), grepl("tax", q), grepl("I", q))) == 2 ){
      #      rem<-c(rem, q)}
      #    if((grepl("spill", q)|| grepl("skill", q)|| grepl("tax", q)) && grepl(":", q) == F && grepl("I", q) == F ){
      #      counter<-counter+1
      #      print(q)
      #      if(counter > 1){rem<-c(rem, q)}
      #    }
      #  }
      #} # for q in regs
      #stop(paste("Remove ", rem, "verify this"))
      #if(length(rem)>0){write(paste("Remove from set of regressors:", paste(rem, collapse=", ")), file=file_name_aic, append=T)}
      #regs<-regs[which(is.na(match(regs, rem)))] # Keep only vars that are not in rem
      if(f == 1){regressors_type<-regs}else if(f == 2){regressors_reg<-regs}
      
      #if(counter>1){stop("Verify that you have included/excluded spill-skill-tax analogy")}
      #rm(counter, rem)
    } # for f in _reg, _type
    rm(f)
      
    } # if r == 2
    #if(with_policy && 
    #   ( is.element("tax:spill", regressors_reg) && is.element("I(tax*tax)", regressors_reg) 
    #     || is.element("spill:skill_macro", regressors_reg))){
    #  if(is.element("tax:spill", regressors_reg)){
    #    if(is.element(is.element("I(tax*tax)", regressors_reg))){
    #      regressors_reg<-regressors_reg[-which(regressors_reg == "I(tax*tax)")]
    #    }
    #    if(is.element("spill:skill_macro", regressors_reg)){
    #      regressors_reg<-regressors_reg[-which(regressors_reg == "spill:skill_macro")]
    #    }
    #  }
    #}
    if(r==2){
      write(paste("\n \n ####### \n REPEAT EVERYTHING WITH SPECIAL INTERACTION TERMS \n"), file=file_name_aic, append=T)
    }
    write(paste(paste(regressors_inter, collapse="+ ")), file=file_name_aic, append=T)
    
  }
  #if((with_policy || with_rand_barr )&& is.element("I(type:frontier)", regressors_reg)){
  #  regressors_reg<-regressors_reg[-match("I(type:frontier)", regressors_reg)]
  #}
  regressors_reg<-unique(regressors_reg)
  regressors_inter<-unique(regressors_inter)
  regressors_type<-unique(regressors_type)
  #if(with_rand_barr == 0){stop("Check this")}
  
  colname_temp<-NULL
  if(exists("macro_control_regression") && macro_control_regression){
    colname_temp<-paste(p,"_",p2,sep="")
  }
  source(paste(script_dir,"/all_agents/regression_analyses/aux/step_aic_fct.r",sep=""))
}
if(use_tax_spill_skill){
  if(with_rand_barr){
    regressors_reg<-c("skill_macro", "skill_micro", "skill")
    sk_temp<-regressors_reg[which(regressors_reg %in% colnames(b_))]
    if(exists("use_sq") && use_sq){regressors_reg<-c("type", sk_temp, paste("I(",sk_temp,"*",sk_temp,")",sep=""))
    }else{regressors_reg<-c("type", sk_temp)}
    rm(sk_temp)
    regressors_reg<-c(regressors_reg, paste("type:",regressors_reg[which(regressors_reg != "type")], sep=""))
    
    regressors_reg_fix<-regressors_reg
    regressors_t<-regressors[!isTRUE(which(regressors %in% regressors_reg))]
    regressors_type<-c()
    for(reg in regressors_t){if(!grepl("type",reg) && !grepl("skill", reg)){regressors_type<-c(regressors_type, reg)}}
    #regressors_type<-regressors_type[!which(grepl("type", regressors_type))]
    colname_temp<-"skill only"
    if(exists("macro_control_regression") && macro_control_regression){
      colname_temp<-paste(colname_temp,"_",p,"_",p2,sep="")
    }
    source(paste(script_dir,"/all_agents/regression_analyses/aux/step_aic_fct.r",sep=""))
    rm(regressors_reg_fix)
  }
  
  
  if(with_rand_policy){
    if(exists("use_sq") && use_sq){regressors_reg<-c("type", "tax", "I(tax * tax)")
    }else{regressors_reg<-c("type", "tax", "I(tax * tax)")}
      
    regressors_reg<-c(regressors_reg, paste("type:",regressors_reg[which(regressors_reg != "type")], sep=""))
    regressors_reg_fix<-regressors_reg
    
    regressors_t<-regressors[!isTRUE(which(regressors %in% regressors_reg))]
    regressors_type<-c()
    for(reg in regressors_t){if(!grepl("type",reg) && !grepl("tax", reg)){regressors_type<-c(regressors_type, reg)}}
    colname_temp<-"tax only"
    if(exists("macro_control_regression") && macro_control_regression){
      colname_temp<-paste(colname_temp,"_",p,"_",p2,sep="")
    }
    source(paste(script_dir,"/all_agents/regression_analyses/aux/step_aic_fct.r",sep=""))
    rm(regressors_reg_fix)
  }
  
  if(with_rand_learn){
    regressors_reg<-c("spill", "chi_spill")
    sp_temp<-regressors_reg[which(regressors_reg %in% colnames(c_))]
    if(exists("use_sq") && use_sq){regressors_reg<-c("type", sp_temp, paste("I(",sp_temp,"*",sp_temp,")",sep=""))
    }else{regressors_reg<-c("type", sp_temp)}
    rm(sp_temp)
    regressors_reg<-c(regressors_reg, paste("type:",regressors_reg[which(regressors_reg != "type")], sep=""))
    regressors_reg_fix<-regressors_reg
    regressors_t<-regressors[!isTRUE(which(regressors %in% regressors_reg))]
    regressors_type<-c()
    for(reg in regressors_t){if(!grepl("type",reg) && !grepl("spill", reg)){regressors_type<-c(regressors_type, reg)}}
    colname_temp<-"spill only"
    if(exists("macro_control_regression") && macro_control_regression){
      colname_temp<-paste(colname_temp,"_",p,"_",p2,sep="")
    }
    source(paste(script_dir,"/all_agents/regression_analyses/aux/step_aic_fct.r",sep=""))
    rm(regressors_reg_fix)
  }
  
  if(with_rand_learn){
    regressors_reg<-c("int", "chi_int")
    sp_temp<-regressors_reg[which(regressors_reg %in% colnames(c_))]
    if(exists("use_sq") && use_sq){regressors_reg<-c("type", sp_temp, paste("I(",sp_temp,"*",sp_temp,")",sep=""))
    }else{regressors_reg<-c("type", sp_temp)}
    rm(sp_temp)
    regressors_reg<-c(regressors_reg, paste("type:",regressors_reg[which(regressors_reg != "type")], sep=""))
    regressors_reg_fix<-regressors_reg
    regressors_t<-regressors[!isTRUE(which(regressors %in% regressors_reg))]
    regressors_type<-c()
    for(reg in regressors_t){if(!grepl("type",reg) && !grepl("int", reg)){regressors_type<-c(regressors_type, reg)}}
    colname_temp<-"int only"
    if(exists("macro_control_regression") && macro_control_regression){
      colname_temp<-paste(colname_temp,"_",p,"_",p2,sep="")
    }
    source(paste(script_dir,"/all_agents/regression_analyses/aux/step_aic_fct.r",sep=""))
    rm(regressors_reg_fix)
    colname_temp<-NULL
  }
  
}


capture.output(stat.desc(as.matrix(target)), file=file_name_aic, append=T)

# Set stepAIC to zero. THis script should be run only one time!
#use_stepAIC<-0
rm(reg_equation, reg_obj, r1, r2, r11, r22,r)
rm(list=ls(pattern="stepaic_"))
rm(list=ls(pattern="_temp1"))
rm(list=ls(pattern="_reg"))
rm(list=ls(pattern="regressors"))
