# Get parameter data: 
# 
# Input: 
### parameters_tmp, 
### vars_temp: which variables to select? Incl fix_vars like Periods, Type, etc. 
### 
# Output: 
### df_data_temp: long data frame consting of data across different 
###               paramters with column "Parameter". 
#
#

for(p in 1:length(parameters_tmp)){
  parameter<-parameters_tmp[p]
  if(exists("bau_in_diff_folder") && bau_in_diff_folder == 1 && p==1){ # && parameter == parameter_bau){
    filename_temp<-paste(data_dir_bau,"data_single_large_",exper_name_bau,"_",parameter,"_",agent,".RData",sep="")
  }else{
    filename_temp<-paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep="")
  }
  if(file.exists(filename_temp)){
    load(filename_temp)
    if(agent=="Firm"){
      object<-object[which(as.numeric(object$active ) == 1),]
    }
    object<-cbind(Parameter=parameter_names[p], object)
    if(p==1){
      ind_vars_temp<-match(vars_temp, colnames(object))
      ind_vars_temp<-ind_vars_temp[which(is.na(ind_vars_temp) == FALSE)]
      df_data_temp<-object[ind_vars_temp]
    }else{
      ind_vars_temp<-match(vars_temp, colnames(object))
      ind_vars_temp<-ind_vars_temp[which(is.na(ind_vars_temp) == FALSE)]
      
      #object<-object[,which(is.na(match(colnames(df_data_temp), colnames(object))) != TRUE)]
      #df_data_temp<-df_data_temp[,which(is.na(match(colnames(object), colnames(df_data_temp))) != TRUE)]
      if(length(intersect(colnames(df_data_temp), colnames(object))) == length(ind_vars_temp)){
        df_data_temp<-rbind(df_data_temp, object[ind_vars_temp])
      }else{
        overlap<-intersect(colnames(df_data_temp), colnames(object))
        df_data_temp<-df_data_temp[match(overlap, colnames(df_data_temp))]
        object<-object[match(overlap, colnames(object))]
        df_data_temp<-rbind(df_data_temp, object)
      }
    }
    rm(object)
  }
} # end for parameter in parameters_tmp
