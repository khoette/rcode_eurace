if(is.element(NA, match(c("skill_divergence", "frontier_divergence"), colnames(data_temp)))){
  stop("You do not have the data you are interested in. Run Compute_divergence.r!")
}
### Plot parameter time series of divergence: 
# Construct batch format data frame used as input for parameter_ts.r
temp<-data_temp_batch[which(data_temp_batch$Type == "eco"),
                      match(c("Periods","Parameter", "s_skill_ratio", "frontier_productivity_ratio"), colnames(data_temp_batch))]
temp0<-temp[,c(1,2)]
temp_rm<-data_temp_batch[which(data_temp_batch$Type == "conv"),
                      match(c("s_skill_ratio", "frontier_productivity_ratio"), colnames(data_temp_batch))]
temp1<-temp[,c(3,4)]-temp_rm[,c(1,2)]
rm(temp, temp_rm)
colnames(temp1)<-c("skill_divergence", "frontier_divergence")
aggr<-"batch"
plot_dir_temp<-plot_dir_selection
source(paste(script_dir, "/aux_all_agents/plot_settings/plot_settings.r", sep=""))
for(value in c("skill_divergence", "frontier_divergence")){
  temp<-cbind(temp0, temp1[,match(value, colnames(temp1))])
  colnames(temp)[3]<-value
  source(paste(script_dir, "/all_agents/parameter/parameter_ts.r", sep=""))
}

#stop("This script is not yet completed!")
file_name_temp<-paste(plot_dir_selection,"Overview_knowledge_stock_divergence.txt", sep="")
if(agent == "Eurostat"){
  vars_to_compare_temp<-c("share_conv_capital_used", "s_skill_ratio", "frontier_productivity_ratio", 
                          "no_active_firms", "monthly_output", 
                          "skill_divergence", "frontier_divergence")
}else{
  stop("You should set the vars to compare!!")
}
for(p in p_temp){
  inds_temp<-which(data_temp$Parameter == p)
  eval(call("<-",as.name(paste("inds_temp_",p, sep="")),as.name(paste("inds_temp",sep=""))))
}

if(length(p_temp)>1){
vars_to_compare_temp<-c("skill_divergence", "frontier_divergence")
write(paste("Are there sign differences in the divergence of knowledge stocks across parameters?"), file = file_name_temp, append = FALSE)
for(v in vars_to_compare_temp){
  ind_var_temp<-match(v, colnames(data_temp))
  write(paste("\n\n Variable: ",v, "\n\n"), file = file_name_temp, append = TRUE)
  data_array_temp<-as.numeric(as.character(data_temp[,ind_var_temp]))
  for(p1 in 1:(length(p_temp)-1)){
    p<-p_temp[p1]
    eval(call("<-",as.name(paste("inds_temp1", sep="")),as.name(paste("inds_temp_",p,sep=""))))
    data1_temp<-data_array_temp[inds_temp1]
    mean1_temp<-mean(data1_temp, na.rm=TRUE)
    sd1_temp<-sd(data1_temp, na.rm=TRUE)
    start_temp<-p1+1
    for(p2 in p_temp[start_temp:length(p_temp)]){
      eval(call("<-",as.name(paste("inds_temp2", sep="")),as.name(paste("inds_temp_",p2,sep=""))))
      data2_temp<-data_array_temp[inds_temp2]
      mean2_temp<-mean(data2_temp, na.rm=TRUE)
      sd2_temp<-sd(data2_temp, na.rm=TRUE)
      write(paste("Compare parameter ", p, "vs.", p2), file = file_name_temp, append = TRUE)
      wil_temp<-wilcox.test(data1_temp, data2_temp, na.rm=TRUE, alternative = "two.sided", paired=FALSE)
      write(paste("\n Means and sd: ",p, ": ", mean1_temp, sd1_temp," and for ",p2,": ", mean2_temp, sd2_temp,"\n Length: ", length(data1_temp), length(data2_temp), "\n"), file = file_name_temp, append = TRUE)
      #write.table(wil_ec, file = filename, append = TRUE)
      capture.output(wil_temp, file=file_name_temp, append = TRUE)
    }
  }
}

} # else if length p_temp > 1