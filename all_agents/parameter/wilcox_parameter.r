# Wilcox test: 

# Create directories, set time intervals, set variables of interest... 
source(paste(script_dir, "/all_agents/parameter/header_wilc_parameter.r", sep=""))
# Get parameter data: Output df_data_temp
source(paste(script_dir,"/all_agents/parameter/get_parameter_data.r", sep=""))

# Settings to write latex table: 
# Create matrix for xtable: 
if(agent=="Eurostat"){
vars_for_tab_temp<-c("share_conv_capital_used",  "sd_share", "eco_price_wage_ratio", 
                     "frontier_productivity_ratio", "s_skill_ratio", "monthly_output", 
                     "unemployment_rate", "no_active_firms")
table_var_names_temp<-c("Share conventional capital used", "Standard dev. share", "Eco-price-wage-ratio", 
                        "Frontier ratio", "Skill ratio", "Monthly output", 
                        "Unemployment rate", "# active firms")
}else if(agent=="Firm"){
  vars_for_tab_temp<-c("share_conventional_vintages_used", "sd_share",
                       "no_employees", "unit_costs", "technology_gap", 
                       "mean_specific_skill_gap", "actual_mark_up")
  table_var_names_temp<-c("Share conventional capital", "Standard dev. share", "# employees", 
                          "Unit costs", "Technology gap", "Skill gap", 
                          "Mark up")
}else if(agent=="Government"){
  vars_for_tab_temp<-c("base_tax_rate", "monthly_budget_balance_gdp_fraction")
  table_var_names_temp<-c("Base tax rate", "Budget balance (pct GDP)")
}
table_var_names_temp<-table_var_names_temp[which(!is.na(match(vars_for_tab_temp, variables_wilcox_temp)))]
vars_for_tab_temp<-vars_for_tab_temp[which(!is.na(match(vars_for_tab_temp, variables_wilcox_temp)))]
t_tab_temp<-c("initial", "medium", "end", "all")
#t_tab_temp <- "all"
p_pairs_temp<-c("var", rep(c("fill_me",""), sum(seq(1,(length(parameters_tmp)-1),1))))

#print(p_pairs_temp)
#stop()
# Which snapshot in time should be considered for 
# creation of table?
t_for_table_temp<-c("initial", "medium", "end", "all")

row_names_for_table_temp<-c("", rep(c(paste(p_pairs_temp)),length(vars_for_tab_temp)))
row_names_temp_2<-row_names_for_table_temp
x<-which(row_names_temp_2 == "fill_me")
row_names_temp_2[x]<-"Mean"
row_names_temp_2[x+1]<-"(Std)"
row_names_temp_2[which(row_names_temp_2 == "var")]<-""
row_names_for_table_temp[which(row_names_for_table_temp == "var")]<-vars_for_tab_temp
no_rows_temp<-length(row_names_for_table_temp)
no_cols_temp<-8
tab_temp<-matrix(c(row_names_for_table_temp, row_names_temp_2, rep("", (no_rows_temp * (no_cols_temp-2)))), nrow = no_rows_temp, ncol=no_cols_temp)
tab_temp[1,]<-c("t","", rep(c("ESCmulticolumnBL2BRBLcBRBLESCemphBLaggrBRBR","", "ESCmulticolumnBL2BRBLcBRBLESCemphBLecoBRBR","", "ESCmulticolumnBL2BRBLcBRBLESCBLemphBLconvBRBR",""),1))
col_order<-c("aggregate","eco","conv")
tab_temp_template<-tab_temp


ind_wilc_temp<-match(c(fix_vars_temp, variables_wilcox_temp), colnames(df_data_temp))
# Remove vars from var list that are not available 
ind_wilc_temp<-ind_wilc_temp[which(is.na(ind_wilc_temp) == FALSE)]
variables_wilcox_temp<-variables_wilcox_temp[which(is.na(match(variables_wilcox_temp, colnames(df_data_temp))) == FALSE)]
data_temp<-df_data_temp
rm(df_data_temp, ind_vars_temp)

cols<-match(variables_wilcox_temp, colnames(data_temp))
for(v in cols){
  data_temp[,v]<-as.numeric(data_temp[,v])
}

ind_fix_temp<-match(c(fix_vars_temp), colnames(data_temp))

temp_data_full<-data_temp

t <- "all"
t_temp<-t_for_table_temp
for(t in t_temp){
  print(paste("Wilcox in ",t))
  
  eval(call("<-",paste("time_temp"),as.name(paste("time_temp_",t,sep=""))))
  
  if(is.element(t, t_for_table_temp)){
    tab_temp<-tab_temp_template
    tab_temp[1,1]<-paste(t)
    tab_temp[1,2]<-paste("[",time_temp[1],",",time_temp[length(time_temp)],"]", sep="")
  }
    
  temp_data<-temp_data_full[which(as.vector(as.character(temp_data_full$Periods)) %in% time_temp),]

  vars_temp<-match(variables_wilcox_temp,colnames(temp_data))
  vars_temp<-vars_temp[which(is.na(vars_temp) == FALSE)]
  
  if(exists("wilcox_average") && wilcox_average && length(time_temp)>1){
    if(agent=="Firm"){
      temp_data<-temp_data[order(temp_data$Parameter, temp_data$Run, temp_data$id),]
    }else{
      temp_data<-temp_data[order(temp_data$Parameter, temp_data$Run),]
    }
    if(exists("exper_name_bau") && exper_name_bau == experiment_name && is.element(parameter_bau, parameters_tmp)){
      print("Check in wilcox parameter analysis whether ordering has functioned. Possibly, paramters have originally the same 
            name, this could cause confusion in ordering by Run!!!")
    }
    select<-temp_data[which(as.numeric(as.character(temp_data$Periods)) == time_temp[length(time_temp)]),]
    len<-length(time_temp)/20
    idx<-c(1:len)
    for(v in vars_temp){
      var_col<-select[,v]
      var_col_full<-temp_data[,v]
      #for(run in all_temp$Run){
      for(i in 1:length(var_col)){
        idx<-c((1+((i-1)*len)): ((i*len)))
        var_col[i]<-mean(var_col_full[idx], na.rm=TRUE, nan.rm = TRUE)
      }
      select[,v]<-var_col
    }
    temp_data<-select
    rm(select, var_col, var_col_full, len, idx)
  }
    if(1==2){
    if(agent=="Eurostat"){
      for(r in unique(all_temp$Run)){
        if(r == all_temp$Run[1]){
          #data<-cbind(all_temp[which(all_temp$Periods == all_temp$Periods[1]),c(1:3)], value=c(NA))
          data<-all_temp[which(all_temp$Periods == all_temp$Periods[1]),]
        }
        data[which(data$Run == r),vars_temp]<-apply(all_temp[which(all_temp$Run == r),vars_temp], 2, mean, na.rm=TRUE)
      }
      all_temp<-data
    }else if(agent == "Firm"){
      for(i in unique(all_temp$id)){ # Note: ids indicate both: id and run
        if(i == all_temp$id[1]){
          #data<-cbind(all_temp[which(all_temp$Periods == all_temp$Periods[1]),], value=c(NA))
          data<-all_temp[which(all_temp$Periods == all_temp$Periods[1]),]
        }
        data[which(data$id == i),vars_temp]<-apply(all_temp[which(all_temp$id == i),vars_temp], 2, mean, na.rm=TRUE)
        
        }
    }
    }

  for(p in 1:length(parameters_tmp)){
    parameter_tmp_data<-temp_data[which(as.character(temp_data$Parameter) == parameter_names[p]),]
    
    eval(call("<-",paste("parameter_tmp_",parameter_names[p], sep=""),as.name(paste("parameter_tmp_data",sep=""))))
  } 
  rm(parameter_tmp_data)
  p<-1; p2<-2
  
  count_temp<-1
  
  for(p in 1:(length(parameters_tmp)-1)){
    eval(call("<-",paste("temp1"),as.name(paste("parameter_tmp_",parameter_names[p],sep=""))))

    for(p2 in  (p+1):length(parameters_tmp)){
      eval(call("<-",paste("temp2"),as.name(paste("parameter_tmp_",parameter_names[p2],sep=""))))

        for(v in variables_wilcox_temp){
          variable_temp<-v
          if(length(vars_for_tab_temp)>0 && is.element(t, t_for_table_temp) && is.element(variable_temp, vars_for_tab_temp)){
            write_table_temp<-1
            if(exists("parameter_names") && length(parameters_tmp) == length(parameter_names)){
              rowname_temp<-paste("(",parameter_names[p],",",parameter_names[p2],")",sep="")
            }else{
              rowname_temp<-paste("(",parameters_tmp[p],",",parameters_tmp[p2],")",sep="")
            }
          }else{
            write_table_temp<-F
          }
          
          filename<-paste(plot_dir_temp, "/",agent,"_results_wilcox_",variable_temp, sep = "")
          
          if(is.na(match(v,colnames(temp1)))){
            print(paste("Warning: Selected variable ",v," not in loaded data set"))
            break
          }
          
          index_temp<-match(v, colnames(temp1))
          
          if(is.element("Type", colnames(temp1)) && is.element("Type", colnames(temp2))){
            rep<-1+length(unique(temp1$Type))
            types<-unique(temp1$Type)
          }
          # temp1 is data for p in parameters_tmp, temp2 is p2 data
          if(write_table_temp && exists("col_order")){
            # Sort types such that it matches structure in latex table
            types<-col_order[!is.na(match(col_order,types))]
            if(is.element(NA,types)){
              stop("is.element(NA,types) in wilcox_parameter.r")
            }
          }
          
          for(i in 1:rep){
            if(i == 1){
              temp01<-temp1
              temp02<-temp2
            }else{
              temp01<-temp1[which(temp1$Type == types[i-1]),]
              temp02<-temp2[which(temp2$Type == types[i-1]),]
            }
            if(write_table_temp){
              col_pos_temp<-2*i + 1
              row_pos_temp<-match(variable_temp, tab_temp[,1]) + count_temp
            }
          
          
          temp1_temp<-temp01[,index_temp]
          temp2_temp<-temp02[,index_temp]
          
          temp1_temp<-temp1_temp[which(!is.na(temp1_temp))]
          temp2_temp<-temp2_temp[which(!is.na(temp2_temp))]
          temp1_temp<-temp1_temp[which(!is.infinite(temp1_temp))]
          temp2_temp<-temp2_temp[which(!is.infinite(temp2_temp))]
          
          
          if((exists("wilcox_average") == TRUE && wilcox_average==1 && min(length(temp1_temp[which(is.na(temp1_temp)==FALSE)]), length(temp2_temp[which(is.na(temp2_temp)==FALSE)]))>=1) || ( min(length(temp1_temp[which(is.na(temp1_temp)==FALSE)]), length(temp2_temp[which(is.na(temp2_temp)==FALSE)]))>=length(time_temp))){

            wil<-wilcox.test(temp1_temp, temp2_temp, na.rm=TRUE, alternative = "two.sided", paired=FALSE)
            
            if(t == t_temp[1] && p == 1 && p2 == 2 && i == 1){
              append <- FALSE
              #print("append = FALSE")
            }else{
              append<-TRUE
            }
            if(exists("wilcox_average") && wilcox_average){
              write(paste("\n\n Average ", t, "i.e. from ",time_temp[1], "to", time_temp[length(time_temp)], "\n"), file = filename, append = append)
              
            }else{
              write(paste("\n\n For period: ", t, "i.e. from ",time_temp[1], "to", time_temp[length(time_temp)], "\n"), file = filename, append = append)
            }
            if(rep == 1){
              write(paste("\n NO DISAGGREGATION BY SCENARIO TYPES"), file = filename, append = TRUE)
            }else{
              write(paste("\n DISAGGREGATION BY SCENARIO TYPE:", types[i-1]), file = filename, append = TRUE)
            }
            
            write(paste("\n Parameter ",parameter_names[p], "vs ", parameter_names[p2], "in", t, "\n"), file = filename, append = TRUE)
            mean1_temp<-round_customized(mean(temp1_temp,  na.rm=TRUE), digit=4)
            sd1_temp<-round_customized(sd(temp1_temp, na.rm=TRUE), digit = 4)
            mean2_temp<-round_customized(mean(temp2_temp, na.rm=TRUE), digit=4)
            sd2_temp<-round_customized(sd(temp2_temp, na.rm=TRUE), digit=4)
            write(paste("\n Means and sd: ",parameter_names[p], ": ", mean1_temp, sd1_temp," and ", parameter_names[p2], ": ", mean2_temp, sd2_temp, "\n Length: ", length(temp1_temp), length(temp2_temp), "\n"), file = filename, append = TRUE)
            
            #write.table(wil_ec, file = filename, append = TRUE)
            capture.output(wil, file=filename, append = TRUE)
            
            if(write_table_temp){
              #stop()
              tab_temp[row_pos_temp,col_pos_temp]<-paste(mean1_temp, " ",mean2_temp)
              tab_temp[row_pos_temp+1,col_pos_temp]<-paste("(",sd1_temp,")   (", sd2_temp,")",sep="")
              tab_temp[row_pos_temp,col_pos_temp+1]<-paste(round_customized(wil$p.value,digit=4))
              if(i==1){
                tab_temp[row_pos_temp,1]<-rowname_temp
              }
            } # end write table
          
          } # check whether sufficient non-NA data in compared arrays
          } # for i in 1:rep -> disaggregation by scenario type
        } # v in wilcox variables
      count_temp<-count_temp+2
      } # p2
  } # p
  for(variable_temp in vars_for_tab_temp){
    var_temp<-table_var_names_temp[match(variable_temp, vars_for_tab_temp)]
    tab_temp[which(tab_temp[,1] == variable_temp),1]<-paste("ESCmulticolumnBL8BRBLlBRBLESCunderlineBL",var_temp,"BRBR",sep="")
  }
  #stop()
  if(is.element(t, t_for_table_temp)){
    
    if(match(t, t_for_table_temp) == 1){
      tab_all_temp<-tab_temp
    }else if(match(t, t_for_table_temp)<length(t_for_table_temp)){
      tab_all_temp<-rbind(tab_all_temp, tab_temp[1:length(tab_temp[,1]),])
    }

    if(match(t, t_for_table_temp) == length(t_for_table_temp)){
      filename_temp_xtable<-paste(plot_dir_temp, "/",agent,"_tab_",t,".tex", sep="")
      write(paste("Latex table with Wilcoxon test statistics results"), file = filename_temp_xtable, append=FALSE)
      #rownames(tab_temp)<-c(tab_temp[,1])
      #colnames(tab_temp)<-c(tab_temp[1,])
      #par_tab_temp0<-tab_temp[2:length(tab_temp[,1]),c(2:length(tab_temp[1,]))]
      caption_temp<-paste("Results of two-sided Wilcoxon test at different phases of diffusion and different subsets of data.")
      par_tab_temp0<-tab_all_temp
      xtab_temp<-xtable(par_tab_temp0, include.colnames = FALSE, include.rownames=FALSE, caption=caption_temp)
      #write(paste("ESCdocumentclass{standalone} \n ESCbegin{document}"), file = filename_temp_xtable, append=FALSE)
      #write(paste("Table with Wilcoxon test results compare eco vs conv per parameter pair \n\n"), file = filename_temp_xtable, append=TRUE)
      write(print(xtab_temp, include.rownames=FALSE, inlude.colnames=FALSE), file = filename_temp_xtable, append=TRUE)
      #write(paste("ESCend{document}"), file = filename_temp_xtable, append=TRUE)
    }
  }
  
}  # t in t temp

rm(temp_data_full, data_temp, p, t, p2, temp1_temp, temp2_temp, wil, t_temp)
rm(list=ls(pattern="parameter_tmp"))
rm(list=ls(pattern="time_temp"))

on.exit(if(is.null(dev.list()) == F){ dev.off()}, add=T)