plot_dir_temp0<-paste(plot_dir_time_series)
if(dir.exists(plot_dir_temp0)!=1){
  dir.create(plot_dir_temp0)
}
source(paste(script_dir,"/aux_all_agents/plot_settings/plot_settings.r",sep=""))

fix_vars_temp0<-c("Type", "id", "Periods", "Firm_Type", "Adopter_Type", "active",
                  "Parameter", "Run", "min_learning_coefficient_var", 
                  "learning_spillover_strength_var", "Number_Runs", "Spill", "Ease", "Learn_Type")

#if(exists("number_cores")){
# registerDoParallel(cores=number_cores)
#  print("Number of cores: ", number_cores)
#}else{
#  registerDoParallel(cores=1)
#  print("No number of cores is set. Default: 1")
#}


#available_aggregation_levels<-c("single_large", "batch", "eco_conv_batch", "single_switch", "eco_conv_switch", "batch_Learn_Type", "batch_Spill", "batch_Ease")
available_aggregation_levels<-c("single_large", "batch", "eco_conv_batch", "single_switch", "eco_conv_switch")

if(exists("aggregation_levels")!=TRUE){
  aggregation_levels<-available_aggregation_levels
}
if(exists("aggregation_levels_for_ts")){
  aggregation_levels<-aggregation_levels_for_ts
  if(setequal(aggregation_levels, aggregation_levels_for_ts) != TRUE){
    aggr_levels0<-aggregation_levels
  }
}
if(agent == "Firm" && (is.element("single_lare", aggregation_levels) ||  is.element("single_switch", aggregation_levels))){
  aggr_levels0<-aggregation_levels
  remove<-match(c("single_large", "single_switch"), aggregation_levels)
  remove<-remove[which(is.na(remove) != TRUE)]
  aggregation_levels<-aggregation_levels[-remove]
  rm(remove)
}


for(aggr in aggregation_levels){
  
if(file.exists(paste(data_directory,"data_",aggr,"_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))){
  load(file=paste(data_directory,"data_",aggr,"_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  print(paste("TS for ", aggr))
  eval(call("<-",(paste("temp_mat",sep="")),object))
  if(agent=="Firm"){
    temp_mat<-temp_mat[which(as.numeric(temp_mat$output ) > 0),]
  }

  source(paste(script_dir, "/aux_all_agents/plot_settings/plot_settings.r", sep=""))
  
    if(dir.exists(paste(plot_dir_temp0,"/",aggr,sep=""))!=1){
      dir.create(paste(plot_dir_temp0,"/",aggr,sep=""))
    }
    plot_dir_temp<-paste(plot_dir_temp0,"/",aggr,sep="")
    data<-temp_mat
    rm(temp_mat)
    
    variables<-setdiff(colnames(data),fix_vars_temp0)


  cols<-match(variables, colnames(data))
  fix_vars<-match(colnames(data)[which(is.element(colnames(data), variables) ==FALSE)], colnames(data))
  if(exists(paste("cut_transition_phase_",agent, sep=""))){
    eval(call("<-",(paste("cut_transition_phase",sep="")),as.name(paste("cut_transition_phase_",agent,sep=""))))
    cut_cols<-match(cut_transition_phase, colnames(data))
    cut_cols<-cut_cols[is.na(cut_cols) != TRUE]
  }
  cols<-cols[is.na(cols) != TRUE]
  
  

  #data<-data[which(is.na(data[cols]) == FALSE),]
  #if(aggr == "single_large"){ # c==1 to make batch runs comparable to disaggr. view! 
  #  min_vec<-vector(mode = "numeric",length = length(variables))
  #  max_vec<-vector(mode = "numeric",length = length(variables))
  #  for(var in 1:length(cols)){
  #    min_vec[var]<-min(as.numeric(data[,cols[var]]), na.rm=TRUE)
  #    max_vec[var]<-max(as.numeric(data[,cols[var]]),na.rm=TRUE)
  #  }
  #}else if(is.element("single_large",aggregation_levels)!=TRUE || aggr == "batch"){
  #  for(var in 1:length(cols)){
  #    min_vec[var]<-min(as.numeric(data[,cols[var]]), na.rm=TRUE)
  #    max_vec[var]<-max(as.numeric(data[,cols[var]]),na.rm=TRUE)
  #  }
  #}
  index<-1

  #foreach(var = cols) %dopar% {
  for(var in cols){
    source(paste(script_dir,"/all_agents/plot_functions/simple_ts.r", sep=""))
  } # var in cols
  
}else{
  print(paste("Aggregation level", aggr, "not (yet?) created", sep= " "))
  aggregation_levels<-aggregation_levels[which(aggregation_levels != aggr)]
}

} # aggr in aggregation levels
rm(list=ls(pattern="temp"))

if(agent == "Firm" && (is.element("single_lare", aggregation_levels) ||  is.element("single_switch", aggregation_levels))){
  aggregation_levels<-aggr_levels0
  rm(aggr_levels0)
}

on.exit(if(is.null(dev.list()) == F){ dev.off()}, add=T)
