# Script is called within an agent-loop. 
# input: agent
if(exists("variables")){
  rm(variables)
}
if(exists("variables_selection")){
  rm(variables_selection)
}

#Find out which data is available. 
#if(exists(paste("list_of_var_", agent, sep="")) != TRUE){
if(exists("USE_SUBSET_OF_DATA") && USE_SUBSET_OF_DATA == 1 ){
  variable_table<-read.delim(paste(experiment_directory,"variables_reduced.txt",sep=""), header=FALSE, sep="\t", skip=1)
} else{
  variable_table<-read.delim(paste(experiment_directory,"variables.txt",sep=""), header=FALSE, sep="\t", skip=1)
}
attach(variable_table)
variable_table_agent<-variable_table[which(variable_table[,3] == agent),]
detach(variable_table)
list_of_var<-as.vector(variable_table_agent[,1])
# Ensure that "id" and "active" are at beginning of variable vector (for firm agent!)
list_of_var<-list_of_var[which(list_of_var != "id")]
list_of_var<-list_of_var[which(list_of_var != "active")]
if(no_agents > 1 && is.na(match("id",list_of_var))!=FALSE){
  list_of_var<-c("id", list_of_var)
}
if(agent == "Firm" && is.na(match("active",list_of_var))!=FALSE){
  list_of_var<-c("active", list_of_var)
}
list_of_var_complete<-list_of_var
eval(call("<-",(paste("list_of_var_",agent,sep="")),list_of_var))
rm(variable_table, variable_table_agent)
#}


if(exists("take_logs")!=TRUE || take_logs==1){
  kind_of_data<-"logs"
}else{
  kind_of_data<-"levels"
}

if(exists("p") == FALSE){
  p<-1
}
if(exists(paste("variables_available_in_panel_", agent, sep="")) != TRUE && file.exists(file=paste(experiment_directory,"/rdata/",kind_of_data,"/levels/","data_single_large_",experiment_name,"_",parameters_tmp[p],"_",agent,".RData",sep=""))){
  if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/levels/","data_batch_",experiment_name,"_",parameters_tmp[1],"_",agent,".RData",sep=""))){
    load(file=paste(experiment_directory,"/rdata/",kind_of_data,"/levels/","data_batch_",experiment_name,"_",parameters_tmp[1],"_",agent,".RData",sep=""))
  }else{
    load(file=paste(experiment_directory,"/rdata/",kind_of_data,"/levels/","data_single_large_",experiment_name,"_",parameters_tmp[1],"_",agent,".RData",sep=""))
  }
  var_temp<-colnames(object)[which(colnames(object) != "Periods")]
  var_temp<-var_temp[which(var_temp!="id")]
  var_temp<-var_temp[which(var_temp!="Run")]
  
  eval(call("<-",(paste("variables_available_in_panel_",agent,sep="")),var_temp))
  
}else if(file.exists(file=paste(experiment_directory,"/rdata/",kind_of_data,"/levels/","data_single_large_",experiment_name,"_",parameters_tmp[1],"_",agent,".RData",sep="")) != TRUE){
  print(paste("Required data or varaible settings do not exist! Run get-data routine! For: ", agent, parameters_tmp[1],kind_of_data))
}

  
if(agent == "Eurostat"){
  # Take logs? 
  take_logs<-1
  # Which var to log? 
  var_log<-c("average_s_skill_1_conv", "average_s_skill_1_eco","average_s_skill_2_conv",
             "average_s_skill_2_eco", "average_s_skill_conv", "average_s_skill_eco", 
             "average_wage_skill_1", "average_wage_skill_2", "avg_firm_output",  "avg_output_conv_firms", 
             "avg_output_eco_firms", "avg_output_firms_conv_invest", "avg_output_firms_eco_invest", 
             "firm_average_productivity_conv", "firm_average_productivity_eco", "gdp", "monthly_output", 
             "monthly_sold_quantity", "monthly_revenue", "output_prod_with_conv_cap", 
             "output_prod_with_eco_cap",  "proxy_total_costs_eco_techn", 
             "proxy_total_costs_conv_techn", "proxy_using_costs_eco_techn","proxy_using_costs_eco_techn", 
             "techn_frontier_conv", "techn_frontier_eco", "techn_frontier", "total_consumption_budget", 
             "total_debt", "total_earnings", "total_output_conv_firms", "total_output_eco_firms", 
             "total_output_conv_investors", "total_output_eco_investors")
  # Optionally: if not all variables, consider only selection of vars: 
  if(use_test_sample == 1){
    variables_selection<-c("unemployment_rate", "monthly_output", "share_conv_capital_used")
  }
  if(exists("list_of_var_Eurostat")){
    list_of_var<-list_of_var_Eurostat
  }
  if(exists("variables_available_in_panel_Eurostat")){
    variables<-variables_available_in_panel_Eurostat
  }
  # variables_selection<-c( .... )
}else if(agent == "Firm"){
  take_logs<-1
  var_log<-c("output", "unit_costs", "technology_conv", "technology_eco", "mean_wage", "price", 
             "eco_costs", "mean_spec_skills_eco", "mean_spec_skills_conv")
  # Optionally: if not all variables, consider only selection of vars: 
  if(use_test_sample == 1){
    variables_selection<-c("unit_costs", "price", "output")
  }
  # variables_selection<-c( .... )
  if(exists("list_of_var_Firm")){
    list_of_var<-list_of_var_Firm
  }
  if(exists("variables_available_in_panel_Firm")){
    variables<-variables_available_in_panel_Firm
  }
  
}else if(agent == "Government"){
  take_logs<-1
  var_log<-c("monthly_budget_balance", "total_debt", "eco_policy_budget", 
             "monthly_income", "monthly_expenditure")
  var_log<-c(var_log, "eco_policy_budget", "monthly_eco_subsidy_payment", 
             "yearly_eco_subsidy_payment", "monthly_eco_tax_revenue", 
             "yearly_eco_tax_revenue", "cumulated_deficit")
  # Optionally: if not all variables, consider only selection of vars: 
  if(use_test_sample == 1){
    variables_selection<-c("eco_policy_budget")
  }
  # variables_selection<-c( .... )
  if(exists("list_of_var_Government")){
    list_of_var<-list_of_var_Government
  }
  if(exists("variables_available_in_panel_Government")){
    variables<-variables_available_in_panel_Government
  }
}

if(exists("list_of_var") && agent=="Firm" &&  is.element(FALSE,is.element(c("id","active"),list_of_var)==TRUE)){
  list_of_var<-list_of_var[which(list_of_var!="id")]
  list_of_var<-list_of_var[which(list_of_var!="active")]
  list_of_var<-c("active","id", list_of_var)
}

if(exists("variables_selection") && agent=="Firm" && is.element(FALSE,is.element(c("id","active"),variables_selection)==TRUE)){
  variables_selection<-variables_selection[which(variables_selection!="id")]
  variables_selection<-variables_selection[which(variables_selection!="active")]
  variables_selection<-c("active","id", variables_selection)
  variables<-variables_selection
}else if(exists("variables_selection")==FALSE){
  if(exists("variables")){
    variables_selection<-variables
  }else{
    variables<-list_of_var
    variables_selection<-variables
  }
  if(agent=="Firm" && is.element(FALSE,is.element(c("id","active"),variables_selection)==TRUE)){
    variables_selection<-variables_selection[which(variables_selection!="id")]
    variables_selection<-variables_selection[which(variables_selection!="active")]
    variables_selection<-c("active","id", variables_selection)
  }
}


if(exists("variables_selection")){
  variables_selection<-unique(variables_selection)
  if(is.element(NA, match(var_log, variables_selection)) == TRUE){
    var_log<-var_log[is.na(match(var_log, variables_selection)) == FALSE]
  }
}
if(exists("data_directory") != TRUE){
  data_directory<-paste(experiment_directory,"/rdata/logs/levels/",sep="")
  parameter<-parameters_tmp[1]
}
if(file.exists(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))){
  
  load(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  
  if(exists(paste("variables_available_in_panel_",agent,sep=""))==FALSE){
    variables<-colnames(object)[which(colnames(object) != "Periods")]
    variables<-variables[which(variables != "Run")]
    variables<-variables[which(variables != "Type")]
    variables<-variables[which(variables != "Firm_Type")]
    variables<-variables[which(variables != "Number_Runs")]
    variables<-variables[which(variables != "id")]
    eval(call("<-",(paste("variables_available_in_panel_",agent,sep="")),as.name(paste("variables"))))
  }
  variables<-variables[which(variables != "Type")]
  if(exists("variables_selection") == FALSE || is.element(NA, match(variables_selection, colnames(object)))){
    variables_selection<-variables
  }
  rm(object)
}

