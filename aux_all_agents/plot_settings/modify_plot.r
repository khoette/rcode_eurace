## Modify plot
## 
## 
if(draw_line != 0){
  plot<-plot + geom_hline(yintercept = draw_line, linetype = "dashed", color = "gray30")
}

if(exists("legend_pos_temp")){
  plot <- plot+ theme(legend.position=legend_pos_temp)
}else{
  plot <- plot+ theme(legend.position=c(0.1, 0.9))
}

if(WHITE_BACKGROUND){
  #plot<- plot + theme_bw() + theme(panel.border = element_blank(), panel.grid.major = element_blank(),
  #                                       panel.grid.minor = element_blank(), axis.line = element_line(colour = "black"))
  plot<- plot + theme_bw() + theme(panel.border = element_blank(), axis.line = element_line(colour = "black"))
}

if(BLACK_WHITE){
  if(is.element(aggr, c("single_large", "single_switch")) == FALSE){
    if(is.element("Type", colnames(temp))){
      plot <- plot + geom_point(aes(shape = Type))
    }else if(is.element("Spill", colnames(temp))){
      plot <- plot + geom_point(aes(shape = Spill))
    }else if(is.element("Ease", colnames(temp))){
      plot <- plot + geom_point(aes(shape = Ease))
    }else if(is.element("Learn_Type", colnames(temp))){
      plot <- plot + geom_point(aes(shape = Learn_Type))
    }
  }else{
    if(is.element("Type", colnames(temp)) && agent!= "Firm"){
      plot <- plot + geom_point(aes(shape = Type, group=interaction(Type,Run)), data =data_mod_temp)
      print("enter")
    }else if(is.element("Spill", colnames(temp))){
      plot <- plot + geom_point(aes(shape = Spill, group=interaction(Spill,Run)))
    }else if(is.element("Ease", colnames(temp))){
      plot <- plot + geom_point(aes(shape = Ease, group=interaction(Ease,Run)))
    }else if(is.element("Learn_Type", colnames(temp))){
      plot <- plot + geom_point(aes(shape = Learn_Type, group=interaction(Learn_Type,Run)))
    }
  }
}else{
  plot <- plot + scale_linetype_manual(values=c(1,1,1,1,1,1,1,1,1,1,1,1,1))
    
}

#plot<-plot+ scale_linetype_manual(name='Regime', values=c('eco'='dashed', 'conv'='dotted', 'switch'='lines'))
