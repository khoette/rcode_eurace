# Set variables, directories etc. used by wilcox_parameter.r
# 
if(exists("wilcox_average")==FALSE){
  wilcox_average<-1
}
# 
if(exists("plot_dir_parameter") != TRUE){
  if(exists("plot_dir_selection")!=TRUE){
    source(paste(script_dir, "/aux_all_agents/create_directories_for_plotting.r", sep=""))
  }
  plot_dir_parameter<-paste(plot_dir_selection,"Parameter/", sep="")
  if(dir.exists(plot_dir_parameter)!=1){
    dir.create(plot_dir_parameter)
  }
}
plot_dir_temp<-paste(plot_dir_parameter,"/Wilcox_parameter", sep="")
if(dir.exists(plot_dir_temp)== FALSE){
  dir.create(plot_dir_temp)
}

time_temp_before<-c(1:600)
time_temp_initial<-c(601:3000)
time_temp_medium<-c(3001:5400)
time_temp_end<-c(5401:15000)
time_temp_early_policy<-c(1000:6000)
time_temp_late_policy<-c(5000:11000)
time_temp_medium_policy<-c(3001:12000)
time_temp_all<-c(1:15000)
time_temp_init<-600
time_temp_last_observation<-15000
time_temp_before_policy_ends<-10000
if(exists("minisnapshot") && minisnapshot==1){
  t_temp<-c("init", "before_policy_ends", "last_observation")
  print(paste("Only minisnapshot wilcox"))
}else{
  t_temp<-c("init", "before_policy_ends", "last_observation","before", "initial", "medium", "end", "all")
  if(experiment_name=="eco_policy_switch"){
    t_temp<-c("early_policy", "late_policy", "medium_policy", t_temp)
  }
}


if(agent == "Eurostat"){
  agent_temp<-"Eurostat"
  variables_wilcox_temp<-c("share_conv_capital_used", "variance_share", "sd_share", 
                           "monthly_output", "no_active_firms", "eco_price_wage_ratio", "price_eco", 
                           "unemployment_rate", "ecoefficiency", "frontier_productivity_ratio", "s_skill_ratio")
  if(exists("with_rand_policy") && with_rand_policy == 1){
    variables_wilcox_temp<-c(variables_wilcox_temp, "eco_tax_rate", "eco_investment_subsidy", 
                             "eco_consumption_subsidy")
    time_temp_init<-640
  }
  if(exists("with_rand_barr") && with_rand_barr == 1){
    variables_wilcox_temp<-c(variables_wilcox_temp, "frontier_gap", "specific_skill_gap")
  }
  if(exists("with_rand_learn") && with_rand_learn == 1){
    variables_wilcox_temp<-c(variables_wilcox_temp, "min_learning_coefficient_var", "learning_spillover_strength_var")
  }
  
}else if(agent == "Firm"){
  agent_temp<-"Firm"
  variables_wilcox_temp<-c("share_conventional_vintages_used", "variance_share", "sd_share", 
                           "no_employees", "output", "revenue", "age", 
                           "unit_costs", "actual_mark_up", "effective_investments", 
                           "no_employees")
}else if(agent == "Government"){
  agent_temp <- "Government"
  variables_wilcox_temp<-c("base_tax_rate", "monthly_budget_balance_gdp_fraction", 
                           "monthly_budget_balance")
}



fix_vars_temp<-c("Parameter", "Periods", "Run", "id", "Type")
vars_temp<-c(fix_vars_temp, variables_wilcox_temp)

