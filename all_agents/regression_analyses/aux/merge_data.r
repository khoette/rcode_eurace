# Load data on "critical technology variables", i.e. data on share conv cap, 
# Period, relative knowledge stocks at the snapshot in time when the last sign 
# flip of the diffusion curve occurred. This can be used as dependent variable 
# in the regression analyses to address questions about the relationship between 
# the "stability of the diffusion process" and spillover parameters, policies, 
# etc.
#stop("Check whether everything worked out...")
if(file.exists(paste(experiment_directory,"/",agent,"data_on_last_sign_flip_in_diffusion_",parameter,".RData", sep="")) && 
   (agent == "Firm" && exists("data_firm")) || (agent == "Eurostat" && exists("data_euro") )){
  load(paste(experiment_directory,"/",agent,"data_on_last_sign_flip_in_diffusion_",parameter,".RData", sep=""))
  
  if(agent == "Firm"){
    additional_vars_temp<-c("Periods", "productivity_ratio", "technology_ratio", "skill_ratio", 
                          "share_conventional_vintages_used")
    colnames_temp<-c("Time", "crit_prod_ratio", "crit_techn_ratio", "crit_skill_ratio", 
                     "crit_share")
  }else if(agent == "Eurostat"){
    additional_vars_temp<-c("Periods", "frontier_productivity_ratio", "s_skill_ratio", 
                            "share_conv_capital_used")
    colnames_temp<-c("Time", "crit_frontier_ratio", "crit_skill_ratio", "crit_share")
  }
  additional_vars_temp<-additional_vars_temp[which(!is.na(match(additional_vars_temp, colnames(data_last_flip))))]

  new_data<-data_last_flip
  
  new_data$Periods<-as.numeric(as.character(new_data$Periods))
  rm(data_last_flip)
  if(agent=="Eurostat"){
    new_data<-new_data[(order(new_data$Run)),]
    old_data<-data_euro
    fix_temp<-"Run"
    name_fix_temp<-"Run_test"
  }else if(agent=="Firm"){
    new_data<-new_data[(order(new_data$Run, new_data$id)),]
    old_data<-data_firm
    fix_temp<-c("Run", "id")
    name_fix_temp<-c("Run_test", "id_test")
  }
  
  crit_levels_temp<-new_data[,match(c(additional_vars_temp,fix_temp), colnames(new_data))]
  colnames(crit_levels_temp)<-c(colnames_temp,name_fix_temp)
  rm(new_data)
  dummy_temp<-matrix(NA, nrow = nrow(old_data), ncol = ncol(crit_levels_temp))
  colnames(dummy_temp)<-c(colnames_temp,name_fix_temp)
  merge_temp<-cbind(old_data, dummy_temp)
  rm(dummy_temp)
  inds_temp<-which(as.numeric(as.character(merge_temp$Periods)) == 20*number_xml)  
  if(agent=="Firm"){ # remove inactive firms from crit_levels_temp to make dimensionality 
    # compatible with data_firm
    ids_temp<-round(as.numeric(as.character(old_data$id[inds_temp]))) + 0.001*round(as.numeric(as.character(old_data$Run[inds_temp])))
    ids_new_temp<-round(as.numeric(as.character(crit_levels_temp$id_test)))+ 0.001*round(as.numeric(as.character(crit_levels_temp$Run_test)))
    crit_levels_temp$id_test<-round(as.numeric(as.character(crit_levels_temp$id_test)))
    inds_new_temp<-match(ids_temp, ids_new_temp)
    data_new_temp<-crit_levels_temp[inds_new_temp,]
  }else if(agent=="Eurostat"){
    data_new_temp<-crit_levels_temp
    merge_temp$Run<-round(as.numeric(as.character(merge_temp$Run)))
  }
  rm(old_data)
  merge_temp[inds_temp,match(c(colnames_temp,name_fix_temp), colnames(merge_temp))]<-data_new_temp
  
  cols_compare_temp<-match(c(fix_temp, name_fix_temp), colnames(merge_temp))
  for(c in cols_compare_temp){
    merge_temp[,c]<-round(as.numeric(as.character(merge_temp[,c])))
  }
  inds_temp<-which(merge_temp$Periods == 20*750)
  test_temp<-merge_temp[inds_temp,match(c(fix_temp), colnames(merge_temp))]- merge_temp[inds_temp,match(c(name_fix_temp), colnames(merge_temp))]
  if(sum(abs(test_temp)) != 0){
    stop("Before continuing, make sure that run-ids are matched properly.")
  }
  cols_rm_temp<-match(name_fix_temp, colnames(merge_temp))
  if(agent=="Firm"){
    data_firm<-merge_temp[,-cols_rm_temp]
  }else{
    data_euro<-merge_temp[,-cols_rm_temp]
  }
  }else{
    print("Data on last sign flip does not exist. To create it run transform_data_for_regr_to_predict_threshold.r script")
  }

rm(list=ls(pattern="temp"))

