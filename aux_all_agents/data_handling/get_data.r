#Find out which data is available.
if(exists("USE_SUBSET_OF_DATA") && USE_SUBSET_OF_DATA == 1 ){
  variable_table<-read.delim(paste(experiment_directory,"variables_reduced.txt",sep=""), header=FALSE, sep="\t", skip=1)
} else{
  variable_table<-read.delim(paste(experiment_directory,"variables.txt",sep=""), header=FALSE, sep="\t", skip=1)
}
attach(variable_table)
variable_table_agent<-variable_table[which(variable_table[,3] == agent),]
detach(variable_table)
list_of_var_complete<-as.vector(variable_table_agent[,1])
# Ensure that "id" and "active" are at beginning of variable vector (for firm agent!)
list_of_var_complete<-list_of_var_complete[which(list_of_var_complete != "id")]
list_of_var_complete<-list_of_var_complete[which(list_of_var_complete != "active")]
if(no_agents > 1 && is.element(NA,(match(c("id", "active"),list_of_var_complete)))!=FALSE){
  list_of_var_complete<-c("active", "id", list_of_var_complete)
  print(paste("Check: active and id added ", agent))
  print(paste(cat(list_of_var_complete)))
}

list_of_var<-list_of_var_complete
eval(call("<-",(paste("list_of_var_",agent,sep="")),list_of_var))

rm(variable_table, variable_table_agent)

#number_xml<-750
if(exists("threshold_eco_scen") != TRUE){
  threshold_eco_scen<-0.5
}
 
for(i in 1:length(list_of_var)){
  variable<-list_of_var[i]
  for(r in 1:runs){
    #con<-dbConnect(dbDriver("SQLite"),paste("/home/kerstin/Schreibtisch/Plots/its_eco_policy_switch/spill/its/eco_policy_switch/0/run_",r,"/iters.db", sep=""))
    con<-dbConnect(dbDriver("SQLite"),paste(experiment_directory,"its/",experiment_name,"/",parameter,"/run_",r,"/iters.db", sep=""))
    
    # Data in the transition phase is ignored. 
    if(delete_transition == 1)    {
      # Get data for "variable" from table "agent".
      X<-dbGetQuery(con, paste(eval(paste("SELECT ",variable," from ",agent, 
                                          " where _ITERATION_NO+0.0>",transition_phase,sep="")))) 
      if(i==1){ # Select additionally indicator variable: Eco-runs defined as those where at the end of 
        # time more eco than conv capital is used.
        eco_share<-dbGetQuery(con, paste(eval(
          paste("SELECT share_conv_capital_used from Eurostat where _ITERATION_NO+0.0>",transition_phase,sep=""))))	
        eco_share<-as.vector(as.numeric(eco_share[,1]))
      }
    }else # Data in transition phase is not ignored.    
    {
      if(i==1){
        eco_share<-dbGetQuery(con, paste(eval(paste("SELECT share_conv_capital_used from Eurostat",sep=""))))	
        eco_share<-as.vector(as.numeric(eco_share[,1]))
      }
      # Get data for "variable" from table "agent".
      X<-dbGetQuery(con, paste(eval(paste("SELECT ",variable," from ",agent,sep=""))))	
    }    
    #X<-dbGetQuery(con, "SELECT output FROM Firm")
    Y<-as.vector(as.numeric(X[,1])) 
    rm(X)
    #number_xml<-length(Y)/no_agents
    Z<-matrix(Y,no_agents,number_xml)
    rm(Y)
    
    if(i==1){
      if(r==1){
        data_all<-array(0, dim=c(no_agents,number_xml,runs,length(list_of_var)))
        # construct additionally a matrix with indication whether techn. regime shifted
        data_eco_id<-array(0, dim=c(number_xml, runs))
        #data_all_eco_id<-array(0, dim=c(no_agents+1,number_xml,runs,length(list_of_var)+1))
      }
    }
    data_all[,,r,i]<-Z
    if(i==1){
      data_eco_id[,r]<-t(eco_share)
      #data_all_eco_id[no_agents+1,,r,length(list_of_var)+1]<-t(eco_share)
    }
    #data_all_eco_id[1:no_agents,,r,i]<-Z
  } # end r in runs
  
  rm(Z,r)
} #end i in list_of_variables



# For eurostat agent: 
# compute skill, frontier, productivity gaps manually. 
if(agent=="Eurostat"){
  for(i in 1:5)
    if(i==1){
      conv<-match("average_s_skill_conv", list_of_var)
      eco<-match("average_s_skill_eco", list_of_var)
      replace<-match("specific_skill_gap", list_of_var)
    }else if(i==2){
      conv<-match("techn_frontier_conv", list_of_var)
      eco<-match("techn_frontier_conv", list_of_var)
      replace<-match("frontier_gap", list_of_var)
    }else if(i==3){
      conv<-match("firm_average_productivity_conv", list_of_var)
      eco<-match("firm_average_productivity_eco", list_of_var)
      replace<-match("avg_productivity_gap", list_of_var)
    }else if(i==4){
    conv<-match("price_highest_vintage_conv", list_of_var)
    eco<-match("price_highest_vintage_eco", list_of_var)
    replace<-match("price_diff_highest_vintage", list_of_var)
    }else if(i == 5){
    conv<-match("proxy_using_costs_conv_techn", list_of_var)
    eco<-match("proxy_using_costs_conv_techn", list_of_var)
    replace<-match("using_cost_difference_eco_conv_capital", list_of_var)
    }
  if(is.na(replace > 0)){ ## Add empty array as placeholder for new variable if replace not element of list of var. 
      test<-array(0,dim=c(length(data_all[,1,1,1]), length(data_all[1,,1,1]), 
                          length(data_all[1,1,,1]), (length(data_all[1,1,1,])+1)))
      test[,,,1:length(data_all[1,1,1,])]<-data_all
      replace<-(length(data_all[1,1,1,])+1)
      print("BE CAREFUL: NOW  YOU SHOULD NOT USE variables.txt TO OBTAIN INFO ON AVAILABLE VARIABLES!!")
      list_of_var<-c(list_of_var, i)
  }else{
    test<-data_all
  }
  
  #if(i==3){
  #  test[,,,replace]<-(data_all[,,,conv]-data_all[,,,eco])/(data_all[,,,conv]+data_all[,,,eco])
  #}else{
    test[,,,replace]<-(data_all[,,,conv]-data_all[,,,eco])/data_all[,,,conv]
  #}
  data_all<-test
  
  #if(is.na(replace > 0)){
  #  test<-array(0,dim=c(length(data_all_eco_id[,1,1,1]), length(data_all_eco_id[1,,1,1]), 
  #                      length(data_all_eco_id[1,1,,1]), (length(data_all_eco_id[1,1,1,])+1)))
  #  test[,,,1:length(data_all_eco_id[1,1,1,])]<-data_all_eco_id
  #  replace<-(length(data_all_eco_id[1,1,1,])+1)
  #}else{
  #  test<-data_all_eco_id
  #}
  #test[,,,replace]<-(data_all_eco_id[,,,conv]-data_all_eco_id[,,,eco])/data_all_eco_id[,,,conv]
  #data_all_eco_id<-test
  rm(test, eco, conv, replace)
}


dbDisconnect(con)



# Remove inactive firms as a default. 
if(agent=="Firm"){
  #data_batch_incl_inactive_firm<-data_batch
  #data_mean_incl_inactive_firm<-data_batch
  filter_variable<-"active"
  source(paste(script_dir,"/aux_all_agents/data_handling/filter_data_all_eco.r", sep=""))
  #data_batch<-apply(data_all, c(1,2,4), mean, na.rm=TRUE)
  #data_mean<-apply(data_all, c(2,3,4), mean, na.rm = TRUE)
}


eval(call("<-",(paste("data_all_",experiment_name,"_",parameter,"_",agent,sep="")),data_all))
eval(call("<-",(paste("eco_id_matrix_",experiment_name,"_",parameter,"_",agent,sep="")),data_eco_id))

if(dir.exists(paste(experiment_directory,"/rdata/",sep=""))!=1){
  dir.create(paste(experiment_directory,"/rdata/",sep=""))
}
if(dir.exists(paste(experiment_directory,"/rdata/raw_data/",sep=""))!=1){
  dir.create(paste(experiment_directory,"/rdata/raw_data/",sep=""))
}
object<-eval(as.name(paste("data_all_",experiment_name,"_",parameter,"_",agent,sep="")))
save(object, file=paste(experiment_directory,"/rdata/raw_data/",paste("data_all_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
object<-eval(as.name(paste("eco_id_matrix_",experiment_name,"_",parameter,"_",agent,sep="")))
save(object, file=paste(experiment_directory,"/rdata/raw_data/",paste("eco_id_matrix_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))

eval(call("<-",(paste("list_of_var_",agent,sep="")),list_of_var))


rm(list=ls(pattern="data_"))
rm(list=ls(pattern="eco_id_"))
rm(object)

