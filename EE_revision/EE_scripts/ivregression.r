
agent<-"Firm"
load(file=paste(experiment_directory,"/reg_data_temp_",agent,".RData",sep=""))

dt<-reg_data_temp

rm(reg_data_temp)

# Make selection about data of interest:
vars<-c("share_conventional_vintages_used", "output","no_employees",
"unit_costs","technology_conv","mean_spec_skills_conv","price",
"technology_gap","mean_specific_skill_gap","eco_tax_rate",
"eco_investment_subsidy","eco_consumption_subsidy","specific_skill_gap","frontier_gap" )
dt_temp<-dt[which(dt$Periods=="600"),match(vars, colnames(dt))]
for(col in c(1:ncol(dt_temp))){
  dt_temp[,col]<-(as.numeric(as.character(dt_temp[,col])))
  dt_temp[which(is.nan(dt_temp[,col])),col]<-NA
  dt_temp[,col]<-scale(dt_temp[,col])
}
attach(dt_temp)

type<-as.factor(as.character(dt$Type[which(dt$Periods=="600")]))

periods<-c("1800","3000","9000","14780")
periods<-periods[which(!is.na(match(periods,unique(dt$Periods))))]
targets<-c("share_conventional_vintages_used", "no_employees", "unit_costs")
targets<-targets[which(!is.na(match(targets, colnames(dt))))]
per<-periods[1]
targ<-targets[1]
#for(targ in targets){
#for(per in periods){
controls<-vars[which(!is.element(vars, c(targ, "eco_consumption_subsidy", "eco_investment_subsidy", "eco_tax_rate", "frontier_gap", "specific_skill_gap")))]

obj<-as.numeric(as.character(dt[which(dt$Periods == per),match(targ, colnames(dt))]))
obj[which(is.nan(obj))]<-NA
names(obj)<-targ

m<-as.formula(eval(paste("obj~type*eco_consumption_subsidy+type*eco_investment_subsidy+type*eco_tax_rate + ", paste(controls,collapse="+") )))
ols<-lm(m)
m_iv<-paste("obj~type*eco_consumption_subsidy+type*eco_investment_subsidy+type*eco_tax_rate + specific_skill_gap + frontier_gap","| frontier_gap + specific_skill_gap +", paste(controls,collapse="+"))
m_iv<-as.formula(eval(m_iv))
iv<-ivreg(m_iv)

  
#} # per in periods
#} # targ in targets
detach(dt_temp)

