for(type_tmp in c("switch","large" )){
  if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/", data_type[d] ,"/",paste("data_single_",type_tmp,"_",experiment_name,"_",parameter,"_", agent,sep=""),".RData",sep="")))
    {
      load(file=paste(experiment_directory,"/rdata/",kind_of_data,"/", data_type[d] ,"/",paste("data_single_",type_tmp,"_",experiment_name,"_",parameter,"_", agent,sep=""),".RData",sep=""))
      if(is.na(match("Learn_Type", colnames(object)))){  ## Run routine only if learn type not yet assigned
        temp_mat<-object
        if(exists("switch_ids_learn") != TRUE){
          if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/", data_type[d] ,"/",paste("data_single_",type_tmp,"_",experiment_name,"_",parameter,"_Eurostat",sep=""),".RData",sep=""))==TRUE){
            load(file=paste(experiment_directory,"/rdata/logs/levels/data_single_",type_tmp,"_",experiment_name,"_",parameter,"_Eurostat.RData",sep=""))
            temp<-object[which(object$Periods == object$Periods[1]),]
            ind_learn<-match(c("learning_spillover_strength_var", "min_learning_coefficient_var"), colnames(temp))
            if(sd(as.numeric(temp[,ind_learn[1]]), na.rm = TRUE) < 1e-5*mean(as.numeric(temp[,ind_learn[1]]), na.rm = TRUE) && sd(as.numeric(temp[,ind_learn[2]]), na.rm = TRUE) < 1e-5*mean(as.numeric(temp[,ind_learn[2]]), na.rm = TRUE)){
              print(paste("Apparently, min learning coeff and spillover were not randomized. Does not make sense to define subsets... "))
            }else{
              if(is.na(match("Learn_Type", colnames(object))) != TRUE){
                switch_ids_learn<-data.frame(cbind(as.character(temp$Run), temp$Learn_Type, temp$Spill, temp$Ease, temp$learning_spillover_strength_var, temp$min_learning_coefficient_var))
                colnames(switch_ids_learn)<-c("Run", "Learn_Type", "Spill", "Ease", "learning_spillover_strength_var", "min_learning_coefficient_var")
              }else{ # Runs not yet classified by Learn type
                load(file=paste(experiment_directory,"/rdata/logs/levels/data_single_large_",experiment_name,"_",parameter,"_Eurostat.RData",sep=""))
                temp<-object[which(object$Periods == object$Periods[1]),]
                Learn_Type<-array(NA, dim=length(temp$Type))
                Spill<-array("high", dim=length(temp$Type))
                Ease<-array("high", dim=length(temp$Type))
                temp<-cbind(Learn_Type, Spill, Ease,temp)
                idx_temp<-match("Learn_Type",colnames(temp))
                for(r in temp$Run){
                  temp2<-temp[which(temp$Run == r),]
                  if(temp2$learning_spillover_strength_var >= 0.5){
                    #temp[which(temp$Run == r),(idx_temp+1)]<-"high"
                    if(temp2$min_learning_coefficient_var >= 0.5){
                      temp[which(temp$Run == r),idx_temp]<-"high-high"
                    }else{
                      temp[which(temp$Run == r),idx_temp]<-"high-low"
                      temp[which(temp$Run == r),(idx_temp+2)]<-"low"
                    }
                  }else{
                    temp[which(temp$Run == r),(idx_temp+1)]<-"low"
                    if(temp2$min_learning_coefficient_var >= 0.5){
                      temp[which(temp$Run == r),idx_temp]<-"low-high"
                    }else{
                      temp[which(temp$Run == r),idx_temp]<-"low-low"
                      temp[which(temp$Run == r),(idx_temp+2)]<-"low"
                    }
                  }
                }# end r in runs
            switch_ids_learn<-data.frame(cbind(as.character(temp$Run), temp$Learn_Type, temp$Spill, temp$Ease, temp$learning_spillover_strength_var, temp$min_learning_coefficient_var))
            colnames(switch_ids_learn)<-c("Run", "Learn_Type", "Spill", "Ease", "learning_spillover_strength_var", "min_learning_coefficient_var")
            }
          }# check whether learning parameters were randomized. 
        }# if file exists
      } # if(exists("switch_ids_learn) != TRUE)

      Learn_Type<-array(NA, dim=length(temp_mat$Run))
      Spill<-array(NA, dim=length(temp_mat$Run))
      Ease<-array(NA, dim=length(temp_mat$Run))
      learning_spillover_strength_var<-array(NA, dim=length(temp_mat$Run))
      min_learning_coefficient_var<-array(NA, dim=length(temp_mat$Run))
  
  
  temp_mat<-cbind(Learn_Type,Spill, Ease, learning_spillover_strength_var, min_learning_coefficient_var, temp_mat)
  idx_temp<-match(c("Learn_Type","Spill", "Ease", "learning_spillover_strength_var", "min_learning_coefficient_var"), colnames(temp_mat))
  idx_temp2<-match(c("Learn_Type","Spill", "Ease", "learning_spillover_strength_var", "min_learning_coefficient_var"), colnames(switch_ids_learn))
  for(r in switch_ids_learn$Run){
    temp_mat[which(temp_mat$Run == r), idx_temp[1]]<-as.character(switch_ids_learn[which(switch_ids_learn$Run == r),idx_temp2[1]])
    temp_mat[which(temp_mat$Run == r), idx_temp[2]]<-as.character(switch_ids_learn[which(switch_ids_learn$Run == r),idx_temp2[2]])
    temp_mat[which(temp_mat$Run == r), idx_temp[3]]<-as.character(switch_ids_learn[which(switch_ids_learn$Run == r),idx_temp2[3]])
    temp_mat[which(temp_mat$Run == r), idx_temp[4]]<-as.character(switch_ids_learn[which(switch_ids_learn$Run == r),idx_temp2[4]])
    temp_mat[which(temp_mat$Run == r), idx_temp[5]]<-as.character(switch_ids_learn[which(switch_ids_learn$Run == r),idx_temp2[5]])

  }
  
  
  eval(call("<-",(paste("data_single_",type_tmp,"_",experiment_name,"_",parameter,"_",agent,sep="")),temp_mat))
  object<-eval(as.name(paste("data_single_",type_tmp,"_",experiment_name,"_",parameter,"_",agent,sep="")))
  save(object, file=paste(experiment_directory,"/rdata/logs/levels/",paste("data_single_",type_tmp,"_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))

  rm(list=ls(pattern="temp"))
  rm(object)
  rm(Learn_Type, learning_spillover_strength_var, min_learning_coefficient_var, Ease, Spill)
  
}else{
  print("Learn Type was already defined")
}
    rm(type_tmp)

  }# if file.exists 
  else{
    print(paste("File does not exist:",paste(experiment_directory,"/rdata/",kind_of_data,"/", data_type[d] ,"/",paste("data_single_",type_tmp,"_",experiment_name,"_",parameter,"_", agent,sep=""),".RData",sep="")))
  }
}

