#for(agent in c("Firm", "Eurostat", "Government")){

names_tmp<-c("eco_conv_batch", "eco_conv_switch")
aggr_tmp<-aggr 
# Get data in data frame for ggplot
print("Get batch data for type subsets")
aggr<-aggr_tmp[1]
for(aggr in aggr_tmp){
  if(aggr == "single_large"){
    type_tmp<-names_tmp[match(aggr, aggr_tmp)]
  }else if(aggr == "single_switch"){
    type_tmp<-"eco_conv_switch"
  }
  
  if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/levels/",paste("data_",type_tmp,"_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))!=TRUE){
  
    load(file=paste(data_directory,"data_",aggr,"_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
    temp_mat_full<-object
    if(agent == "Firm"){
      temp_mat_full<-temp_mat_full[which(temp_mat_full$active ==1),]
    }
    #eval(call("<-",(paste("variables",sep="")),as.name(paste("variables_available_in_panel_",agent,sep=""))))

    fix_vars_temp<-c("Run","id", "Periods", "Type", "Adopter_Type", "Firm_Type")
    cols<-match(fix_vars_temp, colnames(temp_mat_full))
    cols<-match(colnames(temp_mat_full)[-cols[which(!is.na(cols))]], colnames(temp_mat_full))
    cols<-unique(cols[which(is.na(cols) == FALSE)])
    variables<-colnames(temp_mat_full)[cols]
    colnames_temp<-c("Type", "Periods", "Run", colnames(temp_mat_full)[cols])
    if(length(colnames_temp) != length(unique(colnames_temp))){
      stop()
    }
    start<-cols[1]
    periods_temp<-length(unique(temp_mat_full$Periods))
    steps<-(number_xml/periods_temp)*20
    it_vector<-seq(steps,steps*periods_temp,steps)
  
    if(type_tmp != "batch"){
      idx_temp<-match("Type", colnames(temp_mat_full))
    }else{
      idx_temp<-1
    }
    if(is.na(idx_temp) != TRUE){
      if(type_tmp != "batch"){
        types_temp<-unique(temp_mat_full[,idx_temp])
        tmp<-0
      }else{
        tpyes_temp <- 1
        tmp<-0
      }
      for(type in types_temp){
        if(type_tmp=="batch"){
          temp<-temp_mat_full
        }else{
          temp<-temp_mat_full[which(temp_mat_full[,idx_temp] == type),]
        }
        print(paste("batch type: ",type_tmp, type))
  
        for(i in c(1:length(it_vector))){
          temp3<-temp[which(temp$Periods == it_vector[i] ),]
          #for(i in 1:10){
          it<-i + tmp*periods_temp
          for(v1 in 1:length(cols)){
            v<-cols[v1]
            if(it==1 && tmp == 0 && v==start){
              temp2<-data.frame(matrix(0, length(types_temp)*number_xml,length(colnames_temp)))
              print(colnames_temp)
              colnames(temp2) <- colnames_temp
            }
          if(v==start && i == 1){
            temp2$Type[(1+(tmp)*number_xml):((tmp+1)*number_xml)]<-type
            temp2$Periods[(1+tmp*number_xml):((tmp+1)*number_xml)]<-it_vector
            temp2$Run[(1+tmp*number_xml):((tmp+1)*number_xml)]<-length(unique(temp$Run))
          }
            temp4<-temp3[,v]
            temp4<-temp4[which(!is.infinite(temp4))]
            temp4<-temp4[which(!is.nan(temp4))]
          temp2[it,(v1+3)]<-mean(temp4, na.rm=TRUE)
          
        } # v in variables
        #temp<-temp[which(temp$Periods != it_vector[i] ),]
      }# i in it vector
      tmp<-tmp+1
    }# type in types
    temp_mat <- temp2
    indices_temp<-unique(match(c("Type", "Periods", "Run", colnames_temp ), colnames(temp_mat)))
    temp_mat<-temp_mat[,indices_temp]
  
    rm(temp, temp2, i, type, it_vector, start)


    eval(call("<-",(paste("data_",type_tmp,"_",experiment_name,"_",parameter,"_",agent,sep="")),temp_mat))
    object<-eval(as.name(paste("data_",type_tmp,"_",experiment_name,"_",parameter,"_",agent,sep="")))
    save(object, file=paste(data_directory,"/",paste("data_",type_tmp,"_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
    }else{
      print(paste("Data not classified by ", type_tmp, data_type[d], agent, parameter))
    }
  }else{
    print("Data already exists")
  }
  rm(v, v1, tmp, steps, it, cols)
} # aggr in c(single switch, single large)
#}

rm(list=ls(pattern="temp"))
#rm(list=ls(pattern="tmp"))
