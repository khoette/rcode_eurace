dir_temp<-paste(plot_dir_selection,"/Scatterplots/",sep="")
if(dir.exists(dir_temp)==FALSE){
  dir.create(dir_temp)
}

target_var_temp<-c("Type")
if(agent="Eurostat"){
  target_var_temp<-c(target_var_temp, "share_conv_capital_used", "monthly_output")
}

if(exists("day_market_entry")!= TRUE){
  day_market_entry<-600
}
if(exists("end_period")!=TRUE){
  end_period<-15000
}
periods_tmp<-c(0,day_market_entry,end_period)
print(paste("make scatterplots for ", paste(periods_tmp, collapse = " ")))

iterate<-c()

if(exists("with_rand_barr") && with_rand_barr == 1){
  iterate<-c(iterate,"barr")
}
if(exists("with_rand_policy") && with_rand_policy == 1 ){
  iterate<-c(iterate,"pol")
}
if(exists("with_rand_learn") && with_rand_learn == 1){
  iterate<-c(iterate,"learn")
}
load(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))

for(it in iterate){
  if(it == "barr"){
    rand_vars<-c("specific_skill_gap", "frontier_gap")
    xlabel<-"% frontier gap"
    ylabel<-"% specific skill gap"
  }else if(it == "pol"){
    rand_vars<-c("eco_tax_rate", "eco_investment_subsidy", 
                 "eco_consumption_subsidy")
    xlabel<-"policy_var_1"
    ylabel<-"policy_var_2"
  }else if(it == "learn"){
    rand_vars<-c("min_learning_coefficient_var", "learning_spillover_strength_var")
    xlabel<-"Learning intensity"
    ylabel<-"Knowledge transferability"
  }
  
  target_var_temp<-unique(target_var_temp)
  indcs<-match(c(rand_vars, target_var_temp, "Periods"), colnames(object))
  indcs<-unique(indcs[which(is.na(indcs)==FALSE)])
  target_var_temp0<-target_var_temp[which(is.na(match(target_var_temp, colnames(object)))==FALSE)]
  data_tmp0<-object[,indcs]
  
  #  if(exists("make_plots") && make_plots==1 && is.element(NA, match(rand_vars, colnames(object))) == FALSE)
    for(t in periods_tmp){
      if(t == 0){
        data_tmp1<-data_tmp0
        dir_temp<-paste(dir_temp0,"/Full_time_horizon/",sep="")
        if(dir.exists(dir_temp)==FALSE){
          dir.create(dir_temp)
        }
      }else{
        data_tmp1<-data_tmp0[which(data_tmp0$Periods == t),]
        dir_temp<-paste(dir_temp0,"/",t,"/",sep="")
        if(dir.exists(dir_temp)==FALSE){
          dir.create(dir_temp)
        }
      }
      
      for(i in 1:(length(rand_vars)-1)){
        
        rand_var1<-rand_vars[i]
        if(rand_var1 == "eco_tax_rate"){
          xlabel<-"Eco tax rate"
        }else if(rand_var1 == "eco_investment_subsidy"){
          xlabel<-"Eco investment subsidy"
        }else if(rand_var1 == "eco_consumption_subsidy"){
          xlabel<-"Eco consumption subsidy"
        }
        
        for(i2 in (i+1):length(rand_vars)){
          
          rand_var2<-rand_vars[i2]
          
          if(rand_var2 == "eco_tax_rate"){
            ylabel<-"Eco tax rate"
          }else if(rand_var2 == "eco_investment_subsidy"){
            ylabel<-"Eco investment subsidy"
          }else if(rand_var2 == "eco_consumption_subsidy"){
            ylabel<-"Eco consumption subsidy"
          }
          
          data_tmp<-data_tmp1[match(c(rand_var1, rand_var2, target_var_temp0), colnames(data_tmp1))]
          data_tmp[,1]<-as.numeric(data_tmp[,1])
          data_tmp[,2]<-as.numeric(data_tmp[,2])
          
          point_size<-max(max(2.5, 400/length(data_tmp[,1])),0.33)
          alpha<-0.5
          
          for(j1 in 1:length(target_var_temp0)){
            
            if(target_var_temp0[j1]=="share_conv_capital_used"){
              color<-"Share conventional capital used"
            }else if(target_var_temp0[j1]=="share_conventional_vintages_used"){
              color<-"Share conventional capital used at firm level"
            }else if(target_var_temp0[j1]=="no_active_firms"){
              color<-"# active firms"
            }else if(target_var_temp0[j1]=="monthly_output"){
              color<-"Monthly output"
            }else if(target_var_temp0[j1] == "Type"){
              color<-"Type"
            }else{
              color<-target_var_temp0[j1]
            }
            
            #match(target_var_temp0[j1], colnames(data_tmp))
            
            
            if(var(data_tmp[,1], na.rm = TRUE) > 0.00001 * mean(data_tmp[,1], na.rm = TRUE) && var(data_tmp[,2], na.rm = TRUE) > 0.00001 * mean(data_tmp[,2], na.rm = TRUE)){
              
              pair_title <- paste(rand_vars[c(i,i2)], collapse="_")
              #if(t==0){
              min1<-min(data_tmp[,1], na.rm=TRUE)
              min2<-min(data_tmp[,2], na.rm=TRUE)
              max1<-max(data_tmp[,1], na.rm=TRUE)
              max2<-max(data_tmp[,2], na.rm=TRUE)
              #}
              if(t == 0){
                pdf(paste(dir_temp,agent,"_",parameter,"_",pair_title,"_scatterplot_color_",target_var_temp0[j1],".pdf",sep=""))
              }else{
                pdf(paste(dir_temp,agent,"_",parameter,"_",pair_title,"in_",t,"_scatterplot_color_",target_var_temp0[j1],".pdf",sep=""))
              }
              if(is.na(match(target_var_temp0[j1], colnames(data_tmp))) == FALSE){
                colnames(data_tmp)[match(target_var_temp0[j1], colnames(data_tmp))]<-"value"
              }
              value<-as.name(target_var_temp0[j1])
              
              plot<-ggplot(data_tmp, aes(x=as.numeric(data_tmp[,1]), y=as.numeric(data_tmp[,2])))
              source(paste(script_dir, "/aux_all_agents/plot_settings/modify_plot_scatter.r", sep=""))
              
              #plot<-plot+ guides(colour = guide_legend(override.aes = list(shape = 15, alpha=1)))
              plot<- plot + theme(legend.justification=c(0.95,0.95), legend.position="none", legend.title = element_blank()) 
              plot<-plot +scale_x_continuous(limits=c(min1, max1)) + scale_y_continuous(limits=c(min2,max2)) + xlab(xlabel)+ylab(ylabel)
              #plot<-plot +scale_x_discrete(breaks=seq(min1,max,0.01)) + scale_y_discrete(breaks=seq(min2,max2,0.01)) #+ xlab(names[k])+scale_colour_discrete("")+ylab(names[k1])
              
              print(plot)
              dev.off()
              
              
              # Heat map
              if(is.element("value", colnames(data_tmp))){
                pdf(paste(dir_temp,agent,"_",parameter,"_Heat_map_",pair_title,"_color_",target_var_temp0[j1],".pdf",sep=""))
                
                
                # build your data.frame
                #df <- data.frame(x=x, y=y, weight=weight)
                
                colnames(data_tmp)[match(target_var_temp0[j1], colnames(data_tmp))]<-"value"
                indx<-match("value", colnames(data_tmp))
                if(agent=="Eurostat"){
                  df<-data.frame(x=as.numeric(data_tmp[,1]),y=as.numeric(data_tmp[,2]),weight=as.numeric(data_tmp[,indx]))
                }else if(agent=="Firm"){
                  df<-data.frame(x=as.numeric(data_tmp[,1]),y=as.numeric(data_tmp[,2]),weight=as.numeric(data_tmp[,indx]))
                }
                myPalette <- colorRampPalette(rev(brewer.pal(11, "Spectral")), space="Lab")
                
                
                # Plot
                #p0<-ggplot(data_tmp, aes(x=data_tmp[,1], y=data_tmp[,2]), fill=..level..)
                
                plot<-ggplot(df, aes(x=df[,1],y=df[,2], fill=..level..) ) + 
                  stat_density_2d( bins=50, geom = "polygon") +
                  scale_fill_gradientn(colours = myPalette(5)) +
                  theme_minimal() +
                  #coord_fixed(ratio = 1) + 
                  scale_x_continuous() + scale_y_continuous() + xlab(paste(xlabel)) + ylab(paste(ylabel))
                plot<-plot + labs(fill = paste(color)) + theme(legend.position="top", legend.title = element_text(size=8), axis.title = element_text(size=8))
                

                print(plot)
                dev.off()
                
                colnames(data_tmp)[indx]<-target_var_temp0[j1]
              }
            }
            
            #rm(plot, pl0, myPalette, df, indx)
            
          } # end j1 in target_var_temp0
        } # end i2 in length rand vars
      } # end i in length rand vars
      
      
    } # for t in periods_tmp
#  } # if make plots == 1
}  # end it in iterate (through diff rand var settings)
#rm(t, i, i2, target_var_temp0, alpha, point_size, min1, min2, max1, max2)
#rm( list=ls(pattern="data_tmp"))
on.exit(if(is.null(dev.list()) == F){ dev.off()}, add=T)  
