# After having the data. 
# 
plot_dir_temp0<-paste(plot_dir_multi_ts)



#  Color settings: 
high<-"navyblue"
low<-"orangered"
highhigh<-"navyblue"
highlow<-"skyblue"
lowhigh<-"orange"
lowlow<-"orangered"
eco<-"limegreen"
conv<-"salmon"
switch<-"royalblue"

# interaction: 
# high: dark, low: bright
ecol<-"yellowgreen"
ecoh<-"darkgreen"
convl<-"orange"
convh<-"chocolate4"
switchl<-"skyblue"
switchh<-"darkblue"

# linetype settings: 
ltyhigh<-"dashed"
ltylow<-"dotted"


# Run routine twice. Once for single var, once for batch
#df_data_melt_set<-seq(10,length(temp_mat_single[,1]),3000)
#parameter<-bau_value
#available_aggregation_levels<-c("single_large", "batch", "eco_conv_batch")
if(exists("aggregation_levels_for_ts")){
  aggregation_levels<-aggregation_levels_for_ts
}
cases<-match(aggregation_levels, available_aggregation_levels)
cases<-cases[which(is.na(cases) != TRUE)]
if(is.element("normalized_by_per-run_avg", data_type) && data_type[d] == "normalized_by_per-run_avg"){
  cases<-1
}

#available_aggregation_levels<-c("single_large", "batch", "eco_conv_batch", "single_switch", "eco_conv_switch", "batch_Learn_Type", "batch_Spill", "batch_Ease")





# runs through different data sets: 
for(c in cases){
  
  aggr<-available_aggregation_levels[c]
  
  
  if(file.exists(paste(data_directory,"data_",aggr,"_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))){
    load(file=paste(data_directory,"data_",aggr,"_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
    print(paste("MTS for ", aggr))

  
    source(paste(script_dir, "/aux_all_agents/plot_settings/plot_settings.r", sep=""))
    
  eval(call("<-",(paste("temp_mat_data",sep="")),object))
  rm(object)
  
  data<-temp_mat_data
  rm(temp_mat_data)
  eval(call("<-",(paste("name",sep="")),aggr))
  
  
  
  plot_dir_temp<-paste(plot_dir_multi_ts,"/",name, sep="")
  if(dir.exists(plot_dir_temp)!=1){
    dir.create(plot_dir_temp)
  }
  vars_temp<-unique(as.vector(mts_table))
  vars_temp<-vars_temp[which(is.na(vars_temp) != TRUE)]
  

  fix_vars_temp<-colnames(data)[which(is.na(match(colnames(data), variables)))]
 
  indices_fix_temp<-match(fix_vars_temp, colnames(data))
  v<-match(vars_temp, colnames(data)) 
  v<-v[which(is.na(v)==FALSE)]

  min_vec<-vector(mode = "numeric",length = length(v))
  max_vec<-vector(mode = "numeric",length = length(v))

  #if(c==1){ ## Optionally only activate if c==1 to equate y-axes in case of comparison
    index<-1
    for(var in v){
      min_vec[index]<-min(as.numeric(data[,var]), na.rm=TRUE)
      max_vec[index]<-max(as.numeric(data[,var]),na.rm=TRUE)
      index<-index+1
    }
  #}

    indices_temp<-c(indices_fix_temp, v)
    data0<-data[indices_temp]
    indices_fix_temp<-match(fix_vars_temp, colnames(data0))

    # 
    # http://www.lgbmi.com/2012/03/plot-multi-column-data-with-ggplot-ggplot-is/
    #test_melt0<-melt(data, measure.vars = vars_temp)
  # Multiple plots: 
  # 1. Get settings for multiple time series, i.e. table with variable combinations to plot. 
if(exists(paste("cut_transition_phase_",agent, sep=""))){
    eval(call("<-",(paste("cut_transition_phase",sep="")),as.name(paste("cut_transition_phase_",agent,sep=""))))
    cut_cols<-match(cut_transition_phase, colnames(data))
    cut_cols<-cut_cols[is.na(cut_cols) != TRUE]
}else{
  print("Warning: cut cols not exist, but you wanted to remove transition phase, right? Run reset_some_var script.")
  cut_cols<-0
}

   
for(mts in 1:length(mts_table[,1])){
#foreach(mts = 1:length(mts_table[,1])) %doPar% {
  mts_vars<-mts_table[mts, ]
  mts_vars<-mts_vars[which(is.na(mts_vars) == FALSE)]
  indices_temp<-c(match(c(fix_vars_temp,mts_vars), colnames(data0)))
  
  
  if(length(grep("ratio", as.character(paste(mts_vars, collapse="_"))))>0){ ## Draw a horizontal line at y=1 
    draw_line<-1
  }else{
    draw_line<-0
  }
  
  
  if(is.element(NA, indices_temp)){
    print(paste("Required MTS variable is missing: ", paste(mts_vars, collapse=" ")))
    indices_temp<-indices_temp[which(is.na(indices_temp) == FALSE)]
  }else{
    df_data_melt<-melt(data0[indices_temp], measure.vars = mts_vars)
    df_data_melt<-df_data_melt[which(is.na(df_data_melt$value)==FALSE),]
    if(c== 2 && is.element("Type", colnames(df_data_melt))){
      df_data_melt<-df_data_melt[which(is.na(df_data_melt$Type)==FALSE),]
    }
  ind_temp<-match("value", colnames(df_data_melt))

  if(exists(paste("cut_transition_phase_",agent, sep="")) && is.element(TRUE,is.element(mts_vars, c("price_per_productivity_unit_eco", colnames(data)[cut_cols])))){
    df_data_melt[which(as.vector(as.numeric(as.character(df_data_melt$Periods)))<day_market_entry+20),ind_temp]<-NA
    
    #start<-day_market_entry
    start<-0
    min<-min(as.numeric(df_data_melt[,ind_temp]), na.rm=TRUE)
    max<-max(as.numeric(df_data_melt[,ind_temp]), na.rm=TRUE)
  }else{
    start<-0
  }

  min<-min(df_data_melt$value, na.rm=TRUE)
  max<-max(df_data_melt$value, na.rm=TRUE)
  median<-median(df_data_melt$value, na.rm=TRUE)
  #name<-paste((cat(mts_vars[1:length(mts_vars)],sep="_")))
  #pdf(paste(plot_dir_temp,"/",parameter,"_",name,"_mts.pdf",sep=""))
  if(is.na(median) || is.na(max) || is.na(min) || ((max > median*1e30 || abs(min) > abs(median*1e30))  && abs(median) > 1e-3)){
    paste("Presumably, there is an error in the data: Un realistically high value for ",paste(mts_vars, collapse = " "))
  }else{
  pdf(paste(plot_dir_temp,"/",agent,"_",parameter,"_",paste(mts_vars, collapse = "_"),"_mts.pdf",sep=""))
  p1<-ggplot(df_data_melt, aes(x=reorder(Periods, as.numeric(Periods)), y=value))
  if(is.element(aggr, c("single_large", "single_switch"))){
    if(length(mts_vars)<=3){
      plot<-p1+geom_path(aes(color=Type, linetype=variable, group=interaction(variable,Run)), alpha=alpha, lwd=lwd)+ xlab("Periods") + ylab("")
      plot<-plot + scale_colour_manual(name='Regime', values=c('eco'=eco, 'conv'=conv, 'switch'=switch)) 
      
    }else{
      plot<-p1+geom_path(aes(color=interaction(variable,Type),group=interaction(variable,Run)), alpha=alpha, lwd=lwd)+ xlab("Periods")+scale_colour_discrete("") + ylab("")
    }
  }else if(aggr == "batch"){
    plot<-p1+geom_path(aes(color=variable,group=interaction(variable)))+ xlab("Periods") + ylab("") #+scale_colour_discrete("") 
  }else if(is.element(aggr, c("eco_conv_batch", "eco_conv_switch"))){
    #plot<-p1+geom_path(aes(color=interaction(variable, Type), group=interaction(variable,Type)))+ xlab("Periods")+scale_colour_discrete("") + ylab("")
    #plot<-p1+geom_path(aes(color=Type, linetype = variable, group=interaction(variable,Type)))+ xlab("Periods")+scale_colour_discrete("") + ylab("")
    if(length(mts_vars)<=3){
      plot<-p1+geom_path(aes(color=Type, linetype = variable, group=interaction(variable, Type)))+ xlab("Periods") + ylab("")
      plot<-plot + scale_colour_manual(name='', values=c('eco'=eco, 'conv'=conv, 'switch'=switch)) 
      
    }else{
      plot<-p1+geom_path(aes(color=Type,group=interaction(variable, Type)), alpha=alpha, lwd=lwd)+ xlab("Periods") + ylab("") + scale_colour_discrete("")
    }
  }
  plot<-plot +scale_x_discrete(breaks=seq(start,number_xml*20,5000)) + scale_y_continuous(limits=c(min,max)) + theme(legend.position=c(0.1,0.75)) 
 
  if(draw_line != 0){
    plot<-plot + geom_hline(yintercept = draw_line, linetype = "dashed", color = "gray30")
  }
  if(is.element("price_ratio_frontier", mts_vars)){
    plot <- plot+ theme(legend.position=c(0.195, 0.85))
  }else{
    plot <- plot+ theme(legend.position=c(0.175, 0.85))
  }
  
  print(plot)
  dev.off()
  
  rm(plot, p1, min, max, median)
  }
  }# end of else of check whether MTS variable exists. 
  rm(df_data_melt, start)
  on.exit(if(is.null(dev.list()) == F){ dev.off()}, add=T)
}# end mts
    rm(data, data0)
  }else{ # end if check file.exists(DATA)
    print(paste("Data file does not exist: ", data_directory,"data_",aggr,"_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  }
}# end cases

rm(list=ls(pattern="temp"))

