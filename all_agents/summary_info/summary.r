# Compute some summary descriptives on a set of simulation runs
# 
agent<-"Eurostat" 
parameter<-parameters_tmp[1]
aggr<-"single_large"

data_directory<-paste(experiment_directory,"/rdata/logs/levels/",sep="")
vars_of_interest_temp<-c("Periods", "Type", "share_conv_capital_used", "s_skill_ratio", 
                         "frontier_productivity_ratio", "no_active_firms", "monthly_output")

for(p in parameters_tmp){
  load(file=paste(data_directory,"data_",aggr,"_",experiment_name,"_",p,"_",agent,".RData",sep=""))
  cols_temp<-match(vars_of_interest_temp, colnames(object))
  
  if(p == parameters_tmp[1]){
    data_temp<-object[cols_temp]
    data_temp<-cbind(Parameter = rep(p, length(data_temp[,1])), data_temp)
  }else{
    data_temp<-rbind(data_temp, cbind(Parameter = rep(p, length(object[,1])),object[cols_temp]))
  }
}
rm(object)
p_temp<-unique(data_temp$Parameter)

source(paste(script_dir,"/all_agents/summary_info/Compute_divergence.r", sep=""))
type_temp<-unique(data_temp$Type)



file_name_temp<-paste(plot_dir_selection,"Overview_parameter_comparison_basic_descriptives.txt", sep="")
write(paste("Overview descriptive statistics comparing parameter settings"), file = file_name_temp, append = FALSE)
if(agent == "Eurostat"){
  vars_to_compare_temp<-c("share_conv_capital_used","skill_divergence", "frontier_divergence", 
                          "s_skill_ratio", "frontier_productivity_ratio", 
                   "no_active_firms", "monthly_output")
  if(is.element(NA, match(vars_to_compare_temp, colnames(data_temp)))){
    print("Warning: Some of your selected vars not in data table")
    vars_to_compare_temp<-vars_to_compare_temp[!is.na(match(vars_to_compare_temp, colnames(data_temp)))]
  }
}else{
  stop("You should set the vars to compare!!")
}
for(p in p_temp){
  inds_temp<-which(data_temp$Parameter == p)
  temp<-data_temp[inds_temp,]
  conv_temp<-length(temp$Type[which(as.character(temp$Type) == "conv")])/length(temp$Type)
  eco_temp<-length(temp$Type[which(as.character(temp$Type) == "eco")])/length(temp$Type)
  temp<-data_temp[inds_temp,match(vars_to_compare_temp, colnames(data_temp))]
  write(paste("Summary stats for Parameter: ", p, sep=""), file = file_name_temp, append = TRUE)
  write(paste("How many regimes of each type in pct? Eco: ",eco_temp, "Conv: ", conv_temp ), file=file_name_temp, append=TRUE)
  capture.output(stat.desc(as.matrix(temp), basic=FALSE), file = file_name_temp, append = TRUE)
}

source(paste(script_dir,"/all_agents/summary_info/Evaluate_divergence.r", sep=""))



if(with_policy || with_rand_policy || with_rand_learn || with_rand_barr){
  # Descriptive overview of parameter init for different random settings
  
}