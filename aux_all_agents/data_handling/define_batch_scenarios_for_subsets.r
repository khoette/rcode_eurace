
# Get data in data frame for ggplot
print("Get batch data for learning subsets")
for(type_tmp in c("Learn_Type", "Spill", "Ease")){
if(file.exists(paste(experiment_directory,"/rdata/",kind_of_data,"/", data_type[d] ,"/data_batch_",type_tmp,"_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))!=TRUE){
  
load(file=paste(data_directory,"data_single_large_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
temp_mat<-object
if(agent == "Firm"){
  temp_mat<-temp_mat[which(temp_mat$active ==1),]
}
if(exists(eval(as.name(paste("variables_available_in_panel_",agent,sep=""))))){
  eval(call("<-",(paste("variables",sep="")),as.name(paste("variables_available_in_panel_",agent,sep=""))))
}else{
  variables<-colnames(temp_mat)[which(is.na(match(colnames(temp_mat),c("Run","id","Periods","Type","Firm_Type","Adopter_Type"))))]
}
cols<-match(variables, colnames(temp_mat))
cols<-cols[which(is.na(cols) == FALSE)]
start<-cols[1]
periods_temp<-length(unique(temp_mat$Periods))
steps<-(number_xml/periods_temp)*20
it_vector<-seq(steps,steps*periods_temp,steps)

idx_temp<-match(type_tmp, colnames(temp_mat))
if(is.na(idx_temp) != TRUE){
types_temp<-unique(temp_mat[,idx_temp])
tmp<-0
for(type in types_temp){
  temp<-temp_mat[which(temp_mat[,idx_temp] == type),]
  print(paste("batch type: ",type_tmp, type))
  
  for(i in c(1:length(it_vector))){
    temp3<-temp[which(temp$Periods == it_vector[i] ),]
    #for(i in 1:10){
    it<-i + tmp*periods_temp
    for(v in cols){
      if(it==1 && tmp == 0 && v==start){
        temp2<-data.frame(matrix(0, length(types_temp)*number_xml,length(colnames(temp_mat))))
        colnames(temp2) <- colnames(temp_mat)
      }
      if(v==start && i == 1){
          temp2$Type[(1+(tmp)*number_xml):((tmp+1)*number_xml)]<-temp[i,idx_temp]
          temp2$Periods[(1+tmp*number_xml):((tmp+1)*number_xml)]<-it_vector
          temp2$Run[(1+tmp*number_xml):((tmp+1)*number_xml)]<-length(unique(temp$Run))
        }
      temp2[it,v]<-mean(temp3[,v], na.rm=TRUE)
    } # v in variables
    #temp<-temp[which(temp$Periods != it_vector[i] ),]
  }# i in it vector
  tmp<-tmp+1
}# type in types
  temp_mat <- temp2
  indices_temp<-match(c("Type", "Periods", "Run", variables), colnames(temp_mat))
  temp_mat<-temp_mat[,indices_temp]
  
rm(temp, temp2, i, type, it_vector, start)


eval(call("<-",(paste("data_batch_",type_tmp,"_",experiment_name,"_",parameter,"_",agent,sep="")),temp_mat))
object<-eval(as.name(paste("data_batch_",type_tmp,"_",experiment_name,"_",parameter,"_",agent,sep="")))
save(object, file=paste(data_directory,"/",paste("data_batch_",type_tmp,"_",experiment_name,"_",parameter,"_",agent,sep=""),".RData",sep=""))
}else{
  print(paste("Data not classified by ", type_tmp, data_type[d], agent, parameter))
}
}else{
  print("Data already exists")
}


} # type_tmp in c(learn type, spill, ease)
#rm(list=ls(pattern="temp"))
