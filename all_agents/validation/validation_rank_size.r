index_rm_outliers_and_na <- function(x, scale, na.rm = TRUE, ...) {
  qnt <- quantile(x, probs=c(.25, .75), na.rm = na.rm, ...)
  H <- scale*IQR(x, na.rm = na.rm)
  y <- x
  y[x < (qnt[1] - H)] <- NA
  y[x > (qnt[2] + H)] <- NA
  inds<-which(!is.na(y))
}

plot_dir_temp<-paste(plot_dir_validation,"/rank_size_plots/", sep="")
if(dir.exists(plot_dir_temp)!=TRUE){
  dir.create(plot_dir_temp)
}

vars_temp<-c("output", "no_employees", "firm_productivity_conv", 
             "firm_productivity_eco", "total_units_capital_stock_conv", 
             "total_units_capital_stock_eco")
# Get firm data for mark ups: 
load(file=paste(data_directory,"data_single_switch_",experiment_name,"_",parameter,"_Firm.RData",sep=""))
cols<-match(c("Type", "Periods", "Run", "id", vars_temp), colnames(object))
data_temp<-object[cols]
rm(object)
data_temp<-data_temp[order(as.numeric(as.character(data_temp$Run)), as.numeric(as.character(data_temp$Periods))),]

prod_eco<-data_temp[,match("firm_productivity_eco", colnames(data_temp))]
prod_conv<-data_temp[,match("firm_productivity_conv", colnames(data_temp))]
productivity<-pmax(prod_eco, prod_conv, na.rm=TRUE)
capital_eco<-data_temp[,match("total_units_capital_stock_eco", colnames(data_temp))]
capital_conv<-data_temp[,match("total_units_capital_stock_conv", colnames(data_temp))]
cap_stock<-pmax(capital_eco, capital_conv, na.rm=TRUE)
rm(prod_eco, prod_conv, capital_eco, capital_conv)
temp<-data_temp[match(c("Type", "Periods", "Run", "id",
                           "output", "no_employees"), colnames(data_temp))]
temp<-cbind(temp, productivity=productivity, capital_stock=cap_stock)
data_temp<-temp
vars_temp<-c("output", "no_employees", "productivity",
             "capital_stock")

prod_temp<-temp$productivity
cap_temp<-temp$capital_stock
out_temp<-temp$output
empl_temp<-temp$no_employees
run_temp<-temp$Run
id_temp<-temp$id
per_temp<-temp$Periods

class_temp<-rep(NA, length(prod_temp))
periods_temp<-unique(per_temp)
runs_temp<-unique(run_temp)
median_prod_temp<-rep(NA, (length(runs_temp)*length(periods_temp)))
avg_prod_high_temp<-rep(NA, (length(runs_temp)*length(periods_temp)))
avg_prod_low_temp<-rep(NA, (length(runs_temp)*length(periods_temp)))

r<-runs_temp[1]
p<-periods_temp[1]
ind_temp<-0
for(r1 in 1:length(runs_temp)){
  r<-runs_temp[r1]
  inds_r_temp<-which(run_temp == r)
  for(p1 in 1:length(periods_temp)){
    ind_temp<-ind_temp+1
    p<-periods_temp[p1]
    inds_rp_temp<-which(per_temp[inds_r_temp] == p)
    median_temp<-median(prod_temp[inds_rp_temp], na.rm=TRUE)
    median_prod_temp[ind_temp]<-median_temp
    inds_high<-which(prod_temp[inds_rp_temp]>median_temp)
    class_temp[inds_high]<-"high"
    inds_low<-which(prod_temp[inds_rp_temp]<=median_temp)
    class_temp[inds_low]<-"low"
    prod_temp[inds_rp_temp]<-prod_temp[inds_rp_temp] - median_temp
    avg_prod_high_temp[ind_temp]<-mean(prod_temp[inds_high], na.rm=TRUE)
    avg_prod_low_temp[ind_temp]<-mean(prod_temp[inds_low], na.rm=TRUE)
  }
}


# Rank size distribution for firms
# 
# Data from before: firm_level_data on output and no_employees
# 
# Reduce data to snapshot in time
# 
initial <- 580 # before market entry of eco
end<-15000 # after convergence to techn. state

#firm_level_data_work<-firm_level_data[which(firm_level_data$Periods %in% c(initial, end)),]
runs_temp<-unique(data_temp$Run)
periods_temp<-unique(data_temp$Periods)




p<-initial
v<-"no_employees"
v<-"output"
r<-runs_temp[1]
for(i in 1:2){
  for(p in c(initial, end)){
    period<-p
    #for(v in c("output", "no_employees")){
    for(v in vars_temp){
      var<-v
      if(v == "output"){
        var_name <- "Output"
      }else if(v == "no_employees"){
        var_name <- "# employees"
      }else if(v == "capital_stock"){
        var_name <- "Capital stock"
      }else if(v == "productivity"){
        var_name <- "Productivity"
      }else{
        var_name <- v
      }
      data_temp0<-data_temp[which(data_temp$Periods == period),match(c("Run",var), colnames(data_temp))]
      ranks_temp<-array(NA, dim=c(length(data_temp0[,1]),1))
      if(i == 1){
        runs_temp_sel<-as.numeric(runs_temp)
        size_temp<-0.005
        scale_temp<-0.125
        name_temp<-paste("Rank_size_as_",v,"_across_all_runs_in_",p,sep="")
      }else if(i==2){
        runs_temp_sel<-sample(as.numeric(runs_temp), 1)
        size_temp <- 1
        scale_temp<-0.05
        name_temp<-paste("Rank_size_as_",v,"_random_run_",r,"_in_",p,sep="")
      }
      norm_temp<-rep(NA, length(runs_temp_sel))
      for(r1 in 1:length(runs_temp_sel)){
        r<-as.numeric(runs_temp_sel[r1])
        ind_1<-which(as.numeric(data_temp0$Run) == r)
        temp<-data_temp0[ind_1, 2]
        ind_2<-which(!is.na(temp))
        temp_0<-as.numeric(as.character(temp[ind_2]))
        # use neg. value because rank() allocates rank 1 to smallest. 
        # Here: I want rank == 1 for largest
        #median_temp<-median(temp, na.rm=TRUE)
        mean_temp<-mean(as.numeric(temp_0), na.rm=TRUE)
        #data_temp0[ind_1,2]<-data_temp0[ind_1,2]/median_temp
        #data_temp0[ind_1,2]<-data_temp0[ind_1,2]/mean_temp
        runs_temp_sel[r1]<-mean_temp
        temp_0<-temp_0*(-1)
        rank_temp<-as.numeric(rank(temp_0))
        # normalize ranks by number of active firms
        normalize_temp<-length(rank_temp)
        rank_temp<-as.numeric(rank_temp/normalize_temp)
        ranks_temp[ind_1[ind_2]]<-rank_temp
      } # r in runs_temp
    
      data_temp0<-cbind(data_temp0, ranks=ranks_temp)
      data_temp0<-data_temp0[which(!is.na(data_temp0[,1])),]
      inds_in<-index_rm_outliers_and_na(data_temp0[,2], scale_temp)
      Rank<-(data_temp0$ranks[inds_in]*100)
      Var<-(data_temp0[,2][inds_in]*100)
      #ind_temp_na<-which(is.na(Rank))
      #Rank<-Rank[-ind_temp_na]
      #Var<-Var[-ind_temp_na]
      xlabel<-paste("log(",var_name,")", sep="")
    
      pdf(paste(plot_dir_temp,name_temp,".pdf",sep=""))
      plot(log(Var), log(Rank), type="p", xlab=xlabel, cex=size_temp, pch=20)
      dev.off()
    } # v in variables
    rm(var_name)
  } # p in periods
} # i in 1:2 (if 1: all runs, if 2: consider randomly drawn r)

