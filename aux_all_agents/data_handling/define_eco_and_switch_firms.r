# Introduce additional column as type id for those firms who are using more than 50% green cap, 
# and for those who switch
# 
agent<-"Firm"
firm_types<-c("eco", "conv", "switch_ec", "switch_ce")

available_aggregation_levels_temp<-c("single_large", "single_switch")
for(d_temp in data_types_available){
  for(aggr_temp in available_aggregation_levels_temp){
    data_directory<-paste(experiment_directory,"/rdata/",kind_of_data,"/",d_temp,"/",sep="")
    load(file=paste(data_directory,"data_",aggr_temp,"_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
    #print(paste(aggr_temp, d_temp))
    if(aggr_temp == available_aggregation_levels[1] && d_temp == data_types_available[1]){
      max_temp<-max(as.numeric(object$Periods))
      halftime_temp<-as.integer(max(as.numeric(object$Periods))/2)
      subset_temp<-object[which(as.numeric(object$Periods) %in% c(halftime_temp, max_temp)),]
      
      temp<-object
      temp<-cbind(NA, temp)
      colnames(temp)<-c("Firm_Type", colnames(object))
      temp$`Firm_Type`[which(temp$share_conventional_vintages_used[])]
    }
  }
}
