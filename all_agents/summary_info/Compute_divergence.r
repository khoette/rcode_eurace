agent<-"Eurostat" 
parameter<-parameters_tmp[1]
aggr<-"eco_conv_batch"

data_directory<-paste(experiment_directory,"/rdata/logs/levels/",sep="")
vars_of_interest_temp<-c("Periods", "Type", "share_conv_capital_used", "s_skill_ratio", 
                         "frontier_productivity_ratio", "no_active_firms", "monthly_output")

for(p in parameters_tmp){
  load(file=paste(data_directory,"data_",aggr,"_",experiment_name,"_",p,"_",agent,".RData",sep=""))
  cols_temp<-match(vars_of_interest_temp, colnames(object))
  if(p == parameters_tmp[1]){
    data_temp_batch<-object[cols_temp]
    data_temp_batch<-cbind(Parameter = rep(p, length(data_temp_batch[,1])), data_temp_batch)
  }else{
    data_temp_batch<-rbind(data_temp_batch, cbind(Parameter = rep(p, length(object[,1])),object[cols_temp]))
  }
}
rm(object)




#### Now: Compare divergence in knowledge stocks
# Add new variables: Absolute diff. between skill stock eco and conv scenarios. 
# Was macht am meisten Sinn? Differenz in batch data? Difference to benchmark 
# eco conv? E.g. mean or median eco/conv run? If median, how to select this? 
# "crosssectional" median or identify median run? 
# 
periods_temp<-unique(data_temp$Periods)
type_temp<-data_temp[,match("Type", colnames(data_temp))]
pers_temp<-data_temp[,match("Periods", colnames(data_temp))]
prod_temp<-data_temp[,match("frontier_productivity_ratio", colnames(data_temp))]
skill_temp<-data_temp[,match("s_skill_ratio", colnames(data_temp))]
type_temp_batch<-data_temp_batch[,match("Type", colnames(data_temp_batch))]
pers_temp_batch<-data_temp_batch[,match("Periods", colnames(data_temp_batch))]
prod_temp_batch<-data_temp_batch[,match("frontier_productivity_ratio", colnames(data_temp_batch))]
skill_temp_batch<-data_temp_batch[,match("s_skill_ratio", colnames(data_temp_batch))]
#for(v in c("s_skill_ratio", "frontier_productivity_ratio")){
for(p in p_temp){
  print(p)
  ind_par_temp<-which(data_temp$Parameter == p)
  ind_par_temp_batch<-which(data_temp_batch$Parameter == p)
  for(t in unique(type_temp)){
    print(t)
    if(length(unique(type_temp)) != 2){
      stop()
    }
    ind_type_temp<-which(type_temp[ind_par_temp] == t)
    ind_type_temp_batch<-which(type_temp_batch[ind_par_temp_batch] != t)
    for(per in periods_temp){
      ind_pers_temp<-which(pers_temp[ind_type_temp] == per)
      ind_pers_temp_batch<-which(pers_temp_batch[ind_type_temp_batch] == per)
      # Compute absolute difference between per run knowledge stock ratio and the 
      # average knowledge stock ratio in the alternative regime type. 
      # Use this as measure for "divergence". 
      prod_temp[ind_pers_temp]<-abs(prod_temp[ind_pers_temp] - prod_temp_batch[ind_pers_temp_batch])
      skill_temp[ind_pers_temp]<-abs(skill_temp[ind_pers_temp] - skill_temp_batch[ind_pers_temp_batch])
    }
  }
}
#}
data_temp<-cbind(data_temp, frontier_divergence=prod_temp)
data_temp<-cbind(data_temp, skill_divergence=skill_temp)

