data_macro<-data_euro[match(c(macro_vars),colnames(data_euro))]

data_euro$Run<-round(as.numeric(as.character(data_euro$Run)))
runs_rm<-as.numeric(as.character(unique(data_euro$Run)))
periods_rm<-as.numeric(as.character(unique(data_euro$Periods)))
euro_runs_rm<-as.numeric(as.character(data_euro$Run))

data_work<-data_firm
firm_runs_rm<-as.numeric(as.character(data_work$Run))
firm_pers_rm<-as.numeric(as.character(data_work$Periods))
if(periods_rm[1]!=min(firm_pers_rm)){
  if(periods_rm[1] != 820 && min(firm_pers_rm) != 840){stop("Verify your initial conditions date in reg data")}
  data_euro$Periods[which(data_euro$Periods == periods_rm[1])]<-min(firm_pers_rm)
  periods_rm[1]<-min(firm_pers_rm)
  euro_pers_rm<-as.numeric(as.character(data_euro$Periods))
  
}
for(r in runs_rm){
  print(r)
  inds_fr_rm<-which(firm_runs_rm == r)
  inds_er_rm<-which(euro_runs_rm == r) # eurostat index
  for(p in periods_rm){
    inds_frp_rm<-which(firm_pers_rm[inds_fr_rm] == p)
    l_rm<-length(inds_frp_rm)
    if(l_rm>120 ){
      stop("length l_rm > 120")
    }else if(l_rm>90){
      warning(paste("l_rm > 90???", p, r, l_rm))
    }
    # Create big matrix of macro controls, repeat each row in rp subset l_rm times
    euro_row_rm<-c(r, p, as.numeric(data_macro[inds_er_rm[which(euro_pers_rm[inds_er_rm] == p)],]))
    if(is.element(NA, euro_row_rm)){
      stop("na is element")
    }
    matrix_rm<-matrix(euro_row_rm, nrow=l_rm, ncol=length(euro_row_rm), byrow=TRUE)
    if(p == periods_rm[1] && r == runs_rm[1]){
      macro0<-matrix_rm
    }else{
      macro0<-rbind(macro0, matrix_rm)
    }
  } # p in periods temp
} # r in runs rm
macro0<-as.data.frame(macro0)
colnames(macro0)<-c("Run", "Periods", colnames(data_macro))
rm(r, p)
rm(list = ls(pattern="_rm"))
macro0<-macro0[order(macro0$Run,macro0$Periods),]
data_work_temp<-data_work[order(as.numeric(as.character(data_work$Run)),as.numeric(as.character(data_work$Periods))),]
data_work<-cbind(data_work_temp, macro0[which(!is.element(colnames(macro0), c("Run", "Periods")))])
rm(macro0, data_work_temp, data_macro)
