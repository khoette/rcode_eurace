# Construct an multidim. array with the dimensions no_agents x number_xml x runs x variables. 
# This array holds NAs for all variables v if firm i in period t in run r is not active. 
# The array is manually (?) iterated through the multidim. array. (Not any tensor 
# product formula?)
# 
if(filter_variable != "share_conv_capital_used"){
  
for(r in 1:runs){
 for(i in 1:no_agents){
      
      #Distinguish two cases: 1. Dynamic filtering over time or 2. filtering at 
      #reference date (e.g. firms that have changed their technology until the end of time)
      #
      for(t in 1:number_xml){
        index_filter<-match(filter_variable, list_of_var)
        if(exists("op") == FALSE){
          if(exists("reference_date") != TRUE){
            t1 <- t
          }else{
            t1<-reference_date
          }
          
          if(data_all[i,t1,r,index_filter] == 0){
            if(filter_variable == "active"){
              set_to_na_temp<-list_of_var[which(list_of_var != "id")]
              set_to_na_temp<-set_to_na_temp[which(set_to_na_temp != "active")]
              set_to_na_temp<-match(set_to_na_temp, list_of_var)
              data_all[i,t,r,set_to_na_temp]<-NA
              #data_all_eco_id[i,t,r,]<-NA
            }else{
              if(r==1 && t==1 && i==1){
                data_prelim<-data_all
                #data_prelim_eco_id<-data_all_eco_id
              }
              data_prelim[i,t,r,]<-NA
              #data_prelim_eco_id[i,t,r,]<-NA
            }
          }
        }else if(op=="threshold"){
          if(i==1 && t==1 && r==1){
            #subset_below<-array(NA, dim=dim(data_all_eco_id))
            #subset_above<-array(NA, dim=dim(data_all_eco_id))
            subset_below<-array(NA, dim=dim(data_all))
            subset_above<-array(NA, dim=dim(data_all))
          }
          #subset_below<-data_all_eco_id
          #ubset_above<-data_all_eco_id
          #if(is.na(data_all_eco_id[i,t1,r,index_filter]) == FALSE ){
          #  if(data_all_eco_id[i,t1,r,index_filter] < threshold_value){
          #    subset_below[i,t,r,]<-data_all_eco_id[i,t,r,] 
          #  }else{
          #    subset_above[i,t,r,]<-data_all_eco_id[i,t,r,] 
          #  } # remaining: NA remains NA
          if(is.na(data_all[i,t1,r,index_filter]) == FALSE ){
              if(data_all[i,t1,r,index_filter] < threshold_value){
                subset_below[i,t,r,]<-data_all[i,t,r,] 
              }else{
                subset_above[i,t,r,]<-data_all[i,t,r,] 
              } 
          }
        }
      }
    }
  }
}

