### This file generates the regression analyses that were published in the paper. 
### Before selecting the "final" specification, I made comprehensive robustness 
### checks on different model specifications, usind different snapshots in time. 
### This code generates far more results than you will need and it also generates 
### more data than needed. 
### 
### Sorry that it is a bit difficult to identify the "final" regression results out 
### of the mess of data and outputs, but on the other hand it should illustrate 
### the robustness of the findings. 
### Anyway, I learned that I will write (hopefully) more user friendly code in 
### future...
### 
REMOVE_INACTIVE_FIRMS<-1
MAKE_COMPREHENSIVE_REG_TESTS<-1
print("Please read the info on top of the init_cond_regr_new_test.r file")
print("Note that it takes some time until this script is run, especially when using 
firm level data.. Data preprocesing takes some time. ")

if(script_dir == "/home/kerstin/Schreibtisch/eurace/own_R/scripts"){
  aux_folder<-"EE_revision/EE_scripts/aux"
}else{
  aux_folder<-"aux_all_agents"
}


if(exists("MAKE_COMPREHENSIVE_REG_TESTS") == FALSE){
  MAKE_COMPREHENSIVE_REG_TESTS <- 0
}

if(is.element(agent, c("Firm", "Eurostat"))){

  source(paste(script_dir,"/",aux_folder,"/functions.r",sep=""))
# 
# 
# 
# 
eco<-"limegreen"
conv<-"salmon"
switch<-"royalblue"

baseline<-"black"
experiment<-"red"
invest<-"blue"
cons<-"orange"


make_plots<-0
make_many_plots<-0
if(exists("with_policy") && with_policy == 1){
  if(length(parameters_tmp)>1){
    with_policy<-2
  }
}

if(agent=="Eurostat" || agent == "Government"){
  make_plots <- 1
  no_agents<-1
}else if(agent == "Firm"){
  make_plots <- 0
  no_agents<-120
}


agent0<-agent 
if(with_policy){
  agent<-"Regression_init_cond_policy"
}else if(with_rand_learn){
  agent<-"Regression_learning_conditions"
}else{
  agent<-"Regression_init_cond_final_state"
}
if(exists("with_policy") && with_policy == 2){
  parameter0<-parameter
  parameter<-"Parameter_regression_policy"
}
if(exists("par_analysis") && par_analysis == 1){
  parameter0<-parameter
  parameter<-"Parameter_regression"
}
source(paste(script_dir,"/",aux_folder,"/create_directories_for_plotting.r",sep=""))
if(file.exists(plot_dir_selection) == FALSE){
  dir.create(plot_dir_selection)
}
agent<-agent0
if(exists("parameter0")){
  parameter<-parameter0
  rm(parameter0)
}
rm(agent0)
# 
if(agent == "Eurostat"){
  agent_temp<-"Eurostat"
  variables_init_cond_temp<-c("specific_skill_gap", "frontier_gap", "average_s_skill_conv", 
                              "techn_frontier_conv",  "monthly_output", 
                              "no_active_firms", "eco_price_wage_ratio", "share_conv_capital_used", "no_active_firms", 
                              "real_wage",  "techn_frontier_conv", "average_s_skill_conv")
  if(exists("with_rand_learn") && with_rand_learn == 1){
    variables_init_cond_temp<-c(variables_init_cond_temp, "min_learning_coefficient_var", "learning_spillover_strength_var")
  }
  target_var_temp_const<-c("share_conv_capital_used", "no_active_firms", "monthly_output")
  #var_of_interest<-c("std_share", "std_no_firms")
  var_of_interest<-c("share_conv_capital_used", "no_active_firms", "monthly_output")
  indx_var_of_interest<-match(var_of_interest, target_var_temp_const)
}else if(agent == "Firm"){
  agent_temp<-"Firm"
  #variables_init_cond_temp<-c( "technology_conv" , "mean_spec_skills_conv", 
  #                             "no_employees", "output", "age", "price", "unit_costs","Run", "effective_investments",
  #                             "share_conventional_vintages_used", "technology_gap", 
  #                             "mean_specific_skill_gap", "min_learning_coefficient_var", 
  #                             "learning_spillover_strength_var")
  variables_init_cond_temp<-c( "technology_conv" , "mean_spec_skills_conv", 
                               "no_employees", "output", "age", "price", "unit_costs","Run",
                               "share_conventional_vintages_used", "technology_gap", 
                               "mean_specific_skill_gap", "min_learning_coefficient_var", 
                               "learning_spillover_strength_var")
  target_var_temp_const<-c("share_conventional_vintages_used", "output", "no_employees", "unit_costs", "effective_investments")
  target_var_temp_const<-c("share_conventional_vintages_used", "output", "no_employees", "unit_costs")
  
  var_of_interest<-target_var_temp_const
  indx_var_of_interest<-match(var_of_interest, target_var_temp_const)
}else if(agent == "Government"){
  agent_temp<-"Government"
  variables_init_cond_temp<-c( "technology_conv" , "mean_spec_skills_conv", 
                               "no_employees", "output", "age", "price", "unit_costs",
                               "share_conventional_vintages_used", "technology_gap", 
                               "mean_specific_skill_gap", "min_learning_coefficient_var", 
                               "learning_spillover_strength_var", 
                               "no_active_firms", "monthly_output")
  target_var_temp_const<-c("base_tax_rate", "monthly_budget_balance", "monthly_budget_balance_gdp_fraction", 
                           "eco_policy_budget", "eco_policy_budget")
  var_of_interest<-target_var_temp_const
  indx_var_of_interest<-match(var_of_interest, target_var_temp_const)
}

indx_var_of_interest<-indx_var_of_interest[which(is.na(indx_var_of_interest)==FALSE)]

if(with_policy){
  #variables_policy_temp<-c("eco_tax_rate", "eco_investment_subsidy", 
  #                         "eco_consumption_subsidy", "gov_monthly_eco_tax_revenue", 
  #                         "gov_monthly_eco_subsidy_payment", "base_tax_rate",
  #                         "monthly_budget_balance")
  variables_policy_temp<-c("eco_tax_rate", "eco_investment_subsidy", 
                           "eco_consumption_subsidy")
}else{
  variables_policy_temp<-c()
}

if(with_rand_barr && agent != "Eurostat"){
  variables_barr_temp<-c("specific_skill_gap", "frontier_gap")
}else{
  variables_barr_temp<-c()
}

day_market_entry<-600
end_time<-15000
#
#periods_temp<-c(600,1200,1800,2400,3000,3600,4200,4800,5400,6000,9000,12000,15000)
#periods_temp<-c(600,900,1200,1800,2400,3000,6000,9000,12000,15000)
if(with_policy && with_rand_barr){
  periods_temp<-c(600,720,1040,1200,1800,3000,9000,13500,15000-220)
}else{
  if(MAKE_COMPREHENSIVE_REG_TESTS == 1){
    periods_temp<-c(600,720,1200,1800,3000,9000,13500,15000-220)
  }else{
    periods_temp<-c(600,720,15000-220)
    if(with_policy && ((exists("EE_revision") && EE_revision == 0)||exists("EE_revision")==F)){
      periods_temp<-c(600,720,1040,13500,15000-220)
    }
  }
  
}

if(MAKE_COMPREHENSIVE_REG_TESTS){
  time_temp1<-c(1800,3000,9000,(15000-220)) 
  if(exists("with_policy") && with_policy){
    time_temp1<-c(time_temp1, 13500) # 13500 is time before policy is phased out
  }
}else{
  time_temp1 <- 14780
  if(exists("with_policy") && with_policy &&( (exists("EE_revision") && EE_revision == 0)||exists("EE_revision")==F)){
    time_temp1<-c(time_temp1, 13500) # 13500 is time before policy is phased out
  }
}

# That are the target values, i.e. predict target in t1 in time_temp1

kind_of_data<-"logs"
data_directory<-paste(experiment_directory,"/rdata/",kind_of_data,"/",data_type[d],"/",sep="")

aggr_temp<-1
if(agent=="Eurostat" && exists("par_analysis") && par_analysis == 0){
  compute_vars_temp<-0
}else{
  compute_vars_temp<-0
}

eval(call("<-",paste("time_temp"),as.name(paste("periods_temp",sep=""))))




#temp_data<-temp_data[which(as.vector(as.character(temp_data$Periods)) %in% time_temp),]
# inputs: periods_temp, runs, parameters_tmp, no_agents, compute_vars_temp
#         object, year average of data at time steps in periods_temp
# output: reg_data_temp, i.e. 
if(exists("par_analysis") && par_analysis == 1){
  rep<-parameters_tmp
  foldername<-"Parameter_analysis"
}else if(exists("par_analysis") && par_analysis == 0){
  rep<-parameter
}else{
  print("Make a decision whether regression as parameter analysis! I.e. set par_analysis. Default: rep<-parameter")
  rep<-parameter
}

if(exists("par_analysis") && par_analysis == 1){
  dir_temp0<-paste(plot_dir_parameter,"/Regression/",sep="")
}else{
  dir_temp0<-paste(plot_dir_selection,"/Regression/",sep="")
}
if(dir.exists(dir_temp0)==FALSE){
  dir.create(dir_temp0)
}



if(file.exists(paste(experiment_directory,"/reg_data_temp_",agent,".RData", sep="")) != TRUE){
for(p00 in 1:length(rep)){
  p0<-rep[p00]
  if(aggr_temp == 1){
    aggr<-"single_large"
  }else if(aggr_temp == 2){
    aggr<-"single_switch"  
  }
  if(exists("bau_in_diff_folder") && bau_in_diff_folder == 1 && p00 == 1 && par_analysis){
    data_from<-paste(data_dir_bau,"data_", aggr, "_",exper_name_bau,"_",p0,"_",sep="")
  }else{
    data_from<-paste(data_directory,"data_", aggr, "_",experiment_name,"_",p0,"_",sep="")
  }
  
  load(file=paste(data_from,agent,".RData",sep=""))
  if(agent == "Firm" && REMOVE_INACTIVE_FIRMS){
    o<-object[order(as.numeric(as.character(object$Run)), as.numeric(as.character(object$id))),]
    r_id<-round(as.numeric(as.character(o$Run)))+0.001*round(as.numeric(as.character(o$id)))
    n_obs<-unique(r_id) # run * no_agents * number_xml -> unique: run * id
    n_obs_per_r_id<-length(r_id)/length(n_obs) # number of iterations
    
    start<-1
    active<-as.numeric(as.character(o$active))
    remove_inds<-c()
    for(n in 1:length(n_obs)){
      inds<-c(start:(start+n_obs_per_r_id-1))
      start<-start+n_obs_per_r_id
      if(sum(active[inds]) != n_obs_per_r_id){
        remove_inds<-c(remove_inds, inds)
      }
    }
    fix_cols<-c("Adopter_Type", "Firm_Type", "Type", "Periods", "Run", "active", "id")
    cols<-which(is.na(match(colnames(object),fix_cols)))
    object[remove_inds,cols]<-NA
    rm(inds, n_obs_per_r_id, n, remove_inds, cols, fix_cols)
  }
  
  
  
    if((exists("with_rand_barr") && with_rand_barr == 1)||(exists("with_rand_policy") && with_rand_policy == 1 )|| (exists("with_rand_learn") && with_rand_learn == 1)){
      source(paste(script_dir, "/",aux_folder,"/scatterplot_rand_barrs_full.r", sep=""))
    }
  
  source(paste(script_dir,"/",aux_folder,"/transform_data_for_regr.r",sep=""))    
  
  if(length(rep)>1){
    if(p00 == 1){
      reg_data_temp0<-cbind(Parameter = parameter_names[1], reg_data_temp)
    }else{
      temp<-cbind(Parameter = parameter_names[p00], reg_data_temp)
      if(setequal(colnames(temp), colnames(reg_data_temp0))!=TRUE){
        remove<-setdiff(colnames(temp), colnames(reg_data_temp0))
        if(length(remove)>0){
          index_remove<-match(remove, colnames(temp))
          temp<-temp[-index_remove]
        }
        remove2<-setdiff(colnames(reg_data_temp0), colnames(temp))
        if(length(remove2)>0){
          index_remove<-match(remove2, colnames(reg_data_temp0))
          reg_data_temp0<-reg_data_temp0[-index_remove]
        }
        remove<-c(remove, remove2)
        rm(remove2)
      }
      order<-match(colnames(temp), colnames(reg_data_temp0))
      temp<-temp[order]
      reg_data_temp0<-rbind(reg_data_temp0, temp)
    }
    
    if(p00 == length(rep)){
      reg_data_temp<-reg_data_temp0
      rm(reg_data_temp0)
    }
  } 
} # end for p0 in rep
rm(p00, p0)

  save(reg_data_temp, file=paste(experiment_directory,"/reg_data_temp_",agent,".RData", sep=""))

}else{
  load(paste(experiment_directory,"/reg_data_temp_",agent,".RData", sep=""))
  if(REMOVE_INACTIVE_FIRMS){
    remove_inds<-which(is.nan(reg_data_temp$share_conventional_vintages_used ))
    if(length(remove_inds)>0){reg_data_temp<-reg_data_temp[-remove_inds,]}
    rm(remove_inds)
  }
}
fix_cols<-c("id", "Type", "Firm_Type", "Adopter_Type", "Run", "Periods")
fix_cols<-fix_cols[which(!is.na(match(fix_cols,colnames(reg_data_temp))))]
var_cols<-c(1:ncol(reg_data_temp))[-c(match(fix_cols, colnames(reg_data_temp)))]
for(c in var_cols){
  reg_data_temp[,c]<-as.numeric(as.character(reg_data_temp[,c]))
}

# transform policy rates into pct points
if(with_rand_barr){
  indx_pol<-match(c("frontier_gap", "specific_skill_gap"), colnames(reg_data_temp))
  for(col in indx_pol){
    reg_data_temp[,col]<-(reg_data_temp[,col])*100
  }
}
if(with_policy){
  indx_pol<-match(c("eco_tax_rate", "eco_investment_subsidy", "eco_consumption_subsidy"), colnames(reg_data_temp))
  for(col in indx_pol){
    reg_data_temp[,col]<-(reg_data_temp[,col])*100
  }
}


fix_vars_temp<-match(c("Parameter", "id", "Periods", "Type", "Firm_Type", "Adopter_Type", "Run"), colnames(reg_data_temp))

fix_vars_temp<-unique(fix_vars_temp[which(is.na(fix_vars_temp)== FALSE) ])
# I do this for formatting reasons: In the data frames, some of the variables were stored as characters... 
#reg_data_temp<-temp_data
#
#stop("CONTINUE HERE WITH DATA FORMATTING CHECK")
remove<-c()
cols<-c(1:length(reg_data_temp))[-fix_vars_temp]
len<-length(reg_data_temp[,1])
for(col_temp in cols){
  #reg_data_temp[which(is.nan(reg_data_temp[,col_temp])),col_temp]<-NA
  if(is.factor(reg_data_temp[,col_temp]) == FALSE){
    if(length(reg_data_temp[which(is.na(reg_data_temp[,col_temp]) == FALSE),col_temp]) < 0.1*len){
      remove<-c(remove, col_temp)
    }else{
      reg_data_temp[,col_temp]<-as.numeric(reg_data_temp[,col_temp])
      if(sd(reg_data_temp[,col_temp], na.rm=TRUE) < 0.001 * mean(reg_data_temp[,col_temp], na.rm=TRUE)){
        remove<-c(remove, col_temp)
        stop("Whats here?")
      }
    }
  }
}
if(length(remove)>0){
  reg_data_temp<-reg_data_temp[,-remove]
}

rm(remove, cols, col_temp)
target_var_temp_const<-target_var_temp_const[which(is.na(match(target_var_temp_const, colnames(reg_data_temp)))==FALSE)]

j<-1
for(j in 1:length(target_var_temp_const)){  
  print(paste("Target: ", target_var_temp_const[j]))
  #reg_data_temp<-temp_data[which(colnames(temp_data)!=target_var_temp)]
  if(agent == "Firm"){
    reg_data_temp<-reg_data_temp[order(as.numeric(reg_data_temp$id)),]
  }else{
    reg_data_temp<-reg_data_temp[order(reg_data_temp$Run),]
  }
  #if(agent=="Firm" && length(temp_data[which(is.na(temp_data$Firm_Type) == FALSE),])>runs*no_agents*0.25){
  #  temp_data<-temp_data[which(is.na(temp_data$Firm_Type) == FALSE),]
  #}
  time_temp<-time_temp[1]
  t<-time_temp[1]
  for(t in time_temp){
    if(is.element(target_var_temp_const[j], c("std_share", "std_output", "std_no_firms"))){
      time_temp01<-end_time-120
    }else{
      time_temp01<-time_temp1
    }
    t1<-time_temp01[1]
    for(t1 in time_temp01){
      
      print(paste("t in time temp: ", t, "out of length: ", length(time_temp)))
      print(paste("Predict ", target_var_temp_const[j], "in", t1))
      list_of_var_pairs<-data.frame(array(NA, dim=c(1, length(reg_data_temp))))#[-fix_vars_temp]))))
      colnames(list_of_var_pairs)<-colnames(reg_data_temp)#[-fix_vars_temp]
      if(agent == "Eurostat"){
        indices<-c(match(c("specific_skill_gap", "frontier_gap",  "techn_frontier_conv", "average_s_skill_conv"), 
                         colnames(list_of_var_pairs)))
        
        if(exists("with_rand_policy") && with_rand_policy && (experiment_name!="eco_policy_switch" || exists("par_analysis") && par_analysis==0)){
          #indices<-c(indices, match(c("eco_tax_rate", "eco_investment_subsidy", "eco_consumption_subsidy"), 
          #                          colnames(list_of_var_pairs)))
          indices_pol<-c(match(c("eco_tax_rate", "eco_investment_subsidy", "eco_consumption_subsidy"), 
                                    colnames(list_of_var_pairs)))
        }
        
        if(exists("par_analysis") && par_analysis){
          indices<-c(indices, match("Parameter", colnames(list_of_var_pairs)))
        }
        
        if(exists("with_rand_learn") && with_rand_learn){
          indices<-c(indices, match(c("min_learning_coefficient_var", "learning_spillover_strength_var"), 
                           colnames(list_of_var_pairs) ))
        }
        
        indices<-indices[which(is.na(indices) != TRUE)]
        row<-1
        list_of_var_pairs[row, indices]<-t
        if(exists("indices_pol")){
          indices_pol<-indices_pol[which(is.na(indices_pol) != TRUE)]
          list_of_var_pairs[row, indices_pol]<-1040 
          # take for policy later date (policy starts from period 620 in EE revision)
        }
        
        if(((exists("with_rand_policy") && with_rand_policy == 1)||(exists("with_rand_learn") && with_rand_learn == 1)) && 
           exists("with_rand_barr") && with_rand_barr == 1 && exists("par_analysis") && par_analysis==0){
          indices_pol<-c(match(c("eco_tax_rate", "eco_investment_subsidy", "eco_consumption_subsidy", 
                                 "monthly_budget_balance", "min_learning_coefficient_var", "learning_spillover_strength_var"), 
                                                  colnames(list_of_var_pairs) ))
          indices_barr<-c(match(c("frontier_gap", "specific_skill_gap", "techn_frontier_conv", "average_s_skill_conv"),
                                colnames(list_of_var_pairs) ))
          indices_pol<-indices_pol[which(is.na(indices_pol) != TRUE)]
          indices_barr<-indices_barr[which(is.na(indices_barr) != TRUE)]
          
          row<-row+1
          list_of_var_pairs<-rbind(list_of_var_pairs, NA)
          list_of_var_pairs[row, indices_barr] <- 600
          list_of_var_pairs[row, indices_pol] <- max(t, 1040)
        }
        

        
      }else if(agent == "Firm"){

          indices<-c()
          row<-0
          
          if(with_rand_barr){
            indices_barr<-c(match(c("frontier_gap", "specific_skill_gap"), 
                                      colnames(list_of_var_pairs)))
          }
          
          if(exists("with_rand_policy") && with_rand_policy && (experiment_name!="eco_policy_switch" || exists("par_analysis") && par_analysis==0)){
            indices_pol<-c(match(c("eco_tax_rate", "eco_investment_subsidy", "eco_consumption_subsidy"), 
                                 colnames(list_of_var_pairs)))
          }
          
          if(exists("par_analysis") && par_analysis){
            indices<-c(indices, match("Parameter", colnames(list_of_var_pairs)))
          }
          
          if(exists("with_rand_learn") && with_rand_learn){
            indices<-c(indices, match(c("min_learning_coefficient_var", "learning_spillover_strength_var"), 
                                      colnames(list_of_var_pairs) ))
          }
          
          if(exists("with_rand_policy") && with_rand_policy == 1 && exists("with_rand_barr") && with_rand_barr == 1 
             && exists("par_analysis") && par_analysis==0){
            indices_pol<-unique(c(match(c("eco_tax_rate", "eco_investment_subsidy", "eco_consumption_subsidy"), 
                                        colnames(list_of_var_pairs) )))
            indices_barr<-unique(indices_barr, match(c("mean_spec_skills_conv", "technology_conv"),
                                                     colnames(list_of_var_pairs) ))
            indices_pol<-unique(indices_pol[which(is.na(indices_pol) != TRUE)])
            indices_barr<-unique(indices_barr[which(is.na(indices_barr) != TRUE)])
            indices<-match(c("frontier_gap", "specific_skill_gap"), colnames(list_of_var_pairs))
            indices<-indices[which(is.na(indices)==FALSE)]
            
            row<-row+1
            if(row>1){
              list_of_var_pairs<-rbind(list_of_var_pairs, NA)
            }
            list_of_var_pairs[row, indices_barr] <- max(t, 720)
            list_of_var_pairs[row, indices_pol] <- max(t, 1040)
            list_of_var_pairs[row, indices] <- max(t, 600)
          }else{
          
          indices<-unique(indices[which(is.na(indices) != TRUE)])
          row<-row + 1
          list_of_var_pairs[row, indices]<-t
          if(exists("indices_pol")){
            indices_pol<-indices_pol[which(is.na(indices_pol) != TRUE)]
            list_of_var_pairs[row, indices_pol]<-max(t, 1040)
          }
          list_of_var_pairs[row, indices_barr]<-max(720, t)

        
          } # if not rand pol and rand barr 
          
       }
          
      rm(row)
      
      
      w_temp<-nrow(list_of_var_pairs)
      
      w<-1
      for(w in 1:w_temp){ 
        
        print(paste("w in w_temp: ", w, "out of length: ", w_temp))
        main_controls<-list_of_var_pairs[w,]
        main_controls<-main_controls[which(is.na(main_controls)==FALSE)]
        
        print(paste("Var pair: ", paste(colnames(main_controls), collapse = " ")))
        names<-colnames(main_controls)
        times<-as.numeric(main_controls)
        
        fix_vars_temp<-match(c("id", "Periods", "Type", "Firm_Type", "Adopter_Type", "Run", "Parameter"), colnames(reg_data_temp))
        fix_vars_temp<-unique(fix_vars_temp[which(is.na(fix_vars_temp)== FALSE) ])
        
        target<-match(target_var_temp_const[j], colnames(reg_data_temp))
        indices_temp<-match(names, colnames(reg_data_temp))
        indices_temp<-unique(c(fix_vars_temp, indices_temp))
        indices_temp<-indices_temp[which(is.na(indices_temp) == FALSE)]
        indices_controls<-match(variables_init_cond_temp, colnames(reg_data_temp))
        indices_controls<-indices_controls[which(is.element(indices_controls, indices_temp)==FALSE)]
        indices_controls<-indices_controls[which(is.na(indices_controls)!=TRUE)]
        indx_run<-match("Run", colnames(reg_data_temp))
        reg_data_temp[,indx_run]<-as.factor(reg_data_temp[,indx_run])
        
        time<-unique(c(as.numeric(main_controls),t1, t))
        
        data<-reg_data_temp[which(as.numeric(reg_data_temp$Periods) %in% time), unique(c(target, indices_temp, indices_controls))]
        fix_vars_temp<-c("id", "Periods", "Firm_Type", "Adopter_Type", "Run", setdiff(c("Type", "Parameter"), names))
        fix_vars_temp<-match(fix_vars_temp, colnames(data))
        
        fix_vars_temp<-unique(fix_vars_temp[which(is.na(fix_vars_temp)== FALSE) ])
        
        
        number_controls<-length(names)
        if(number_controls>0){
          k<-1
          for(k in 1:number_controls){
            k1<-match(names[k], colnames(data))
            k2<-main_controls[1,k]
            eval(call("<-",as.name(paste("temp_rm_",k,sep="")),c(data[which(data$Periods == k2), k1])))
          }
          #rm(k, k1, k2)
          
          
          fix_vars_temp<-unique(c(fix_vars_temp, match(target_var_temp_const[j], colnames(data))))
          fix_vars_temp<-fix_vars_temp[which(is.na(fix_vars_temp) ==FALSE)]
          k1<-length(fix_vars_temp)
          
          
          for(k in 1:number_controls){
            if(k==1){
              dt<-cbind(data[which(as.character(data$Periods) == as.character(t1)),fix_vars_temp], eval(as.name(paste("temp_rm_",k, sep=""))))
            }else{
              dt<-cbind(dt, eval(as.name(paste("temp_rm_",k, sep=""))))
            }
            colnames(dt)[(k1+k)]<-names[k]
          } # k in number controls
   
          rm(k, k1)
          rm(list=ls(pattern="temp_rm_"))
          
          if(length(dt[which(is.na(dt[,target])==FALSE),target])>0.5*length(dt[,target])){
            
            ind<-match(names, colnames(dt))
            ind<-ind[is.na(ind)==FALSE]
            
            
            if(exists("par_analysis") && par_analysis==1 && j>1){
              make_many_plots<-0
            }
            
            if(exists("make_many_plots") && make_many_plots==1 && t1 == time_temp01[length(time_temp01)] && is.element(t, time_temp[c(1, floor(length(time_temp)/2), length(time_temp))])){
              
              dir_temp<-paste(dir_temp0,"/Scatterplots/",sep="")
              if(dir.exists(dir_temp)==FALSE){
                dir.create(dir_temp)
              }
              k2<-2
              for(k in 1:(number_controls-1)){
                if(var(dt[,match(names[k], colnames(dt))], na.rm = TRUE) > 0.00001 * mean(dt[,match(names[k], colnames(dt))], na.rm = TRUE)){
                  
                  
                  for(k1 in k2:(number_controls)){
                    if(k2 <= number_controls){
                      
                      if(var(dt[,match(names[k1], colnames(dt))], na.rm = TRUE) > 0.00001 * mean(dt[,match(names[k1], colnames(dt))], na.rm = TRUE)){
                        pair_title <- paste(names[k], times[k], names[k1], times[k1], collapse="_")
                        ind<-match(names[c(k, k1)], colnames(dt))
                        ind<-ind[is.na(ind)==FALSE]
                        min1<-min(dt[,ind[1]], na.rm=TRUE)
                        min2<-min(dt[,ind[2]], na.rm=TRUE)
                        max1<-max(dt[,ind[1]], na.rm=TRUE)
                        max2<-max(dt[,ind[2]], na.rm=TRUE)
                        
                        if(names[k1] == "frontier_gap" && names[k] == "specific_skill_gap"){
                          ylabel<-"% frontier gap"
                          xlabel<-"% specific skill gap"
                        }else{
                          xlabel<-names[k]
                          ylabel<-names[k1]
                        }
                        
                        if(exists("par_analysis") && par_analysis==1 && j==1){
                          pdf(paste(dir_temp,agent,"_",parameter,"_",pair_title,"_scatterplot_color_by_parameter_in_",t1,".pdf",sep=""))
                        }else if(exists("par_analysis")!=TRUE || par_analysis==0){
                          pdf(paste(dir_temp,agent,"_",parameter,"_",pair_title,"_scatterplot_color_",target_var_temp_const[j],"_in_",t1,".pdf",sep=""))
                        }
                        if(agent == "Eurostat"){
                          point_size <-0.5
                          alpha = 0.5
                        }else if(agent == "Firm"){
                          point_size = 0.25
                          alpha = 0.025
                        }
                        if(is.na(match(target_var_temp_const[j], colnames(dt))) == FALSE){
                          colnames(dt)[match(target_var_temp_const[j], colnames(dt))]<-"value"
                        }
                        p1<-ggplot(dt, aes(x=as.numeric(dt[,ind[1]]), y=as.numeric(dt[,ind[2]])))
                        if(exists("par_analysis") && par_analysis==1){
                          plot<-p1+geom_point(size=point_size, aes(color=as.factor(Parameter)), alpha=2*alpha)
                          if(length(parameters_tmp)==2 || experiment_name=="eco_policy_switch" ){
                            plot<-plot+scale_color_manual(name='', values=c('Baseline' = baseline, 'Experiment'=experiment, 'Consumption'=cons, 'Investment'=invest))
                          }else{
                            plot<-plot+scale_color_discrete()
                          }
                        }else if(is.element("value", colnames(dt))){
                          plot<-p1+geom_point(size=point_size, aes(color=value), alpha=alpha)+scale_colour_continuous(low=eco,  high=conv)
                        }else{
                          plot<-p1+geom_point(size=point_size, aes(color=Type))+scale_colour_manual(name='', values=c('eco' = eco, 'conv'=conv, 'switch'=switch))
                        }
                        
                        plot<-plot+ guides(colour = guide_legend(override.aes = list(shape = 15, alpha=1)))
                        plot<- plot + theme(legend.justification=c(0.95,0.95), legend.position="none", legend.title = element_blank()) 
                        plot<-plot +scale_x_continuous(limits=c(min1, max1)) + scale_y_continuous(limits=c(min2,max2)) + xlab(xlabel)+ylab(ylabel)
                        #plot<-plot +scale_x_discrete(breaks=seq(min1,max,0.01)) + scale_y_discrete(breaks=seq(min2,max2,0.01)) #+ xlab(names[k])+scale_colour_discrete("")+ylab(names[k1])
                        print(plot)
                        dev.off()
                        
                        
                        # Heat map
                        if(is.element("value", colnames(dt))){
                          pdf(paste(dir_temp,agent,"_",parameter,"_Heat_map_",pair_title,"_color_",target_var_temp_const[j],".pdf",sep=""))
                          
                          
                          # build your data.frame
                          #df <- data.frame(x=x, y=y, weight=weight)
                          
                          colnames(dt)[match(target_var_temp_const[j], colnames(dt))]<-"value"
                          indx<-match("value", colnames(dt))
                          if(agent=="Eurostat"){
                            df<-data.frame(x=as.numeric(dt[,ind[1]]),y=as.numeric(dt[,ind[2]]),weight=as.numeric(dt[,indx]))
                          }else if(agent=="Firm"){
                            df<-data.frame(x=as.numeric(dt[,ind[1]]),y=as.numeric(dt[,ind[2]]),weight=as.numeric(dt[,indx]))
                          }
                          myPalette <- colorRampPalette(rev(brewer.pal(11, "Spectral")), space="Lab")
                          
                          
                          # Plot
                          #p1<-ggplot(dt, aes(x=dt[,ind[1]], y=dt[ind[2]]), fill=..level..)
                          
                          plot<-ggplot(df, aes(x=df[,1],y=df[,2], fill=..level..) ) + 
                            stat_density_2d( bins=50, geom = "polygon") +
                            scale_fill_gradientn(colours = myPalette(5)) +
                            theme_minimal() +
                            #coord_fixed(ratio = 1) + 
                            scale_x_continuous() + scale_y_continuous() + xlab(xlabel) + ylab(ylabel)
                          plot<-plot + labs(fill = paste(target_var_temp_const[j])) + theme(legend.position="top", legend.title = element_text(size=8), axis.title = element_text(size=8))
                          
                          print(plot)
                          dev.off()
                        }
                        k2<-k2+1
                      }
                    }
                  } # k1 loop
                } # if var in dt[names[k]] not too small
              } # k loop 
              #rm(k, k1, k2)
              #rm(plot, p1, myPalette, df, indx, min1, min2, max1, max2)
              
            } # if make plots == 1
            
            # Get type array: 
            if(is.na(match("Type", colnames(reg_data_temp)))!=TRUE){
              type_temp<-as.factor(reg_data_temp[,match("Type", colnames(reg_data_temp))])
            }else{
              type_temp<-NA
            }
            if(is.na(match("Run", colnames(reg_data_temp)))!=TRUE){
              run_temp<-as.factor(reg_data_temp[,match("Run", colnames(reg_data_temp))])
            }else{
              run_temp<-NA
            }            
            
            dir_temp<-paste(dir_temp0,"/Regression_results/",sep="")
            if(dir.exists(dir_temp) == FALSE){
              dir.create(dir_temp)
            }
            
            
            
            target<-match(c("value",target_var_temp_const[j]), colnames(dt))
            target<-target[which(is.na(target)==FALSE)]
            colnames(dt)[target]<-paste(target_var_temp_const[j])
            min_target<-min(dt[,target], na.rm = TRUE)
            max_target<-max(dt[,target], na.rm = TRUE)
            
            other_controls0<-data[which(data$Periods == t), -c(fix_vars_temp)]
            
            indx_pol<-match(c("eco_tax_rate", "eco_investment_subsidy", "eco_consumption_subsidy"), colnames(data))
            indx_pol<-indx_pol[which(is.na(indx_pol)==FALSE)]
            if(with_policy && exists("indx_pol") && length(indx_pol)>0){
              pol_controls<-data[which(data$Periods == max(t, 1040)),indx_pol]
              indx_pol<-match(c("eco_tax_rate", "eco_investment_subsidy", "eco_consumption_subsidy"), colnames(other_controls0))
              other_controls0[indx_pol]<-pol_controls
              
            }
            
            
            
            
            
            filename<-paste(dir_temp,agent,"_",target_var_temp_const[j], "_in_",t1,"_",paste(names,collapse = "_"),"_in_",paste(unique(as.numeric(list_of_var_pairs[w,])), collapse="_"),"_contr_in_",t, sep = "")
            
            
            title<-paste("\n Regression results: ", target_var_temp_const[j], "in", t1, "separately on", paste(names, collapse=" ") , "\n \n ",collapse = " ")
            
            write(paste(title), file = paste(filename), append = FALSE)
            
            write.table(as.matrix(list_of_var_pairs[w,]), col.names=TRUE, row.names=FALSE, file=filename, append=TRUE,  eol="\n", fileEncoding = "")
            
            
            if(with_policy==1 && (target_var_temp_const[j]!="share_conventional_vintages_used" || t1<((number_xml*20)-500))){
              indx_type<-match("Type", colnames(dt))
              indx_pol<-match(c("eco_tax_rate", "eco_investment_subsidy", "eco_consumption_subsidy"), colnames(dt))
              
              
              model <- as.formula(dt[,target] ~ as.factor(dt[,indx_type])*as.matrix(dt[,indx_pol]))
              model_bin <- as.formula(round(dt[,target]) ~ as.factor(dt[,indx_type])*as.matrix(dt[,indx_pol]))
              title<-paste("\n Regression results policy-type interaction: ", target_var_temp_const[j], "in", t1, "ONLY on policy variables with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              
              c<-cbind(dt, other_controls0)
              
              indx_controls<-match(c("id", "Periods", "Firm_Type", "Adopter_Type", "Run", "Type", "eco_tax_rate", "eco_investment_subsidy", "eco_consumption_subsidy", target_var_temp_const[j]), colnames(c))
              indx_controls<-unique(indx_controls[which(is.na(indx_controls)==FALSE)])
              c<- as.matrix(c[-indx_controls])
              
              
              
              model <- as.formula(dt[,target] ~ as.factor(dt[,indx_type])*as.matrix(dt[,indx_pol]) + c)
              model_bin <- as.formula(round(dt[,target]) ~ as.factor(dt[,indx_type])*as.matrix(dt[,indx_pol]) + c)
              title<-paste("\n Regression results policy-type interaction and controls: ", target_var_temp_const[j], "in", t1, "ONLY on policy vars with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              
            }

            indx_type<-match("Type", colnames(dt))
            
            if(agent == "Firm" && is.element("Type", colnames(dt))){
              
              indx_type<-match("Type", colnames(dt))
              model <- as.formula(dt[,target] ~ as.factor(dt[,indx_type]))
              model_bin<-as.formula(round(dt[,target]) ~ as.factor(dt[,indx_type]))
              title<-paste("\n To which extent does scenario type explain ", target_var_temp_const[j], "in", t1, "? Model: ", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              
            } # if agent == Firm and type in colnames data
            k<-1
            for(k in 1:(number_controls)){
              
              ind<-match(names[k], colnames(dt))
              
              model <- as.formula(dt[,target] ~ dt[,ind])
              model_bin <- as.formula(round(dt[,target]) ~ dt[,ind])
              title<-paste("\n Regression results: ", target_var_temp_const[j], "in", t1, "ONLY on", names[k],"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              sq<-dt[,ind]**2
              model <- as.formula(dt[,target] ~ dt[,ind] + sq)
              model_bin <- as.formula(round(dt[,target]) ~ dt[,ind] + sq)
              title<-paste("\n Regression results: ", target_var_temp_const[j], "in", t1, "ONLY on", names[k],"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
                
                if(is.na(indx_type)!=TRUE && (target_var_temp_const[j]!="share_conv_capital_used" || t1 < (end_time/2))){
                  
                  c<-match(c("id", "Periods", "Firm_Type", "Adopter_Type", "Run", "Type", target_var_temp_const[j], names[k]), colnames(dt))
                  c<-unique(c[which(is.na(c)==FALSE)])
                  c<- as.matrix(dt[-c])
                    
                  model <- as.formula(dt[,target] ~ as.factor(dt[,indx_type]) :dt[,ind])
                  model_bin <- as.formula(round(dt[,target]) ~ as.factor(dt[,indx_type]):dt[,ind])
                  title<-paste("\n Regression results ONLY ON TYPE INTERACTION: ", target_var_temp_const[j], "in", t1, "ONLY on", names[k],"with", paste(model, collapse=" "),collapse = " ")
                  regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                  
                  model <- as.formula(dt[,target] ~ as.factor(dt[,indx_type]) :dt[,ind] + as.factor(dt[,indx_type]):sq)
                  model_bin <- as.formula(round(dt[,target]) ~ as.factor(dt[,indx_type]):dt[,ind]+ as.factor(dt[,indx_type]):sq)
                  title<-paste("\n Regression results ONLY ON INTERACTION: ", target_var_temp_const[j], "in", t1, "ONLY on", names[k],"with", paste(model, collapse=" "),collapse = " ")
                  regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                  
                  model <- as.formula(dt[,target] ~ as.factor(dt[,indx_type]) :dt[,ind] + dt[,ind])
                  model_bin <- as.formula(round(dt[,target]) ~ as.factor(dt[,indx_type]):dt[,ind] + dt[,ind] )
                  title<-paste("\n Regression results WITH TYPE INTERACTION and var: ", target_var_temp_const[j], "in", t1, "ONLY on", names[k],"with", paste(model, collapse=" "),collapse = " ")
                  regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                  
                  model <- as.formula(dt[,target] ~ as.factor(dt[,indx_type]) *dt[,ind] + as.factor(dt[,indx_type])*sq + dt[,ind] + sq)
                  model_bin <- as.formula(round(dt[,target]) ~ as.factor(dt[,indx_type]):dt[,ind]+ as.factor(dt[,indx_type]):sq + dt[,ind] + sq)
                  title<-paste("\n Regression results WITH TYPE INTERACTION and var: ", target_var_temp_const[j], "in", t1, "ONLY on", names[k],"with", paste(model, collapse=" "),collapse = " ")
                  regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                  
                  
                  model <- as.formula(dt[,target] ~ as.factor(dt[,indx_type]) *dt[,ind])
                  model_bin <- as.formula(round(dt[,target]) ~ as.factor(dt[,indx_type]) *dt[,ind])
                  title<-paste("\n Regression results WITH TYPE INTERACTION: ", target_var_temp_const[j], "in", t1, "ONLY on", names[k],"with", paste(model, collapse=" "),collapse = " ")
                  regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                  
                  model <- as.formula(dt[,target] ~ as.factor(dt[,indx_type]) *dt[,ind] + as.factor(dt[,indx_type])*sq)
                  model_bin <- as.formula(round(dt[,target]) ~ as.factor(dt[,indx_type]) *dt[,ind]+ as.factor(dt[,indx_type])*sq)
                  title<-paste("\n Regression results WITH TYPE INTERACTION: ", target_var_temp_const[j], "in", t1, "ONLY on", names[k],"with", paste(model, collapse=" "),collapse = " ")
                  regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                  
                  
                  model <- as.formula(dt[,target] ~ as.factor(dt[,indx_type]) *dt[,ind] + as.factor(dt[,indx_type])*sq + c)
                  model_bin <- as.formula(round(dt[,target]) ~ as.factor(dt[,indx_type]) *dt[,ind]+ as.factor(dt[,indx_type])*sq + c)
                  title<-paste("\n Regression results WITH TYPE INTERACTION AND ALL CONTROLS: ", target_var_temp_const[j], "in", t1, "ONLY on", names[k],"with", paste(model, collapse=" "),collapse = " ")
                  regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                  
                }# if control for type temp interaction
    
            }# end k in number controls -> single variable regression
            

            
            #if(1==2){
            i_temp<-c()
            if(exists("with_rand_policy") && with_rand_policy && (experiment_name!="eco_policy_switch" || exists("par_analysis") && par_analysis==0)){
              i_temp<-c(i_temp, "pol")
            }
            if(exists("with_rand_barr") && with_rand_barr==1){
              i_temp<-c(i_temp, "barr")
              if(agent=="Firm"){
                i_temp<-c(i_temp, "barr_firm")
                i_temp<-c(i_temp, "barr_firm2")
                i_temp<-c(i_temp, "barr_firm3")
              }else if(agent == "Eurostat"){
                i_temp<-c(i_temp, "barr_euro")
              }
            }
            
            i_temp<-c(i_temp, "names")
            i_temp<-"barr"
            
            i<-i_temp[1]
            for(i in i_temp){
              if(i == "names"){
                names_temp<-names
              }else if(i == "barr"){
                if(agent == "Firm"){
                  names_temp<-c("frontier_gap", "specific_skill_gap")
                }else{
                  names_temp<-c("frontier_gap", "specific_skill_gap")
                }
              }else if(i == "barr_euro"){
                names_temp<-c("frontier_gap", "specific_skill_gap", "techn_frontier_conv", "average_s_skill_conv")
              }else if(i == "barr_firm"){
                  names_temp<-c("technology_gap", "mean_specific_skill_gap", "technology_conv", "mean_spec_skills_conv")
              }else if(i == "barr_firm2"){
                names_temp<-c("frontier_gap", "mean_specific_skill_gap", "technology_conv", "mean_spec_skills_conv")
              }else if(i == "barr_firm3"){
                names_temp<-c("technology_gap", "specific_skill_gap", "technology_conv", "mean_spec_skills_conv")
              }else if(i == "pol"){
                names_temp<-c("eco_tax_rate", "eco_investment_subsidy", "eco_consumption_subsidy")
              }
              ind0<-c()
              if(agent == "Firm"){
                if(is.element("specific_skill_gap", names_temp) || is.element("mean_specific_skill_gap", names_temp)){
                  ind0<-c(ind0, match(c("mean_specific_skill_gap", "specific_skill_gap"), colnames(other_controls0)))
                }
                if(is.element("frontier_gap", names_temp) || is.element("technology_gap", names_temp)){
                  ind0<-c(ind0, match(c("frontier_gap", "technology_gap"), colnames(other_controls0)))
                }
              }
              
              ind0<-unique(c(ind0, match(names_temp, colnames(other_controls0))))
              ind0<-ind0[which(is.na(ind0) != TRUE)]
              other_controls<-other_controls0[,-ind0]
              indx_all<-match(names_temp, colnames(dt))
              
              sq<-as.matrix(dt[,indx_all])**2
              model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_all]))
              model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_all]))
              title<-paste("\n \n \n  NOW: Main vars in simple lin. model: ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_all])+sq)
              model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_all])+sq)
              title<-paste("\n \n \n  NOW: Main vars in simple lin. model: ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
        
              if((i == "barr" || i=="barr_firm" || i=="barr_firm2" || i=="barr_firm3" || i=="barr_euro")){
                indx_skill<-match(intersect(names_temp, c("specific_skill_gap","mean_specific_skill_gap", "mean_spec_skills_conv", "average_s_skill_conv")), colnames(dt))
                indx_front<-match(intersect(names_temp, c("frontier_gap", "technology_conv",  "techn_frontier_conv")), colnames(dt))
                indx_skill<-indx_skill[which(is.na(indx_skill)==FALSE)]
                indx_front<-indx_front[which(is.na(indx_front)==FALSE)]
                
                sq<-as.matrix(dt[,indx_skill])**2
                model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_skill]))
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_skill]))
                title<-paste("\n \n \n  NOW: only skill barr ", paste(colnames(dt)[c(indx_skill)], collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_skill])+sq)
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_skill])+sq)
                title<-paste("\n \n \n  NOW: only skill barr ", paste(colnames(dt)[c(indx_skill)], collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                
                sq<-as.matrix(dt[,indx_front])**2
                model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_front]))
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front]))
                title<-paste("\n \n \n  NOW: only front barr ", paste(colnames(dt[c(indx_front)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_front])+sq)
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front])+sq)
                title<-paste("\n \n \n  NOW: only front barr ", paste(colnames(dt[c(indx_front)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                
                
                
                if(with_rand_learn){
                  
                  indx_type<-match("Type", colnames(dt))
                  for(i2 in c(1:3)){
                    if(i2==1){
                      indx_learn<-match(c("min_learning_coefficient_var"), colnames(dt))
                    }else if(i2==2){
                      indx_learn<-match(c("learning_spillover_strength_var"), colnames(dt))
                    }else if(i2==3){
                      indx_learn<-match(c("min_learning_coefficient_var", "learning_spillover_strength_var"), colnames(dt))
                    }
                    
                    model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_learn]))
                    model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_learn]))
                    title<-paste("\n \n \n  NOW: only skill barr with learning interaction", paste(colnames(dt[c(indx_front, indx_learn)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                    regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                
                    model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_front])*as.matrix(dt[,indx_learn]))
                    model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front])*as.matrix(dt[,indx_learn]))
                    title<-paste("\n \n \n  NOW: only front barr with learning_interaction", paste(colnames(dt[c(indx_front, indx_learn)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                    regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                    
                    model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_front])*as.matrix(dt[,indx_learn]) + dt[,indx_front[1]]:dt[,indx_skill[1]])
                    model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front])*as.matrix(dt[,indx_learn])+ dt[,indx_front[1]]:dt[,indx_skill[1]])
                    title<-paste("\n \n \n  NOW: only front barr with learning_interaction and barr interaction term", paste(colnames(dt[c(indx_front, indx_learn)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                    regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                  
                    model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_learn]) + dt[,indx_front[1]]:dt[,indx_skill[1]] + as.factor(dt[,indx_type]): as.matrix(dt[,indx_learn]))
                    model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_learn])+ dt[,indx_front[1]]:dt[,indx_skill[1]]+ as.factor(dt[,indx_type]): as.matrix(dt[,indx_learn]))
                    title<-paste("\n \n \n  NOW: only skill barr with learning_interaction and type interaction", paste(colnames(dt[c(indx_front, indx_learn)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                    regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                    
                    model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_front])*as.matrix(dt[,indx_learn]) + dt[,indx_front[1]]:dt[,indx_skill[1]]+ as.factor(dt[,indx_type]):as.matrix(dt[,indx_learn]))
                    model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_learn])+ dt[,indx_front[1]]:dt[,indx_skill[1]]+ as.factor(dt[,indx_type])*as.matrix(dt[,indx_learn]))
                    title<-paste("\n \n \n  NOW: only front barr with learning_interaction and type interaction", paste(colnames(dt[c(indx_front, indx_learn)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                    regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                    
                    
                  } # end i2 in indx_learn
                } # end if with rand learn
                
                sq<-as.matrix(dt[,c(indx_front, indx_skill)])**2
                
                if(length(indx_front)>1 && length(indx_skill)>1){
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]:dt[,indx_skill[1]] +  dt[,indx_front[2]]:dt[,indx_skill[2]])
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]:dt[,indx_skill[1]] +  dt[,indx_front[2]]:dt[,indx_skill[2]])
                }else{
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]:dt[,indx_skill[1]])
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]:dt[,indx_skill[1]])
                }
                title<-paste("\n \n \n  NOW: barrs purely with interaction term ", paste(colnames(dt[c(indx_skill, indx_front)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                if(length(indx_front)>1 && length(indx_skill)>1){
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]:dt[,indx_skill[1]] +  dt[,indx_front[2]]:dt[,indx_skill[2]] + sq)
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]:dt[,indx_skill[1]] +  dt[,indx_front[2]]:dt[,indx_skill[2]]+ sq)
                }else{
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]:dt[,indx_skill[1]] + sq)
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]:dt[,indx_skill[1]] + sq)
                }
                title<-paste("\n \n \n  NOW: barrs purely with interaction term ", paste(colnames(dt[c(indx_skill, indx_front)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                
                
                if(length(indx_front)>1 && length(indx_skill)>1){
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]:dt[,indx_skill[1]] +  dt[,indx_front[2]]:dt[,indx_skill[2]] + as.matrix(other_controls))
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]:dt[,indx_skill[1]] +  dt[,indx_front[2]]:dt[,indx_skill[2]] + as.matrix(other_controls))
                }else{
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]:dt[,indx_skill[1]] + as.matrix(other_controls))
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]:dt[,indx_skill[1]] + as.matrix(other_controls))
                }
                title<-paste("\n \n \n  NOW: barrs purely with interaction term and controls", paste(colnames(dt[c(indx_skill, indx_front)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                if(length(indx_front)>1 && length(indx_skill)>1){
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]:dt[,indx_skill[1]] +  dt[,indx_front[2]]:dt[,indx_skill[2]] + as.matrix(other_controls) + sq)
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]:dt[,indx_skill[1]] +  dt[,indx_front[2]]:dt[,indx_skill[2]] + as.matrix(other_controls) + sq)
                }else{
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]:dt[,indx_skill[1]] + as.matrix(other_controls) + sq)
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]:dt[,indx_skill[1]] + as.matrix(other_controls) + sq)
                }
                title<-paste("\n \n \n  NOW: barrs purely with interaction term and controls", paste(colnames(dt[c(indx_skill, indx_front)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                
                
                if(length(indx_front)>1 && length(indx_skill)>1){
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]*dt[,indx_skill[1]] +  dt[,indx_front[2]]*dt[,indx_skill[2]])
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]*dt[,indx_skill[1]] +  dt[,indx_front[2]]*dt[,indx_skill[2]])
                }else{
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]*dt[,indx_skill[1]])
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]*dt[,indx_skill[1]])
                }
                title<-paste("\n \n \n  NOW: barrs with interaction term ", paste(colnames(dt[c(indx_skill, indx_front)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                if(length(indx_front)>1 && length(indx_skill)>1){
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]*dt[,indx_skill[1]] +  dt[,indx_front[2]]*dt[,indx_skill[2]] + sq)
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]*dt[,indx_skill[1]] +  dt[,indx_front[2]]*dt[,indx_skill[2]] + sq)
                }else{
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]*dt[,indx_skill[1]] + sq)
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]*dt[,indx_skill[1]] + sq)
                }
                title<-paste("\n \n \n  NOW: barrs with interaction term ", paste(colnames(dt[c(indx_skill, indx_front)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                
                
                
                if(length(indx_front)>1 && length(indx_skill)>1){
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]*dt[,indx_skill[1]] + dt[,indx_front[2]]*dt[,indx_skill[2]] + as.matrix(other_controls))
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]*dt[,indx_skill[1]] + dt[,indx_front[2]]*dt[,indx_skill[2]] + as.matrix(other_controls))
                }else{
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]*dt[,indx_skill[1]] + as.matrix(other_controls))
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]*dt[,indx_skill[1]] + as.matrix(other_controls))
                }
                title<-paste("\n \n \n  NOW: barrs with interaction term and controls", paste(colnames(dt[c(indx_skill, indx_front)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                if(length(indx_front)>1 && length(indx_skill)>1){
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]*dt[,indx_skill[1]] + dt[,indx_front[2]]*dt[,indx_skill[2]] + as.matrix(other_controls) + sq)
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]*dt[,indx_skill[1]] + dt[,indx_front[2]]*dt[,indx_skill[2]] + as.matrix(other_controls) + sq)
                }else{
                  model <- as.formula(dt[,target] ~ dt[,indx_front[1]]*dt[,indx_skill[1]] + as.matrix(other_controls) + sq)
                  model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]]*dt[,indx_skill[1]] + as.matrix(other_controls) + sq)
                }
                title<-paste("\n \n \n  NOW: barrs with interaction term and controls", paste(colnames(dt[c(indx_skill, indx_front)]), collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
                regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
                
              } # if i == "barr"
              model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_all]) + as.matrix(other_controls))
              model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_all]) + as.matrix(other_controls))
              title<-paste("\n \n \n  Regression results: ", target_var_temp_const[j], "in", t1, "on ALL controls in simple lin. model ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_all]) + as.matrix(other_controls) + sq)
              model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_all]) + as.matrix(other_controls) + sq)
              title<-paste("\n \n \n  Regression results: ", target_var_temp_const[j], "in", t1, "on ALL controls in simple lin. model ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
            } # end loop through reg on all policy, barr and all main controls
            
            #}# end loop names
            if(exists("with_rand_barr") && with_rand_barr==1){
              
              
              if(agent == "Firm"){
                if(w==2){
                  iterate_temp<-c(1:4)
                }else if(w==1){
                  iterate_temp<-c(1,3)
                }
              }else{
                iterate_temp<-1
              }
              
              for(i1 in iterate_temp){
                if(agent == "Eurostat"){
                  indx_skill<-match("specific_skill_gap", colnames(dt))
                  indx_front<-match("frontier_gap", colnames(dt))
                }else if(agent == "Firm"){
                  if(i1 == 1){
                    indx_skill<-match("mean_specific_skill_gap", colnames(dt))
                    indx_front<-match("technology_gap", colnames(dt))
                  }else if(i1 == 2){
                    indx_skill<-match(c("mean_specific_skill_gap", "mean_spec_skills_conv"), colnames(dt))
                    indx_front<-match(c("technology_gap","technology_conv"), colnames(dt))
                  }else if(i1 == 3){
                    indx_skill<-match("specific_skill_gap", colnames(dt))
                    indx_front<-match("frontier_gap", colnames(dt))
                  }else if(i1 == 4){
                    indx_skill<-match(c("specific_skill_gap","mean_spec_skills_conv"), colnames(dt))
                    indx_front<-match(c("frontier_gap","technology_conv"), colnames(dt))
                  }
                }
                
                
                if(is.element(NA, c(indx_front,indx_skill))){
                  next
                }
                indx_type<-match("Type", colnames(dt))
                type_vec<-as.factor(dt[,indx_type])
                
                
                
                title<-paste("\n \n \n  NOW: indx_skill given by ", paste(colnames(dt)[indx_skill], collapse=""), "and indx_front: ", paste(colnames(dt)[indx_front], collapse=""))
                write(paste(title), file = paste(filename), append = TRUE)
                
              if(with_rand_policy){
                indx_pol<-match(c("eco_tax_rate", "eco_investment_subsidy", "eco_consumption_subsidy"), colnames(dt))
                ind0<-unique(c(colnames(dt[c(indx_front, indx_skill)]), "eco_tax_rate", "eco_investment_subsidy", "eco_consumption_subsidy", "specific_skill_gap", 
                      "frontier_gap", "technology_gap", "mean_specific_skill_gap"))
              }else if(with_rand_learn){
                indx_pol<-match(c("min_learning_coefficient_var", "learning_spillover_strength_var"), colnames(dt))
                ind0<-unique(c(colnames(dt[c(indx_front, indx_skill)]), "min_learning_coefficient_var", "learning_spillover_strength_var", "specific_skill_gap", 
                               "frontier_gap", "technology_gap", "mean_specific_skill_gap"))
              }else{
                indx_pol<-c()
                ind0<-unique(c(colnames(dt[c(indx_front, indx_skill)]), "specific_skill_gap", 
                               "frontier_gap", "technology_gap", "mean_specific_skill_gap"))
              }
              ind0<-match(ind0, colnames(other_controls0))
              ind0<-ind0[which(is.na(ind0) != TRUE)]
              other_controls<-other_controls0[,-ind0]
              
              
              if(length(indx_front)>1){
                model <- as.formula(dt[,target] ~ dt[,indx_front[1]] * dt[,indx_skill[1]] + dt[,indx_front[2]] * dt[,indx_skill[2]])
                model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]] * dt[,indx_skill[1]] + dt[,indx_front[2]] * dt[,indx_skill[2]])
              }else{
                model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_front]) * as.matrix(dt[,indx_skill]))
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front]) * as.matrix(dt[,indx_skill]))
              }
             
              title<-paste("\n \n \n indx_skill given by ", paste(colnames(dt)[indx_skill], collapse=" "), "and indx_front: ", paste(colnames(dt)[indx_front], collapse=" "))
              write(paste(title), file = paste(filename), append = TRUE)
              title<-paste("\n  Regression results: ", target_var_temp_const[j], "in", t1, "only barriers ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
 
              if(length(indx_pol)>0){
                
              model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_pol]) )
              model_bin<-as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_pol]) )
              title<-paste("\n \n \n  Regression results: ", target_var_temp_const[j], "in", t1, "only policy ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              
              model <- as.formula(dt[,target] ~ type_vec*as.matrix(dt[,indx_pol]) )
              model_bin<-as.formula(round(dt[,target]) ~ type_vec*as.matrix(dt[,indx_pol]) )
              title<-paste("\n \n \n  Regression results: ", target_var_temp_const[j], "in", t1, "only policy WITH TYPE INTERACTION", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              if(length(indx_front)>1){
                model <- as.formula((dt[,target]) ~ dt[,indx_front[1]] : dt[,indx_skill[1]] + dt[,indx_front[2]] : dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front]))
                model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]] : dt[,indx_skill[1]] + dt[,indx_front[2]] : dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front]))
              }else{
                model <- as.formula((dt[,target]) ~ as.matrix(dt[,indx_front]) : as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front]))
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front]) : as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front]))
              }
              title<-paste("\n \n \n indx_skill given by ", paste(colnames(dt)[indx_skill], collapse=" "), "and indx_front: ", paste(colnames(dt)[indx_front], collapse=" "))
              write(paste(title), file = paste(filename), append = TRUE)
              title<-paste("\n  Regression results: ", target_var_temp_const[j], "in", t1, "both but not interacting ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              sq<-as.matrix(dt[,c(indx_front, indx_skill)]**2)
              if(length(indx_front)>1){
                model <- as.formula((dt[,target]) ~ dt[,indx_front[1]] : dt[,indx_skill[1]] + dt[,indx_front[2]] : dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front]) + sq)
                model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]] : dt[,indx_skill[1]] + dt[,indx_front[2]] : dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front]) + sq)
              }else{
                model <- as.formula((dt[,target]) ~ as.matrix(dt[,indx_front]) : as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front]) + sq)
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front]) : as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front]) + sq)
              }
              title<-paste("\n \n \n indx_skill given by ", paste(colnames(dt)[indx_skill], collapse=" "), "and indx_front: ", paste(colnames(dt)[indx_front], collapse=" "))
              write(paste(title), file = paste(filename), append = TRUE)
              title<-paste("\n  Regression results: ", target_var_temp_const[j], "in", t1, "both but not interacting ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              
              if(length(indx_front)>1){
                model <- as.formula((dt[,target]) ~ dt[,indx_front[1]] : dt[,indx_skill[1]] + dt[,indx_front[2]] : dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_skill]))
                model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]] : dt[,indx_skill[1]] + dt[,indx_front[2]] : dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol])+ as.matrix(dt[,indx_skill]))
              }else{
                model <- as.formula((dt[,target]) ~ as.matrix(dt[,indx_front]) : as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol])+ as.matrix(dt[,indx_skill]))
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front]) : as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol])+ as.matrix(dt[,indx_skill]))
              }
              title<-paste("\n \n \n indx_skill given by ", paste(colnames(dt)[indx_skill], collapse=" "), "and indx_front: ", paste(colnames(dt)[indx_front], collapse=" "))
              write(paste(title), file = paste(filename), append = TRUE)
              title<-paste("\n  Regression results: ", target_var_temp_const[j], "in", t1, "both but not interacting ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              
              if(length(indx_front)>1){
                model <- as.formula((dt[,target]) ~ dt[,indx_front[1]] : dt[,indx_skill[1]] + dt[,indx_front[2]] : dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_skill]) + sq)
                model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]] : dt[,indx_skill[1]] + dt[,indx_front[2]] : dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol])+ as.matrix(dt[,indx_skill])+ sq)
              }else{
                model <- as.formula((dt[,target]) ~ as.matrix(dt[,indx_front]) : as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol])+ as.matrix(dt[,indx_skill])+ sq)
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front]) : as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol])+ as.matrix(dt[,indx_skill])+ sq)
              }
              title<-paste("\n \n \n indx_skill given by ", paste(colnames(dt)[indx_skill], collapse=" "), "and indx_front: ", paste(colnames(dt)[indx_front], collapse=" "))
              write(paste(title), file = paste(filename), append = TRUE)
              title<-paste("\n  Regression results: ", target_var_temp_const[j], "in", t1, "both but not interacting ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
                                        
              if(length(indx_front)>1){
                model <- as.formula((dt[,target]) ~ dt[,indx_front[1]] : dt[,indx_skill[1]] + dt[,indx_front[2]] : dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol]))
                model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]] : dt[,indx_skill[1]] + dt[,indx_front[2]] : dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol]))
              }else{
                model <- as.formula((dt[,target]) ~ as.matrix(dt[,indx_front]) : as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol]))
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front]) : as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol]))
              }
              title<-paste("\n \n \n indx_skill given by ", paste(colnames(dt)[indx_skill], collapse=" "), "and indx_front: ", paste(colnames(dt)[indx_front], collapse=" "))
              write(paste(title), file = paste(filename), append = TRUE)
              title<-paste("\n  Regression results: ", target_var_temp_const[j], "in", t1, "both but not interacting ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)

              
              
              if(length(indx_front)>1){
                model <- as.formula((dt[,target]) ~ dt[,indx_front[1]] : dt[,indx_skill[1]] + dt[,indx_front[2]] : dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol]) + sq)
                model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]] : dt[,indx_skill[1]] + dt[,indx_front[2]] : dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol]) + sq)
              }else{
                model <- as.formula((dt[,target]) ~ as.matrix(dt[,indx_front]) : as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol]) + sq)
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front]) : as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol]) + sq)
              }
              title<-paste("\n \n \n indx_skill given by ", paste(colnames(dt)[indx_skill], collapse=" "), "and indx_front: ", paste(colnames(dt)[indx_front], collapse=" "))
              write(paste(title), file = paste(filename), append = TRUE)
              title<-paste("\n  Regression results: ", target_var_temp_const[j], "in", t1, "both but not interacting ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              
              
              if(length(indx_front)>1){
                model <- as.formula((dt[,target]) ~ dt[,indx_front[1]] * dt[,indx_skill[1]] + dt[,indx_front[2]] * dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol]))
                model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]] * dt[,indx_skill[1]] + dt[,indx_front[2]] * dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol]))
              }else{
                model <- as.formula((dt[,target]) ~ as.matrix(dt[,indx_front]) * as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol]))
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front]) * as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol]))
              }
              title<-paste("\n \n \n indx_skill given by ", paste(colnames(dt)[indx_skill], collapse=" "), "and indx_front: ", paste(colnames(dt)[indx_front], collapse=" "))
              write(paste(title), file = paste(filename), append = TRUE)
              title<-paste("\n  Regression results: ", target_var_temp_const[j], "in", t1, "both but not interacting ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              
              if(length(indx_front)>1){
                model <- as.formula((dt[,target]) ~ dt[,indx_front[1]] * dt[,indx_skill[1]] + dt[,indx_front[2]] * dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol]) + sq)
                model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_front[1]] * dt[,indx_skill[1]] + dt[,indx_front[2]] * dt[,indx_skill[2]]+ as.matrix(dt[,indx_pol]) + sq)
              }else{
                model <- as.formula((dt[,target]) ~ as.matrix(dt[,indx_front]) * as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol]) + sq)
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front]) * as.matrix(dt[,indx_skill]) + as.matrix(dt[,indx_pol]) + sq)
              }
              title<-paste("\n \n \n indx_skill given by ", paste(colnames(dt)[indx_skill], collapse=" "), "and indx_front: ", paste(colnames(dt)[indx_front], collapse=" "))
              write(paste(title), file = paste(filename), append = TRUE)
              title<-paste("\n  Regression results: ", target_var_temp_const[j], "in", t1, "both but not interacting ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              
              title<-paste("\n \n \n indx_skill given by ", paste(colnames(dt)[indx_skill], collapse=" "), "and indx_front: ", paste(colnames(dt)[indx_front], collapse=" "))
              write(paste(title), file = paste(filename), append = TRUE)
              
              if(length(indx_front)>1){
                model <- as.formula(dt[,target] ~ dt[,indx_skill[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]] * dt[,indx_skill[1]] + dt[,indx_front[2]] * dt[,indx_skill[2]] + sq)
                model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_skill[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]] * dt[,indx_skill[1]] + dt[,indx_front[2]] * dt[,indx_skill[2]]+ sq)
              }else{
                model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_pol]) +as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_front])+ sq)
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_pol]) +as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_front])+ sq)
              }
              title<-paste("\n  Regression results: ", target_var_temp_const[j], "in", t1, "WITH POLICY BARRIER INTERACTION ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              title<-paste("\n \n \n indx_skill given by ", paste(colnames(dt)[indx_skill], collapse=" "), "and indx_front: ", paste(colnames(dt)[indx_front], collapse=" "))
              write(paste(title), file = paste(filename), append = TRUE)
              
              if(length(indx_front)>1){
                model <- as.formula(dt[,target] ~ dt[,indx_skill[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]] * dt[,indx_skill[1]] + dt[,indx_front[2]] * dt[,indx_skill[2]])
                model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_skill[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]] * dt[,indx_skill[1]] + dt[,indx_front[2]] * dt[,indx_skill[2]])
              }else{
                model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_pol]) +as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_front]))
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_pol]) +as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_front]))
              }
              title<-paste("\n  Regression results: ", target_var_temp_const[j], "in", t1, "WITH POLICY BARRIER INTERACTION ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              

              if(length(indx_front)>1){
                model <- as.formula(dt[,target] ~ dt[,indx_skill[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]] * dt[,indx_skill[1]] + dt[,indx_front[2]] * dt[,indx_skill[2]] + as.matrix(other_controls))
                model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_skill[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]] * dt[,indx_skill[1]] + dt[,indx_front[2]] * dt[,indx_skill[2]] + as.matrix(other_controls))
              }else{
                model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_skill]) + as.matrix(other_controls))
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_skill]) + as.matrix(other_controls))
              }
              ols<-lm(model)
              
              title<-paste("\n \n \n indx_skill given by ", paste(colnames(dt)[indx_skill], collapse=" "), "and indx_front: ", paste(colnames(dt)[indx_front], collapse=" "))
              write(paste(title), file = paste(filename), append = TRUE)
              title<-paste("\n  Regression results: ", target_var_temp_const[j], "in", t1, "WITH POLICY BARRIER INTERACTION and controls ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              
              
              if(length(indx_front)>1){
                model <- as.formula(dt[,target] ~ dt[,indx_skill[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]] * dt[,indx_skill[1]] + dt[,indx_front[2]] * dt[,indx_skill[2]] + as.matrix(other_controls) + sq)
                model_bin <- as.formula(round(dt[,target]) ~ dt[,indx_skill[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]]*as.matrix(dt[,indx_pol]) + dt[,indx_front[1]] * dt[,indx_skill[1]] + dt[,indx_front[2]] * dt[,indx_skill[2]] + as.matrix(other_controls) + sq)
              }else{
                model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_skill]) + as.matrix(other_controls) + sq)
                model_bin <- as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_pol]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_skill]) + as.matrix(other_controls) + sq)
              }
              ols<-lm(model)
              
              title<-paste("\n \n \n indx_skill given by ", paste(colnames(dt)[indx_skill], collapse=" "), "and indx_front: ", paste(colnames(dt)[indx_front], collapse=" "))
              write(paste(title), file = paste(filename), append = TRUE)
              title<-paste("\n  Regression results: ", target_var_temp_const[j], "in", t1, "WITH POLICY BARRIER INTERACTION and controls ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              regr_and_capt_output(model, model_bin, title, filename, min_target, max_target, app=TRUE)
              
              

              } # if length(indx_pol)>0
              } # if i1 through iterate_temp
              } # if rand barr
            #}
            
            if((exists("with_rand_learn") && with_rand_learn )
               &&(exists("with_rand_barr") && with_rand_barr==1)){
              
              if(agent == "Eurostat"){
                indx_skill<-match("specific_skill_gap", colnames(dt))
                indx_front<-match("frontier_gap", colnames(dt))
              }else if(agent == "Firm"){
                indx_skill<-match("mean_specific_skill_gap", colnames(dt))
                indx_front<-match("technology_gap", colnames(dt))
              }
              
              indx_learn<-match(c("min_learning_coefficient_var", "learning_spillover_strength_var"), colnames(dt))
              indx_min<-match("min_learning_coefficient_var", colnames(dt))
              indx_spill<-match("learning_spillover_strength_var", colnames(dt))
              
              ind0<-match(c("min_learning_coefficient_var", "learning_spillover_strength_var", "specific_skill_gap", 
                            "frontier_gap", "technology_gap", "mean_specific_skill_gap" ), colnames(other_controls0))
              ind0<-ind0[which(is.na(ind0) != TRUE)]
              other_controls<-other_controls0[,-ind0]
              
              model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_learn]) )
              ols<-lm(model)
              
              title<-paste("\n \n \n  Regression results: ", target_var_temp_const[j], "in", t1, "only learning ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              write(paste(title), file = paste(filename), append = TRUE)
              
              capture.output(summary(ols, digits=4), file=filename, append = TRUE)
              
              
              if(agent != "Firm" && 1==2 && (range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0)){
                gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(gama), file=filename, append = TRUE)
              }
              if(min_target >= 0 && max_target <= 1){
                model<-as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_learn]) )
                logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                
                capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(logit), file=filename, append = TRUE)
                capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(probit), file=filename, append = TRUE)
              }
              
              model <- as.formula(dt[,target] ~ as.factor(dt[,indx_type]):as.matrix(dt[,indx_learn]) )
              ols<-lm(model)
              
              title<-paste("\n \n \n  Regression results: ", target_var_temp_const[j], "in", t1, "only learning ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              write(paste(title), file = paste(filename), append = TRUE)
              
              capture.output(summary(ols, digits=4), file=filename, append = TRUE)
              
              
              if(agent != "Firm" && 1==2 && (range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0)){
                gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(gama), file=filename, append = TRUE)
              }
              if(min_target >= 0 && max_target <= 1){
                model<-as.formula(round(dt[,target]) ~ as.factor(dt[,indx_type]):as.matrix(dt[,indx_learn]) )
                logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                
                capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(logit), file=filename, append = TRUE)
                capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(probit), file=filename, append = TRUE)
              }
              
              model <- as.formula(dt[,target] ~ dt[,indx_spill]*dt[,indx_min] )
              ols<-lm(model)
              
              title<-paste("\n \n \n  Regression results: ", target_var_temp_const[j], "in", t1, "only learning and interaction", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              write(paste(title), file = paste(filename), append = TRUE)
              
              capture.output(summary(ols, digits=4), file=filename, append = TRUE)
              
              
              if(agent != "Firm" && 1==2 && (range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0)){
                gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(gama), file=filename, append = TRUE)
              }
              if(min_target >= 0 && max_target <= 1){
                model<-as.formula(round(dt[,target]) ~ dt[,indx_spill]*dt[,indx_min]  )
                logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                
                capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(logit), file=filename, append = TRUE)
                capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(probit), file=filename, append = TRUE)
              }
              
              
              model <- as.formula(dt[,target] ~ as.factor(dt[,indx_type]):dt[,indx_spill]*dt[,indx_min] )
              ols<-lm(model)
              
              title<-paste("\n \n \n  Regression results: ", target_var_temp_const[j], "in", t1, "only learning and interaction", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              write(paste(title), file = paste(filename), append = TRUE)
              
              capture.output(summary(ols, digits=4), file=filename, append = TRUE)
              
              
              if(agent != "Firm" && 1==2 && (range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0)){
                gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(gama), file=filename, append = TRUE)
              }
              if(min_target >= 0 && max_target <= 1){
                model<-as.formula(round(dt[,target]) ~ as.factor(dt[,indx_type]):dt[,indx_spill]*dt[,indx_min]  )
                logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                
                capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(logit), file=filename, append = TRUE)
                capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(probit), file=filename, append = TRUE)
              }
              
              
              
              
              model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_front]) * as.matrix(dt[,indx_skill]))
              ols<-lm(model)
              
              title<-paste("\n \n \n  Regression results: ", target_var_temp_const[j], "in", t1, "only barriers ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              write(paste(title), file = paste(filename), append = TRUE)
              
              capture.output(summary(ols, digits=4), file=filename, append = TRUE)
              
              
              if(agent != "Firm" && 1==2 && (range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0)){
                gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(gama), file=filename, append = TRUE)
              }
              if(min_target >= 0 && max_target <= 1){
                model<-as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front]) * as.matrix(dt[,indx_skill]) )
                logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                
                capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(logit), file=filename, append = TRUE)
                capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(probit), file=filename, append = TRUE)
              }
              
              model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_front]) * as.matrix(dt[,indx_skill]) + dt[,indx_spill]*dt[,indx_min] + as.matrix(dt[,indx_learn]) )
              ols<-lm(model)
              
              title<-paste("\n \n \n  Regression results: ", target_var_temp_const[j], "in", t1, "both but not interacting ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              write(paste(title), file = paste(filename), append = TRUE)
              
              capture.output(summary(ols, digits=4), file=filename, append = TRUE)
              
              
              if(agent != "Firm" && 1==2 && (range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0)){
                gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(gama), file=filename, append = TRUE)
              }
              if(min_target >= 0 && max_target <= 1){
                model<-as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_front]) * as.matrix(dt[,indx_skill]) + dt[,indx_spill]*dt[,indx_min] + as.matrix(dt[,indx_learn]) )
                logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                
                capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(logit), file=filename, append = TRUE)
                capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(probit), file=filename, append = TRUE)
              }
              
              
              
              model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_learn]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_learn]) +as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_front]))
              ols<-lm(model)
              
              title<-paste("\n \n \n  Regression results: ", target_var_temp_const[j], "in", t1, "WITH POLICY BARRIER INTERACTION ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              write(paste(title), file = paste(filename), append = TRUE)
              
              capture.output(summary(ols, digits=4), file=filename, append = TRUE)
              
              
              if(agent != "Firm" && 1==2 && (range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0)){
                gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(gama), file=filename, append = TRUE)
              }
              if(min_target >= 0 && max_target <= 1){
                model<-as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_learn]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_learn])+as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_front]))
                logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                
                capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(logit), file=filename, append = TRUE)
                capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(probit), file=filename, append = TRUE)
              }
              
              
              
              model <- as.formula(dt[,target] ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_learn]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_learn]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_skill]) + as.matrix(other_controls))
              ols<-lm(model)
              
              title<-paste("\n \n \n  Regression results: ", target_var_temp_const[j], "in", t1, "WITH POLICY BARRIER INTERACTION and controls ", paste(names_temp, collapse = " + "),"with", paste(model, collapse=" "),collapse = " ")
              write(paste(title), file = paste(filename), append = TRUE)
              
              capture.output(summary(ols, digits=4), file=filename, append = TRUE)
              
              
              if(agent != "Firm" && 1==2 && (range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0)){
                gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(gama), file=filename, append = TRUE)
              }
              if(min_target >= 0 && max_target <= 1){
                model<-as.formula(round(dt[,target]) ~ as.matrix(dt[,indx_skill])*as.matrix(dt[,indx_learn]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_learn]) + as.matrix(dt[,indx_front])*as.matrix(dt[,indx_skill]) + as.matrix(other_controls))
                logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                
                capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(logit), file=filename, append = TRUE)
                capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                capture.output(PseudoR2(probit), file=filename, append = TRUE)
              }
              
              
            } # if with rand learn and rand barr
            
            if(1==2){
            title<-paste("\n \n ##########################################################################\n \n ",collapse = " ")
            
            write(paste(title), file = paste(filename), append = TRUE)
            
                    
            title<-paste("NOW: INTERACTION MODELS: ", target_var_temp_const[j], "in", t1, "on", paste(names, collapse=" ") , "\n \n \n ",collapse = " ")
            
            write(paste(title), file = paste(filename), append = TRUE)
            
            
            indx_type<-match("Type", colnames(dt))
            
            
            for(k in 1:(number_controls-1)){
              if(var(dt[,match(names[k], colnames(dt))], na.rm = TRUE) > 0.0001* mean(dt[,ind[1]], na.rm = TRUE)){
                
                k2<-k+1
                
                for(k1 in k2:(number_controls)){
                  ind<-match(names[c(k, k1)], colnames(dt))
                  ind<-ind[is.na(ind)==FALSE]
                  
                  if(var(dt[,ind[2]], na.rm = TRUE) > 0.0001 * mean(dt[,ind[2]], na.rm = TRUE) && var(dt[,target], na.rm = TRUE) > 0.0001 * mean(dt[,target], na.rm = TRUE)){
                    
                    ind0<-match(names[c(k, k1)], colnames(other_controls0))
                    ind0<-ind0[which(is.na(ind0) != TRUE)]
                    other_controls<-other_controls0[,-ind0]
                    indx_all<-match(names, colnames(dt))
                    
                    
                    model <- as.formula(dt[,target] ~ dt[,ind[1]] + dt[,ind[2]])
                    ols<-lm(model)
                    #ols<-lm(dt[,target] ~ as.matrix(dt[,ind]) + dt[,ind[1]] * dt[,ind[2]])
                    title<-paste("\n Regression results: ", target_var_temp_const[j], "on BOTH ",names[k],  names[k1],"with", paste(model, collapse=" "),collapse = " ")
                    write(paste(title), file = paste(filename), append = TRUE)
                    
                    capture.output(summary(ols, digits=4), file=filename, append = TRUE)
                    
                    
                    if(agent != "Firm" && 1==2 && (range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0)){
                      gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                      capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                      capture.output(PseudoR2(gama), file=filename, append = TRUE)
                    }
                    if(min_target >= 0 && max_target <= 1){
                      model<-as.formula(round(dt[,target]) ~ dt[,ind[1]] + dt[,ind[2]])
                      logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                      probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                      
                      capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                      capture.output(PseudoR2(logit), file=filename, append = TRUE)
                      capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                      capture.output(PseudoR2(probit), file=filename, append = TRUE)
                    }
                    
                    
                    model <- as.formula(dt[,target] ~ dt[,ind[1]] * dt[,ind[2]])
                    ols<-lm(model)
                    #ols<-lm(dt[,target] ~ as.matrix(dt[,ind]) + dt[,ind[1]] * dt[,ind[2]])
                    title<-paste("\n Regression results: ", target_var_temp_const[j], "in",t1,"on", names[k], names[k1],"and interaction with", paste(model, collapse=" "),collapse = " ")
                    
                    write(paste(title), file = paste(filename), append = TRUE)
                    
                    capture.output(summary(ols, digits=4), file=filename, append = TRUE)
                    
                    
                    if(agent != "Firm" && 1==2 && (range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0)){
                      gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                      capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                      capture.output(PseudoR2(gama), file=filename, append = TRUE)
                    }
                    if(min_target >= 0 && max_target <= 1){
                      model<-as.formula(round(dt[,target]) ~ dt[,ind[1]] * dt[,ind[2]])
                      logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                      probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                      
                      capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                      capture.output(PseudoR2(logit), file=filename, append = TRUE)
                      capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                      capture.output(PseudoR2(probit), file=filename, append = TRUE)
                    }
                    
                    
                    dt<-cbind(dt, other_controls)
                    indx_all<-match(names, colnames(df_da<fta))
                    
                    ind2<-match(colnames(other_controls), colnames(dt))
                    model <- as.formula(dt[,target] ~ dt[,ind[1]] * dt[,ind[2]] + as.matrix(other_controls))
                    ols<-lm(model)
                    #ols<-lm(dt[,target] ~ as.matrix(dt[,ind]) + dt[,ind[1]] * dt[,ind[2]])
                    title<-paste("\n Regression results: ", target_var_temp_const[j], "in",t1,"on", names[k], names[k1],"and CONTROLS with", paste(model, collapse=" "),collapse = " ")
       
                    write(paste(title), file = paste(filename), append = TRUE)
                    
                    capture.output(summary(ols, digits=4), file=filename, append = TRUE)
                    
                    if(agent != "Firm" && 1==2 && min_target>0 && 1==2){
                      #start<- rep(0, (4+length(other_controls)))
                      gama<-glm(model, family=Gamma(link = "log"),  maxit = 100 )
                      capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                      capture.output(PseudoR2(gama), file=filename, append = TRUE)
                    }
                    
                    if(min_target >= 0 && max_target <= 1){
                      model<-as.formula(round(dt[,target]) ~ dt[,ind[1]] * dt[,ind[2]] + as.matrix(other_controls))
                      logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                      probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                      
                      capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                      capture.output(PseudoR2(logit), file=filename, append = TRUE)
                      capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                      capture.output(PseudoR2(probit), file=filename, append = TRUE)
                    }
                    
                    if(is.na(indx_type) != TRUE && target_var_temp_const != "share_conv_capital_used" && end_time < (0.8*(number_xml*20))){
                      
                      dt<-cbind(dt, other_controls)
                      ind2<-match(colnames(other_controls), colnames(dt))
                      indx_all<-match(names, colnames(dt))
                      
                      model <- as.formula(dt[,target] ~ as.factor(dt[,indx_type]) *as.matrix(dt[,ind]) + dt[,ind[1]] * dt[,ind[2]] + as.factor(dt[,indx_type]) *as.matrix(other_controls))
                      ols<-lm(model)
                      #ols<-lm(dt[,target] ~ as.matrix(dt[,ind]) + dt[,ind[1]] * dt[,ind[2]])
                      title<-paste("\n Regression results: ", target_var_temp_const[j], "in",t1,"on", names[k], names[k1],"with", paste(model, collapse=" "),collapse = " ")
                      if(k==1 && k1==1){
                        write(paste(title), file = paste(filename), append = FALSE)
                      }else{
                        write(paste(title), file = paste(filename), append = TRUE)
                      }
                      capture.output(summary(ols, digits=4), file=filename, append = TRUE)
                      
                      if(agent != "Firm" && 1==2 && min_target>0 && 1==2){
                        gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                        capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                        capture.output(PseudoR2(gama), file=filename, append = TRUE)
                      }
                      
                      if(min_target >= 0 && max_target <= 1){
                        model<-as.formula(round(dt[,target]) ~ as.factor(dt[,indx_type])  * dt[,ind[1]] * dt[,ind[2]] + as.factor(dt[,indx_type]) *as.matrix(other_controls))
                        logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                        probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                        
                        capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                        capture.output(PseudoR2(logit), file=filename, append = TRUE)
                        capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                        capture.output(PseudoR2(probit), file=filename, append = TRUE)
                      }
                    }
                  } # end if check: Sufficient variation in ind[2] and target
                  
                  if(parameter_analysis == 1){
                    if(var(dt[,ind[2]], na.rm = TRUE) > 0.0001 * mean(dt[,ind[2]], na.rm = TRUE) && var(dt[,target], na.rm = TRUE) > 0.0001 * mean(dt[,target], na.rm = TRUE)){
                      
                      ind0<-match(names[c(k, k1)], colnames(other_controls0))
                      ind0<-ind0[which(is.na(ind0) != TRUE)]
                      other_controls<-other_controls0[,-ind0]
                      indx_par<-match("Parameter", colnames(other_controls0))
                      indx_all<-match(names, colnames(dt))
                      
                      
                      if(is.na(indx_par) != TRUE){
                        
                        par_vec<-as.factor(other_controls0[,indx_par])
                        other_controls0<-other_controls0[,-indx_par]
                        
                        model <- as.formula(dt[,target] ~ par_vec)
                        ols<-lm(model)
                        #ols<-lm(dt[,target] ~ as.matrix(dt[,ind]) + dt[,ind[1]] * dt[,ind[2]])
                        title<-paste("\n NOW WITH PARAMETER DUMMY:  Regression results: ", target_var_temp_const[j], "in",t1,"on", names[k], names[k1],"with", paste(model, collapse=" "),collapse = " ")
                        write(paste(title), file = paste(filename), append = TRUE)
                        capture.output(summary(ols, digits=4), file=filename, append = TRUE)
                        if(agent != "Firm" && 1==2 && range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0){
                          gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                          capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(gama), file=filename, append = TRUE)
                        }
                        if(min_target >= 0 && max_target <= 1){
                          model<-as.formula(round(dt[,target]) ~ par_vec)
                          logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                          probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                          
                          capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(logit), file=filename, append = TRUE)
                          capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(probit), file=filename, append = TRUE)
                        }
                        
                        
                        model <- as.formula(dt[,target] ~ par_vec +  dt[,ind[1]] + dt[,ind[2]])
                        ols<-lm(model)
                        #ols<-lm(dt[,target] ~ as.matrix(dt[,ind]) + dt[,ind[1]] * dt[,ind[2]])
                        title<-paste("\n NOW WITH PARAMETER DUMMY:  Regression results: ", target_var_temp_const[j], "in",t1,"on", paste(colnames(dt)[ind], collapse = " "),"with", paste(model, collapse=" "),collapse = " ")
                        write(paste(title), file = paste(filename), append = TRUE)
                        capture.output(summary(ols, digits=4), file=filename, append = TRUE)
                        if(agent != "Firm" && 1==2 && range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0){
                          gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                          capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(gama), file=filename, append = TRUE)
                        }
                        if(min_target >= 0 && max_target <= 1){
                          model<-as.formula(round(dt[,target]) ~ par_vec +  dt[,ind[1]] + dt[,ind[2]])
                          logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                          probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                          
                          capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(logit), file=filename, append = TRUE)
                          capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(probit), file=filename, append = TRUE)
                        }
                        
                        
                        model <- as.formula(dt[,target] ~ par_vec +  dt[,ind[1]] * dt[,ind[2]])
                        ols<-lm(model)
                        #ols<-lm(dt[,target] ~ as.matrix(dt[,ind]) + dt[,ind[1]] * dt[,ind[2]])
                        title<-paste("\n NOW WITH PARAMETER DUMMY:  Regression results: ", target_var_temp_const[j], "in",t1,"on", paste(colnames(dt)[ind], collapse = " "),"with", paste(model, collapse=" "),collapse = " ")
                        write(paste(title), file = paste(filename), append = TRUE)
                        
                        
                        capture.output(summary(ols, digits=4), file=filename, append = TRUE)
                        
                        if(agent != "Firm" && 1==2 && range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0){
                          gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                          capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(gama), file=filename, append = TRUE)
                        }
                        if(min_target >= 0 && max_target <= 1){
                          model<-as.formula(round(dt[,target]) ~ par_vec +  dt[,ind[1]] * dt[,ind[2]])
                          logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                          probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                          
                          capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(logit), file=filename, append = TRUE)
                          capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(probit), file=filename, append = TRUE)
                        }
                        
                        model <- as.formula(dt[,target] ~ par_vec * as.matrix(dt[,ind]) + par_vec * dt[,ind[1]] * dt[,ind[2]])
                        ols<-lm(model)
                        #ols<-lm(dt[,target] ~ as.matrix(dt[,ind]) + dt[,ind[1]] * dt[,ind[2]])
                        title<-paste("\n NOW WITH PARAMETER DUMMY:  Regression results: ", target_var_temp_const[j], "in",t1,"on", names[k], names[k1],"with", paste(model, collapse=" "),collapse = " ")
                        write(paste(title), file = paste(filename), append = TRUE)
                        
                        
                        capture.output(summary(ols, digits=4), file=filename, append = TRUE)
                        
                        if(agent != "Firm" && 1==2 && (range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0)){
                          gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                          capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(gama), file=filename, append = TRUE)
                        }
                        if(min_target >= 0 && max_target <= 1){
                          model<-as.formula(round(dt[,target]) ~  par_vec *  dt[,ind[1]] * dt[,ind[2]])
                          logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                          probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                          
                          capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(logit), file=filename, append = TRUE)
                          capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(probit), file=filename, append = TRUE)
                        }
                        
                        
                        model <- as.formula(dt[,target] ~ par_vec * dt[,ind[1]] * dt[,ind[2]])
                        ols<-lm(model)
                        #ols<-lm(dt[,target] ~ as.matrix(dt[,ind]) + dt[,ind[1]] * dt[,ind[2]])
                        title<-paste("\n NOW WITH PARAMETER DUMMY:  Regression results: ", target_var_temp_const[j], "in",t1,"on", paste(colnames(dt)[ind], collapse = " "),"with", paste(model, collapse=" "),collapse = " ")
                        if(k==1 && k1==1){
                          write(paste(title), file = paste(filename), append = TRUE)
                        }
                        
                        capture.output(summary(ols, digits=4), file=filename, append = TRUE)
                        
                        if(agent != "Firm" && 1==2 && (range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0)){
                          gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                          capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(gama), file=filename, append = TRUE)
                        }
                        if(min_target >= 0 && max_target <= 1){
                          model<-as.formula(round(dt[,target]) ~ par_vec * as.matrix(dt[,ind]) + par_vec * dt[,ind[1]] * dt[,ind[2]])
                          logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                          probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                          
                          capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(logit), file=filename, append = TRUE)
                          capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(probit), file=filename, append = TRUE)
                        }
                        
                        
                        dt<-cbind(dt, other_controls)
                        ind2<-match(colnames(other_controls), colnames(dt))
                        indx_all<-match(names, colnames(dt))
                        
                        model <- as.formula(dt[,target] ~ par_vec * dt[,ind[1]] * dt[,ind[2]] + par_vec * as.matrix(other_controls))
                        ols<-lm(model)
                        #ols<-lm(dt[,target] ~ as.matrix(dt[,ind]) + dt[,ind[1]] * dt[,ind[2]])
                        title<-paste("\n Regression results: ", target_var_temp_const[j], "in",t1,"on", paste(colnames(dt)[ind], collapse = " "),"with", paste(model, collapse=" "),collapse = " ")
                        if(k==1 && k1==1){
                          write(paste(title), file = paste(filename), append = FALSE)
                        }else{
                          write(paste(title), file = paste(filename), append = TRUE)
                        }
                        capture.output(summary(ols, digits=4), file=filename, append = TRUE)
                        
                        if(agent != "Firm" && 1==2 && min_target>0 && 1==2){
                          #start<- rep(0, (4+length(other_controls)))
                          gama<-glm(model, family=Gamma(link = "log"),  maxit = 100 )
                          capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(gama), file=filename, append = TRUE)
                        }
                        
                        if(min_target >= 0 && max_target <= 1){
                          model<-as.formula(round(dt[,target])  ~ par_vec * dt[,ind[1]] * dt[,ind[2]] + par_vec * as.matrix(other_controls))
                          logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                          probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                          
                          capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(logit), file=filename, append = TRUE)
                          capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                          capture.output(PseudoR2(probit), file=filename, append = TRUE)
                        }
                        
                        if(is.na(indx_type) != TRUE && target_var_temp_const != "share_conv_capital_used" && end_time < (0.8*(number_xml*20))){
                          
                          dt<-cbind(dt, other_controls)
                          ind2<-match(colnames(other_controls), colnames(dt))
                          model <- as.formula(dt[,target]  ~ par_vec * as.factor(dt[,indx_type])  + par_vec *as.factor(dt[,indx_type])  * dt[,ind[1]] * dt[,ind[2]] + par_vec * as.factor(dt[,indx_type]) *as.matrix(other_controls))
                          ols<-lm(model)
                          #ols<-lm(dt[,target] ~ as.matrix(dt[,ind]) + dt[,ind[1]] * dt[,ind[2]])
                          title<-paste("\n Regression results: ", target_var_temp_const[j], "in",t1,"on", names[k], names[k1],"with", paste(model, collapse=" "),collapse = " ")
                          
                          write(paste(title), file = paste(filename), append = TRUE)
                          
                          capture.output(summary(ols, digits=4), file=filename, append = TRUE)
                          
                          if(agent != "Firm" && 1==2 && min_target>0 && 1==2){
                            gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
                            capture.output(summary(gama, digits=4), file=filename, append = TRUE)
                            capture.output(PseudoR2(gama), file=filename, append = TRUE)
                          }
                          
                          if(min_target >= 0 && max_target <= 1){
                            model<-as.formula(round(dt[,target]) ~ par_vec * as.factor(dt[,indx_type])  + par_vec *as.factor(dt[,indx_type])  * dt[,ind[1]] * dt[,ind[2]] + par_vec * as.factor(dt[,indx_type]) *as.matrix(other_controls))
                            logit<-glm(model, family=binomial(link="logit"), maxit = 100)
                            probit<-glm(model, family=binomial(link="probit"), maxit = 100)
                            
                            capture.output(summary(logit, digits=4), file=filename, append = TRUE)
                            capture.output(PseudoR2(logit), file=filename, append = TRUE)
                            capture.output(summary(probit, digits=4), file=filename, append = TRUE)
                            capture.output(PseudoR2(probit), file=filename, append = TRUE)
                          }
                        }
                        
                      } # end if check: Sufficient variation in ind[2] and target
                    } # end check whether sufficient non-NA
                  } # if par_analysis == 1
                }# k1 loop
              } # end if check: Sufficient variation in ind[1]
            }# k loop
            }# end if 1==2
            
            

            if(1==2){
              logit<-multinom(reg_data_temp4[,target_temp] ~ as.matrix(reg_data_temp4[,c(start_temp,start_temp+1)]), data=reg_data_temp4)
              probit<-multinom(reg_data_temp4[,target_temp] ~ as.matrix(reg_data_temp4[,c(start_temp,start_temp+1)]), data=reg_data_temp4, probit=TRUE)
              
              write(paste("\n Multinomial regression results:",target_var_temp, "on ",paste(colnames(reg_data_temp2)[c(2:((length(ind)+1)))],collapse="+"),"\n"), file = file_temp, append = TRUE)
              #write.table(wil_cs, file = dir_temp, append = TRUE)
              
              capture.output(summary(logit, digits=4), file=file_temp, append = TRUE)
              capture.output(PseudoR2(logit), file=file_temp, append = TRUE)
              capture.output(summary(probit, digits=4), file=file_temp, append = TRUE)
              capture.output(PseudoR2(probit), file=file_temp, append = TRUE)
              
              target_temp<-match(target_var_temp, colnames(reg_data_temp5))
              logit<-multinom(reg_data_temp5[,target_temp] ~ as.matrix(reg_data_temp5[,c(start:length(reg_data_temp5))]), data=reg_data_temp5, logit = TRUE)
              probit<-multinom(reg_data_temp5[,target_temp] ~ as.matrix(reg_data_temp5[,c(start:length(reg_data_temp5))]), data=reg_data_temp5, probit=TRUE)
              
              write(paste("\n Multinomial regression results with controls:",target_var_temp, "on ",paste(colnames(reg_data_temp2)[c(2:((length(ind)+1)))],collapse="+"),"\n"), file = file_temp, append = TRUE)
              #write.table(wil_cs, file = dir_temp, append = TRUE)
              
              capture.output(summary(logit, digits=4), file=file_temp, append = TRUE)
              capture.output(PseudoR2(logit), file=file_temp, append = TRUE)
              capture.output(summary(probit, digits=4), file=file_temp, append = TRUE)
              capture.output(PseudoR2(probit), file=file_temp, append = TRUE)
              
            } # if 1==2
            
            rm(ols)
            if(exists("logit")){
              rm(logit, probit)
            }
            if(exists("gama")){
              rm(gama)
            }

          if(exists("with_rand_policy") && with_rand_policy == 1 && exists("with_rand_barr") && with_rand_barr == 1 && is.element("eco_tax_rate", names) && is.element("frontier_gap", names)){
            indices_pol<-c(match(c("eco_tax_rate", "eco_investment_subsidy", "eco_consumption_subsidy", 
                                   "monthly_budget_balance"), 
                                 colnames(dt) ))
            indices_barr<-c(match(c("frontier_gap", "specific_skill_gap"),
                                  colnames(dt) ))
            indices_pol<-indices_pol[which(is.na(indices_pol) != TRUE)]
            indices_barr<-indices_barr[which(is.na(indices_barr) != TRUE)]
            
            
            model <- as.formula(dt[,target] ~ as.matrix(dt[,c(indices_pol, indices_barr)]))
            ols<-lm(model)
            #ols<-lm(dt[,target] ~ as.matrix(dt[,ind]) + dt[,ind[1]] * dt[,ind[2]])
            title<-paste("\n POLICY WITH RANDOM BARRIER: Regression results: ", target_var_temp_const[j], "in",t1,"on policy with barrier interaction with ", paste(model, collapse=" "), "and", paste(colnames(dt)[c(indices_barr,indices_pol)], collapse=""),collapse = " ")
            write(paste(title), file = paste(filename), append = TRUE)
            
            
            capture.output(summary(ols, digits=4), file=filename, append = TRUE)
            
            if(agent != "Firm" && 1==2 && range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<0){
              gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
              capture.output(summary(gama, digits=4), file=filename, append = TRUE)
              capture.output(PseudoR2(gama), file=filename, append = TRUE)
            }
            if(min_target >= 0 && max_target <= 1){
              model<-as.formula(round(dt[,target]) ~ as.matrix(dt[,c(indices_pol, indices_barr)]))            
              logit<-glm(model, family=binomial(link="logit"), maxit = 100)
              probit<-glm(model, family=binomial(link="probit"), maxit = 100)
              
              capture.output(summary(logit, digits=4), file=filename, append = TRUE)
              capture.output(PseudoR2(logit), file=filename, append = TRUE)
              capture.output(summary(probit, digits=4), file=filename, append = TRUE)
              capture.output(PseudoR2(probit), file=filename, append = TRUE)
            }
            
            model <- as.formula(dt[,target] ~ dt[,indices_barr[1]]*as.matrix(dt[,indices_pol]) + dt[,indices_barr[2]]*as.matrix(dt[,indices_pol]))
            ols<-lm(model)
            #ols<-lm(dt[,target] ~ as.matrix(dt[,ind]) + dt[,ind[1]] * dt[,ind[2]])
            title<-paste("\n POLICY WITH RANDOM BARRIER: Regression results: ", target_var_temp_const[j], "in",t1,"on policy with barrier interaction with ", paste(model, collapse=" "), "and", paste(colnames(dt)[c(indices_barr,indices_pol)], collapse=""),collapse = " ")
            write(paste(title), file = paste(filename), append = TRUE)
            
            
            capture.output(summary(ols, digits=4), file=filename, append = TRUE)
            
            if(agent != "Firm" && 1==2 && range(dt[,target], na.rm=TRUE)[1]>0 || range(dt[,target], na.rm=TRUE)[2]<1){
              gama<-glm(model, family=Gamma(link = "log"), maxit = 100)
              capture.output(summary(gama, digits=4), file=filename, append = TRUE)
              capture.output(PseudoR2(gama), file=filename, append = TRUE)
            }
            if(min_target >= 0 && max_target <= 1){
              model<-as.formula(round(dt[,target]) ~ dt[,indices_barr[1]]*as.matrix(dt[,indices_pol]) + dt[,indices_barr[2]]*as.matrix(dt[,indices_pol]))            
              logit<-glm(model, family=binomial(link="logit"), maxit = 100)
              probit<-glm(model, family=binomial(link="probit"), maxit = 100)
              
              capture.output(summary(logit, digits=4), file=filename, append = TRUE)
              capture.output(PseudoR2(logit), file=filename, append = TRUE)
              capture.output(summary(probit, digits=4), file=filename, append = TRUE)
              capture.output(PseudoR2(probit), file=filename, append = TRUE)
            }
            
          } # if rand_policy and rand_barr
          
          #rm(dt)
         
            #} # if 1==2 (only type expers)
            #
            write(paste("\n Descriptive statistics of data","\n"), file = filename , append = TRUE)
            
            capture.output(stat.desc(as.matrix(dt[,c(target, indx_all)])), file=filename , append = TRUE)
            capture.output(stat.desc(as.matrix(other_controls)), file=filename , append = TRUE)
            
        } # w in w_temp
    } # number controls > 0
      } # w in w_temp (rows of list_of_var_pairs)
        }# t1 in time temp
        } # t in periods_temp
} # j in target vars const
#rm(list=ls(pattern="temp"), dt, data, w, t1, t, j)
rm(list = ls(pattern="indices"))
rm(list = ls(pattern="indx_"))
on.exit(if(is.null(dev.list()) == F){ dev.off()}, add=T)  
#} # aggr in 1:2

} # Do run regressions only for Firm and Eurostat, but not for Gov. 

rm(aux_folder)