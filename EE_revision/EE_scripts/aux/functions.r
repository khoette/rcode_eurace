regr_and_capt_output = function(model_ols, model_binary, title_text, file_name, min_targ, max_targ, app=TRUE){
  
  write(paste(title_text), file = paste(file_name), append = app)
  
  ols<-lm(model_ols)
  capture.output(summary(ols, digits=4), file=file_name, append = TRUE)
  write(paste("AIC: ",paste(AIC(ols)), "BIC: ",paste(BIC(ols))), file=file_name, append = TRUE)
  
  if(1==2 && (min_targ>0 || max_targ<0)){
    gama<-glm(model_ols, family=Gamma(link = "log"), maxit = 100)
    capture.output(summary(gama, digits=4), file=file_name, append = TRUE)
    capture.output(PseudoR2(gama), file=file_name, append = TRUE)
  }
  if(min_targ >= 0 && max_targ <= 1){
    logit<-glm(model_binary, family=binomial(link="logit"), maxit = 100)
    probit<-glm(model_binary, family=binomial(link="probit"), maxit = 100)
    
    capture.output(summary(logit, digits=4), file=file_name, append = TRUE)
    capture.output(PseudoR2(logit), file=file_name, append = TRUE)
    write(paste("AIC: ",paste(AIC(logit)), "BIC: ",paste(BIC(logit))), file=file_name, append = TRUE)
    
    capture.output(summary(probit, digits=4), file=file_name, append = TRUE)
    capture.output(PseudoR2(probit), file=file_name, append = TRUE)
    write(paste("AIC: ",paste(AIC(probit)), "BIC: ",paste(BIC(probit))), file=file_name, append = TRUE)
    
    
    #mlogit<-margins(logit)
    #mprobit<-margins(probit)
    #capture.output(smmary(mlogit), file=file_name, append = TRUE)
    #capture.output(summary(mprobit), file=file_name, append = TRUE)
    
    
  }
  
}
     
