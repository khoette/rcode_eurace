# Wilcox test: 

if(exists("wilcox_average")==FALSE){
  wilcox_average<-1
}

if(agent=="Eurostat" && (with_rand_policy || with_rand_learn || with_rand_barr)){
  write_par_table_temp<-1
}else{
  write_par_table_temp<-F
}


#time_temp<-c(600,1800,3000,4200,5400,6600, 7800,9000,10200, 11400, 12600, 13800, 15000)
time_temp_before<-c(1:600)
time_temp_initial<-c(601:3000)
time_temp_medium<-c(3001:5400)
time_temp_end<-c(5401:15000)
time_temp_all<-c(1:15000)
time_temp_init<-600
time_temp_init_pol<-620
time_temp_last_observation<-15000
time_temp_before_policy_ends<-10000

if(write_par_table_temp){
  # Initialize variables that are needed to generate automatically generated 
  # Latex tables (using xtable)
  # ESC -> \ and BL -> {, BR->} (Made in this way to ensure compatibility with 
  # xtable package)
  pars_for_tab_temp<-c()
  table_par_names_temp<-c()
}
if(agent == "Eurostat"){
  agent_temp<-"Eurostat"
  variables_wilcox_temp<-c("share_conv_capital_used","variance_share", "monthly_output", 
                           "no_active_firms", "eco_price_wage_ratio", "price_eco", 
                           "unemployment_rate", "frontier_productivity_ratio", 
                           "s_skill_ratio")
  if(exists("with_rand_policy") && with_rand_policy == 1){
    variables_wilcox_temp<-c(variables_wilcox_temp, "eco_tax_rate", "eco_investment_subsidy", 
                             "eco_consumption_subsidy")
    time_temp_init<-time_temp_init
    pars_for_tab_temp<-c(pars_for_tab_temp, "eco_tax_rate", "eco_investment_subsidy", 
                         "eco_consumption_subsidy")
    table_par_names_temp<-c(table_par_names_temp, "DOLESCthetaDOL","DOLESCsigmaUPiDOL","DOLESCsigmaUPcDOL")
  }
  variables_wilcox_temp<-c(variables_wilcox_temp, "frontier_gap", "specific_skill_gap")
  if(exists("with_rand_barr") && with_rand_barr == 1){
    pars_for_tab_temp<-c(pars_for_tab_temp, "frontier_gap", "specific_skill_gap")
    table_par_names_temp<-c(table_par_names_temp, "DOLESCbetaUPADOL", "DOLESCbetaUPbDOL")
  }
  if(exists("with_rand_learn") && with_rand_learn == 1){
    variables_wilcox_temp<-c(variables_wilcox_temp, "min_learning_coefficient_var", "learning_spillover_strength_var")
    pars_for_tab_temp<-c(pars_for_tab_temp, "min_learning_coefficient_var", "learning_spillover_strength_var")
    table_par_names_temp<-c(table_par_names_temp, "DOLESCchiUPBLintBRDOL", "DOLESCchiUPBLspillBRDOL")
  }

}else if(agent == "Firm"){
  agent_temp<-"Firm"
  variables_wilcox_temp<-c("no_employees", "output", "revenue", "age", "unit_costs", 
                           "actual_mark_up", "effective_investments", "no_employees", 
                           "share_conventional_vintages_used","variance_share")
}else if(agent == "Government"){
  agent_temp <- "Government"
  variables_wilcox_temp<-"base_tax_rate"
}

plot_dir_wilcox_type0<-paste(plot_dir_wilcox_type,"/",parameter, sep="")
if(dir.exists(plot_dir_wilcox_type0) == FALSE){
  dir.create(plot_dir_wilcox_type0)
}



data_directory<-paste(experiment_directory,"/rdata/",kind_of_data,"/",data_type[d],"/",sep="")

if((exists("minisnapshot") && minisnapshot==1)){#|| agent=="Firm"){
  t_temp<-c("init", "init_pol", "before_policy_ends", "last_observation")
  print(paste("Only minisnapshot wilcox"))
}else{
  t_temp<-c("init", "init_pol", "before_policy_ends", "last_observation","before", "initial", "medium", "end", "all")
}

#fix_vars_temp<-c("Type", "Firm_Type", "Periods", "id", "Run", "Learn_Type", "Ease", "Spill")
fix_vars_temp<-c("Type", "Periods", "id", "Run")
aggregation_levels_temp<-c("single_large", "single_switch")
aggr_temp<-aggregation_levels_temp[1]
variable_temp<-variables_wilcox_temp[1]

if(write_par_table_temp){
  # Create matrix for parameter init overview table: 
  n_rows_temp<-(length(pars_for_tab_temp) + 2)
  par_tab_temp<-matrix(NA, ncol=5, nrow=n_rows_temp)
  par_tab_temp[1,]<-c("","","ESCemphBLecoBR", "ESCemphBLconvBR","")
  par_tab_temp[2,]<-c("","Mean (Std)","Mean (Std)", "Mean (Std)","p-value")
  par_tab_temp[,1]<-c("","",table_par_names_temp)
  par_caption_temp<-c("Overview of parameter and variable initialization.BR
  ESCfloatfootBLThe four columns on 
  the right-hand side show the initialization by regime type, i.e. ESCemphBLecoBR and 
  ESCemphBLconvBR.")


}
# Create matrix for xtable: 
vars_for_tab_temp<-c("share_conv_capital_used","variance_share", "eco_price_wage_ratio", 
                     "frontier_gap", "specific_skill_gap", 
                     "frontier_productivity_ratio", "s_skill_ratio", "monthly_output", 
                     "unemployment_rate", "no_active_firms", 
                     "no_employees", "unit_costs", "effective_investments", 
                     "actual_mark_up", "share_conventional_vintages_used")
table_var_names_temp<-c("Share conventional capital used", "Variance share", "Eco-price-wage-ratio", 
                        "% frontier gap", "% skill gap", "Frontier ratio", "Skill ratio",
                        "Monthly output", "Unemployment rate", "# active firms", "# employees", 
                        "Unit costs", "Investment", "Mark up", "Share conv. capital on firm level")
if(with_policy){
  vars_for_tab_temp<-c(vars_for_tab_temp, c("base_tax_rate", "monthly_budget_balance_gdp_fraction", 
                       "monthly_budget_balance"))
  table_var_names_temp<-c(table_var_names_temp, c("Base tax rate", "Budget balance (pct GDP)", "Budget balance"))
}
table_var_names_temp<-table_var_names_temp[which(!is.na(match(vars_for_tab_temp, variables_wilcox_temp)))]

vars_for_tab_temp<-vars_for_tab_temp[which(!is.na(match(vars_for_tab_temp, variables_wilcox_temp)))]
t_tab_temp<-c("initial", "medium", "end", "all")
x<-ceiling(0.5*length(vars_for_tab_temp))
t_for_table_temp<-c("", "t", rep(c("", "[601,3000]", "[3001,5400]", "[5401,15000]", "[1,15000]"),x))
no_rows_temp<-length(t_for_table_temp)
no_cols_temp<-7
tab_temp<-matrix(c(t_for_table_temp, rep("", (no_rows_temp * (no_cols_temp-1)))), nrow = no_rows_temp, ncol=no_cols_temp)
tab_temp[2,]<-c("t", rep(c("ESCemphBLecoBR", "ESCemphBLconvBR", "ESCemphBLeco,convBR"), 2))

aggregation_levels_temp<-"single_large"

for(aggr_temp in aggregation_levels_temp){
  load(file=paste(data_directory,"data_",aggr_temp,"_",experiment_name,"_",parameter,"_",agent_temp,".RData",sep=""))
  
  ind_fix_temp<-match(c(fix_vars_temp,variables_wilcox_temp), colnames(object))
  ind_fix_temp<-ind_fix_temp[is.na(ind_fix_temp) == FALSE]
  temp_data_full<-object[,c(ind_fix_temp)]
  rm(object)

  for(variable_temp in variables_wilcox_temp){
    
    print(variable_temp)

    mini_mat_temp<-matrix(NA, nrow=length(t_tab_temp), ncol = 3)
    
    if(is.element(variable_temp, colnames(temp_data_full))){
      
      if(is.element(variable_temp, c("min_learning_coefficient_var", "learning_spillover_strength_var"))){
        t_temp0<-"init"
      }else{
        t_temp0<-t_temp
      }
  
    if(aggr_temp==aggregation_levels_temp[1]){
      append<-FALSE
    }else{
      append<-TRUE
      write(paste("\n \n ######################################################################## \n "), append = TRUE)
      
    }
      if(exists("minisnapshot") && minisnapshot==1){
        filename_temp0<-paste(plot_dir_wilcox_type0,"/minisnapshot_",agent,"_results_wilcox_",variable_temp, sep = "")
      }else{
        filename_temp0<-paste(plot_dir_wilcox_type0,"/",agent,"_results_wilcox_",variable_temp, sep = "")
      }
    write(paste("\n \n Variable: ", variable_temp, "aggr type: ", aggr_temp, "\n"), file = filename_temp0, append = append)
    
    
    cols_temp_temp<-c(match(c(fix_vars_temp, variable_temp), colnames(temp_data_full)))
    cols_temp_temp<-cols_temp_temp[which(is.na(cols_temp_temp)==FALSE)]
    temp_data_var<-temp_data_full[,cols_temp_temp]
    col_temp_temp<-match(variable_temp, colnames(temp_data_var))
    temp_data_var[,col_temp_temp]<-as.numeric(temp_data_var[,col_temp_temp])

  
    for(t in t_temp0){
      
      eval(call("<-",paste("time_temp"),as.name(paste("time_temp_",t,sep=""))))
      
      write(paste("\n#########################################################################################"), file=filename_temp0, append=TRUE)
      if(exists("wilcox_average") && wilcox_average){
        write(paste("\n\n Average ", t, "i.e. from ",time_temp[1], "to", time_temp[length(time_temp)], "\n"), file = filename_temp0, append = TRUE)
        
      }else{
        write(paste("\n\n For period: ", t, "i.e. from ",time_temp[1], "to", time_temp[length(time_temp)], "\n"), file = filename_temp0, append = TRUE)
      }
      temp_data<-temp_data_var[which(as.vector(as.character(temp_data_var$Periods)) %in% time_temp),]
      
      all_temp<-temp_data[order(temp_data$Run),]
      if(agent=="Firm"){
        all_temp<-temp_data[order(temp_data$id),]
      }
      
      ind_temp<-match(variable_temp, colnames(all_temp))
      
      
      if(exists("wilcox_average") && wilcox_average && length(time_temp)>1){
        select<-all_temp[which(as.numeric(as.character(all_temp$Periods)) == time_temp[length(time_temp)]),]
        var_col<-select[,ind_temp]
        var_col_full<-all_temp[,ind_temp]
        len<-length(time_temp)/20
        index<-c(1:len)
        #for(run in all_temp$Run){
        for(i in 1:(length(unique(all_temp$Run))*no_agents)){
          index<-c((1+((i-1)*len)): ((i*len)))
          var_col[i]<-mean(var_col_full[index], na.rm=TRUE, nan.rm = TRUE)
          
        }
        select[,ind_temp]<-var_col
        all_temp<-select
        rm(select, var_col, var_col_full, index, len)
      }
      
      types_to_compare<-c("Type","Learn_Type", "Spill", "Ease","Firm_Type")
      rep<-match(types_to_compare, colnames(all_temp))
      rep<-rep[which(is.na(rep)==FALSE)]
      
      for(index_type in rep){
        # Start with a comparison of types against types, 
        # i.e. eco vs conv vs. switch and also diff. learn types agains one another if available
        types_temp<-unique(all_temp[,index_type])
        types_temp<-as.character(types_temp)

        if(aggr_temp== "single_large" && is.element(variable_temp, vars_for_tab_temp) && 
           setequal(c("eco","conv"),types_temp)){
          write_table_temp<-T
          if(is.element(t, t_tab_temp)){
            tab_written_temp<-T
          }else{
            tab_written_temp<-F
          }
          
        }else{
          write_table_temp<-F
          tab_written_temp<-F
        }
      
        write(paste("\n#########################################################################################"), file=filename_temp0, append=TRUE)
        write(paste("\n ", colnames(all_temp)[index_type]), file=filename_temp0, append=TRUE)
        
        write(paste("Types vs types, i.e. all in: ", paste(as.character(types_temp), collapse = " - ")), file=filename_temp0, append=TRUE)
        if(write_par_table_temp && is.element(variable_temp, pars_for_tab_temp) 
           && t == "init" && aggr_temp == "single_large" && 
           setequal(as.character(types_temp), c("eco", "conv"))){
          wr_par_temp<-T
        }else{
          wr_par_temp<-F
        }
        
        if((write_table_temp && tab_written_temp) || wr_par_temp){
          o_temp<-match(c("eco", "conv"), types_temp)
          if(wr_par_temp){
            row_i_temp<-match(variable_temp, pars_for_tab_temp)
            row_str_temp<-c(rep("replace me",4))
          }
          if((write_table_temp && tab_written_temp)){
            index_temp<-match(t, t_tab_temp)
            string_temp<-c(rep(NA,3))
          }
        }
        
        for(ty in 1:(length(types_temp)-1)){
          type1_temp<-as.character(types_temp[ty])
          scen_type_1<-all_temp[which(all_temp[,index_type] == type1_temp),ind_temp]
          scen_type_1<-scen_type_1[which(!is.na(scen_type_1))]
          scen_type_1<-scen_type_1[which(!is.infinite(scen_type_1))]
          for(ty2 in (1+ty):length(types_temp)){
            type2_temp<-as.character(types_temp[ty2])
            scen_type_2<-all_temp[which(all_temp[,index_type] == type2_temp),ind_temp]
            scen_type_2<-scen_type_2[which(!is.na(scen_type_2))]
            scen_type_2<-scen_type_2[which(!is.infinite(scen_type_2))]
            
            if(length(scen_type_1[which(!is.na(scen_type_1))])>0.1*nrow(all_temp) && length(scen_type_2[which(!is.na(scen_type_2))])>0.1*nrow(all_temp)){
              wil_scen_types<-wilcox.test(scen_type_1, scen_type_2, na.rm=TRUE, alternative = "two.sided", paired=FALSE)
              write(paste("Results wilcox: ", type1_temp, "vs",type2_temp,sep = " "), file = filename_temp0,  append = TRUE)
              capture.output(wil_scen_types, file=filename_temp0, append = TRUE)
              sd1_temp<-sd(scen_type_1,na.rm = TRUE)
              sd2_temp<-sd(scen_type_2,na.rm = TRUE)
              m1_temp<-mean(scen_type_1, na.rm=TRUE)
              m2_temp<-mean(scen_type_2, na.rm=TRUE)
              write(paste("\n mean and sd 1:", m1_temp, sd1_temp," and 2: ", m2_temp, sd2_temp, "\n Length: ", length(scen_type_1), length(scen_type_2), "\n"), file = filename_temp0, append = TRUE)
              if(write_table_temp && tab_written_temp){
                string_temp[c(o_temp[1], o_temp[2])]<-c(paste(round_customized(m1_temp,digit=4)," (",round_customized(sd1_temp,digit=4),")",sep=""), paste(round_customized(m2_temp,digit=4)," (",round_customized(sd2_temp, digit=4),")",sep=""))
                string_temp[3]<-round_customized(as.numeric(wil_scen_types$p.value), digit=4)
              }
              if(wr_par_temp){
                mean_all_temp<-mean(c(scen_type_1,scen_type_2), na.rm=TRUE)
                sd_all_temp<-sd(c(scen_type_1, scen_type_2), na.rm=TRUE)
                #row_i_temp<-match(variable_temp, pars_for_tab_temp)
                #row_str_temp<-c(rep("replace me",4))
                row_str_temp[c((o_temp[1]+1), (o_temp[2]+1))]<-c(paste(round_customized(m1_temp,digit=4)," (",round_customized(sd1_temp,digit=4),")",sep=""), paste(round_customized(m2_temp,digit=4)," (",round_customized(sd2_temp, digit=4),")",sep=""))
                row_str_temp[4]<-as.numeric(wil_scen_types$p.value)
                row_str_temp[1]<-c(paste(round_customized(mean_all_temp, digit=4)," (",round_customized(sd_all_temp,digit=4),")",sep="" ))
                par_tab_temp[(row_i_temp+2),c(2:5)]<-row_str_temp
              }
            }else{
              write(paste("This type data seems to be not avaiable here, i.e. <10pct of array entries are non-NA"))
            }
          }
        } # end ty in types_temp
        

        
        if(write_table_temp && tab_written_temp){
          mini_mat_temp[index_temp,]<-string_temp
        }

    } # for  index_type in rep, i.e. within type comparisons
     
      
    if(length(rep)>1){ 
    write(paste("\n \n ######################################################################## \n "))
    write(paste("Now: Comparison ACROSS types \n \n "))

    for(index_type in rep){
      # Start with a comparison of types against types, 
      # i.e. eco vs conv vs. switch and also diff.
      #  learn types agains one another if available
      types_temp<-unique(all_temp[,index_type])
      
      write(paste("\n#########################################################################################"), file=filename_temp0, append=TRUE)
      write(paste("\n GROUP: \n", colnames(all_temp)[index_type]), file=filename_temp0, append=TRUE)
      
      
      for(index_type2_temp in rep[-index_type]){
        types_temp2<-unique(all_temp[,index_type2_temp])
        write(paste("Types: ", paste(types_temp2, collapse = " "), " within each group in: ", paste(types_temp, collapse = " ")), file = filename_temp0, append = TRUE)
        write(paste("\n##############"), file=filename_temp0, append=TRUE)
        write(paste("\n ", colnames(all_temp)[index_type2_temp]), file=filename_temp0, append=TRUE)
        
        
        for(ty in 1:(length(types_temp))){
          group_temp<-as.character(types_temp[ty])
          group_data_temp<-all_temp[which(all_temp[,index_type] == group_temp),]
          write(paste("\n Group: ", paste(group_temp, collapes=" - ")), file = filename_temp0, append=TRUE)
          
          for(ty1 in 1:(length(types_temp2)-1)){
            type1_temp<-as.character(types_temp2[ty1])
            scen_type_1<-all_temp[which(all_temp[,index_type2_temp] == type1_temp),ind_temp]
            for(ty2 in (1+ty1):(length(types_temp2))){
              type2_temp<-as.character(types_temp2[ty2])
              scen_type_2<-all_temp[which(all_temp[,index_type2_temp] == type2_temp),ind_temp]
              if(length(type1_temp[which(is.na(type1_temp)==FALSE)])>0.1*length(type1_temp) && length(type2_temp[which(is.na(type2_temp)==FALSE)])>0.1*length(type2_temp)){
                wil_scen_types<-wilcox.test(scen_type_1, scen_type_2, na.rm=TRUE, alternative = "two.sided", paired=FALSE)
                write(paste("Results wilcox: ", type1_temp, "vs",type2_temp, "within group ", group_temp, sep = ""), file = filename_temp0, append = TRUE)
                capture.output(wil_scen_types, file=filename_temp0, append = TRUE)
                write(paste("\n Means and sd 1: ", mean(scen_type_1, na.rm=TRUE), sd(scen_type_1, na.rm=TRUE)," and 2: ", mean(scen_type_2, na.rm=TRUE), sd(scen_type_2, na.rm=TRUE), "\n Length: ", length(scen_type_1), length(scen_type_2), "\n"), file = filename_temp0, append = TRUE)
                
              }else{
                write(paste("This type data seems to be not avaiable here, i.e. <10pct of array entries are non-NA"))
              }
            } # for ty2
            
          } # for ty1
          rm(ty1, ty2)
        } # end ty in types_temp, i.e. go through all group members 
      } # for index_type2_temp in rep[-index_type] -> define types within group (e.g. spill or ease)
    } # for  index_type in rep, i.e. within type comparisons -> define group (e.g. eco, conv)
    
    } # end if condition: length(rep)>1 
    
      #rm(rep, ty)
      #rm(list=ls(pattern="type1"), list=ls(pattern="type2"))
    
  }# end t in t_temp
    if(write_table_temp){
      row_ind_temp<-2+((ceiling(match(variable_temp, vars_for_tab_temp)/2))*((length(t_tab_temp) +1)))
      row_ind_temp<-seq((row_ind_temp-(length(t_tab_temp)-1)),(row_ind_temp),1)
      if(match(variable_temp, vars_for_tab_temp)%%2 == 0){
        col_ind_temp<-c(5:7)
      }else{
        col_ind_temp<-c(2:4)
      }
      tab_temp[row_ind_temp, col_ind_temp]<-mini_mat_temp
      if(!is.na(match(variable_temp,vars_for_tab_temp))){
        vname_temp<-table_var_names_temp[match(variable_temp, vars_for_tab_temp)]
      }else{
        vname_temp<-variable_temp
      }
      tab_temp[(row_ind_temp[1]-1),col_ind_temp[1]]<-paste("ESCmulticolumnBL3BRBLlBRBLESCunderlineBL",vname_temp,"BRBR",sep="")
      
    } 

  #rm(t)
  }else{ # end check whether variable available
    print(paste(variable_temp,"is not avaiable in data (object)"))
  }
}# end variables in variables_wilcox_temp
  
  if(exists("tab_temp") && length(tab_temp[,1]) > 3){
    caption_temp<-paste("Wilcoxon test on equality of means for different snapshots in time.", 
                        paste(t_for_table_temp,collapse=", "), experiment_directory)
    
    xtab_temp<-xtable(tab_temp, caption=caption_temp)
    filename_temp<-paste(plot_dir_wilcox_type0,"/wilcoxon_test_stat_eco_vs_conv_",aggr_temp,"_",agent,".tex", sep="")
    #write(paste("ESCdocumentclass{standalone} \n ESCbegin{document}"), file = filename_temp, append=FALSE)
    #write(paste("Table with Wilcoxon test results compare eco vs conv \n\n"), file = filename_temp, append=TRUE)
    write(print(xtab_temp, include.rownames=FALSE,include.colnames=FALSE), file = filename_temp, append=FALSE)
    #write(paste("ESCend{document}"), file = filename_temp, append=TRUE)
  }
  if(write_par_table_temp){
    #rownames(par_tab_temp)<-c(par_tab_temp[,1])
    #colnames(par_tab_temp)<-c(par_tab_temp[1,])
    #par_tab_temp0<-par_tab_temp[2:length(par_tab_temp[F,1]),c(2:5)]
    par_tab_temp0<-par_tab_temp
    caption_temp<-paste("Wilcoxon test on equality of means in initial conditions.", experiment_directory)
    par_tab_temp0<-par_tab_temp0
    par_xtab_temp<-xtable(par_tab_temp0, caption=caption_temp)
    filename_temp<-paste(plot_dir_wilcox_type0,"/overview_parameter_init_",aggr_temp,"_",agent,".tex", sep="")
    #write(paste("ESCdocumentclass{standalone} \n ESCbegin{document}"), file = filename_temp, append=FALSE)
    #write(paste("Table with Wilcoxon test results compare eco vs conv \n\n"), file = filename_temp, append=TRUE)
    write(print(par_xtab_temp), file = filename_temp, append=FALSE)
    #write(paste("ESCend{document}"), file = filename_temp, append=TRUE)
  }
}# end aggr_temp in aggregation_levels_temp

#rm(append)


#rm(list=ls(pattern="temp"))
on.exit(if(is.null(dev.list()) == F){ dev.off()}, add=T)
