# Settings for plotting
# 
WHITE_BACKGROUND <- 1
BLACK_WHITE<-0
DIFFERENT_LINE_SHAPES<-1
LEGEND <- FALSE
OTHER_NAMES<-TRUE # Manipulates names of some plots
# i.e. names adjusted such that it fits to your tex file.
# The varied names are set in settings_axix_lables.r

if(BLACK_WHITE || DIFFERENT_LINE_SHAPES){
  # Point shapes
  pt_eco<-0
  pt_conv<-8
  pt_switch<-10
}else{
  pt_eco<-1
  pt_conv<-pt_eco
  pt_switch<-pt_eco
}

if(BLACK_WHITE==0){
  #  Color settings: 
  high<-"navyblue"
  low<-"orangered"
  highhigh<-"navyblue"
  highlow<-"skyblue"
  lowhigh<-"orange"
  lowlow<-"orangered"
  eco<-"limegreen"
  conv<-"salmon"
  switch<-"royalblue"
  
  gradient_low<-"yellow"
  gradient_high<-"blue"
  
  # interaction: 
  # high: dark, low: bright
  # also applied for experiment vs baseline: 
  # baseline: bright, experiment: dark
  ecol<-"yellowgreen" # 0.5
  ecoh<-"darkgreen"
  convl<-"orange" # 0.0
  convh<-"chocolate4"
  switchl<-"skyblue" # 1.0
  switchh<-"darkblue"
  baseline_col<-"gray60"
  
  baseline<-"cornsilk4"
  experiment<-"darkred"
  invest<-"cornflowerblue"
  cons<-"coral1"

  # interaction: 
  # high: dark, low: bright
  ecol<-"yellowgreen"
  ecoh<-"darkgreen"
  convl<-"orange"
  convh<-"chocolate4"
  switchl<-"skyblue"
  switchh<-"darkblue"
  
  # linetype settings: 
  ltyhigh<-"dashed"
  ltylow<-"dotted"
  lty0<-"solid"
  lty1<-"dotted"
  lty2<-"longdash"
  lty3<-"twodash"
  lty4<-"dashed"
  
}else{

  # Set every color black. 
  high<-"black"
  low<-"black"
  highhigh<-"black"
  highlow<-"black"
  lowhigh<-"black"
  lowlow<-"black"
  eco<-"black"
  conv<-"black"
  switch<-"black"
  
  # interaction: 
  # high: dark, low: bright
  ecol<-"gray10"
  ecoh<-"black"
  convl<-"gray10"
  convh<-"black"
  switchl<-"gray10"
  switchh<-"black"
}

# linetype settings: 
ltyhigh<-"dashed"
ltylow<-"dotted"
lty0<-"solid"
lty1<-"dotted"
lty2<-"longdash"
lty3<-"twodash"
lty4<-"dashed"


lwd<-min(1,max(0.25,12.5/runs))
alpha<-1

if(exists("aggr") && aggr=="batch"){
  lwd<-1.5
}
