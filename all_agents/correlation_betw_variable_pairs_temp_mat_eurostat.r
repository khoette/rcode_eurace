# Correlation among variable pairs: 
# 
# Plotting settings: 
# For single run analysis (to ensure that lines are not too fat in case of many runs)
source(paste(script_dir, "/aux_all_agents/plot_settings/plot_settings.r", sep=""))

point_size<-min(1, max(0.075,12.5/runs))
# 
folder_name<-data_type[d]
if(exists("pair_title") == FALSE){
  pair_title<-""
}

#parameter<-bau_value
for(aggr in aggregation_levels){
  
  selected_var_pairs_temp<-selected_var_pairs
  
  load(file=paste(data_directory,"data_",aggr, "_",experiment_name,"_",parameter,"_",agent,".RData",sep=""))
  temp_mat<-object
  if(add_gov==1){
    load(file=paste(data_directory,"data_",aggr, "_",experiment_name,"_",parameter,"_Government.RData",sep=""))
    temp_mat_gov<-object
  }
  rm(object)
  fix_vars_temp<-colnames(temp_mat)[which(is.na(match(colnames(temp_mat), variables_available_in_panel)))]
  

remove<-c()
for(row in 1:length(selected_var_pairs_temp[,1])){
  if(is.element(NA, selected_var_pairs_temp[row,])){
    remove<-c(remove,row)
  }
}
if(length(remove)>0){
  selected_var_pairs_temp<-selected_var_pairs_temp[-remove,]
}
rm(row, remove)


  vars_temp<-unique(as.vector(selected_var_pairs_temp))
  
  vars_temp<-vars_temp[which(is.na(vars_temp) != TRUE)]
  if(add_gov != 1){
    variables_temp<-variables[vars_temp]
  }else{
    vars_temp_gov<-(vars_temp-length(variables_available_in_panel_Eurostat))
    vars_temp_gov<-vars_temp_gov[which(vars_temp_gov>0)]
    variables_temp<-c(variables_available_in_panel_Eurostat[vars_temp], variables_available_in_panel_Government[vars_temp_gov])
    variables_temp<-variables_temp[which(is.na(variables_temp)!=TRUE)]
    ind_gov<-match(variables_temp, colnames(temp_mat_gov))
    ind_gov<-ind_gov[which(is.na(ind_gov)==FALSE)]
    temp_mat<-cbind(temp_mat, temp_mat_gov[,ind_gov])
  }
  
  if(length(variables_temp) > 0){
    
  print(paste("Start corr analysis for ", agent, parameter, data_type[d], aggr))
  plot_dir_temp0<-paste(plot_dir_validation_cor,"/Variable_pairs_time_series_",aggr,"/",sep="")
  # Ensure folder existence
  if(dir.exists(plot_dir_temp0)!=1){
    dir.create(plot_dir_temp0)
  }
  
  if(agent=="Firm" && is.element("active", colnames(temp_mat))){
    data<-temp_mat[which(temp_mat$active == 1),]
    if(data_type[d] == "levels"){
      data<-data[which(data$output > 1e-3),]
    }
  }else{
    data<-temp_mat
  }
  rm(temp_mat)
 
  indices_fix_temp<-match(fix_vars_temp, colnames(data))

  vars_temp<-match(variables_temp, colnames(data))
  if(is.element(NA, vars_temp)){
    print("There must be an error in the corr var settings: Desired var not available in loaded data in correl betw. var pairs")
    print(variables_temp[is.na(vars_temp)])
    vars_temp<-vars_temp[which(is.na(vars_temp)!=TRUE)]
  }
  data<-data[c(indices_fix_temp, vars_temp)]
  
  if(exists(paste("cut_transition_phase_",agent, sep=""))){
    eval(call("<-",(paste("cut_transition_phase",sep="")),as.name(paste("cut_transition_phase_",agent,sep=""))))
    cut_cols<-match(cut_transition_phase, colnames(data))
    cut_cols<-cut_cols[is.na(cut_cols) != TRUE]
  }else{
    print("Warning: cut cols not exist, but you wanted to remove transition phase, right? Run reset_some_var script.")
    cut_cols<-0
  }

  
  ind<-c(length(fix_vars_temp)+1, length(fix_vars_temp)+2)

if((agent=="Firm" && is.element("id",colnames(data))==FALSE)!=TRUE){  
for(v in 1:length(selected_var_pairs_temp[,1])){
  if(add_gov == 0){
    vars<-variables[selected_var_pairs_temp[v,]]
  }else{
    vars<-all_vars[selected_var_pairs_temp[v,]]
  }
  cols<-match(vars, colnames(data))
 
  df_data<-cbind(data[fix_vars_temp], data[vars])
  df_data<-df_data[which(is.na(df_data[,ind[1]])==FALSE),]
  df_data<-df_data[which(is.na(df_data[,ind[2]])==FALSE ),]
  df_data<-df_data[which(is.infinite(df_data[,ind[1]])==FALSE),]
  df_data<-df_data[which(is.infinite(df_data[,ind[2]])==FALSE),]
    
  #df_data<-df_data[which(is.infinite(df_data[,ind])==FALSE),]
  
  min1<-min(df_data[,ind[1]], na.rm = TRUE )
  max1<-max(df_data[,ind[1]], na.rm = TRUE )
  min2<-min(df_data[,ind[2]], na.rm = TRUE )
  max2<-max(df_data[,ind[2]], na.rm = TRUE )
  median1<-median(df_data[,ind[1]], na.rm = TRUE )
  median2<-median(df_data[,ind[2]], na.rm = TRUE )
    
  if(((max1 > median1*1e30 || abs(min1) > abs(median1*1e30 ))  && abs(median1) > 1e-3) || ((max2 > median2*1e30 || abs(min2) > abs(median2*1e30))   && abs(median2) > 1e-3)){
    paste("Presumably, there is an error in the data: Un realistically high value for ", vars)
  }else{
  length_time<-20*number_xml
    
    # Construct data frame: Fix variables (e.g. Period, Run, ID) + columns with variable pair)
  df_data_melt<-melt(df_data, measure.vars = vars)
  ind_temp<-match("value", colnames(df_data_melt))
    
  if(exists(paste("cut_transition_phase_",agent, sep="")) && is.element(TRUE,is.element(vars, colnames(data)[cut_cols]))){
    df_data_melt[which(as.vector(as.numeric(as.character(df_data_melt$Periods)))<day_market_entry+20),ind_temp]<-NA
      
    #start<-day_market_entry
    start<-0
    min<-min(as.numeric(df_data_melt[,ind_temp]), na.rm=TRUE)
    max<-max(as.numeric(df_data_melt[,ind_temp]), na.rm=TRUE)
  }else{
    start<-0
  }
  df_data_melt<-df_data_melt[is.na(df_data_melt[ind_temp])==FALSE,]
  
  # Plot 1: MTS of both vars. 
  # 
  plot_dir_temp<-paste(plot_dir_temp0,"/TS/",sep="")
  # Ensure folder existence
  if(dir.exists(plot_dir_temp)!=1){
    dir.create(plot_dir_temp)
  }
      
  pdf(paste(plot_dir_temp,agent,"_",parameter,"_",pair_title,"_multi_ts_",vars[1],"_and_",vars[2],".pdf",sep=""))
  #p1<-ggplot(df_data, aes(x=reorder(Periods, as.numeric(Periods)), y=value, color =interaction(variable,Type)))
  p1<-ggplot(df_data_melt, aes(x=reorder(Periods, as.numeric(Periods)), y=value))
  if(is.element(aggr, c("single_switch", "single_large"))){
    #plot<- p+ geom_point(shape = ".", alpha = 0.01, aes(color=interaction(variable,Type))) 
    plot<-p1+geom_path(aes(color=interaction(variable,Type),group=interaction(variable,Run,Type)),alpha=alpha, lwd=lwd)
  }else if(is.element(aggr, c("eco_conv_batch", "eco_conv_switch"))){
    plot<-p1+geom_path(aes(color=interaction(Type,variable),group=interaction(Type,variable)))
  }else if(aggr == "batch"){
    plot<-p1+geom_path(aes(color=variable ,group=variable))
  }
  plot<-plot+ guides(colour = guide_legend(override.aes = list(shape = 15, alpha=1)))
  plot<- plot + theme(legend.justification=c(0.05,0.95), legend.position=c(0.05,0.95), legend.title = element_blank()) + ylab(paste(variables[selected_var_pairs_temp[v,1]],"/",variables[selected_var_pairs_temp[v,2]],sep=""))
  plot<-plot +scale_x_discrete(breaks=seq(0,length_time*20,5000)) + scale_y_continuous(limits=c(min(min1,min2),max(max1,max2))) + xlab("Periods")+scale_colour_discrete("")
  print(plot)
  dev.off()

    # Reshape data again: 
  df_data_melt_temp<-cbind(df_data_melt[which(df_data_melt$variable == colnames(df_data)[ind[1]]),],df_data_melt[which(df_data_melt$variable == colnames(df_data)[ind[2]]),ind])
  ind0<-append(ind, ind+2)
  colnames(df_data_melt_temp)[ind0]<-c("variable1", "value1", "variable2", "value2")
    
  # Now: Plot var1 against var2, i.e. var1 on x-axis, var2 on y-axis
  plot_dir_temp<-paste(plot_dir_temp0,"/Pair-plot/",sep="")
  # Ensure folder existence
  if(dir.exists(plot_dir_temp)!=1){
    dir.create(plot_dir_temp)
  }
  #df_data<-cbind(df_data0, data[vars])
  pdf(paste(plot_dir_temp,agent,"_",parameter,"_",pair_title,"_scatterplot_",vars[1],"_and_",vars[2],".pdf",sep=""))
  #p1<-ggplot(df_data, aes(x=reorder(Periods, as.numeric(Periods)), y=value, color =interaction(variable,Type)))
  p1<-ggplot(df_data_melt_temp, aes(x=value1, y=value2))
  if(aggr != "batch"){
    plot<-p1+geom_point(size=point_size, aes(color=Type))
  }else{
    plot<-p1+geom_point(size=point_size)
  }
  plot<-plot+ guides(colour = guide_legend(override.aes = list(shape = 15, alpha=1)))
  plot<- plot + theme(legend.justification=c(0.95,0.95), legend.position=c(0.95,0.95), legend.title = element_blank()) + ylab(paste(vars[1],"/",vars[2],sep=""))
  plot<-plot +scale_x_continuous(limits=c(min1, max1)) + scale_y_continuous(limits=c(min2,max2)) + xlab(vars[1])+scale_colour_discrete("")+ylab(vars[2])
  print(plot)
  dev.off()
} # Else of outlier check
} # End v in selected var pairs   
}# End if NOT(agent==Firm && c==1)  
    
  
## Now: Start correlation analysis: 
## 
plot_dir_temp<-paste(plot_dir_temp0,"/Corr_matrix/",sep="")
if(dir.exists(plot_dir_temp)!=1){
  dir.create(plot_dir_temp)
}
for(cor_type in 1:1){ 
  if(cor_type == 1){
    method<-"pearson"
  }else if(cor_type == 2){
    method<-"kendall"  
  }
# Write correlation matrix
#pdf(paste(plot_dir_temp,parameter,"_","Corrlation_matrix.tex", sep=""))
columns<-match(variables_temp, colnames(data))
length<-length(columns)
# Alternative: Use Kendall! 
corr_matrix<-cor(data[,columns], data[,columns], method = method, use="pairwise.complete.obs")
corr_matrix<-data.frame(corr_matrix)
#corr_matrix<-corr_matrix[which(is.na(corr_matrix)==FALSE),which(is.na(corr_matrix)==FALSE)]
columns<-match(colnames(corr_matrix), colnames(data))
corr_matrix<-corr_matrix[, colSums(is.na(corr_matrix)) != nrow(corr_matrix)] # What do I do here? Guess: remove NA columns/rows
corr_matrix<-corr_matrix[rowSums(is.na(corr_matrix)) != ncol(corr_matrix), ]
columns<-match(colnames(corr_matrix), colnames(data))
  
colnames(corr_matrix)<-c(1:length(columns)) # Use numbers if matrix large. Variable names in legend
rownames(corr_matrix)<-c(1:length(columns))
legend<-rbind(c(1:length(columns)), colnames(data)[columns])
cap<-" "
for(ind in 1:length(legend[1,])){
  cap<-paste(cap, paste(legend[1,ind], legend[2,ind], sep=" "), sep = "; ")
}
caption<-paste(pair_caption,": Legend of variables: ", cap, sep="")
if(length(corr_matrix)%%15 < 3){
  limit<-15 + (length(corr_matrix)%%15)
}else{
  limit<-15
}
if(length(corr_matrix)>limit){
  for(rows in 1:ceiling(length(corr_matrix)/limit)){
    row<-c(((rows-1)*limit + 1): ((rows-1)*limit  + min(limit, length(corr_matrix)-(rows-1)*limit)))
    for(cols in 1:ceiling((length(corr_matrix)/limit))){
      col<-c(((cols-1)*limit + 1): ((cols-1)*limit  + min(limit, length(corr_matrix)-(cols-1)*limit)))
      if(rows == 1 && cols==1){
        write.table(corr_matrix[row,col],file=paste(plot_dir_temp,pair_title,"_corr_mat_",aggr,sep=""), 
                    quote=FALSE, eol="\r\n",  sep=" \t", dec=".", append=FALSE)
        tab<-xtable(corr_matrix[row,col], caption = caption)
        #align(tab)<-paste(rep("lp{2.5cm}",min(limit+1, 1+length(corr_matrix)-(cols-1)*limit)), sep="")
        print(tab, file=paste(plot_dir_temp,"Correlation_matrix_",aggr,".tex",sep=""),  append = FALSE)
        #tab<-matrix(tab,c(length(row)), length(col))
        #tab<-xtable(tab)
        #print(tab, file=paste(plot_dir_temp,"Correlations/Correlation_matrix.tex",sep=""), append=FALSE)
          
      }else{
        write.table(corr_matrix[row,col],file=paste(plot_dir_temp,"/corr_mat_",aggr,sep=""), 
                  quote=FALSE, eol="\r\n",  sep=" \t", dec=".", append = TRUE)
        tab<-xtable(corr_matrix[row,col],  caption = caption, append=TRUE)
        #align(tab)<-paste(rep("lp{2.5cm}",min(limit+1, 1+length(corr_matrix)-(cols-1)*limit)), sep="")
        
        print(tab, file=paste(plot_dir_temp,"/Correlation_matrix_",method,"_",aggr,".tex",sep=""), append = TRUE)
        #tab<-matrix(c(length(row)), length(col), 1)
        #tab<-xtable(tab)
        #print(tab, file=paste(plot_dir_temp,"Correlations/Correlation_matrix.tex",sep=""), append=TRUE)
      }
    }
  }
}else{
  write.table(corr_matrix,file=paste(plot_dir_temp,"/",pair_title,"_corr_mat_",aggr,sep=""), 
              quote=FALSE, eol="\r\n",  sep=" \t", dec=".")
  tab<-xtable(corr_matrix, caption = caption)
  #align(tab)<-paste(rep("lp{2.5cm}",min(limit+1, 1+length(corr_matrix)-(cols-1)*limit)), sep="")
  print(tab, file=paste(plot_dir_temp,"/",pair_title,"_correlation_matrix_",method,"_",aggr,".tex",sep=""),  append = FALSE)
  #tab<-matrix(tab,c(length(row)), length(col))
  #tab<-xtable(tab)
  #print(tab, file=paste(plot_dir_temp,"Correlations/Correlation_matrix.tex",sep=""), append=FALSE)
}
rm(method, cor_type)
} # for cor type in 1:1 
    
#ccf<-ccf(df_data[,ind[1]], df_data[,ind[2]], na.action = na.pass, type= "correlation",  main=paste(variables[selected_var_pairs_temp[v,1]], " & ", variables[selected_var_pairs_temp[v,2]], sep=""), ci.col = "red")
#dev.off()

#Autocorrleation: 
plot_dir_temp<-paste(plot_dir_temp0,"/Autocorrelation/",sep="")
if(dir.exists(plot_dir_temp)!=1){
  dir.create(plot_dir_temp)
}

    
for(v in variables_temp){
  
  if(length(data[which(is.na(data[v])),v])<0.5*length(data[,v])){
    median<-median((data[,v]), na.rm=TRUE)
    min<-min((data[,v]), na.rm=TRUE)
    max<-max((data[,v]), na.rm=TRUE)
    if((max > median*1e30 || abs(min) > abs(median*1e30))  && abs(median) > 1e-3){
    paste("Presumably, there is an error in the data: Un realistically high value for ", v)
  }else{
      
    df_data<-data[,v]
    df_data[which(is.infinite(df_data)==TRUE)]<-NA
      
    # Compute auto correlation variable:  
    if(var(data[,v], na.rm=TRUE)>1e-3){
      pdf(paste(plot_dir_temp,agent,"_",parameter,"_","Autocorrelation_of_",v,".pdf",sep=""))
      acf(df_data, na.action = na.pass, type= "correlation",  main=paste(v,sep=""), ci.col = "red")
      dev.off()
    }
  }
} # end test whether sufficient data
} # end v in vars
    
if(1==2){    
    # Compute cross correlation for each variable pair: 
    # 
    if(dir.exists(paste(plot_dir_temp0,"/Cross_cor/",sep=""))!=1){
      dir.create(paste(plot_dir_temp0,"/Cross_cor/",sep=""))
    }
    plot_dir_temp1<-paste(plot_dir_temp0,"/Cross_cor/",sep="")
    
    ind<-c(1,2)
    for(v in 1:length(selected_var_pairs_temp[,1])){
      
      if(is.na(selected_var_pairs_temp[v,1]) || is.na(selected_var_pairs_temp[v,2]) ){
        if(exists("temp_minus") == FALSE){ # Remove NA rows from list of selected var pairs
          temp_minus<-v}
        else{
          temp_minus<-c(temp_minus, v) 
        }
      }else{

        vars<-variables[selected_var_pairs_temp[v,]]
        cols<-match(vars, colnames(data))
        df_data<-data[vars]
        df_data[which(is.infinite(df_data[,1])==TRUE),]<-NA
        df_data[which(is.infinite(df_data[,1])==TRUE),]<-NA
        if(length(df_data[which(is.na(df_data[,1]) || is.na(df_data[,2])),])<0.5*length(df_data[,1])){
          pdf(paste(plot_dir_temp1,agent,"_",parameter,"_","Cross_cor_",vars[1],"_and_",vars[2],".pdf",sep=""))
          ccf<-ccf(df_data[,vars[1]], df_data[,vars[2]], na.action = na.pass, type= "correlation",  main=paste(vars[1], " & ", vars[2], sep=""), ci.col = "red")
          dev.off()
        }
      }
    }
    
} 
    # if condition: var pairs are set
    # v in var pairs
    
} # end if length variables_temp > 0
rm(corr_matrix, data, df_data_melt, tab)
} # end aggr in aggregation levels

  
# Remove all temp objects
rm(list=ls(pattern="temp"))
  
  
  
  
  
if(1==2){
    

    # Compute cross correlation for each variable pair: 
    # 
    #if(dir.exists(paste(plot_dir_temp,"/Cross_cor/",sep=""))!=1){
    #  dir.create(paste(plot_dir_temp,"/Cross_cor/",sep=""))
    #}
    #plot_dir_temp1<-paste(plot_dir_temp,"/Cross_cor/",sep="")
    
    #pdf(paste(plot_dir_temp1,agent,"_",parameter,"_","Cross_cor_",vars[1],"_and_",vars[2],".pdf",sep=""))
    #ccf<-ccf(df_data[,ind0[1]], df_data[,ind0[2]], na.action = na.pass, type= "correlation",  main=paste(vars[1], " & ", vars[2], sep=""), ci.col = "red")
    #dev.off()
  # if condition: var pairs are set
 # v in var pairs
  if(exists("temp_minus")){
    selected_var_pairs_temp<-selected_var_pairs_temp[- temp_minus , ]
  }
 # c in cases

for(c in 1:3){
  
  if(c==1){
    plot_dir_temp0<-paste(plot_dir_validation_cor,"/Variable_pairs_time_series_single_large/",sep="")
    df_data<-temp_mat_single_large
  }else if(c==2){
    plot_dir_temp0<-paste(plot_dir_validation_cor,"/Variable_pairs_time_series_eco_conv_batch/",sep="")
    df_data<-temp_mat_eco_conv_batch
    
  }else{
    plot_dir_temp0<-paste(plot_dir_validation_cor,"/Variable_pairs_time_series_batch/",sep="")
    df_data<-data
  }
  plot_dir_temp<-paste(plot_dir_temp0,"/Corr_matrix/",sep="")
  # Ensure folder existence
  if(dir.exists(plot_dir_temp0)!=1){
    dir.create(plot_dir_temp0)
  }
  if(dir.exists(plot_dir_temp)!=1){
    dir.create(plot_dir_temp)
  }


for(cor_type in 1:1){ 
  if(cor_type == 1){
    method<-"pearson"
  }else if(cor_type == 2){
    method<-"kendall"  
  }
# Write correlation matrix
  #pdf(paste(plot_dir_temp,agent,"_",parameter,"_","Corrlation_matrix.tex", sep=""))
  columns<-match(variables[unique(as.vector(selected_var_pairs_temp))], colnames(df_data))
  length<-length(columns)
  # Alternative: Use Kendall! 
  corr_matrix<-cor(df_data[,columns], df_data[,columns], method = method, use="pairwise.complete.obs")
  corr_matrix<-data.frame(corr_matrix)
  #corr_matrix<-corr_matrix[which(is.na(corr_matrix)==FALSE),which(is.na(corr_matrix)==FALSE)]
  columns<-match(colnames(corr_matrix), colnames(df_data))
  corr_matrix<-corr_matrix[, colSums(is.na(corr_matrix)) != nrow(corr_matrix)]
  corr_matrix<-corr_matrix[rowSums(is.na(corr_matrix)) != ncol(corr_matrix), ]
  columns<-match(colnames(corr_matrix), colnames(df_data))
  
  #colnames(corr_matrix)<-list_of_var[indices]
  #rownames(corr_matrix)<-list_of_var[indices]
  colnames(corr_matrix)<-c(1:length(columns))
  rownames(corr_matrix)<-c(1:length(columns))
  legend<-rbind(c(1:length(columns)), colnames(df_data)[columns])
  cap<-" "
  for(ind in 1:length(legend[1,])){
    cap<-paste(cap, paste(legend[1,ind], legend[2,ind], sep=" "), sep = "; ")
  }
  caption<-paste(pair_caption,": Legend of variables: ", cap, sep="")
  if(length(corr_matrix)%%15 < 3){
    limit<-15 + (length(corr_matrix)%%15)
  }else{
    limit<-15
  }
  if(length(corr_matrix)>limit){
    for(rows in 1:ceiling(length(corr_matrix)/limit)){
      row<-c(((rows-1)*limit + 1): ((rows-1)*limit  + min(limit, length(corr_matrix)-(rows-1)*limit)))
      for(cols in 1:ceiling((length(corr_matrix)/limit))){
        col<-c(((cols-1)*limit + 1): ((cols-1)*limit  + min(limit, length(corr_matrix)-(cols-1)*limit)))
        if(rows == 1 && cols==1){
          write.table(corr_matrix[row,col],file=paste(plot_dir_temp,pair_title,"_corr_mat_",aggr,sep=""), 
                      quote=FALSE, eol="\r\n",  sep=" \t", dec=".", append=FALSE)
          tab<-xtable(corr_matrix[row,col], caption = caption)
          #align(tab)<-paste(rep("lp{2.5cm}",min(limit+1, 1+length(corr_matrix)-(cols-1)*limit)), sep="")
          print(tab, file=paste(plot_dir_temp,"Correlation_matrix_",aggr,".tex",sep=""),  append = FALSE)
          #tab<-matrix(tab,c(length(row)), length(col))
          #tab<-xtable(tab)
          #print(tab, file=paste(plot_dir_temp,"Correlations/Correlation_matrix.tex",sep=""), append=FALSE)
          
        }else{
          write.table(corr_matrix[row,col],file=paste(plot_dir_temp,"/corr_mat_",aggr,sep=""), 
                      quote=FALSE, eol="\r\n",  sep=" \t", dec=".", append = TRUE)
          tab<-xtable(corr_matrix[row,col],  caption = caption, append=TRUE)
          #align(tab)<-paste(rep("lp{2.5cm}",min(limit+1, 1+length(corr_matrix)-(cols-1)*limit)), sep="")
          
          print(tab, file=paste(plot_dir_temp,"/Correlation_matrix_",method,"_",aggr,".tex",sep=""), append = TRUE)
          #tab<-matrix(c(length(row)), length(col), 1)
          #tab<-xtable(tab)
          #print(tab, file=paste(plot_dir_temp,"Correlations/Correlation_matrix.tex",sep=""), append=TRUE)
        }
      }
    }
  }else{
    write.table(corr_matrix,file=paste(plot_dir_temp,"/",pair_title,"_corr_mat_",aggr,sep=""), 
                quote=FALSE, eol="\r\n",  sep=" \t", dec=".")
    tab<-xtable(corr_matrix, caption = caption)
    #align(tab)<-paste(rep("lp{2.5cm}",min(limit+1, 1+length(corr_matrix)-(cols-1)*limit)), sep="")
    print(tab, file=paste(plot_dir_temp,"/",pair_title,"_correlation_matrix_",method,"_",aggr,".tex",sep=""),  append = FALSE)
    #tab<-matrix(tab,c(length(row)), length(col))
    #tab<-xtable(tab)
    #print(tab, file=paste(plot_dir_temp,"Correlations/Correlation_matrix.tex",sep=""), append=FALSE)
  }
} 
rm(method, cor_type)

#ccf<-ccf(df_data[,ind[1]], df_data[,ind[2]], na.action = na.pass, type= "correlation",  main=paste(variables[selected_var_pairs_temp[v,1]], " & ", variables[selected_var_pairs_temp[v,2]], sep=""), ci.col = "red")
#dev.off()
#
# Autocorrleation: 
#plot_dir_temp<-paste(plot_dir_temp0,"/Autocorrelation/",sep="")
#if(dir.exists(plot_dir_temp)!=1){
#  dir.create(plot_dir_temp)
#}
#for(v in length(columns)){
  # Compute cross correlation for each variable pair: 
  # 
  # 
  
#  pdf(paste(plot_dir_temp,agent,"_",parameter,"_","Autocorrelation_of_",colnames(df_data)[columns[v]],".pdf",sep=""))
#  acf(df_data[,columns[v]], na.action = na.pass, type= "correlation",  main=paste(colnames(df_data)[columns[v]],sep=""), ci.col = "red")
#  dev.off()
#}
}
# Remove all temp objects
rm(list=ls(pattern="temp"))
}
